; ModuleID = 'tcg-llvm'
source_filename = "tcg-llvm"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.CPUX86State = type { [8 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [6 x %struct.SegmentCache], %struct.SegmentCache, %struct.SegmentCache, %struct.SegmentCache, %struct.SegmentCache, [5 x i32], i32, i32, i16, i16, [8 x i8], [8 x %u
%struct.SegmentCache = type { i32, i32, i32, i32 }
%union.FPReg = type { %struct.floatx80 }
%struct.floatx80 = type { i64, i16 }
%struct.float_status = type { i8, i8, i8, i8, i8, i8, i8 }
%union.XMMReg = type { [2 x i64] }
%union.MMXReg = type { i64 }
%union.anon = type { [4 x %struct.CPUBreakpoint*] }
%struct.CPUBreakpoint = type { i32, i32, %struct.anon }
%struct.TranslationBlock = type { i32, i32, i64, i16, i16, i8*, i32, %struct.TranslationBlock*, [2 x %struct.TranslationBlock*], [2 x i64], [2 x i16], [2 x i16], [2 x %struct.TranslationBlock*], %struct.TranslationBlock*, i32, i8*, i64, i64, i64, i32, i6
%struct.tb_precise_pc_t_ = type { i16, i16, i16, i8, i8 }
%struct.CPUTLBEntry = type { i32, i32, i32, i32, i64, i64, i8*, [24 x i8] }
%struct.CPUTLBRAMEntry = type { i64, i64, i8* }
%struct.anon = type { %struct.CPUBreakpoint*, %struct.CPUBreakpoint** }
%struct.anon.0 = type { %struct.CPUWatchpoint*, %struct.CPUWatchpoint** }
%struct.CPUWatchpoint = type { i32, i32, i32, %struct.anon.0 }
%struct.__jmp_buf_tag = type { [8 x i64], i32, %struct.__sigset_t }
%struct.__sigset_t = type { [16 x i64] }
%struct.MTRRVar = type { i64, i64 }
%struct.DeviceState = type opaque
%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%struct.se_libcpu_interface_t = type { i32, %struct.mode, %struct.exec, %struct.tb, %struct.tlb, %struct.regs, %struct.mem, %struct.expr, %struct.libcpu, %struct.events, %struct.anon.1 }
%struct.mode = type { i32*, i32*, i8**, i8**, i32*, i32*, i32*, i32* }
%struct.exec = type { void (i8*, i8*)*, void ()*, i32 ()*, i32 ()*, i32 ()*, i32 ()*, void ()*, void (i8*)*, i64 (%struct.CPUX86State*, %struct.TranslationBlock*)*, void (i32, i32, i32, i64, i32)*, i32* }
%struct.tb = type { void (%struct.TranslationBlock*)*, void (%struct.TranslationBlock*)*, void (...)*, void (%struct.TranslationBlock*)*, i32 (%struct.TranslationBlock*)*, void (%struct.TranslationBlock*)* }
%struct.tlb = type { void ()*, void (i8*, i32, i32)*, void (%struct.CPUX86State*, i32, i64, i64)* }
%struct.regs = type { void (i32, i8*, i32)*, void (i32, i8*, i32)*, void (%struct.CPUX86State*)* }
%struct.mem = type { i8 (i64)*, void (i64, i8)*, void (i64, i8*, i32)*, void (i64, i8*, i32)*, void (i64, i8*, i64)*, void (i64, i8*, i64)*, void (i64, i8*, i64)*, i64 (i32)*, i32 (i64)*, i32 (i64, i32)*, i64 (i64)*, i8 (i8*, i32)*, i16 (i16*, i32)*, i32
%struct.expr = type { i8* ()*, void (i8*)*, void ()*, i8* (i8*, i8*, i64)*, i64 (i8*)*, void (i8*, i64)*, void (i8*, i32, i32)*, i8* (i8*, i32, i32)*, i8* (i8*, i64)*, i8* (i8*, i64)* }
%struct.libcpu = type { i32 (i32)*, i32 (i32)* }
%struct.events = type { i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, i32*, void (i32, i32)*, void (i64, i64)*, void (i64, i32, i8*)*, void (i64, i32, i8*)*, void (i64, i64, i32, i32, i64)*, v
%struct.anon.1 = type { void (i8*, ...)* }
%struct.MemoryDescOps = type { i64 (i64, i32)*, void (i64, i64, i32)* }

@env = local_unnamed_addr global %struct.CPUX86State* null, align 8
@parity_table = external local_unnamed_addr constant [256 x i8], align 16
@loglevel = external local_unnamed_addr global i32, align 4
@logfile = external local_unnamed_addr global %struct._IO_FILE*, align 8
@.str.16 = private unnamed_addr constant [36 x i8] c"check_exception old: 0x%x new 0x%x\0A\00", align 1
@.str.17 = private unnamed_addr constant [14 x i8] c"Triple fault\0A\00", align 1
@.str.15 = private unnamed_addr constant [37 x i8] c"vmexit(%08x, %016lx, %016lx, %08x)!\0A\00", align 1
@g_sqi = external local_unnamed_addr global %struct.se_libcpu_interface_t, align 8
@se_do_interrupt_all.count = internal unnamed_addr global i32 0, align 4
@.str = private unnamed_addr constant [65 x i8] c"%6d: v=%02x e=%04x i=%d cpl=%d IP=%04x:%08x pc=%08x SP=%04x:%08x\00", align 1
@.str.1 = private unnamed_addr constant [10 x i8] c" CR2=%08x\00", align 1
@.str.2 = private unnamed_addr constant [10 x i8] c" EAX=%08x\00", align 1
@switch.table = private unnamed_addr constant [10 x i32] [i32 1, i32 0, i32 1, i32 1, i32 1, i32 1, i32 1, i32 0, i32 0, i32 1]
@.str.18 = private unnamed_addr constant [12 x i8] c"invalid tss\00", align 1
@.str.4 = private unnamed_addr constant [12 x i8] c"SMM: enter\0A\00", align 1
@.str.5 = private unnamed_addr constant [16 x i8] c"SMM: after RSM\0A\00", align 1
@.str.6 = private unnamed_addr constant [13 x i8] c"vmrun! %08x\0A\00", align 1
@.str.7 = private unnamed_addr constant [18 x i8] c"Injecting(%#hx): \00", align 1
@.str.8 = private unnamed_addr constant [5 x i8] c"INTR\00", align 1
@.str.9 = private unnamed_addr constant [4 x i8] c"NMI\00", align 1
@.str.10 = private unnamed_addr constant [6 x i8] c"EXEPT\00", align 1
@.str.11 = private unnamed_addr constant [5 x i8] c"SOFT\00", align 1
@.str.12 = private unnamed_addr constant [10 x i8] c" %#x %#x\0A\00", align 1
@.str.13 = private unnamed_addr constant [32 x i8] c"vmload! %08x\0AFS: %016lx | %08x\0A\00", align 1
@.str.14 = private unnamed_addr constant [32 x i8] c"vmsave! %08x\0AFS: %016lx | %08x\0A\00", align 1
@rclb_table = external local_unnamed_addr constant [32 x i8], align 16
@rclw_table = external local_unnamed_addr constant [32 x i8], align 16

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @compute_eflags() local_unnamed_addr #0 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 7
  %3 = load i32, i32* %2, align 8
  %4 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 1
  %5 = load i32, i32* %4, align 16
  %6 = tail call i32 @helper_cc_compute_all(i32 %5)
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 6
  %8 = load i32, i32* %7, align 4
  %9 = and i32 %8, 1024
  %10 = or i32 %3, %6
  %11 = or i32 %10, %9
  %12 = or i32 %11, 2
  ret i32 %12
}

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @helper_cc_compute_all(i32) local_unnamed_addr #0 {
  switch i32 %0, label %829 [
    i32 1, label %2
    i32 2, label %6
    i32 3, label %28
    i32 4, label %52
    i32 6, label %75
    i32 7, label %108
    i32 8, label %142
    i32 10, label %174
    i32 11, label %208
    i32 12, label %243
    i32 14, label %276
    i32 15, label %308
    i32 16, label %342
    i32 18, label %373
    i32 19, label %406
    i32 20, label %441
    i32 22, label %473
    i32 23, label %488
    i32 24, label %505
    i32 26, label %521
    i32 27, label %547
    i32 28, label %575
    i32 30, label %602
    i32 31, label %628
    i32 32, label %656
    i32 34, label %683
    i32 35, label %707
    i32 36, label %733
    i32 38, label %757
    i32 39, label %780
    i32 40, label %805
  ]

; <label>:2:                                      ; preds = %1
  %3 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %4 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 2
  %5 = load i32, i32* %4, align 4
  br label %829

; <label>:6:                                      ; preds = %1
  %7 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %7, i64 0, i32 2
  %9 = load i32, i32* %8, align 4
  %10 = icmp ne i32 %9, 0
  %11 = zext i1 %10 to i32
  %12 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %7, i64 0, i32 3
  %13 = load i32, i32* %12, align 8
  %14 = and i32 %13, 255
  %15 = zext i32 %14 to i64
  %16 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %15
  %17 = load i8, i8* %16, align 1
  %18 = zext i8 %17 to i32
  %19 = icmp eq i32 %14, 0
  %20 = zext i1 %19 to i32
  %21 = shl nuw nsw i32 %20, 6
  %22 = and i32 %13, 128
  %23 = shl nuw nsw i32 %11, 11
  %24 = or i32 %22, %11
  %25 = or i32 %24, %18
  %26 = or i32 %25, %23
  %27 = or i32 %26, %21
  br label %829

; <label>:28:                                     ; preds = %1
  %29 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %30 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %29, i64 0, i32 2
  %31 = load i32, i32* %30, align 4
  %32 = icmp ne i32 %31, 0
  %33 = zext i1 %32 to i32
  %34 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %29, i64 0, i32 3
  %35 = load i32, i32* %34, align 8
  %36 = zext i32 %35 to i64
  %37 = and i64 %36, 255
  %38 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %37
  %39 = load i8, i8* %38, align 1
  %40 = zext i8 %39 to i32
  %41 = and i32 %35, 65535
  %42 = icmp eq i32 %41, 0
  %43 = zext i1 %42 to i32
  %44 = shl nuw nsw i32 %43, 6
  %45 = lshr i32 %35, 8
  %46 = and i32 %45, 128
  %47 = shl nuw nsw i32 %33, 11
  %48 = or i32 %40, %33
  %49 = or i32 %48, %46
  %50 = or i32 %49, %47
  %51 = or i32 %50, %44
  br label %829

; <label>:52:                                     ; preds = %1
  %53 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %54 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %53, i64 0, i32 2
  %55 = load i32, i32* %54, align 4
  %56 = icmp ne i32 %55, 0
  %57 = zext i1 %56 to i32
  %58 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %53, i64 0, i32 3
  %59 = load i32, i32* %58, align 8
  %60 = zext i32 %59 to i64
  %61 = and i64 %60, 255
  %62 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %61
  %63 = load i8, i8* %62, align 1
  %64 = zext i8 %63 to i32
  %65 = icmp eq i32 %59, 0
  %66 = zext i1 %65 to i32
  %67 = shl nuw nsw i32 %66, 6
  %68 = lshr i32 %59, 24
  %69 = and i32 %68, 128
  %70 = shl nuw nsw i32 %57, 11
  %71 = or i32 %64, %57
  %72 = or i32 %71, %69
  %73 = or i32 %72, %70
  %74 = or i32 %73, %67
  br label %829

; <label>:75:                                     ; preds = %1
  %76 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %77 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %76, i64 0, i32 2
  %78 = load i32, i32* %77, align 4
  %79 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %76, i64 0, i32 3
  %80 = load i32, i32* %79, align 8
  %81 = sub i32 %80, %78
  %82 = zext i32 %80 to i64
  %83 = and i32 %80, 255
  %84 = and i32 %78, 255
  %85 = icmp ult i32 %83, %84
  %86 = zext i1 %85 to i32
  %87 = and i64 %82, 255
  %88 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %87
  %89 = load i8, i8* %88, align 1
  %90 = zext i8 %89 to i32
  %91 = xor i32 %80, %78
  %92 = xor i32 %91, %81
  %93 = and i32 %92, 16
  %94 = icmp eq i32 %83, 0
  %95 = zext i1 %94 to i32
  %96 = shl nuw nsw i32 %95, 6
  %97 = and i32 %80, 128
  %98 = xor i32 %78, 128
  %99 = xor i32 %98, %81
  %100 = and i32 %99, %91
  %101 = shl i32 %100, 4
  %102 = and i32 %101, 2048
  %103 = or i32 %90, %97
  %104 = or i32 %103, %86
  %105 = or i32 %104, %93
  %106 = or i32 %105, %96
  %107 = or i32 %106, %102
  br label %829

; <label>:108:                                    ; preds = %1
  %109 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %110 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %109, i64 0, i32 2
  %111 = load i32, i32* %110, align 4
  %112 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %109, i64 0, i32 3
  %113 = load i32, i32* %112, align 8
  %114 = sub i32 %113, %111
  %115 = and i32 %113, 65535
  %116 = and i32 %111, 65535
  %117 = icmp ult i32 %115, %116
  %118 = zext i1 %117 to i32
  %119 = zext i32 %113 to i64
  %120 = and i64 %119, 255
  %121 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %120
  %122 = load i8, i8* %121, align 1
  %123 = zext i8 %122 to i32
  %124 = xor i32 %113, %111
  %125 = xor i32 %124, %114
  %126 = and i32 %125, 16
  %127 = icmp eq i32 %115, 0
  %128 = zext i1 %127 to i32
  %129 = shl nuw nsw i32 %128, 6
  %130 = lshr i32 %113, 8
  %131 = and i32 %130, 128
  %132 = xor i32 %111, 32768
  %133 = xor i32 %132, %114
  %134 = and i32 %133, %124
  %135 = lshr i32 %134, 4
  %136 = and i32 %135, 2048
  %137 = or i32 %131, %123
  %138 = or i32 %137, %118
  %139 = or i32 %138, %126
  %140 = or i32 %139, %129
  %141 = or i32 %140, %136
  br label %829

; <label>:142:                                    ; preds = %1
  %143 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %144 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %143, i64 0, i32 2
  %145 = load i32, i32* %144, align 4
  %146 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %143, i64 0, i32 3
  %147 = load i32, i32* %146, align 8
  %148 = sub i32 %147, %145
  %149 = icmp ult i32 %147, %145
  %150 = zext i1 %149 to i32
  %151 = zext i32 %147 to i64
  %152 = and i64 %151, 255
  %153 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %152
  %154 = load i8, i8* %153, align 1
  %155 = zext i8 %154 to i32
  %156 = xor i32 %147, %145
  %157 = xor i32 %156, %148
  %158 = and i32 %157, 16
  %159 = icmp eq i32 %147, 0
  %160 = zext i1 %159 to i32
  %161 = shl nuw nsw i32 %160, 6
  %162 = lshr i32 %147, 24
  %163 = and i32 %162, 128
  %164 = xor i32 %145, -2147483648
  %165 = xor i32 %164, %148
  %166 = and i32 %165, %156
  %167 = lshr i32 %166, 20
  %168 = and i32 %167, 2048
  %169 = or i32 %150, %155
  %170 = or i32 %169, %163
  %171 = or i32 %170, %158
  %172 = or i32 %171, %161
  %173 = or i32 %172, %168
  br label %829

; <label>:174:                                    ; preds = %1
  %175 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %176 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %175, i64 0, i32 2
  %177 = load i32, i32* %176, align 4
  %178 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %175, i64 0, i32 3
  %179 = load i32, i32* %178, align 8
  %180 = sub i32 %179, %177
  %181 = add i32 %180, -1
  %182 = zext i32 %179 to i64
  %183 = and i32 %179, 255
  %184 = and i32 %177, 255
  %185 = icmp ule i32 %183, %184
  %186 = zext i1 %185 to i32
  %187 = and i64 %182, 255
  %188 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %187
  %189 = load i8, i8* %188, align 1
  %190 = zext i8 %189 to i32
  %191 = xor i32 %179, %177
  %192 = xor i32 %181, %191
  %193 = and i32 %192, 16
  %194 = icmp eq i32 %183, 0
  %195 = zext i1 %194 to i32
  %196 = shl nuw nsw i32 %195, 6
  %197 = and i32 %179, 128
  %198 = xor i32 %177, 128
  %199 = xor i32 %198, %181
  %200 = and i32 %199, %191
  %201 = shl i32 %200, 4
  %202 = and i32 %201, 2048
  %203 = or i32 %190, %197
  %204 = or i32 %203, %186
  %205 = or i32 %204, %193
  %206 = or i32 %205, %196
  %207 = or i32 %206, %202
  br label %829

; <label>:208:                                    ; preds = %1
  %209 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %210 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %209, i64 0, i32 2
  %211 = load i32, i32* %210, align 4
  %212 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %209, i64 0, i32 3
  %213 = load i32, i32* %212, align 8
  %214 = sub i32 %213, %211
  %215 = add i32 %214, -1
  %216 = and i32 %213, 65535
  %217 = and i32 %211, 65535
  %218 = icmp ule i32 %216, %217
  %219 = zext i1 %218 to i32
  %220 = zext i32 %213 to i64
  %221 = and i64 %220, 255
  %222 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %221
  %223 = load i8, i8* %222, align 1
  %224 = zext i8 %223 to i32
  %225 = xor i32 %213, %211
  %226 = xor i32 %215, %225
  %227 = and i32 %226, 16
  %228 = icmp eq i32 %216, 0
  %229 = zext i1 %228 to i32
  %230 = shl nuw nsw i32 %229, 6
  %231 = lshr i32 %213, 8
  %232 = and i32 %231, 128
  %233 = xor i32 %211, 32768
  %234 = xor i32 %233, %215
  %235 = and i32 %234, %225
  %236 = lshr i32 %235, 4
  %237 = and i32 %236, 2048
  %238 = or i32 %232, %224
  %239 = or i32 %238, %219
  %240 = or i32 %239, %227
  %241 = or i32 %240, %230
  %242 = or i32 %241, %237
  br label %829

; <label>:243:                                    ; preds = %1
  %244 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %245 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %244, i64 0, i32 2
  %246 = load i32, i32* %245, align 4
  %247 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %244, i64 0, i32 3
  %248 = load i32, i32* %247, align 8
  %249 = sub i32 %248, %246
  %250 = add i32 %249, -1
  %251 = icmp ule i32 %248, %246
  %252 = zext i1 %251 to i32
  %253 = zext i32 %248 to i64
  %254 = and i64 %253, 255
  %255 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %254
  %256 = load i8, i8* %255, align 1
  %257 = zext i8 %256 to i32
  %258 = xor i32 %248, %246
  %259 = xor i32 %250, %258
  %260 = and i32 %259, 16
  %261 = icmp eq i32 %248, 0
  %262 = zext i1 %261 to i32
  %263 = shl nuw nsw i32 %262, 6
  %264 = lshr i32 %248, 24
  %265 = and i32 %264, 128
  %266 = xor i32 %246, -2147483648
  %267 = xor i32 %266, %250
  %268 = and i32 %267, %258
  %269 = lshr i32 %268, 20
  %270 = and i32 %269, 2048
  %271 = or i32 %252, %257
  %272 = or i32 %271, %265
  %273 = or i32 %272, %263
  %274 = or i32 %273, %260
  %275 = or i32 %274, %270
  br label %829

; <label>:276:                                    ; preds = %1
  %277 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %278 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %277, i64 0, i32 3
  %279 = load i32, i32* %278, align 8
  %280 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %277, i64 0, i32 2
  %281 = load i32, i32* %280, align 4
  %282 = add i32 %281, %279
  %283 = and i32 %282, 255
  %284 = and i32 %281, 255
  %285 = icmp ult i32 %283, %284
  %286 = zext i1 %285 to i32
  %287 = and i32 %279, 255
  %288 = zext i32 %287 to i64
  %289 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %288
  %290 = load i8, i8* %289, align 1
  %291 = zext i8 %290 to i32
  %292 = xor i32 %282, %279
  %293 = xor i32 %292, %281
  %294 = and i32 %293, 16
  %295 = icmp eq i32 %287, 0
  %296 = zext i1 %295 to i32
  %297 = shl nuw nsw i32 %296, 6
  %298 = and i32 %279, 128
  %299 = xor i32 %282, %281
  %300 = and i32 %299, %292
  %301 = shl i32 %300, 4
  %302 = and i32 %301, 2048
  %303 = or i32 %291, %298
  %304 = or i32 %303, %297
  %305 = or i32 %304, %286
  %306 = or i32 %305, %294
  %307 = or i32 %306, %302
  br label %829

; <label>:308:                                    ; preds = %1
  %309 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %310 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %309, i64 0, i32 3
  %311 = load i32, i32* %310, align 8
  %312 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %309, i64 0, i32 2
  %313 = load i32, i32* %312, align 4
  %314 = add i32 %313, %311
  %315 = and i32 %314, 65535
  %316 = and i32 %313, 65535
  %317 = icmp ult i32 %315, %316
  %318 = zext i1 %317 to i32
  %319 = zext i32 %311 to i64
  %320 = and i64 %319, 255
  %321 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %320
  %322 = load i8, i8* %321, align 1
  %323 = zext i8 %322 to i32
  %324 = xor i32 %314, %311
  %325 = xor i32 %324, %313
  %326 = and i32 %325, 16
  %327 = and i32 %311, 65535
  %328 = icmp eq i32 %327, 0
  %329 = zext i1 %328 to i32
  %330 = shl nuw nsw i32 %329, 6
  %331 = lshr i32 %311, 8
  %332 = and i32 %331, 128
  %333 = xor i32 %314, %313
  %334 = and i32 %333, %324
  %335 = lshr i32 %334, 4
  %336 = and i32 %335, 2048
  %337 = or i32 %323, %332
  %338 = or i32 %337, %330
  %339 = or i32 %338, %318
  %340 = or i32 %339, %326
  %341 = or i32 %340, %336
  br label %829

; <label>:342:                                    ; preds = %1
  %343 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %344 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %343, i64 0, i32 3
  %345 = load i32, i32* %344, align 8
  %346 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %343, i64 0, i32 2
  %347 = load i32, i32* %346, align 4
  %348 = add i32 %347, %345
  %349 = icmp ult i32 %348, %347
  %350 = zext i1 %349 to i32
  %351 = zext i32 %345 to i64
  %352 = and i64 %351, 255
  %353 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %352
  %354 = load i8, i8* %353, align 1
  %355 = zext i8 %354 to i32
  %356 = xor i32 %348, %345
  %357 = xor i32 %356, %347
  %358 = and i32 %357, 16
  %359 = icmp eq i32 %345, 0
  %360 = zext i1 %359 to i32
  %361 = shl nuw nsw i32 %360, 6
  %362 = lshr i32 %345, 24
  %363 = and i32 %362, 128
  %364 = xor i32 %348, %347
  %365 = and i32 %364, %356
  %366 = lshr i32 %365, 20
  %367 = and i32 %366, 2048
  %368 = or i32 %355, %363
  %369 = or i32 %368, %361
  %370 = or i32 %369, %350
  %371 = or i32 %370, %358
  %372 = or i32 %371, %367
  br label %829

; <label>:373:                                    ; preds = %1
  %374 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %375 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %374, i64 0, i32 3
  %376 = load i32, i32* %375, align 8
  %377 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %374, i64 0, i32 2
  %378 = load i32, i32* %377, align 4
  %379 = add i32 %376, 1
  %380 = add i32 %379, %378
  %381 = and i32 %380, 255
  %382 = and i32 %378, 255
  %383 = icmp ule i32 %381, %382
  %384 = zext i1 %383 to i32
  %385 = and i32 %376, 255
  %386 = zext i32 %385 to i64
  %387 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %386
  %388 = load i8, i8* %387, align 1
  %389 = zext i8 %388 to i32
  %390 = xor i32 %380, %376
  %391 = xor i32 %390, %378
  %392 = and i32 %391, 16
  %393 = icmp eq i32 %385, 0
  %394 = zext i1 %393 to i32
  %395 = shl nuw nsw i32 %394, 6
  %396 = and i32 %376, 128
  %397 = xor i32 %380, %378
  %398 = and i32 %397, %390
  %399 = shl i32 %398, 4
  %400 = and i32 %399, 2048
  %401 = or i32 %389, %396
  %402 = or i32 %401, %395
  %403 = or i32 %402, %384
  %404 = or i32 %403, %392
  %405 = or i32 %404, %400
  br label %829

; <label>:406:                                    ; preds = %1
  %407 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %408 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %407, i64 0, i32 3
  %409 = load i32, i32* %408, align 8
  %410 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %407, i64 0, i32 2
  %411 = load i32, i32* %410, align 4
  %412 = add i32 %409, 1
  %413 = add i32 %412, %411
  %414 = and i32 %413, 65535
  %415 = and i32 %411, 65535
  %416 = icmp ule i32 %414, %415
  %417 = zext i1 %416 to i32
  %418 = zext i32 %409 to i64
  %419 = and i64 %418, 255
  %420 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %419
  %421 = load i8, i8* %420, align 1
  %422 = zext i8 %421 to i32
  %423 = xor i32 %413, %409
  %424 = xor i32 %423, %411
  %425 = and i32 %424, 16
  %426 = and i32 %409, 65535
  %427 = icmp eq i32 %426, 0
  %428 = zext i1 %427 to i32
  %429 = shl nuw nsw i32 %428, 6
  %430 = lshr i32 %409, 8
  %431 = and i32 %430, 128
  %432 = xor i32 %413, %411
  %433 = and i32 %432, %423
  %434 = lshr i32 %433, 4
  %435 = and i32 %434, 2048
  %436 = or i32 %422, %431
  %437 = or i32 %436, %429
  %438 = or i32 %437, %417
  %439 = or i32 %438, %425
  %440 = or i32 %439, %435
  br label %829

; <label>:441:                                    ; preds = %1
  %442 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %443 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %442, i64 0, i32 3
  %444 = load i32, i32* %443, align 8
  %445 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %442, i64 0, i32 2
  %446 = load i32, i32* %445, align 4
  %447 = add i32 %444, 1
  %448 = add i32 %447, %446
  %449 = icmp ule i32 %448, %446
  %450 = zext i1 %449 to i32
  %451 = zext i32 %444 to i64
  %452 = and i64 %451, 255
  %453 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %452
  %454 = load i8, i8* %453, align 1
  %455 = zext i8 %454 to i32
  %456 = xor i32 %448, %444
  %457 = xor i32 %456, %446
  %458 = and i32 %457, 16
  %459 = icmp eq i32 %444, 0
  %460 = zext i1 %459 to i32
  %461 = shl nuw nsw i32 %460, 6
  %462 = lshr i32 %444, 24
  %463 = and i32 %462, 128
  %464 = xor i32 %448, %446
  %465 = and i32 %464, %456
  %466 = lshr i32 %465, 20
  %467 = and i32 %466, 2048
  %468 = or i32 %455, %463
  %469 = or i32 %468, %461
  %470 = or i32 %469, %450
  %471 = or i32 %470, %458
  %472 = or i32 %471, %467
  br label %829

; <label>:473:                                    ; preds = %1
  %474 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %475 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %474, i64 0, i32 3
  %476 = load i32, i32* %475, align 8
  %477 = and i32 %476, 255
  %478 = zext i32 %477 to i64
  %479 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %478
  %480 = load i8, i8* %479, align 1
  %481 = zext i8 %480 to i32
  %482 = icmp eq i32 %477, 0
  %483 = zext i1 %482 to i32
  %484 = shl nuw nsw i32 %483, 6
  %485 = and i32 %476, 128
  %486 = or i32 %481, %485
  %487 = or i32 %486, %484
  br label %829

; <label>:488:                                    ; preds = %1
  %489 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %490 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %489, i64 0, i32 3
  %491 = load i32, i32* %490, align 8
  %492 = zext i32 %491 to i64
  %493 = and i64 %492, 255
  %494 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %493
  %495 = load i8, i8* %494, align 1
  %496 = zext i8 %495 to i32
  %497 = and i32 %491, 65535
  %498 = icmp eq i32 %497, 0
  %499 = zext i1 %498 to i32
  %500 = shl nuw nsw i32 %499, 6
  %501 = lshr i32 %491, 8
  %502 = and i32 %501, 128
  %503 = or i32 %502, %496
  %504 = or i32 %503, %500
  br label %829

; <label>:505:                                    ; preds = %1
  %506 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %507 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %506, i64 0, i32 3
  %508 = load i32, i32* %507, align 8
  %509 = zext i32 %508 to i64
  %510 = and i64 %509, 255
  %511 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %510
  %512 = load i8, i8* %511, align 1
  %513 = zext i8 %512 to i32
  %514 = icmp eq i32 %508, 0
  %515 = zext i1 %514 to i32
  %516 = shl nuw nsw i32 %515, 6
  %517 = lshr i32 %508, 24
  %518 = and i32 %517, 128
  %519 = or i32 %518, %513
  %520 = or i32 %519, %516
  br label %829

; <label>:521:                                    ; preds = %1
  %522 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %523 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %522, i64 0, i32 3
  %524 = load i32, i32* %523, align 8
  %525 = add i32 %524, 31
  %526 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %522, i64 0, i32 2
  %527 = load i32, i32* %526, align 4
  %528 = and i32 %524, 255
  %529 = zext i32 %528 to i64
  %530 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %529
  %531 = load i8, i8* %530, align 1
  %532 = zext i8 %531 to i32
  %533 = xor i32 %525, %524
  %534 = and i32 %533, 16
  %535 = icmp eq i32 %528, 0
  %536 = zext i1 %535 to i32
  %537 = shl nuw nsw i32 %536, 6
  %538 = and i32 %524, 128
  %539 = icmp eq i32 %528, 128
  %540 = zext i1 %539 to i32
  %541 = shl nuw nsw i32 %540, 11
  %542 = or i32 %538, %527
  %543 = or i32 %542, %532
  %544 = or i32 %543, %534
  %545 = or i32 %544, %537
  %546 = or i32 %545, %541
  br label %829

; <label>:547:                                    ; preds = %1
  %548 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %549 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %548, i64 0, i32 3
  %550 = load i32, i32* %549, align 8
  %551 = add i32 %550, 31
  %552 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %548, i64 0, i32 2
  %553 = load i32, i32* %552, align 4
  %554 = zext i32 %550 to i64
  %555 = and i64 %554, 255
  %556 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %555
  %557 = load i8, i8* %556, align 1
  %558 = zext i8 %557 to i32
  %559 = xor i32 %551, %550
  %560 = and i32 %559, 16
  %561 = and i32 %550, 65535
  %562 = icmp eq i32 %561, 0
  %563 = zext i1 %562 to i32
  %564 = shl nuw nsw i32 %563, 6
  %565 = lshr i32 %550, 8
  %566 = and i32 %565, 128
  %567 = icmp eq i32 %561, 32768
  %568 = zext i1 %567 to i32
  %569 = shl nuw nsw i32 %568, 11
  %570 = or i32 %566, %553
  %571 = or i32 %570, %558
  %572 = or i32 %571, %560
  %573 = or i32 %572, %564
  %574 = or i32 %573, %569
  br label %829

; <label>:575:                                    ; preds = %1
  %576 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %577 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 3
  %578 = load i32, i32* %577, align 8
  %579 = add i32 %578, 31
  %580 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 2
  %581 = load i32, i32* %580, align 4
  %582 = zext i32 %578 to i64
  %583 = and i64 %582, 255
  %584 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %583
  %585 = load i8, i8* %584, align 1
  %586 = zext i8 %585 to i32
  %587 = xor i32 %579, %578
  %588 = and i32 %587, 16
  %589 = icmp eq i32 %578, 0
  %590 = zext i1 %589 to i32
  %591 = shl nuw nsw i32 %590, 6
  %592 = lshr i32 %578, 24
  %593 = and i32 %592, 128
  %594 = icmp eq i32 %578, -2147483648
  %595 = zext i1 %594 to i32
  %596 = shl nuw nsw i32 %595, 11
  %597 = or i32 %593, %581
  %598 = or i32 %597, %586
  %599 = or i32 %598, %588
  %600 = or i32 %599, %591
  %601 = or i32 %600, %596
  br label %829

; <label>:602:                                    ; preds = %1
  %603 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %604 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %603, i64 0, i32 3
  %605 = load i32, i32* %604, align 8
  %606 = add i32 %605, 1
  %607 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %603, i64 0, i32 2
  %608 = load i32, i32* %607, align 4
  %609 = and i32 %605, 255
  %610 = zext i32 %609 to i64
  %611 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %610
  %612 = load i8, i8* %611, align 1
  %613 = zext i8 %612 to i32
  %614 = xor i32 %606, %605
  %615 = and i32 %614, 16
  %616 = icmp eq i32 %609, 0
  %617 = zext i1 %616 to i32
  %618 = shl nuw nsw i32 %617, 6
  %619 = and i32 %605, 128
  %620 = icmp eq i32 %609, 127
  %621 = zext i1 %620 to i32
  %622 = shl nuw nsw i32 %621, 11
  %623 = or i32 %619, %608
  %624 = or i32 %623, %613
  %625 = or i32 %624, %615
  %626 = or i32 %625, %618
  %627 = or i32 %626, %622
  br label %829

; <label>:628:                                    ; preds = %1
  %629 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %630 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %629, i64 0, i32 3
  %631 = load i32, i32* %630, align 8
  %632 = add i32 %631, 1
  %633 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %629, i64 0, i32 2
  %634 = load i32, i32* %633, align 4
  %635 = zext i32 %631 to i64
  %636 = and i64 %635, 255
  %637 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %636
  %638 = load i8, i8* %637, align 1
  %639 = zext i8 %638 to i32
  %640 = xor i32 %632, %631
  %641 = and i32 %640, 16
  %642 = and i32 %631, 65535
  %643 = icmp eq i32 %642, 0
  %644 = zext i1 %643 to i32
  %645 = shl nuw nsw i32 %644, 6
  %646 = lshr i32 %631, 8
  %647 = and i32 %646, 128
  %648 = icmp eq i32 %642, 32767
  %649 = zext i1 %648 to i32
  %650 = shl nuw nsw i32 %649, 11
  %651 = or i32 %647, %634
  %652 = or i32 %651, %639
  %653 = or i32 %652, %641
  %654 = or i32 %653, %645
  %655 = or i32 %654, %650
  br label %829

; <label>:656:                                    ; preds = %1
  %657 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %658 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %657, i64 0, i32 3
  %659 = load i32, i32* %658, align 8
  %660 = add i32 %659, 1
  %661 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %657, i64 0, i32 2
  %662 = load i32, i32* %661, align 4
  %663 = zext i32 %659 to i64
  %664 = and i64 %663, 255
  %665 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %664
  %666 = load i8, i8* %665, align 1
  %667 = zext i8 %666 to i32
  %668 = xor i32 %660, %659
  %669 = and i32 %668, 16
  %670 = icmp eq i32 %659, 0
  %671 = zext i1 %670 to i32
  %672 = shl nuw nsw i32 %671, 6
  %673 = lshr i32 %659, 24
  %674 = and i32 %673, 128
  %675 = icmp eq i32 %659, 2147483647
  %676 = zext i1 %675 to i32
  %677 = shl nuw nsw i32 %676, 11
  %678 = or i32 %674, %662
  %679 = or i32 %678, %667
  %680 = or i32 %679, %669
  %681 = or i32 %680, %672
  %682 = or i32 %681, %677
  br label %829

; <label>:683:                                    ; preds = %1
  %684 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %685 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %684, i64 0, i32 2
  %686 = load i32, i32* %685, align 4
  %687 = lshr i32 %686, 7
  %688 = and i32 %687, 1
  %689 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %684, i64 0, i32 3
  %690 = load i32, i32* %689, align 8
  %691 = and i32 %690, 255
  %692 = zext i32 %691 to i64
  %693 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %692
  %694 = load i8, i8* %693, align 1
  %695 = zext i8 %694 to i32
  %696 = icmp eq i32 %691, 0
  %697 = zext i1 %696 to i32
  %698 = shl nuw nsw i32 %697, 6
  %699 = and i32 %690, 128
  %700 = xor i32 %690, %686
  %701 = shl i32 %700, 4
  %702 = and i32 %701, 2048
  %703 = or i32 %699, %688
  %704 = or i32 %703, %695
  %705 = or i32 %704, %702
  %706 = or i32 %705, %698
  br label %829

; <label>:707:                                    ; preds = %1
  %708 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %709 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %708, i64 0, i32 2
  %710 = load i32, i32* %709, align 4
  %711 = lshr i32 %710, 15
  %712 = and i32 %711, 1
  %713 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %708, i64 0, i32 3
  %714 = load i32, i32* %713, align 8
  %715 = zext i32 %714 to i64
  %716 = and i64 %715, 255
  %717 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %716
  %718 = load i8, i8* %717, align 1
  %719 = zext i8 %718 to i32
  %720 = and i32 %714, 65535
  %721 = icmp eq i32 %720, 0
  %722 = zext i1 %721 to i32
  %723 = shl nuw nsw i32 %722, 6
  %724 = lshr i32 %714, 8
  %725 = and i32 %724, 128
  %726 = xor i32 %714, %710
  %727 = lshr i32 %726, 4
  %728 = and i32 %727, 2048
  %729 = or i32 %719, %712
  %730 = or i32 %729, %725
  %731 = or i32 %730, %728
  %732 = or i32 %731, %723
  br label %829

; <label>:733:                                    ; preds = %1
  %734 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %735 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %734, i64 0, i32 2
  %736 = load i32, i32* %735, align 4
  %737 = lshr i32 %736, 31
  %738 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %734, i64 0, i32 3
  %739 = load i32, i32* %738, align 8
  %740 = zext i32 %739 to i64
  %741 = and i64 %740, 255
  %742 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %741
  %743 = load i8, i8* %742, align 1
  %744 = zext i8 %743 to i32
  %745 = icmp eq i32 %739, 0
  %746 = zext i1 %745 to i32
  %747 = shl nuw nsw i32 %746, 6
  %748 = lshr i32 %739, 24
  %749 = and i32 %748, 128
  %750 = xor i32 %739, %736
  %751 = lshr i32 %750, 20
  %752 = and i32 %751, 2048
  %753 = or i32 %744, %737
  %754 = or i32 %753, %749
  %755 = or i32 %754, %747
  %756 = or i32 %755, %752
  br label %829

; <label>:757:                                    ; preds = %1
  %758 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %759 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %758, i64 0, i32 2
  %760 = load i32, i32* %759, align 4
  %761 = and i32 %760, 1
  %762 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %758, i64 0, i32 3
  %763 = load i32, i32* %762, align 8
  %764 = and i32 %763, 255
  %765 = zext i32 %764 to i64
  %766 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %765
  %767 = load i8, i8* %766, align 1
  %768 = zext i8 %767 to i32
  %769 = icmp eq i32 %764, 0
  %770 = zext i1 %769 to i32
  %771 = shl nuw nsw i32 %770, 6
  %772 = and i32 %763, 128
  %773 = xor i32 %763, %760
  %774 = shl i32 %773, 4
  %775 = and i32 %774, 2048
  %776 = or i32 %772, %761
  %777 = or i32 %776, %768
  %778 = or i32 %777, %775
  %779 = or i32 %778, %771
  br label %829

; <label>:780:                                    ; preds = %1
  %781 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %782 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %781, i64 0, i32 2
  %783 = load i32, i32* %782, align 4
  %784 = and i32 %783, 1
  %785 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %781, i64 0, i32 3
  %786 = load i32, i32* %785, align 8
  %787 = zext i32 %786 to i64
  %788 = and i64 %787, 255
  %789 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %788
  %790 = load i8, i8* %789, align 1
  %791 = zext i8 %790 to i32
  %792 = and i32 %786, 65535
  %793 = icmp eq i32 %792, 0
  %794 = zext i1 %793 to i32
  %795 = shl nuw nsw i32 %794, 6
  %796 = lshr i32 %786, 8
  %797 = and i32 %796, 128
  %798 = xor i32 %786, %783
  %799 = lshr i32 %798, 4
  %800 = and i32 %799, 2048
  %801 = or i32 %791, %784
  %802 = or i32 %801, %797
  %803 = or i32 %802, %800
  %804 = or i32 %803, %795
  br label %829

; <label>:805:                                    ; preds = %1
  %806 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %807 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %806, i64 0, i32 2
  %808 = load i32, i32* %807, align 4
  %809 = and i32 %808, 1
  %810 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %806, i64 0, i32 3
  %811 = load i32, i32* %810, align 8
  %812 = zext i32 %811 to i64
  %813 = and i64 %812, 255
  %814 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %813
  %815 = load i8, i8* %814, align 1
  %816 = zext i8 %815 to i32
  %817 = icmp eq i32 %811, 0
  %818 = zext i1 %817 to i32
  %819 = shl nuw nsw i32 %818, 6
  %820 = lshr i32 %811, 24
  %821 = and i32 %820, 128
  %822 = xor i32 %811, %808
  %823 = lshr i32 %822, 20
  %824 = and i32 %823, 2048
  %825 = or i32 %816, %809
  %826 = or i32 %825, %821
  %827 = or i32 %826, %819
  %828 = or i32 %827, %824
  br label %829

; <label>:829:                                    ; preds = %1, %805, %780, %757, %733, %707, %683, %656, %628, %602, %575, %547, %521, %505, %488, %473, %441, %406, %373, %342, %308, %276, %243, %208, %174, %142, %108, %75, %52, %28, %6, %2
  %830 = phi i32 [ %828, %805 ], [ %804, %780 ], [ %779, %757 ], [ %756, %733 ], [ %732, %707 ], [ %706, %683 ], [ %682, %656 ], [ %655, %628 ], [ %627, %602 ], [ %601, %575 ], [ %574, %547 ], [ %546, %521 ], [ %520, %505 ], [ %504, %488 ], [ %487, %473 
  ret i32 %830
}

; Function Attrs: norecurse nounwind uwtable
define void @cpu_set_eflags(%struct.CPUX86State* nocapture, i32) local_unnamed_addr #1 {
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %0, i64 0, i32 1
  store i32 1, i32* %3, align 16
  %4 = and i32 %1, 2261
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %0, i64 0, i32 2
  store i32 %4, i32* %5, align 4
  %6 = lshr i32 %1, 9
  %7 = and i32 %6, 2
  %8 = xor i32 %7, 2
  %9 = add nsw i32 %8, -1
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %0, i64 0, i32 6
  store i32 %9, i32* %10, align 4
  %11 = and i32 %1, -3286
  %12 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %0, i64 0, i32 7
  store i32 %11, i32* %12, align 8
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_write_eflags(i32, i32) local_unnamed_addr #1 {
  %3 = and i32 %0, 2261
  %4 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 2
  store i32 %3, i32* %5, align 4
  %6 = lshr i32 %0, 9
  %7 = and i32 %6, 2
  %8 = xor i32 %7, 2
  %9 = add nsw i32 %8, -1
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 6
  store i32 %9, i32* %10, align 4
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 7
  %12 = load i32, i32* %11, align 8
  %13 = xor i32 %1, -1
  %14 = and i32 %12, %13
  %15 = and i32 %0, -3286
  %16 = and i32 %15, %1
  %17 = or i32 %14, %16
  store i32 %17, i32* %11, align 8
  ret void
}

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @helper_read_eflags() local_unnamed_addr #0 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 1
  %3 = load i32, i32* %2, align 16
  %4 = tail call i32 @helper_cc_compute_all(i32 %3)
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 6
  %6 = load i32, i32* %5, align 4
  %7 = and i32 %6, 1024
  %8 = or i32 %7, %4
  %9 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 7
  %10 = load i32, i32* %9, align 8
  %11 = and i32 %10, -196609
  %12 = or i32 %8, %11
  ret i32 %12
}

; Function Attrs: uwtable
define void @helper_check_iob(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 13, i32 3
  %4 = load i32, i32* %3, align 4
  %5 = trunc i32 %4 to i16
  %6 = icmp slt i16 %5, 0
  %7 = and i32 %4, 3840
  %8 = icmp eq i32 %7, 2304
  %9 = and i1 %6, %8
  br i1 %9, label %10, label %35

; <label>:10:                                     ; preds = %1
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 13, i32 2
  %12 = load i32, i32* %11, align 8
  %13 = icmp ult i32 %12, 103
  br i1 %13, label %35, label %14

; <label>:14:                                     ; preds = %10
  %15 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 13, i32 1
  %16 = load i32, i32* %15, align 4
  %17 = add i32 %16, 102
  %18 = tail call fastcc i32 @lduw_kernel(i32 %17)
  %19 = ashr i32 %0, 3
  %20 = add nsw i32 %18, %19
  %21 = add nsw i32 %20, 1
  %22 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 13, i32 2
  %24 = load i32, i32* %23, align 8
  %25 = icmp ugt i32 %21, %24
  br i1 %25, label %35, label %26

; <label>:26:                                     ; preds = %14
  %27 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 13, i32 1
  %28 = load i32, i32* %27, align 4
  %29 = add i32 %28, %20
  %30 = tail call fastcc i32 @lduw_kernel(i32 %29)
  %31 = and i32 %0, 7
  %32 = shl i32 1, %31
  %33 = and i32 %30, %32
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %36, label %35

; <label>:35:                                     ; preds = %26, %14, %10, %1
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:36:                                     ; preds = %26
  ret void
}

; Function Attrs: uwtable
define internal fastcc i32 @lduw_kernel(i32) unnamed_addr #2 {
  %2 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %3 = load i32, i32* %2, align 4
  %4 = icmp eq i32 %3, 0
  br i1 %4, label %6, label %5

; <label>:5:                                      ; preds = %1
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %6

; <label>:6:                                      ; preds = %1, %5
  %7 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %8 = load i32, i32* %7, align 4
  %9 = icmp eq i32 %8, 0
  br i1 %9, label %12, label %10

; <label>:10:                                     ; preds = %6
  %11 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %12

; <label>:12:                                     ; preds = %6, %10
  %13 = phi i32 [ %11, %10 ], [ %0, %6 ]
  %14 = lshr i32 %13, 12
  %15 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %14, i32 0, i32 1048575, i32 0)
  %16 = and i32 %15, 1023
  %17 = zext i32 %16 to i64
  %18 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %19 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 77, i64 0, i64 %17, i32 0
  %20 = load i32, i32* %19, align 8
  %21 = and i32 %13, -4095
  %22 = icmp eq i32 %20, %21
  br i1 %22, label %26, label %23, !prof !2

; <label>:23:                                     ; preds = %12
  %24 = tail call zeroext i16 @__ldw_mmu(i32 %13, i32 0)
  %25 = zext i16 %24 to i32
  br label %39

; <label>:26:                                     ; preds = %12
  %27 = zext i32 %13 to i64
  %28 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 77, i64 0, i64 %17, i32 4
  %29 = load i64, i64* %28, align 8
  %30 = add i64 %29, %27
  %31 = inttoptr i64 %30 to i16*
  %32 = load i16, i16* %31, align 2
  %33 = zext i16 %32 to i32
  %34 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %35 = load i32, i32* %34, align 4
  %36 = icmp eq i32 %35, 0
  br i1 %36, label %39, label %37

; <label>:37:                                     ; preds = %26
  %38 = zext i16 %32 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %13, i64 %38, i32 4, i32 0, i64 0)
  br label %39

; <label>:39:                                     ; preds = %26, %37, %23
  %40 = phi i32 [ %25, %23 ], [ %33, %37 ], [ %33, %26 ]
  ret i32 %40
}

; Function Attrs: noreturn uwtable
define internal fastcc void @raise_exception_err(i32, i32) unnamed_addr #3 {
  tail call fastcc void @raise_interrupt(i32 %0, i32 0, i32 %1, i32 0) #12
  unreachable
}

; Function Attrs: noreturn uwtable
define internal fastcc void @raise_interrupt(i32, i32, i32, i32) unnamed_addr #3 {
  %5 = icmp eq i32 %1, 0
  br i1 %5, label %6, label %62

; <label>:6:                                      ; preds = %4
  %7 = add nsw i32 %0, 64
  %8 = sext i32 %2 to i64
  tail call void @helper_svm_check_intercept_param(i32 %7, i64 %8)
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 64
  %11 = load i32, i32* %10, align 4
  %12 = icmp eq i32 %11, 0
  %13 = add i32 %11, -10
  %14 = icmp ult i32 %13, 4
  %15 = or i1 %12, %14
  %16 = icmp eq i32 %0, 0
  %17 = add i32 %0, -10
  %18 = icmp ult i32 %17, 4
  %19 = or i1 %16, %18
  %20 = load i32, i32* @loglevel, align 4
  %21 = and i32 %20, 16
  %22 = icmp eq i32 %21, 0
  br i1 %22, label %29, label %23

; <label>:23:                                     ; preds = %6
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %25 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %24, i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.16, i64 0, i64 0), i32 %11, i32 %0)
  %26 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %27 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 64
  %28 = load i32, i32* %27, align 4
  br label %29

; <label>:29:                                     ; preds = %23, %6
  %30 = phi i32 [ %11, %6 ], [ %28, %23 ]
  %31 = phi %struct.CPUX86State* [ %9, %6 ], [ %26, %23 ]
  %32 = icmp eq i32 %30, 8
  br i1 %32, label %33, label %48

; <label>:33:                                     ; preds = %29
  %34 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 8
  %35 = load i32, i32* %34, align 4
  %36 = and i32 %35, 2097152
  %37 = icmp eq i32 %36, 0
  br i1 %37, label %39, label %38

; <label>:38:                                     ; preds = %33
  tail call void @helper_vmexit(i32 127, i64 0)
  unreachable

; <label>:39:                                     ; preds = %33
  %40 = load i32, i32* @loglevel, align 4
  %41 = and i32 %40, 512
  %42 = icmp eq i32 %41, 0
  br i1 %42, label %46, label %43

; <label>:43:                                     ; preds = %39
  %44 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %45 = tail call i64 @fwrite(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.17, i64 0, i64 0), i64 13, i64 1, %struct._IO_FILE* %44)
  br label %46

; <label>:46:                                     ; preds = %43, %39
  tail call void @libcpu_system_reset_request()
  %47 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %74

; <label>:48:                                     ; preds = %29
  %49 = and i1 %19, %15
  br i1 %49, label %55, label %50

; <label>:50:                                     ; preds = %48
  %51 = icmp eq i32 %30, 14
  %52 = icmp eq i32 %0, 14
  %53 = or i1 %52, %19
  %54 = and i1 %53, %51
  br i1 %54, label %55, label %56

; <label>:55:                                     ; preds = %50, %48
  br label %56

; <label>:56:                                     ; preds = %55, %50
  %57 = phi i32 [ 0, %55 ], [ %2, %50 ]
  %58 = phi i32 [ 8, %55 ], [ %0, %50 ]
  switch i32 %0, label %59 [
    i32 13, label %60
    i32 12, label %60
    i32 11, label %60
    i32 10, label %60
    i32 0, label %60
  ]

; <label>:59:                                     ; preds = %56
  switch i32 %58, label %74 [
    i32 14, label %60
    i32 8, label %60
  ]

; <label>:60:                                     ; preds = %59, %59, %56, %56, %56, %56, %56
  %61 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 64
  store i32 %58, i32* %61, align 4
  br label %74

; <label>:62:                                     ; preds = %4
  %63 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %64 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %63, i64 0, i32 8
  %65 = load i32, i32* %64, align 4
  %66 = and i32 %65, 2097152
  %67 = icmp eq i32 %66, 0
  br i1 %67, label %74, label %68, !prof !2

; <label>:68:                                     ; preds = %62
  %69 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %63, i64 0, i32 42
  %70 = load i64, i64* %69, align 8
  %71 = and i64 %70, 2097152
  %72 = icmp eq i64 %71, 0
  br i1 %72, label %74, label %73

; <label>:73:                                     ; preds = %68
  tail call void @helper_vmexit(i32 117, i64 0)
  unreachable

; <label>:74:                                     ; preds = %68, %62, %60, %59, %46
  %75 = phi %struct.CPUX86State* [ %47, %46 ], [ %31, %59 ], [ %31, %60 ], [ %63, %62 ], [ %63, %68 ]
  %76 = phi i32 [ %2, %46 ], [ %57, %59 ], [ %57, %60 ], [ %2, %62 ], [ %2, %68 ]
  %77 = phi i32 [ 65537, %46 ], [ %58, %59 ], [ %58, %60 ], [ %0, %62 ], [ %0, %68 ]
  %78 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %75, i64 0, i32 92
  store i32 %77, i32* %78, align 8
  %79 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %75, i64 0, i32 58
  store i32 %76, i32* %79, align 16
  %80 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %75, i64 0, i32 59
  store i32 %1, i32* %80, align 4
  %81 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %75, i64 0, i32 5
  %82 = load i32, i32* %81, align 16
  %83 = add i32 %82, %3
  %84 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %75, i64 0, i32 60
  store i32 %83, i32* %84, align 8
  tail call void @cpu_loop_exit(%struct.CPUX86State* %75) #12
  unreachable
}

; Function Attrs: uwtable
define void @helper_svm_check_intercept_param(i32, i64) local_unnamed_addr #2 {
  %3 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %4 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 8
  %5 = load i32, i32* %4, align 4
  %6 = and i32 %5, 2097152
  %7 = icmp eq i32 %6, 0
  br i1 %7, label %103, label %8, !prof !2

; <label>:8:                                      ; preds = %2
  switch i32 %0, label %94 [
    i32 0, label %9
    i32 1, label %9
    i32 2, label %9
    i32 3, label %9
    i32 4, label %9
    i32 5, label %9
    i32 6, label %9
    i32 7, label %9
    i32 8, label %9
    i32 16, label %17
    i32 17, label %17
    i32 18, label %17
    i32 19, label %17
    i32 20, label %17
    i32 21, label %17
    i32 22, label %17
    i32 23, label %17
    i32 24, label %17
    i32 32, label %26
    i32 33, label %26
    i32 34, label %26
    i32 35, label %26
    i32 36, label %26
    i32 37, label %26
    i32 38, label %26
    i32 39, label %26
    i32 48, label %35
    i32 49, label %35
    i32 50, label %35
    i32 51, label %35
    i32 52, label %35
    i32 53, label %35
    i32 54, label %35
    i32 55, label %35
    i32 64, label %44
    i32 65, label %44
    i32 66, label %44
    i32 67, label %44
    i32 68, label %44
    i32 69, label %44
    i32 70, label %44
    i32 71, label %44
    i32 72, label %44
    i32 73, label %44
    i32 74, label %44
    i32 75, label %44
    i32 76, label %44
    i32 77, label %44
    i32 78, label %44
    i32 79, label %44
    i32 80, label %44
    i32 81, label %44
    i32 82, label %44
    i32 83, label %44
    i32 84, label %44
    i32 85, label %44
    i32 86, label %44
    i32 87, label %44
    i32 88, label %44
    i32 89, label %44
    i32 90, label %44
    i32 91, label %44
    i32 92, label %44
    i32 93, label %44
    i32 94, label %44
    i32 95, label %44
    i32 124, label %52
  ]

; <label>:9:                                      ; preds = %8, %8, %8, %8, %8, %8, %8, %8, %8
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 43
  %11 = load i16, i16* %10, align 16
  %12 = zext i16 %11 to i32
  %13 = shl i32 1, %0
  %14 = and i32 %12, %13
  %15 = icmp eq i32 %14, 0
  br i1 %15, label %103, label %16

; <label>:16:                                     ; preds = %9
  tail call void @helper_vmexit(i32 %0, i64 %1)
  unreachable

; <label>:17:                                     ; preds = %8, %8, %8, %8, %8, %8, %8, %8, %8
  %18 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 44
  %19 = load i16, i16* %18, align 2
  %20 = zext i16 %19 to i32
  %21 = add i32 %0, -16
  %22 = shl i32 1, %21
  %23 = and i32 %20, %22
  %24 = icmp eq i32 %23, 0
  br i1 %24, label %103, label %25

; <label>:25:                                     ; preds = %17
  tail call void @helper_vmexit(i32 %0, i64 %1)
  unreachable

; <label>:26:                                     ; preds = %8, %8, %8, %8, %8, %8, %8, %8
  %27 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 45
  %28 = load i16, i16* %27, align 4
  %29 = zext i16 %28 to i32
  %30 = add i32 %0, -32
  %31 = shl i32 1, %30
  %32 = and i32 %29, %31
  %33 = icmp eq i32 %32, 0
  br i1 %33, label %103, label %34

; <label>:34:                                     ; preds = %26
  tail call void @helper_vmexit(i32 %0, i64 %1)
  unreachable

; <label>:35:                                     ; preds = %8, %8, %8, %8, %8, %8, %8, %8
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 46
  %37 = load i16, i16* %36, align 2
  %38 = zext i16 %37 to i32
  %39 = add i32 %0, -48
  %40 = shl i32 1, %39
  %41 = and i32 %38, %40
  %42 = icmp eq i32 %41, 0
  br i1 %42, label %103, label %43

; <label>:43:                                     ; preds = %35
  tail call void @helper_vmexit(i32 %0, i64 %1)
  unreachable

; <label>:44:                                     ; preds = %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8, %8
  %45 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 47
  %46 = load i32, i32* %45, align 8
  %47 = add i32 %0, -64
  %48 = shl i32 1, %47
  %49 = and i32 %46, %48
  %50 = icmp eq i32 %49, 0
  br i1 %50, label %103, label %51

; <label>:51:                                     ; preds = %44
  tail call void @helper_vmexit(i32 %0, i64 %1)
  unreachable

; <label>:52:                                     ; preds = %8
  %53 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 42
  %54 = load i64, i64* %53, align 8
  %55 = and i64 %54, 268435456
  %56 = icmp eq i64 %55, 0
  br i1 %56, label %103, label %57

; <label>:57:                                     ; preds = %52
  %58 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 40
  %59 = load i64, i64* %58, align 8
  %60 = add i64 %59, 72
  %61 = tail call i64 @ldq_phys(i64 %60)
  %62 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %63 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %62, i64 0, i32 0, i64 1
  %64 = load i32, i32* %63, align 4
  %65 = and i32 %64, -8192
  switch i32 %65, label %70 [
    i32 -1073676288, label %76
    i32 -1073741824, label %72
  ]

; <label>:66:                                     ; preds = %70
  %67 = shl i32 %64, 1
  %68 = lshr i32 %64, 2
  %69 = and i32 %68, 536870911
  br label %81

; <label>:70:                                     ; preds = %57
  %71 = icmp ult i32 %64, 8192
  br i1 %71, label %66, label %80

; <label>:72:                                     ; preds = %57
  %73 = shl i32 %64, 1
  %74 = add i32 %73, -2147467264
  %75 = lshr i32 %74, 3
  br label %81

; <label>:76:                                     ; preds = %57
  %77 = shl i32 %64, 1
  %78 = add i32 %77, 2147385344
  %79 = lshr i32 %78, 3
  br label %81

; <label>:80:                                     ; preds = %70
  tail call void @helper_vmexit(i32 124, i64 %1)
  unreachable

; <label>:81:                                     ; preds = %76, %72, %66
  %82 = phi i32 [ %78, %76 ], [ %74, %72 ], [ %67, %66 ]
  %83 = phi i32 [ %79, %76 ], [ %75, %72 ], [ %69, %66 ]
  %84 = and i32 %82, 6
  %85 = zext i32 %83 to i64
  %86 = add i64 %85, %61
  %87 = tail call i32 @ldub_phys(i64 %86)
  %88 = trunc i64 %1 to i32
  %89 = shl i32 1, %88
  %90 = shl i32 %89, %84
  %91 = and i32 %87, %90
  %92 = icmp eq i32 %91, 0
  br i1 %92, label %103, label %93

; <label>:93:                                     ; preds = %81
  tail call void @helper_vmexit(i32 124, i64 %1)
  unreachable

; <label>:94:                                     ; preds = %8
  %95 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 42
  %96 = load i64, i64* %95, align 8
  %97 = add i32 %0, -96
  %98 = zext i32 %97 to i64
  %99 = shl i64 1, %98
  %100 = and i64 %96, %99
  %101 = icmp eq i64 %100, 0
  br i1 %101, label %103, label %102

; <label>:102:                                    ; preds = %94
  tail call void @helper_vmexit(i32 %0, i64 %1)
  unreachable

; <label>:103:                                    ; preds = %81, %94, %9, %17, %26, %35, %44, %52, %2
  ret void
}

; Function Attrs: nounwind
declare i32 @fprintf(%struct._IO_FILE* nocapture, i8* nocapture readonly, ...) #4

; Function Attrs: noreturn uwtable
define void @helper_vmexit(i32, i64) local_unnamed_addr #3 {
  %3 = load i32, i32* @loglevel, align 4
  %4 = and i32 %3, 2
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %17, label %6

; <label>:6:                                      ; preds = %2
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %8 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %9 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %8, i64 0, i32 40
  %10 = load i64, i64* %9, align 8
  %11 = add i64 %10, 128
  %12 = tail call i64 @ldq_phys(i64 %11)
  %13 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %14 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %13, i64 0, i32 5
  %15 = load i32, i32* %14, align 16
  %16 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %7, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.15, i64 0, i64 0), i32 %0, i64 %1, i64 %12, i32 %15)
  br label %17

; <label>:17:                                     ; preds = %2, %6
  %18 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %19 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 8
  %20 = load i32, i32* %19, align 4
  %21 = and i32 %20, 3
  %22 = and i32 %20, 8
  %23 = icmp eq i32 %22, 0
  %24 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 40
  %25 = load i64, i64* %24, align 8
  %26 = add i64 %25, 104
  br i1 %23, label %32, label %27

; <label>:27:                                     ; preds = %17
  tail call void @stl_phys(i64 %26, i32 1)
  %28 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %29 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %28, i64 0, i32 8
  %30 = load i32, i32* %29, align 4
  %31 = and i32 %30, -9
  store i32 %31, i32* %29, align 4
  br label %34

; <label>:32:                                     ; preds = %17
  tail call void @stl_phys(i64 %26, i32 0)
  %33 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %34

; <label>:34:                                     ; preds = %32, %27
  %35 = phi %struct.CPUX86State* [ %33, %32 ], [ %28, %27 ]
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 40
  %37 = load i64, i64* %36, align 8
  %38 = add i64 %37, 1024
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 0, i32 0
  %40 = load i32, i32* %39, align 4
  tail call void @stw_phys(i64 %38, i32 %40)
  %41 = add i64 %37, 1032
  %42 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 0, i32 1
  %43 = load i32, i32* %42, align 4
  %44 = zext i32 %43 to i64
  tail call void @stq_phys(i64 %41, i64 %44)
  %45 = add i64 %37, 1028
  %46 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 0, i32 2
  %47 = load i32, i32* %46, align 4
  tail call void @stl_phys(i64 %45, i32 %47)
  %48 = add i64 %37, 1026
  %49 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 0, i32 3
  %50 = load i32, i32* %49, align 4
  %51 = lshr i32 %50, 8
  %52 = and i32 %51, 255
  %53 = lshr i32 %50, 12
  %54 = and i32 %53, 3840
  %55 = or i32 %52, %54
  tail call void @stw_phys(i64 %48, i32 %55)
  %56 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %57 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %56, i64 0, i32 40
  %58 = load i64, i64* %57, align 8
  %59 = add i64 %58, 1040
  %60 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %56, i64 0, i32 11, i64 1, i32 0
  %61 = load i32, i32* %60, align 4
  tail call void @stw_phys(i64 %59, i32 %61)
  %62 = add i64 %58, 1048
  %63 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %56, i64 0, i32 11, i64 1, i32 1
  %64 = load i32, i32* %63, align 4
  %65 = zext i32 %64 to i64
  tail call void @stq_phys(i64 %62, i64 %65)
  %66 = add i64 %58, 1044
  %67 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %56, i64 0, i32 11, i64 1, i32 2
  %68 = load i32, i32* %67, align 4
  tail call void @stl_phys(i64 %66, i32 %68)
  %69 = add i64 %58, 1042
  %70 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %56, i64 0, i32 11, i64 1, i32 3
  %71 = load i32, i32* %70, align 4
  %72 = lshr i32 %71, 8
  %73 = and i32 %72, 255
  %74 = lshr i32 %71, 12
  %75 = and i32 %74, 3840
  %76 = or i32 %73, %75
  tail call void @stw_phys(i64 %69, i32 %76)
  %77 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %78 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %77, i64 0, i32 40
  %79 = load i64, i64* %78, align 8
  %80 = add i64 %79, 1056
  %81 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %77, i64 0, i32 11, i64 2, i32 0
  %82 = load i32, i32* %81, align 4
  tail call void @stw_phys(i64 %80, i32 %82)
  %83 = add i64 %79, 1064
  %84 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %77, i64 0, i32 11, i64 2, i32 1
  %85 = load i32, i32* %84, align 4
  %86 = zext i32 %85 to i64
  tail call void @stq_phys(i64 %83, i64 %86)
  %87 = add i64 %79, 1060
  %88 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %77, i64 0, i32 11, i64 2, i32 2
  %89 = load i32, i32* %88, align 4
  tail call void @stl_phys(i64 %87, i32 %89)
  %90 = add i64 %79, 1058
  %91 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %77, i64 0, i32 11, i64 2, i32 3
  %92 = load i32, i32* %91, align 4
  %93 = lshr i32 %92, 8
  %94 = and i32 %93, 255
  %95 = lshr i32 %92, 12
  %96 = and i32 %95, 3840
  %97 = or i32 %94, %96
  tail call void @stw_phys(i64 %90, i32 %97)
  %98 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %99 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %98, i64 0, i32 40
  %100 = load i64, i64* %99, align 8
  %101 = add i64 %100, 1072
  %102 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %98, i64 0, i32 11, i64 3, i32 0
  %103 = load i32, i32* %102, align 4
  tail call void @stw_phys(i64 %101, i32 %103)
  %104 = add i64 %100, 1080
  %105 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %98, i64 0, i32 11, i64 3, i32 1
  %106 = load i32, i32* %105, align 4
  %107 = zext i32 %106 to i64
  tail call void @stq_phys(i64 %104, i64 %107)
  %108 = add i64 %100, 1076
  %109 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %98, i64 0, i32 11, i64 3, i32 2
  %110 = load i32, i32* %109, align 4
  tail call void @stl_phys(i64 %108, i32 %110)
  %111 = add i64 %100, 1074
  %112 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %98, i64 0, i32 11, i64 3, i32 3
  %113 = load i32, i32* %112, align 4
  %114 = lshr i32 %113, 8
  %115 = and i32 %114, 255
  %116 = lshr i32 %113, 12
  %117 = and i32 %116, 3840
  %118 = or i32 %115, %117
  tail call void @stw_phys(i64 %111, i32 %118)
  %119 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %120 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %119, i64 0, i32 40
  %121 = load i64, i64* %120, align 8
  %122 = add i64 %121, 1128
  %123 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %119, i64 0, i32 14, i32 1
  %124 = load i32, i32* %123, align 4
  %125 = zext i32 %124 to i64
  tail call void @stq_phys(i64 %122, i64 %125)
  %126 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %127 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %126, i64 0, i32 40
  %128 = load i64, i64* %127, align 8
  %129 = add i64 %128, 1124
  %130 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %126, i64 0, i32 14, i32 2
  %131 = load i32, i32* %130, align 8
  tail call void @stl_phys(i64 %129, i32 %131)
  %132 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %133 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %132, i64 0, i32 40
  %134 = load i64, i64* %133, align 8
  %135 = add i64 %134, 1160
  %136 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %132, i64 0, i32 15, i32 1
  %137 = load i32, i32* %136, align 4
  %138 = zext i32 %137 to i64
  tail call void @stq_phys(i64 %135, i64 %138)
  %139 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %140 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %139, i64 0, i32 40
  %141 = load i64, i64* %140, align 8
  %142 = add i64 %141, 1156
  %143 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %139, i64 0, i32 15, i32 2
  %144 = load i32, i32* %143, align 8
  tail call void @stl_phys(i64 %142, i32 %144)
  %145 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %146 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %145, i64 0, i32 40
  %147 = load i64, i64* %146, align 8
  %148 = add i64 %147, 1232
  %149 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %145, i64 0, i32 37
  %150 = load i64, i64* %149, align 16
  tail call void @stq_phys(i64 %148, i64 %150)
  %151 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %152 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 40
  %153 = load i64, i64* %152, align 8
  %154 = add i64 %153, 1368
  %155 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 16, i64 0
  %156 = load i32, i32* %155, align 8
  %157 = zext i32 %156 to i64
  tail call void @stq_phys(i64 %154, i64 %157)
  %158 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %159 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %158, i64 0, i32 40
  %160 = load i64, i64* %159, align 8
  %161 = add i64 %160, 1600
  %162 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %158, i64 0, i32 16, i64 2
  %163 = load i32, i32* %162, align 8
  %164 = zext i32 %163 to i64
  tail call void @stq_phys(i64 %161, i64 %164)
  %165 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %166 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %165, i64 0, i32 40
  %167 = load i64, i64* %166, align 8
  %168 = add i64 %167, 1360
  %169 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %165, i64 0, i32 16, i64 3
  %170 = load i32, i32* %169, align 4
  %171 = zext i32 %170 to i64
  tail call void @stq_phys(i64 %168, i64 %171)
  %172 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %173 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %172, i64 0, i32 40
  %174 = load i64, i64* %173, align 8
  %175 = add i64 %174, 1352
  %176 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %172, i64 0, i32 16, i64 4
  %177 = load i32, i32* %176, align 8
  %178 = zext i32 %177 to i64
  tail call void @stq_phys(i64 %175, i64 %178)
  %179 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %180 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %179, i64 0, i32 40
  %181 = load i64, i64* %180, align 8
  %182 = add i64 %181, 96
  %183 = tail call i32 @ldl_phys(i64 %182)
  %184 = and i32 %183, -272
  %185 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %186 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %185, i64 0, i32 48
  %187 = load i8, i8* %186, align 4
  %188 = zext i8 %187 to i32
  %189 = and i32 %188, 15
  %190 = or i32 %189, %184
  %191 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %185, i64 0, i32 75
  %192 = load i32, i32* %191, align 16
  %193 = and i32 %192, 256
  %194 = or i32 %190, %193
  %195 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %185, i64 0, i32 40
  %196 = load i64, i64* %195, align 8
  %197 = add i64 %196, 96
  tail call void @stl_phys(i64 %197, i32 %194)
  %198 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %199 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %198, i64 0, i32 40
  %200 = load i64, i64* %199, align 8
  %201 = add i64 %200, 1392
  %202 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %198, i64 0, i32 7
  %203 = load i32, i32* %202, align 8
  %204 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %198, i64 0, i32 1
  %205 = load i32, i32* %204, align 16
  %206 = tail call i32 @helper_cc_compute_all(i32 %205) #5
  %207 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %198, i64 0, i32 6
  %208 = load i32, i32* %207, align 4
  %209 = and i32 %208, 1024
  %210 = or i32 %203, %206
  %211 = or i32 %210, %209
  %212 = or i32 %211, 2
  %213 = zext i32 %212 to i64
  tail call void @stq_phys(i64 %201, i64 %213)
  %214 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %215 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 40
  %216 = load i64, i64* %215, align 8
  %217 = add i64 %216, 1400
  %218 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 5
  %219 = load i32, i32* %218, align 16
  %220 = zext i32 %219 to i64
  tail call void @stq_phys(i64 %217, i64 %220)
  %221 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %222 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %221, i64 0, i32 40
  %223 = load i64, i64* %222, align 8
  %224 = add i64 %223, 1496
  %225 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %221, i64 0, i32 0, i64 4
  %226 = load i32, i32* %225, align 16
  %227 = zext i32 %226 to i64
  tail call void @stq_phys(i64 %224, i64 %227)
  %228 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %229 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %228, i64 0, i32 40
  %230 = load i64, i64* %229, align 8
  %231 = add i64 %230, 1528
  %232 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %228, i64 0, i32 0, i64 0
  %233 = load i32, i32* %232, align 16
  %234 = zext i32 %233 to i64
  tail call void @stq_phys(i64 %231, i64 %234)
  %235 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %236 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %235, i64 0, i32 40
  %237 = load i64, i64* %236, align 8
  %238 = add i64 %237, 1376
  %239 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %235, i64 0, i32 61, i64 7
  %240 = load i32, i32* %239, align 4
  %241 = zext i32 %240 to i64
  tail call void @stq_phys(i64 %238, i64 %241)
  %242 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %243 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %242, i64 0, i32 40
  %244 = load i64, i64* %243, align 8
  %245 = add i64 %244, 1384
  %246 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %242, i64 0, i32 61, i64 6
  %247 = load i32, i32* %246, align 4
  %248 = zext i32 %247 to i64
  tail call void @stq_phys(i64 %245, i64 %248)
  %249 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %250 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %249, i64 0, i32 40
  %251 = load i64, i64* %250, align 8
  %252 = add i64 %251, 1227
  %253 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %249, i64 0, i32 8
  %254 = load i32, i32* %253, align 4
  %255 = and i32 %254, 3
  tail call void @stb_phys(i64 %252, i32 %255)
  %256 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %257 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %256, i64 0, i32 9
  %258 = load i32, i32* %257, align 16
  %259 = and i32 %258, -11
  store i32 %259, i32* %257, align 16
  %260 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %256, i64 0, i32 8
  %261 = load i32, i32* %260, align 4
  %262 = and i32 %261, -2097153
  store i32 %262, i32* %260, align 4
  %263 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %256, i64 0, i32 47
  store i32 0, i32* %263, align 8
  %264 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %256, i64 0, i32 75
  %265 = load i32, i32* %264, align 16
  %266 = and i32 %265, -257
  store i32 %266, i32* %264, align 16
  %267 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %256, i64 0, i32 41
  %268 = bitcast i64* %267 to <2 x i64>*
  store <2 x i64> zeroinitializer, <2 x i64>* %268, align 16
  %269 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %256, i64 0, i32 39
  %270 = load i64, i64* %269, align 16
  %271 = add i64 %270, 1128
  %272 = tail call i64 @ldq_phys(i64 %271)
  %273 = trunc i64 %272 to i32
  %274 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %275 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %274, i64 0, i32 14, i32 1
  store i32 %273, i32* %275, align 4
  %276 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %274, i64 0, i32 39
  %277 = load i64, i64* %276, align 16
  %278 = add i64 %277, 1124
  %279 = tail call i32 @ldl_phys(i64 %278)
  %280 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %281 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %280, i64 0, i32 14, i32 2
  store i32 %279, i32* %281, align 8
  %282 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %280, i64 0, i32 39
  %283 = load i64, i64* %282, align 16
  %284 = add i64 %283, 1160
  %285 = tail call i64 @ldq_phys(i64 %284)
  %286 = trunc i64 %285 to i32
  %287 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %288 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %287, i64 0, i32 15, i32 1
  store i32 %286, i32* %288, align 4
  %289 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %287, i64 0, i32 39
  %290 = load i64, i64* %289, align 16
  %291 = add i64 %290, 1156
  %292 = tail call i32 @ldl_phys(i64 %291)
  %293 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %294 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %293, i64 0, i32 15, i32 2
  store i32 %292, i32* %294, align 8
  %295 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %293, i64 0, i32 39
  %296 = load i64, i64* %295, align 16
  %297 = add i64 %296, 1368
  %298 = tail call i64 @ldq_phys(i64 %297)
  %299 = or i64 %298, 1
  %300 = trunc i64 %299 to i32
  tail call void @cpu_x86_update_cr0(%struct.CPUX86State* %293, i32 %300)
  %301 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %302 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %301, i64 0, i32 39
  %303 = load i64, i64* %302, align 16
  %304 = add i64 %303, 1352
  %305 = tail call i64 @ldq_phys(i64 %304)
  %306 = trunc i64 %305 to i32
  tail call void @cpu_x86_update_cr4(%struct.CPUX86State* %301, i32 %306)
  %307 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %308 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %307, i64 0, i32 39
  %309 = load i64, i64* %308, align 16
  %310 = add i64 %309, 1360
  %311 = tail call i64 @ldq_phys(i64 %310)
  %312 = trunc i64 %311 to i32
  tail call void @cpu_x86_update_cr3(%struct.CPUX86State* %307, i32 %312)
  %313 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %314 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %313, i64 0, i32 39
  %315 = load i64, i64* %314, align 16
  %316 = add i64 %315, 1232
  %317 = tail call i64 @ldq_phys(i64 %316)
  %318 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %313, i64 0, i32 37
  store i64 %317, i64* %318, align 16
  %319 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %313, i64 0, i32 8
  %320 = load i32, i32* %319, align 4
  %321 = and i32 %320, -1064961
  %322 = trunc i64 %317 to i32
  %323 = shl i32 %322, 4
  %324 = and i32 %323, 16384
  %325 = or i32 %321, %324
  %326 = shl i32 %322, 8
  %327 = and i32 %326, 1048576
  %328 = or i32 %325, %327
  store i32 %328, i32* %319, align 4
  %329 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %330 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %329, i64 0, i32 7
  store i32 0, i32* %330, align 8
  %331 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %329, i64 0, i32 39
  %332 = load i64, i64* %331, align 16
  %333 = add i64 %332, 1392
  %334 = tail call i64 @ldq_phys(i64 %333)
  %335 = trunc i64 %334 to i32
  %336 = and i32 %335, 2261
  %337 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %338 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 2
  store i32 %336, i32* %338, align 4
  %339 = lshr i32 %335, 9
  %340 = and i32 %339, 2
  %341 = xor i32 %340, 2
  %342 = add nsw i32 %341, -1
  %343 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 6
  store i32 %342, i32* %343, align 4
  %344 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 7
  %345 = load i32, i32* %344, align 8
  %346 = and i32 %345, 3285
  %347 = and i32 %335, -3286
  %348 = or i32 %346, %347
  store i32 %348, i32* %344, align 8
  %349 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 1
  store i32 1, i32* %349, align 16
  %350 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 39
  %351 = load i64, i64* %350, align 16
  %352 = add i64 %351, 1024
  %353 = tail call i32 @lduw_phys(i64 %352)
  %354 = add i64 %351, 1032
  %355 = tail call i64 @ldq_phys(i64 %354)
  %356 = trunc i64 %355 to i32
  %357 = add i64 %351, 1028
  %358 = tail call i32 @ldl_phys(i64 %357)
  %359 = add i64 %351, 1026
  %360 = tail call i32 @lduw_phys(i64 %359)
  %361 = shl i32 %360, 8
  %362 = and i32 %361, 65280
  %363 = shl i32 %360, 12
  %364 = and i32 %363, 15728640
  %365 = or i32 %362, %364
  %366 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 11, i64 0, i32 0
  store i32 %353, i32* %366, align 4
  %367 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 11, i64 0, i32 1
  store i32 %356, i32* %367, align 4
  %368 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 11, i64 0, i32 2
  store i32 %358, i32* %368, align 4
  %369 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 11, i64 0, i32 3
  store i32 %365, i32* %369, align 4
  %370 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 8
  %371 = load i32, i32* %370, align 4
  %372 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 11, i64 2, i32 3
  %373 = load i32, i32* %372, align 4
  %374 = lshr i32 %373, 17
  %375 = and i32 %374, 32
  %376 = trunc i32 %371 to i16
  %377 = icmp slt i16 %376, 0
  br i1 %377, label %407, label %378

; <label>:378:                                    ; preds = %34
  %379 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 16, i64 0
  %380 = load i32, i32* %379, align 8
  %381 = and i32 %380, 1
  %382 = icmp eq i32 %381, 0
  br i1 %382, label %393, label %383

; <label>:383:                                    ; preds = %378
  %384 = bitcast i32* %344 to i64*
  %385 = load i64, i64* %384, align 8
  %386 = and i64 %385, 131072
  %387 = icmp ne i64 %386, 0
  %388 = and i32 %371, 16
  %389 = icmp eq i32 %388, 0
  %390 = or i1 %389, %387
  %391 = lshr i64 %385, 32
  %392 = trunc i64 %391 to i32
  br i1 %390, label %393, label %396

; <label>:393:                                    ; preds = %383, %378
  %394 = phi i32 [ %392, %383 ], [ %371, %378 ]
  %395 = or i32 %375, 64
  br label %407

; <label>:396:                                    ; preds = %383
  %397 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 11, i64 3, i32 1
  %398 = load i32, i32* %397, align 4
  %399 = or i32 %356, %398
  %400 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %337, i64 0, i32 11, i64 2, i32 1
  %401 = load i32, i32* %400, align 4
  %402 = or i32 %399, %401
  %403 = icmp ne i32 %402, 0
  %404 = zext i1 %403 to i32
  %405 = shl nuw nsw i32 %404, 6
  %406 = or i32 %405, %375
  br label %407

; <label>:407:                                    ; preds = %396, %393, %34
  %408 = phi i32 [ %371, %34 ], [ %394, %393 ], [ %392, %396 ]
  %409 = phi i32 [ %375, %34 ], [ %395, %393 ], [ %406, %396 ]
  %410 = and i32 %408, -97
  %411 = or i32 %410, %409
  store i32 %411, i32* %370, align 4
  %412 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %413 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %412, i64 0, i32 39
  %414 = load i64, i64* %413, align 16
  %415 = add i64 %414, 1040
  %416 = tail call i32 @lduw_phys(i64 %415)
  %417 = add i64 %414, 1048
  %418 = tail call i64 @ldq_phys(i64 %417)
  %419 = trunc i64 %418 to i32
  %420 = add i64 %414, 1044
  %421 = tail call i32 @ldl_phys(i64 %420)
  %422 = add i64 %414, 1042
  %423 = tail call i32 @lduw_phys(i64 %422)
  %424 = shl i32 %423, 8
  %425 = and i32 %424, 65280
  %426 = shl i32 %423, 12
  %427 = and i32 %426, 15728640
  %428 = or i32 %425, %427
  %429 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %412, i64 0, i32 11, i64 1, i32 0
  store i32 %416, i32* %429, align 4
  %430 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %412, i64 0, i32 11, i64 1, i32 1
  store i32 %419, i32* %430, align 4
  %431 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %412, i64 0, i32 11, i64 1, i32 2
  store i32 %421, i32* %431, align 4
  %432 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %412, i64 0, i32 11, i64 1, i32 3
  store i32 %428, i32* %432, align 4
  %433 = lshr exact i32 %427, 18
  %434 = and i32 %433, 16
  %435 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %412, i64 0, i32 8
  %436 = load i32, i32* %435, align 4
  %437 = and i32 %436, -32785
  %438 = or i32 %437, %434
  store i32 %438, i32* %435, align 4
  %439 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %412, i64 0, i32 11, i64 2, i32 3
  %440 = load i32, i32* %439, align 4
  %441 = lshr i32 %440, 17
  %442 = and i32 %441, 32
  %443 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %412, i64 0, i32 16, i64 0
  %444 = load i32, i32* %443, align 8
  %445 = and i32 %444, 1
  %446 = icmp eq i32 %445, 0
  br i1 %446, label %457, label %447

; <label>:447:                                    ; preds = %407
  %448 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %412, i64 0, i32 7
  %449 = bitcast i32* %448 to i64*
  %450 = load i64, i64* %449, align 8
  %451 = and i64 %450, 131072
  %452 = icmp ne i64 %451, 0
  %453 = icmp eq i32 %434, 0
  %454 = or i1 %453, %452
  %455 = lshr i64 %450, 32
  %456 = trunc i64 %455 to i32
  br i1 %454, label %457, label %460

; <label>:457:                                    ; preds = %447, %407
  %458 = phi i32 [ %456, %447 ], [ %438, %407 ]
  %459 = or i32 %442, 64
  br label %473

; <label>:460:                                    ; preds = %447
  %461 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %412, i64 0, i32 11, i64 3, i32 1
  %462 = load i32, i32* %461, align 4
  %463 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %412, i64 0, i32 11, i64 0, i32 1
  %464 = load i32, i32* %463, align 4
  %465 = or i32 %464, %462
  %466 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %412, i64 0, i32 11, i64 2, i32 1
  %467 = load i32, i32* %466, align 4
  %468 = or i32 %465, %467
  %469 = icmp ne i32 %468, 0
  %470 = zext i1 %469 to i32
  %471 = shl nuw nsw i32 %470, 6
  %472 = or i32 %471, %442
  br label %473

; <label>:473:                                    ; preds = %457, %460
  %474 = phi i32 [ %458, %457 ], [ %456, %460 ]
  %475 = phi i32 [ %459, %457 ], [ %472, %460 ]
  %476 = and i32 %474, -97
  %477 = or i32 %476, %475
  store i32 %477, i32* %435, align 4
  %478 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %479 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %478, i64 0, i32 39
  %480 = load i64, i64* %479, align 16
  %481 = add i64 %480, 1056
  %482 = tail call i32 @lduw_phys(i64 %481)
  %483 = add i64 %480, 1064
  %484 = tail call i64 @ldq_phys(i64 %483)
  %485 = trunc i64 %484 to i32
  %486 = add i64 %480, 1060
  %487 = tail call i32 @ldl_phys(i64 %486)
  %488 = add i64 %480, 1058
  %489 = tail call i32 @lduw_phys(i64 %488)
  %490 = shl i32 %489, 8
  %491 = and i32 %490, 65280
  %492 = shl i32 %489, 12
  %493 = and i32 %492, 15728640
  %494 = or i32 %491, %493
  %495 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %478, i64 0, i32 11, i64 2, i32 0
  store i32 %482, i32* %495, align 4
  %496 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %478, i64 0, i32 11, i64 2, i32 1
  store i32 %485, i32* %496, align 4
  %497 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %478, i64 0, i32 11, i64 2, i32 2
  store i32 %487, i32* %497, align 4
  %498 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %478, i64 0, i32 11, i64 2, i32 3
  store i32 %494, i32* %498, align 4
  %499 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %478, i64 0, i32 8
  %500 = load i32, i32* %499, align 4
  %501 = lshr exact i32 %493, 17
  %502 = and i32 %501, 32
  %503 = trunc i32 %500 to i16
  %504 = icmp slt i16 %503, 0
  br i1 %504, label %535, label %505

; <label>:505:                                    ; preds = %473
  %506 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %478, i64 0, i32 16, i64 0
  %507 = load i32, i32* %506, align 8
  %508 = and i32 %507, 1
  %509 = icmp eq i32 %508, 0
  br i1 %509, label %521, label %510

; <label>:510:                                    ; preds = %505
  %511 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %478, i64 0, i32 7
  %512 = bitcast i32* %511 to i64*
  %513 = load i64, i64* %512, align 8
  %514 = and i64 %513, 131072
  %515 = icmp ne i64 %514, 0
  %516 = and i32 %500, 16
  %517 = icmp eq i32 %516, 0
  %518 = or i1 %517, %515
  %519 = lshr i64 %513, 32
  %520 = trunc i64 %519 to i32
  br i1 %518, label %521, label %524

; <label>:521:                                    ; preds = %510, %505
  %522 = phi i32 [ %520, %510 ], [ %500, %505 ]
  %523 = or i32 %502, 64
  br label %535

; <label>:524:                                    ; preds = %510
  %525 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %478, i64 0, i32 11, i64 3, i32 1
  %526 = load i32, i32* %525, align 4
  %527 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %478, i64 0, i32 11, i64 0, i32 1
  %528 = load i32, i32* %527, align 4
  %529 = or i32 %528, %526
  %530 = or i32 %529, %485
  %531 = icmp ne i32 %530, 0
  %532 = zext i1 %531 to i32
  %533 = shl nuw nsw i32 %532, 6
  %534 = or i32 %533, %502
  br label %535

; <label>:535:                                    ; preds = %473, %521, %524
  %536 = phi i32 [ %500, %473 ], [ %522, %521 ], [ %520, %524 ]
  %537 = phi i32 [ %502, %473 ], [ %523, %521 ], [ %534, %524 ]
  %538 = and i32 %536, -97
  %539 = or i32 %538, %537
  store i32 %539, i32* %499, align 4
  %540 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %541 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %540, i64 0, i32 39
  %542 = load i64, i64* %541, align 16
  %543 = add i64 %542, 1072
  %544 = tail call i32 @lduw_phys(i64 %543)
  %545 = add i64 %542, 1080
  %546 = tail call i64 @ldq_phys(i64 %545)
  %547 = trunc i64 %546 to i32
  %548 = add i64 %542, 1076
  %549 = tail call i32 @ldl_phys(i64 %548)
  %550 = add i64 %542, 1074
  %551 = tail call i32 @lduw_phys(i64 %550)
  %552 = shl i32 %551, 8
  %553 = and i32 %552, 65280
  %554 = shl i32 %551, 12
  %555 = and i32 %554, 15728640
  %556 = or i32 %553, %555
  %557 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %540, i64 0, i32 11, i64 3, i32 0
  store i32 %544, i32* %557, align 4
  %558 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %540, i64 0, i32 11, i64 3, i32 1
  store i32 %547, i32* %558, align 4
  %559 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %540, i64 0, i32 11, i64 3, i32 2
  store i32 %549, i32* %559, align 4
  %560 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %540, i64 0, i32 11, i64 3, i32 3
  store i32 %556, i32* %560, align 4
  %561 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %540, i64 0, i32 8
  %562 = load i32, i32* %561, align 4
  %563 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %540, i64 0, i32 11, i64 2, i32 3
  %564 = load i32, i32* %563, align 4
  %565 = lshr i32 %564, 17
  %566 = and i32 %565, 32
  %567 = trunc i32 %562 to i16
  %568 = icmp slt i16 %567, 0
  br i1 %568, label %599, label %569

; <label>:569:                                    ; preds = %535
  %570 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %540, i64 0, i32 16, i64 0
  %571 = load i32, i32* %570, align 8
  %572 = and i32 %571, 1
  %573 = icmp eq i32 %572, 0
  br i1 %573, label %585, label %574

; <label>:574:                                    ; preds = %569
  %575 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %540, i64 0, i32 7
  %576 = bitcast i32* %575 to i64*
  %577 = load i64, i64* %576, align 8
  %578 = and i64 %577, 131072
  %579 = icmp ne i64 %578, 0
  %580 = and i32 %562, 16
  %581 = icmp eq i32 %580, 0
  %582 = or i1 %581, %579
  %583 = lshr i64 %577, 32
  %584 = trunc i64 %583 to i32
  br i1 %582, label %585, label %588

; <label>:585:                                    ; preds = %574, %569
  %586 = phi i32 [ %584, %574 ], [ %562, %569 ]
  %587 = or i32 %566, 64
  br label %599

; <label>:588:                                    ; preds = %574
  %589 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %540, i64 0, i32 11, i64 0, i32 1
  %590 = load i32, i32* %589, align 4
  %591 = or i32 %590, %547
  %592 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %540, i64 0, i32 11, i64 2, i32 1
  %593 = load i32, i32* %592, align 4
  %594 = or i32 %591, %593
  %595 = icmp ne i32 %594, 0
  %596 = zext i1 %595 to i32
  %597 = shl nuw nsw i32 %596, 6
  %598 = or i32 %597, %566
  br label %599

; <label>:599:                                    ; preds = %535, %585, %588
  %600 = phi i32 [ %562, %535 ], [ %586, %585 ], [ %584, %588 ]
  %601 = phi i32 [ %566, %535 ], [ %587, %585 ], [ %598, %588 ]
  %602 = and i32 %600, -97
  %603 = or i32 %602, %601
  store i32 %603, i32* %561, align 4
  %604 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %605 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %604, i64 0, i32 39
  %606 = load i64, i64* %605, align 16
  %607 = add i64 %606, 1400
  %608 = tail call i64 @ldq_phys(i64 %607)
  %609 = trunc i64 %608 to i32
  %610 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %611 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %610, i64 0, i32 5
  store i32 %609, i32* %611, align 16
  %612 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %610, i64 0, i32 39
  %613 = load i64, i64* %612, align 16
  %614 = add i64 %613, 1496
  %615 = tail call i64 @ldq_phys(i64 %614)
  %616 = trunc i64 %615 to i32
  %617 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %618 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %617, i64 0, i32 0, i64 4
  store i32 %616, i32* %618, align 16
  %619 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %617, i64 0, i32 39
  %620 = load i64, i64* %619, align 16
  %621 = add i64 %620, 1528
  %622 = tail call i64 @ldq_phys(i64 %621)
  %623 = trunc i64 %622 to i32
  %624 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %625 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %624, i64 0, i32 0, i64 0
  store i32 %623, i32* %625, align 16
  %626 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %627 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %626, i64 0, i32 39
  %628 = load i64, i64* %627, align 16
  %629 = add i64 %628, 1384
  %630 = tail call i64 @ldq_phys(i64 %629)
  %631 = trunc i64 %630 to i32
  %632 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %633 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %632, i64 0, i32 61, i64 6
  store i32 %631, i32* %633, align 4
  %634 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %632, i64 0, i32 39
  %635 = load i64, i64* %634, align 16
  %636 = add i64 %635, 1376
  %637 = tail call i64 @ldq_phys(i64 %636)
  %638 = trunc i64 %637 to i32
  %639 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %640 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %639, i64 0, i32 61, i64 7
  store i32 %638, i32* %640, align 4
  %641 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %642 = load i32, i32* %641, align 4
  %643 = icmp eq i32 %642, 0
  br i1 %643, label %644, label %646, !prof !2

; <label>:644:                                    ; preds = %599
  %645 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %639, i64 0, i32 8
  br label %652

; <label>:646:                                    ; preds = %599
  %647 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  %648 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %639, i64 0, i32 8
  %649 = load i32, i32* %648, align 4
  %650 = and i32 %649, 3
  tail call void %647(i32 %650, i32 0)
  %651 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %652

; <label>:652:                                    ; preds = %644, %646
  %653 = phi %struct.CPUX86State* [ %639, %644 ], [ %651, %646 ]
  %654 = phi i32* [ %645, %644 ], [ %648, %646 ]
  %655 = load i32, i32* %654, align 4
  %656 = and i32 %655, -4
  store i32 %656, i32* %654, align 4
  %657 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %653, i64 0, i32 40
  %658 = load i64, i64* %657, align 8
  %659 = add i64 %658, 112
  %660 = zext i32 %0 to i64
  tail call void @stq_phys(i64 %659, i64 %660)
  %661 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %662 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %661, i64 0, i32 40
  %663 = load i64, i64* %662, align 8
  %664 = add i64 %663, 120
  tail call void @stq_phys(i64 %664, i64 %1)
  %665 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %666 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %665, i64 0, i32 40
  %667 = load i64, i64* %666, align 8
  %668 = add i64 %667, 136
  %669 = add i64 %667, 168
  %670 = tail call i32 @ldl_phys(i64 %669)
  tail call void @stl_phys(i64 %668, i32 %670)
  %671 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %672 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %671, i64 0, i32 40
  %673 = load i64, i64* %672, align 8
  %674 = add i64 %673, 140
  %675 = add i64 %673, 172
  %676 = tail call i32 @ldl_phys(i64 %675)
  tail call void @stl_phys(i64 %674, i32 %676)
  %677 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %678 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %677, i64 0, i32 40
  %679 = load i64, i64* %678, align 8
  %680 = add i64 %679, 168
  tail call void @stl_phys(i64 %680, i32 0)
  %681 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %682 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %681, i64 0, i32 9
  %683 = load i32, i32* %682, align 16
  %684 = and i32 %683, -2
  store i32 %684, i32* %682, align 16
  %685 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %681, i64 0, i32 16, i64 0
  %686 = load i32, i32* %685, align 8
  %687 = or i32 %686, 1
  store i32 %687, i32* %685, align 8
  %688 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %681, i64 0, i32 7
  %689 = load i32, i32* %688, align 8
  %690 = and i32 %689, -131073
  store i32 %690, i32* %688, align 8
  %691 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %681, i64 0, i32 92
  store i32 -1, i32* %691, align 8
  %692 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %681, i64 0, i32 58
  store i32 0, i32* %692, align 16
  %693 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %681, i64 0, i32 64
  store i32 -1, i32* %693, align 4
  %694 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %695 = load i32, i32* %694, align 4
  %696 = icmp eq i32 %695, 0
  br i1 %696, label %700, label %697, !prof !2

; <label>:697:                                    ; preds = %652
  %698 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  tail call void %698(i32 %21, i32 0)
  %699 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %700

; <label>:700:                                    ; preds = %652, %697
  %701 = phi %struct.CPUX86State* [ %681, %652 ], [ %699, %697 ]
  tail call void @cpu_loop_exit(%struct.CPUX86State* %701) #12
  unreachable
}

; Function Attrs: nounwind
declare i64 @fwrite(i8* nocapture, i64, i64, %struct._IO_FILE* nocapture) #5

declare void @libcpu_system_reset_request() local_unnamed_addr #6

; Function Attrs: noreturn
declare void @cpu_loop_exit(%struct.CPUX86State*) local_unnamed_addr #7

declare i64 @ldq_phys(i64) local_unnamed_addr #6

declare void @stl_phys(i64, i32) local_unnamed_addr #6

declare void @stw_phys(i64, i32) local_unnamed_addr #6

declare void @stq_phys(i64, i64) local_unnamed_addr #6

declare i32 @ldl_phys(i64) local_unnamed_addr #6

declare void @stb_phys(i64, i32) local_unnamed_addr #6

declare void @cpu_x86_update_cr0(%struct.CPUX86State*, i32) local_unnamed_addr #6

declare void @cpu_x86_update_cr4(%struct.CPUX86State*, i32) local_unnamed_addr #6

declare void @cpu_x86_update_cr3(%struct.CPUX86State*, i32) local_unnamed_addr #6

declare i32 @lduw_phys(i64) local_unnamed_addr #6

declare i32 @ldub_phys(i64) local_unnamed_addr #6

declare void @tcg_llvm_before_memory_access(i32, i64, i32, i32) local_unnamed_addr #6

declare i32 @tcg_llvm_fork_and_concretize(i32, i32, i32, i32) local_unnamed_addr #6

; Function Attrs: uwtable
define zeroext i16 @__ldw_mmu(i32, i32) local_unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %7, label %6

; <label>:6:                                      ; preds = %2
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %7

; <label>:7:                                      ; preds = %2, %6
  %8 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %9 = load i32, i32* %8, align 4
  %10 = icmp eq i32 %9, 0
  br i1 %10, label %13, label %11

; <label>:11:                                     ; preds = %7
  %12 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %13

; <label>:13:                                     ; preds = %7, %11
  %14 = phi i32 [ %12, %11 ], [ %0, %7 ]
  %15 = lshr i32 %14, 12
  %16 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %15, i32 0, i32 1048575, i32 0)
  %17 = and i32 %16, 1023
  %18 = zext i32 %17 to i64
  %19 = sext i32 %1 to i64
  %20 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %21 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 %19, i64 %18
  %22 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %21, i64 0, i32 0
  %23 = load i32, i32* %22, align 8
  %24 = and i32 %14, -4096
  %25 = and i32 %23, -4088
  %26 = icmp eq i32 %24, %25
  br i1 %26, label %29, label %27, !prof !2

; <label>:27:                                     ; preds = %13
  %28 = shl i32 %16, 12
  br label %86

; <label>:29:                                     ; preds = %86, %13
  %30 = phi %struct.CPUX86State* [ %20, %13 ], [ %88, %86 ]
  %31 = phi %struct.CPUTLBEntry* [ %21, %13 ], [ %89, %86 ]
  %32 = phi i32 [ %23, %13 ], [ %91, %86 ]
  %33 = and i32 %32, 4095
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %68, label %35, !prof !2

; <label>:35:                                     ; preds = %29
  %36 = and i32 %14, 1
  %37 = icmp eq i32 %36, 0
  br i1 %37, label %38, label %72

; <label>:38:                                     ; preds = %35
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 78, i64 %19, i64 %18
  %40 = load i64, i64* %39, align 8
  %41 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 83
  store %struct.CPUTLBEntry* %31, %struct.CPUTLBEntry** %41, align 16
  %42 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %40)
  %43 = and i64 %40, 4294963200
  %44 = zext i32 %14 to i64
  %45 = add nuw nsw i64 %43, %44
  %46 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %47 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %46, i64 0, i32 72
  store i64 0, i64* %47, align 16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %44, i32 0)
  %48 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %42, i32 0)
  br i1 %48, label %49, label %56

; <label>:49:                                     ; preds = %38
  %50 = and i64 %45, 4294963200
  %51 = tail call i64 @se_notdirty_mem_read(i64 %50)
  %52 = and i64 %45, 4095
  %53 = or i64 %51, %52
  %54 = inttoptr i64 %53 to i16*
  %55 = load i16, i16* %54, align 2
  br label %58

; <label>:56:                                     ; preds = %38
  %57 = tail call zeroext i16 @io_readw_mmu(i64 %40, i32 %14, i8* null)
  br label %58

; <label>:58:                                     ; preds = %49, %56
  %59 = phi i16 [ %55, %49 ], [ %57, %56 ]
  %60 = zext i16 %59 to i64
  %61 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %44, i64 %60, i32 2, i32 0)
  %62 = trunc i64 %61 to i16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %63 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %64 = load i32, i32* %63, align 4
  %65 = icmp eq i32 %64, 0
  br i1 %65, label %94, label %66

; <label>:66:                                     ; preds = %58
  %67 = and i64 %61, 65535
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %67, i32 2, i32 1, i64 0)
  br label %94

; <label>:68:                                     ; preds = %29
  %69 = and i32 %14, 4095
  %70 = add nuw nsw i32 %69, 1
  %71 = icmp ugt i32 %70, 4095
  br i1 %71, label %72, label %74, !prof !3

; <label>:72:                                     ; preds = %35, %68
  %73 = tail call fastcc zeroext i16 @slow_ldw_mmu(i32 %14, i32 %1)
  br label %94

; <label>:74:                                     ; preds = %68
  %75 = zext i32 %14 to i64
  %76 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 77, i64 %19, i64 %18, i32 4
  %77 = load i64, i64* %76, align 8
  %78 = add i64 %77, %75
  %79 = inttoptr i64 %78 to i16*
  %80 = load i16, i16* %79, align 2
  %81 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %82 = load i32, i32* %81, align 4
  %83 = icmp eq i32 %82, 0
  br i1 %83, label %94, label %84

; <label>:84:                                     ; preds = %74
  %85 = zext i16 %80 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %85, i32 2, i32 0, i64 0)
  br label %94

; <label>:86:                                     ; preds = %27, %86
  %87 = phi %struct.CPUX86State* [ %20, %27 ], [ %88, %86 ]
  tail call void @tlb_fill(%struct.CPUX86State* %87, i32 %14, i32 %28, i32 0, i32 %1, i8* null)
  %88 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %89 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 77, i64 %19, i64 %18
  %90 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %89, i64 0, i32 0
  %91 = load i32, i32* %90, align 8
  %92 = and i32 %91, -4088
  %93 = icmp eq i32 %24, %92
  br i1 %93, label %29, label %86, !prof !2

; <label>:94:                                     ; preds = %58, %74, %66, %84, %72
  %95 = phi i16 [ %73, %72 ], [ %62, %66 ], [ %62, %58 ], [ %80, %84 ], [ %80, %74 ]
  ret i16 %95
}

declare void @tcg_llvm_after_memory_access(i32, i64, i32, i32, i64) local_unnamed_addr #6

declare %struct.MemoryDescOps* @phys_get_ops(i64) local_unnamed_addr #6

declare void @tcg_llvm_write_mem_io_vaddr(i64, i32) local_unnamed_addr #6

declare zeroext i1 @se_ismemfunc(%struct.MemoryDescOps*, i32) local_unnamed_addr #6

declare i64 @se_notdirty_mem_read(i64) local_unnamed_addr #6

declare zeroext i16 @io_readw_mmu(i64, i32, i8*) local_unnamed_addr #6

declare i64 @tcg_llvm_trace_mmio_access(i64, i64, i32, i32) local_unnamed_addr #6

; Function Attrs: uwtable
define internal fastcc zeroext i16 @slow_ldw_mmu(i32, i32) unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %7, label %6

; <label>:6:                                      ; preds = %2
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %7

; <label>:7:                                      ; preds = %2, %6
  %8 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %9 = load i32, i32* %8, align 4
  %10 = icmp eq i32 %9, 0
  br i1 %10, label %13, label %11

; <label>:11:                                     ; preds = %7
  %12 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %13

; <label>:13:                                     ; preds = %7, %11
  %14 = phi i32 [ %12, %11 ], [ %0, %7 ]
  %15 = lshr i32 %14, 12
  %16 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %15, i32 0, i32 1048575, i32 0)
  %17 = and i32 %16, 1023
  %18 = zext i32 %17 to i64
  %19 = sext i32 %1 to i64
  %20 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %21 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 %19, i64 %18
  %22 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %21, i64 0, i32 0
  %23 = load i32, i32* %22, align 8
  %24 = and i32 %14, -4096
  %25 = and i32 %23, -4088
  %26 = icmp eq i32 %24, %25
  br i1 %26, label %29, label %27

; <label>:27:                                     ; preds = %13
  %28 = shl i32 %16, 12
  br label %98

; <label>:29:                                     ; preds = %98, %13
  %30 = phi %struct.CPUX86State* [ %20, %13 ], [ %100, %98 ]
  %31 = phi %struct.CPUTLBEntry* [ %21, %13 ], [ %101, %98 ]
  %32 = phi i32 [ %23, %13 ], [ %103, %98 ]
  %33 = and i32 %32, 4095
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %68, label %35

; <label>:35:                                     ; preds = %29
  %36 = and i32 %14, 1
  %37 = icmp eq i32 %36, 0
  br i1 %37, label %38, label %72

; <label>:38:                                     ; preds = %35
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 78, i64 %19, i64 %18
  %40 = load i64, i64* %39, align 8
  %41 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 83
  store %struct.CPUTLBEntry* %31, %struct.CPUTLBEntry** %41, align 16
  %42 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %40)
  %43 = and i64 %40, 4294963200
  %44 = zext i32 %14 to i64
  %45 = add nuw nsw i64 %43, %44
  %46 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %47 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %46, i64 0, i32 72
  store i64 0, i64* %47, align 16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %44, i32 0)
  %48 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %42, i32 0)
  br i1 %48, label %49, label %56

; <label>:49:                                     ; preds = %38
  %50 = and i64 %45, 4294963200
  %51 = tail call i64 @se_notdirty_mem_read(i64 %50)
  %52 = and i64 %45, 4095
  %53 = or i64 %51, %52
  %54 = inttoptr i64 %53 to i16*
  %55 = load i16, i16* %54, align 2
  br label %58

; <label>:56:                                     ; preds = %38
  %57 = tail call zeroext i16 @io_readw_mmu(i64 %40, i32 %14, i8* null)
  br label %58

; <label>:58:                                     ; preds = %49, %56
  %59 = phi i16 [ %55, %49 ], [ %57, %56 ]
  %60 = zext i16 %59 to i64
  %61 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %44, i64 %60, i32 2, i32 0)
  %62 = trunc i64 %61 to i16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %63 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %64 = load i32, i32* %63, align 4
  %65 = icmp eq i32 %64, 0
  br i1 %65, label %106, label %66

; <label>:66:                                     ; preds = %58
  %67 = and i64 %61, 65535
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %67, i32 2, i32 1, i64 0)
  br label %106

; <label>:68:                                     ; preds = %29
  %69 = and i32 %14, 4095
  %70 = add nuw nsw i32 %69, 1
  %71 = icmp ugt i32 %70, 4095
  br i1 %71, label %72, label %86

; <label>:72:                                     ; preds = %35, %68
  %73 = and i32 %14, -2
  %74 = add i32 %73, 2
  %75 = tail call fastcc zeroext i16 @slow_ldw_mmu(i32 %73, i32 %1)
  %76 = tail call fastcc zeroext i16 @slow_ldw_mmu(i32 %74, i32 %1)
  %77 = shl i32 %14, 3
  %78 = and i32 %77, 8
  %79 = zext i16 %75 to i32
  %80 = lshr i32 %79, %78
  %81 = zext i16 %76 to i32
  %82 = sub nsw i32 16, %78
  %83 = shl i32 %81, %82
  %84 = or i32 %83, %80
  %85 = trunc i32 %84 to i16
  ret i16 %85

; <label>:86:                                     ; preds = %68
  %87 = zext i32 %14 to i64
  %88 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 77, i64 %19, i64 %18, i32 4
  %89 = load i64, i64* %88, align 8
  %90 = add i64 %89, %87
  %91 = inttoptr i64 %90 to i16*
  %92 = load i16, i16* %91, align 2
  %93 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %94 = load i32, i32* %93, align 4
  %95 = icmp eq i32 %94, 0
  br i1 %95, label %106, label %96

; <label>:96:                                     ; preds = %86
  %97 = zext i16 %92 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %97, i32 2, i32 0, i64 0)
  br label %106

; <label>:98:                                     ; preds = %27, %98
  %99 = phi %struct.CPUX86State* [ %20, %27 ], [ %100, %98 ]
  tail call void @tlb_fill(%struct.CPUX86State* %99, i32 %14, i32 %28, i32 0, i32 %1, i8* null)
  %100 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %101 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %100, i64 0, i32 77, i64 %19, i64 %18
  %102 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %101, i64 0, i32 0
  %103 = load i32, i32* %102, align 8
  %104 = and i32 %103, -4088
  %105 = icmp eq i32 %24, %104
  br i1 %105, label %29, label %98

; <label>:106:                                    ; preds = %58, %86, %66, %96
  %107 = phi i16 [ %62, %66 ], [ %62, %58 ], [ %92, %96 ], [ %92, %86 ]
  ret i16 %107
}

; Function Attrs: uwtable
define void @tlb_fill(%struct.CPUX86State*, i32, i32, i32, i32, i8*) local_unnamed_addr #2 {
  %7 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 14), align 8
  %8 = load i32, i32* %7, align 4
  %9 = icmp eq i32 %8, 0
  br i1 %9, label %13, label %10, !prof !2

; <label>:10:                                     ; preds = %6
  %11 = load void (i64, i32, i8*)*, void (i64, i32, i8*)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 22), align 8
  %12 = zext i32 %1 to i64
  tail call void %11(i64 %12, i32 %3, i8* %5)
  br label %13

; <label>:13:                                     ; preds = %6, %10
  %14 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %15 = tail call i32 @cpu_x86_handle_mmu_fault(%struct.CPUX86State* %14, i32 %2, i32 %3, i32 %4)
  %16 = icmp eq i32 %15, 0
  br i1 %16, label %55, label %17

; <label>:17:                                     ; preds = %13
  %18 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %19 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 92
  %20 = load i32, i32* %19, align 8
  %21 = icmp eq i32 %20, 14
  br i1 %21, label %22, label %34

; <label>:22:                                     ; preds = %17
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 47
  %24 = load i32, i32* %23, align 8
  %25 = and i32 %24, 16384
  %26 = icmp eq i32 %25, 0
  br i1 %26, label %32, label %27

; <label>:27:                                     ; preds = %22
  %28 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 40
  %29 = load i64, i64* %28, align 8
  %30 = add i64 %29, 128
  %31 = zext i32 %1 to i64
  tail call void @stq_phys(i64 %30, i64 %31)
  br label %34

; <label>:32:                                     ; preds = %22
  %33 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 16, i64 2
  store i32 %1, i32* %33, align 8
  br label %34

; <label>:34:                                     ; preds = %27, %32, %17
  %35 = icmp eq i8* %5, null
  br i1 %35, label %42, label %36

; <label>:36:                                     ; preds = %34
  %37 = ptrtoint i8* %5 to i64
  %38 = tail call %struct.TranslationBlock* @tb_find_pc(i64 %37)
  %39 = icmp eq %struct.TranslationBlock* %38, null
  br i1 %39, label %42, label %40

; <label>:40:                                     ; preds = %36
  %41 = tail call i32 @cpu_restore_state(%struct.TranslationBlock* nonnull %38, %struct.CPUX86State* %0, i64 %37)
  br label %42

; <label>:42:                                     ; preds = %36, %34, %40
  %43 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 13), align 8
  %44 = load i32, i32* %43, align 4
  %45 = icmp eq i32 %44, 0
  br i1 %45, label %49, label %46, !prof !2

; <label>:46:                                     ; preds = %42
  %47 = load void (i64, i32, i8*)*, void (i64, i32, i8*)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 21), align 8
  %48 = zext i32 %1 to i64
  tail call void %47(i64 %48, i32 %3, i8* %5)
  br label %49

; <label>:49:                                     ; preds = %42, %46
  %50 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %51 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %50, i64 0, i32 92
  %52 = load i32, i32* %51, align 8
  %53 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %50, i64 0, i32 58
  %54 = load i32, i32* %53, align 16
  tail call fastcc void @raise_exception_err(i32 %52, i32 %54) #12
  unreachable

; <label>:55:                                     ; preds = %13
  ret void
}

declare i32 @cpu_x86_handle_mmu_fault(%struct.CPUX86State*, i32, i32, i32) local_unnamed_addr #6

declare %struct.TranslationBlock* @tb_find_pc(i64) local_unnamed_addr #6

declare i32 @cpu_restore_state(%struct.TranslationBlock*, %struct.CPUX86State*, i64) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_check_iow(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 13, i32 3
  %4 = load i32, i32* %3, align 4
  %5 = trunc i32 %4 to i16
  %6 = icmp slt i16 %5, 0
  %7 = and i32 %4, 3840
  %8 = icmp eq i32 %7, 2304
  %9 = and i1 %6, %8
  br i1 %9, label %10, label %35

; <label>:10:                                     ; preds = %1
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 13, i32 2
  %12 = load i32, i32* %11, align 8
  %13 = icmp ult i32 %12, 103
  br i1 %13, label %35, label %14

; <label>:14:                                     ; preds = %10
  %15 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 13, i32 1
  %16 = load i32, i32* %15, align 4
  %17 = add i32 %16, 102
  %18 = tail call fastcc i32 @lduw_kernel(i32 %17)
  %19 = ashr i32 %0, 3
  %20 = add nsw i32 %18, %19
  %21 = add nsw i32 %20, 1
  %22 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 13, i32 2
  %24 = load i32, i32* %23, align 8
  %25 = icmp ugt i32 %21, %24
  br i1 %25, label %35, label %26

; <label>:26:                                     ; preds = %14
  %27 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 13, i32 1
  %28 = load i32, i32* %27, align 4
  %29 = add i32 %28, %20
  %30 = tail call fastcc i32 @lduw_kernel(i32 %29)
  %31 = and i32 %0, 7
  %32 = ashr i32 %30, %31
  %33 = and i32 %32, 3
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %36, label %35

; <label>:35:                                     ; preds = %26, %14, %10, %1
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:36:                                     ; preds = %26
  ret void
}

; Function Attrs: uwtable
define void @helper_check_iol(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 13, i32 3
  %4 = load i32, i32* %3, align 4
  %5 = trunc i32 %4 to i16
  %6 = icmp slt i16 %5, 0
  %7 = and i32 %4, 3840
  %8 = icmp eq i32 %7, 2304
  %9 = and i1 %6, %8
  br i1 %9, label %10, label %35

; <label>:10:                                     ; preds = %1
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 13, i32 2
  %12 = load i32, i32* %11, align 8
  %13 = icmp ult i32 %12, 103
  br i1 %13, label %35, label %14

; <label>:14:                                     ; preds = %10
  %15 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 13, i32 1
  %16 = load i32, i32* %15, align 4
  %17 = add i32 %16, 102
  %18 = tail call fastcc i32 @lduw_kernel(i32 %17)
  %19 = ashr i32 %0, 3
  %20 = add nsw i32 %18, %19
  %21 = add nsw i32 %20, 1
  %22 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 13, i32 2
  %24 = load i32, i32* %23, align 8
  %25 = icmp ugt i32 %21, %24
  br i1 %25, label %35, label %26

; <label>:26:                                     ; preds = %14
  %27 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 13, i32 1
  %28 = load i32, i32* %27, align 4
  %29 = add i32 %28, %20
  %30 = tail call fastcc i32 @lduw_kernel(i32 %29)
  %31 = and i32 %0, 7
  %32 = ashr i32 %30, %31
  %33 = and i32 %32, 15
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %36, label %35

; <label>:35:                                     ; preds = %26, %14, %10, %1
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:36:                                     ; preds = %26
  ret void
}

; Function Attrs: uwtable
define void @helper_outb(i32, i32) local_unnamed_addr #2 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  store i32 %0, i32* %3, align 4
  store i32 %1, i32* %4, align 4
  %5 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 7), align 8
  %6 = load i32, i32* %5, align 4
  %7 = icmp eq i32 %6, 0
  br i1 %7, label %12, label %8

; <label>:8:                                      ; preds = %2
  %9 = bitcast i32* %3 to i8*
  call void @tcg_llvm_get_value(i8* %9, i32 4, i1 zeroext false)
  %10 = load i32, i32* %3, align 4
  %11 = load i32, i32* %4, align 4
  br label %12

; <label>:12:                                     ; preds = %2, %8
  %13 = phi i32 [ %1, %2 ], [ %11, %8 ]
  %14 = phi i32 [ %0, %2 ], [ %10, %8 ]
  %15 = zext i32 %14 to i64
  %16 = zext i32 %13 to i64
  %17 = call i64 @tcg_llvm_trace_port_access(i64 %15, i64 %16, i32 8, i32 1)
  %18 = icmp eq i64 %17, 0
  br i1 %18, label %31, label %19

; <label>:19:                                     ; preds = %12
  %20 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 6), align 8
  %21 = load i32, i32* %20, align 4
  %22 = icmp eq i32 %21, 0
  br i1 %22, label %27, label %23

; <label>:23:                                     ; preds = %19
  %24 = and i32 %13, 255
  store i32 %24, i32* %4, align 4
  %25 = bitcast i32* %4 to i8*
  call void @tcg_llvm_get_value(i8* %25, i32 4, i1 zeroext false)
  %26 = load i32, i32* %4, align 4
  br label %27

; <label>:27:                                     ; preds = %19, %23
  %28 = phi i32 [ %13, %19 ], [ %26, %23 ]
  %29 = load i32, i32* %3, align 4
  %30 = trunc i32 %28 to i8
  call void @cpu_outb(i32 %29, i8 zeroext %30)
  br label %31

; <label>:31:                                     ; preds = %12, %27
  ret void
}

declare void @tcg_llvm_get_value(i8*, i32, i1 zeroext) local_unnamed_addr #6

declare i64 @tcg_llvm_trace_port_access(i64, i64, i32, i32) local_unnamed_addr #6

declare void @cpu_outb(i32, i8 zeroext) local_unnamed_addr #6

; Function Attrs: uwtable
define i32 @helper_inb(i32) local_unnamed_addr #2 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 7), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %9, label %6

; <label>:6:                                      ; preds = %1
  %7 = bitcast i32* %2 to i8*
  call void @tcg_llvm_get_value(i8* %7, i32 4, i1 zeroext false)
  %8 = load i32, i32* %2, align 4
  br label %9

; <label>:9:                                      ; preds = %1, %6
  %10 = phi i32 [ %0, %1 ], [ %8, %6 ]
  %11 = call zeroext i8 @cpu_inb(i32 %10)
  %12 = load i32, i32* %2, align 4
  %13 = zext i32 %12 to i64
  %14 = zext i8 %11 to i64
  %15 = call i64 @tcg_llvm_trace_port_access(i64 %13, i64 %14, i32 8, i32 0)
  %16 = trunc i64 %15 to i32
  ret i32 %16
}

declare zeroext i8 @cpu_inb(i32) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_outw(i32, i32) local_unnamed_addr #2 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  store i32 %0, i32* %3, align 4
  store i32 %1, i32* %4, align 4
  %5 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 7), align 8
  %6 = load i32, i32* %5, align 4
  %7 = icmp eq i32 %6, 0
  br i1 %7, label %12, label %8

; <label>:8:                                      ; preds = %2
  %9 = bitcast i32* %3 to i8*
  call void @tcg_llvm_get_value(i8* %9, i32 4, i1 zeroext false)
  %10 = load i32, i32* %3, align 4
  %11 = load i32, i32* %4, align 4
  br label %12

; <label>:12:                                     ; preds = %2, %8
  %13 = phi i32 [ %1, %2 ], [ %11, %8 ]
  %14 = phi i32 [ %0, %2 ], [ %10, %8 ]
  %15 = zext i32 %14 to i64
  %16 = zext i32 %13 to i64
  %17 = call i64 @tcg_llvm_trace_port_access(i64 %15, i64 %16, i32 16, i32 1)
  %18 = icmp eq i64 %17, 0
  br i1 %18, label %31, label %19

; <label>:19:                                     ; preds = %12
  %20 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 6), align 8
  %21 = load i32, i32* %20, align 4
  %22 = icmp eq i32 %21, 0
  br i1 %22, label %27, label %23

; <label>:23:                                     ; preds = %19
  %24 = and i32 %13, 65535
  store i32 %24, i32* %4, align 4
  %25 = bitcast i32* %4 to i8*
  call void @tcg_llvm_get_value(i8* %25, i32 4, i1 zeroext false)
  %26 = load i32, i32* %4, align 4
  br label %27

; <label>:27:                                     ; preds = %19, %23
  %28 = phi i32 [ %13, %19 ], [ %26, %23 ]
  %29 = load i32, i32* %3, align 4
  %30 = trunc i32 %28 to i16
  call void @cpu_outw(i32 %29, i16 zeroext %30)
  br label %31

; <label>:31:                                     ; preds = %12, %27
  ret void
}

declare void @cpu_outw(i32, i16 zeroext) local_unnamed_addr #6

; Function Attrs: uwtable
define i32 @helper_inw(i32) local_unnamed_addr #2 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 7), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %9, label %6

; <label>:6:                                      ; preds = %1
  %7 = bitcast i32* %2 to i8*
  call void @tcg_llvm_get_value(i8* %7, i32 4, i1 zeroext false)
  %8 = load i32, i32* %2, align 4
  br label %9

; <label>:9:                                      ; preds = %1, %6
  %10 = phi i32 [ %0, %1 ], [ %8, %6 ]
  %11 = call zeroext i16 @cpu_inw(i32 %10)
  %12 = load i32, i32* %2, align 4
  %13 = zext i32 %12 to i64
  %14 = zext i16 %11 to i64
  %15 = call i64 @tcg_llvm_trace_port_access(i64 %13, i64 %14, i32 16, i32 0)
  %16 = trunc i64 %15 to i32
  ret i32 %16
}

declare zeroext i16 @cpu_inw(i32) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_outl(i32, i32) local_unnamed_addr #2 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  store i32 %0, i32* %3, align 4
  store i32 %1, i32* %4, align 4
  %5 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 7), align 8
  %6 = load i32, i32* %5, align 4
  %7 = icmp eq i32 %6, 0
  br i1 %7, label %12, label %8

; <label>:8:                                      ; preds = %2
  %9 = bitcast i32* %3 to i8*
  call void @tcg_llvm_get_value(i8* %9, i32 4, i1 zeroext false)
  %10 = load i32, i32* %3, align 4
  %11 = load i32, i32* %4, align 4
  br label %12

; <label>:12:                                     ; preds = %2, %8
  %13 = phi i32 [ %1, %2 ], [ %11, %8 ]
  %14 = phi i32 [ %0, %2 ], [ %10, %8 ]
  %15 = zext i32 %14 to i64
  %16 = zext i32 %13 to i64
  %17 = call i64 @tcg_llvm_trace_port_access(i64 %15, i64 %16, i32 32, i32 1)
  %18 = icmp eq i64 %17, 0
  br i1 %18, label %29, label %19

; <label>:19:                                     ; preds = %12
  %20 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 6), align 8
  %21 = load i32, i32* %20, align 4
  %22 = icmp eq i32 %21, 0
  br i1 %22, label %26, label %23

; <label>:23:                                     ; preds = %19
  %24 = bitcast i32* %4 to i8*
  call void @tcg_llvm_get_value(i8* %24, i32 4, i1 zeroext false)
  %25 = load i32, i32* %4, align 4
  br label %26

; <label>:26:                                     ; preds = %19, %23
  %27 = phi i32 [ %13, %19 ], [ %25, %23 ]
  %28 = load i32, i32* %3, align 4
  call void @cpu_outl(i32 %28, i32 %27)
  br label %29

; <label>:29:                                     ; preds = %12, %26
  ret void
}

declare void @cpu_outl(i32, i32) local_unnamed_addr #6

; Function Attrs: uwtable
define i32 @helper_inl(i32) local_unnamed_addr #2 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 7), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %9, label %6

; <label>:6:                                      ; preds = %1
  %7 = bitcast i32* %2 to i8*
  call void @tcg_llvm_get_value(i8* %7, i32 4, i1 zeroext false)
  %8 = load i32, i32* %2, align 4
  br label %9

; <label>:9:                                      ; preds = %1, %6
  %10 = phi i32 [ %0, %1 ], [ %8, %6 ]
  %11 = call i32 @cpu_inl(i32 %10)
  %12 = load i32, i32* %2, align 4
  %13 = zext i32 %12 to i64
  %14 = zext i32 %11 to i64
  %15 = call i64 @tcg_llvm_trace_port_access(i64 %13, i64 %14, i32 32, i32 0)
  %16 = trunc i64 %15 to i32
  ret i32 %16
}

declare i32 @cpu_inl(i32) local_unnamed_addr #6

; Function Attrs: uwtable
define void @se_do_interrupt_all(i32, i32, i32, i32, i32) local_unnamed_addr #2 {
  %6 = load i32, i32* @loglevel, align 4
  %7 = and i32 %6, 16
  %8 = icmp eq i32 %7, 0
  br i1 %8, label %62, label %9

; <label>:9:                                      ; preds = %5
  %10 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 16, i64 0
  %12 = load i32, i32* %11, align 8
  %13 = and i32 %12, 1
  %14 = icmp eq i32 %13, 0
  br i1 %14, label %62, label %15

; <label>:15:                                     ; preds = %9
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %17 = icmp eq %struct._IO_FILE* %16, null
  br i1 %17, label %38, label %18

; <label>:18:                                     ; preds = %15
  %19 = load i32, i32* @se_do_interrupt_all.count, align 4
  %20 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 8
  %21 = load i32, i32* %20, align 4
  %22 = and i32 %21, 3
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 11, i64 1, i32 0
  %24 = bitcast i32* %23 to i64*
  %25 = load i64, i64* %24, align 8
  %26 = trunc i64 %25 to i32
  %27 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 5
  %28 = load i32, i32* %27, align 16
  %29 = lshr i64 %25, 32
  %30 = trunc i64 %29 to i32
  %31 = add i32 %30, %28
  %32 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 11, i64 2, i32 0
  %33 = load i32, i32* %32, align 8
  %34 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 0, i64 4
  %35 = load i32, i32* %34, align 16
  %36 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* nonnull %16, i8* getelementptr inbounds ([65 x i8], [65 x i8]* @.str, i64 0, i64 0), i32 %19, i32 %0, i32 %2, i32 %1, i32 %22, i32 %26, i32 %28, i32 %31, i32 %33, i32 %35)
  %37 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  br label %38

; <label>:38:                                     ; preds = %15, %18
  %39 = phi %struct._IO_FILE* [ null, %15 ], [ %37, %18 ]
  %40 = icmp eq i32 %0, 14
  %41 = icmp ne %struct._IO_FILE* %39, null
  br i1 %40, label %42, label %48

; <label>:42:                                     ; preds = %38
  br i1 %41, label %43, label %54

; <label>:43:                                     ; preds = %42
  %44 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %45 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %44, i64 0, i32 16, i64 2
  %46 = load i32, i32* %45, align 8
  %47 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* nonnull %39, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.1, i64 0, i64 0), i32 %46)
  br label %54

; <label>:48:                                     ; preds = %38
  br i1 %41, label %49, label %54

; <label>:49:                                     ; preds = %48
  %50 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %51 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %50, i64 0, i32 0, i64 0
  %52 = load i32, i32* %51, align 16
  %53 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* nonnull %39, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.2, i64 0, i64 0), i32 %52)
  br label %54

; <label>:54:                                     ; preds = %42, %43, %48, %49
  %55 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %56 = icmp eq %struct._IO_FILE* %55, null
  br i1 %56, label %59, label %57

; <label>:57:                                     ; preds = %54
  %58 = tail call i32 @fputc(i32 10, %struct._IO_FILE* nonnull %55)
  br label %59

; <label>:59:                                     ; preds = %54, %57
  %60 = load i32, i32* @se_do_interrupt_all.count, align 4
  %61 = add nsw i32 %60, 1
  store i32 %61, i32* @se_do_interrupt_all.count, align 4
  br label %62

; <label>:62:                                     ; preds = %9, %5, %59
  %63 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %64 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %63, i64 0, i32 16, i64 0
  %65 = load i32, i32* %64, align 8
  %66 = and i32 %65, 1
  %67 = icmp eq i32 %66, 0
  %68 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %63, i64 0, i32 8
  %69 = load i32, i32* %68, align 4
  %70 = and i32 %69, 2097152
  %71 = icmp ne i32 %70, 0
  br i1 %67, label %820, label %72

; <label>:72:                                     ; preds = %62
  br i1 %71, label %73, label %103

; <label>:73:                                     ; preds = %72
  %74 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %63, i64 0, i32 40
  %75 = load i64, i64* %74, align 8
  %76 = add i64 %75, 168
  %77 = tail call i32 @ldl_phys(i64 %76)
  %78 = icmp slt i32 %77, 0
  br i1 %78, label %103, label %79

; <label>:79:                                     ; preds = %73
  %80 = icmp eq i32 %1, 0
  %81 = select i1 %80, i32 768, i32 1024
  %82 = or i32 %81, %0
  %83 = or i32 %82, -2147483648
  %84 = add i32 %0, -8
  %85 = icmp ult i32 %84, 10
  br i1 %85, label %86, label %97

; <label>:86:                                     ; preds = %79
  %87 = sext i32 %84 to i64
  %88 = lshr i64 386, %87
  %89 = and i64 %88, 1
  %90 = icmp eq i64 %89, 0
  br i1 %90, label %91, label %97

; <label>:91:                                     ; preds = %86
  %92 = or i32 %82, -2147481600
  %93 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %94 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %93, i64 0, i32 40
  %95 = load i64, i64* %94, align 8
  %96 = add i64 %95, 172
  tail call void @stl_phys(i64 %96, i32 %2)
  br label %97

; <label>:97:                                     ; preds = %91, %86, %79
  %98 = phi i32 [ %92, %91 ], [ %83, %86 ], [ %83, %79 ]
  %99 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %100 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %99, i64 0, i32 40
  %101 = load i64, i64* %100, align 8
  %102 = add i64 %101, 168
  tail call void @stl_phys(i64 %102, i32 %98)
  br label %103

; <label>:103:                                    ; preds = %97, %73, %72
  %104 = icmp ne i32 %1, 0
  %105 = or i32 %4, %1
  %106 = icmp eq i32 %105, 0
  br i1 %106, label %107, label %114

; <label>:107:                                    ; preds = %103
  %108 = add i32 %0, -8
  %109 = icmp ult i32 %108, 10
  br i1 %109, label %110, label %114

; <label>:110:                                    ; preds = %107
  %111 = sext i32 %108 to i64
  %112 = getelementptr inbounds [10 x i32], [10 x i32]* @switch.table, i64 0, i64 %111
  %113 = load i32, i32* %112, align 4
  br label %114

; <label>:114:                                    ; preds = %110, %107, %103
  %115 = phi i32 [ 0, %103 ], [ %113, %110 ], [ 0, %107 ]
  %116 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br i1 %104, label %120, label %117

; <label>:117:                                    ; preds = %114
  %118 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %116, i64 0, i32 5
  %119 = load i32, i32* %118, align 16
  br label %120

; <label>:120:                                    ; preds = %117, %114
  %121 = phi i32 [ %119, %117 ], [ %3, %114 ]
  %122 = shl nsw i32 %0, 3
  %123 = or i32 %122, 7
  %124 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %116, i64 0, i32 15, i32 2
  %125 = load i32, i32* %124, align 4
  %126 = icmp ugt i32 %123, %125
  br i1 %126, label %127, label %129

; <label>:127:                                    ; preds = %120
  %128 = or i32 %122, 2
  tail call fastcc void @raise_exception_err(i32 13, i32 %128) #12
  unreachable

; <label>:129:                                    ; preds = %120
  %130 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %116, i64 0, i32 15, i32 1
  %131 = load i32, i32* %130, align 4
  %132 = add i32 %131, %122
  %133 = tail call fastcc i32 @ldl_kernel(i32 %132)
  %134 = add i32 %132, 4
  %135 = tail call fastcc i32 @ldl_kernel(i32 %134)
  %136 = lshr i32 %135, 8
  %137 = trunc i32 %136 to i5
  switch i5 %137, label %175 [
    i5 5, label %138
    i5 6, label %177
    i5 7, label %177
    i5 14, label %177
    i5 15, label %177
  ]

; <label>:138:                                    ; preds = %129
  %139 = trunc i32 %135 to i16
  %140 = icmp slt i16 %139, 0
  br i1 %140, label %143, label %141

; <label>:141:                                    ; preds = %138
  %142 = or i32 %122, 2
  tail call fastcc void @raise_exception_err(i32 11, i32 %142) #12
  unreachable

; <label>:143:                                    ; preds = %138
  tail call fastcc void @switch_tss(i32 %122, i32 %133, i32 %135, i32 2, i32 %121)
  %144 = icmp eq i32 %115, 0
  br i1 %144, label %904, label %145

; <label>:145:                                    ; preds = %143
  %146 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %147 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %146, i64 0, i32 13, i32 3
  %148 = load i32, i32* %147, align 4
  %149 = lshr i32 %148, 11
  %150 = and i32 %149, 1
  %151 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %146, i64 0, i32 11, i64 2, i32 3
  %152 = load i32, i32* %151, align 4
  %153 = lshr i32 %152, 6
  %154 = and i32 %153, 65536
  %155 = xor i32 %154, 65536
  %156 = add nsw i32 %155, -1
  %157 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %146, i64 0, i32 0, i64 4
  %158 = load i32, i32* %157, align 16
  %159 = shl i32 2, %150
  %160 = sub i32 %158, %159
  %161 = and i32 %156, %160
  %162 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %146, i64 0, i32 11, i64 2, i32 1
  %163 = load i32, i32* %162, align 4
  %164 = add i32 %161, %163
  %165 = icmp eq i32 %150, 0
  br i1 %165, label %167, label %166

; <label>:166:                                    ; preds = %145
  tail call fastcc void @stl_kernel(i32 %164, i32 %2)
  br label %168

; <label>:167:                                    ; preds = %145
  tail call fastcc void @stw_kernel(i32 %164, i32 %2)
  br label %168

; <label>:168:                                    ; preds = %167, %166
  %169 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %170 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %169, i64 0, i32 0, i64 4
  %171 = load i32, i32* %170, align 16
  %172 = sub nsw i32 0, %155
  %173 = and i32 %171, %172
  %174 = or i32 %173, %161
  store i32 %174, i32* %170, align 16
  br label %904

; <label>:175:                                    ; preds = %129
  %176 = or i32 %122, 2
  tail call fastcc void @raise_exception_err(i32 13, i32 %176) #12
  unreachable

; <label>:177:                                    ; preds = %129, %129, %129, %129
  %178 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %179 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %178, i64 0, i32 8
  %180 = load i32, i32* %179, align 4
  %181 = and i32 %180, 3
  br i1 %104, label %182, label %188

; <label>:182:                                    ; preds = %177
  %183 = lshr i32 %135, 13
  %184 = and i32 %183, 3
  %185 = icmp ult i32 %184, %181
  br i1 %185, label %186, label %188

; <label>:186:                                    ; preds = %182
  %187 = or i32 %122, 2
  tail call fastcc void @raise_exception_err(i32 13, i32 %187) #12
  unreachable

; <label>:188:                                    ; preds = %182, %177
  %189 = trunc i32 %135 to i16
  %190 = icmp slt i16 %189, 0
  br i1 %190, label %193, label %191

; <label>:191:                                    ; preds = %188
  %192 = or i32 %122, 2
  tail call fastcc void @raise_exception_err(i32 11, i32 %192) #12
  unreachable

; <label>:193:                                    ; preds = %188
  %194 = lshr i32 %133, 16
  %195 = and i32 %135, -65536
  %196 = and i32 %133, 65535
  %197 = or i32 %195, %196
  %198 = and i32 %194, 65532
  %199 = icmp eq i32 %198, 0
  br i1 %199, label %200, label %201

; <label>:200:                                    ; preds = %193
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:201:                                    ; preds = %193
  %202 = and i32 %194, 4
  %203 = icmp eq i32 %202, 0
  %204 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %178, i64 0, i32 12
  %205 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %178, i64 0, i32 14
  %206 = select i1 %203, %struct.SegmentCache* %205, %struct.SegmentCache* %204
  %207 = or i32 %194, 7
  %208 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %206, i64 0, i32 2
  %209 = load i32, i32* %208, align 4
  %210 = icmp ugt i32 %207, %209
  br i1 %210, label %211, label %212

; <label>:211:                                    ; preds = %201
  tail call fastcc void @raise_exception_err(i32 13, i32 %198) #12
  unreachable

; <label>:212:                                    ; preds = %201
  %213 = and i32 %194, 65528
  %214 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %206, i64 0, i32 1
  %215 = load i32, i32* %214, align 4
  %216 = add i32 %215, %213
  %217 = tail call fastcc i32 @ldl_kernel(i32 %216)
  %218 = add i32 %216, 4
  %219 = tail call fastcc i32 @ldl_kernel(i32 %218)
  %220 = and i32 %219, 6144
  %221 = icmp eq i32 %220, 6144
  br i1 %221, label %223, label %222

; <label>:222:                                    ; preds = %212
  tail call fastcc void @raise_exception_err(i32 13, i32 %198) #12
  unreachable

; <label>:223:                                    ; preds = %212
  %224 = lshr i32 %219, 13
  %225 = and i32 %224, 3
  %226 = icmp ugt i32 %225, %181
  br i1 %226, label %227, label %228

; <label>:227:                                    ; preds = %223
  tail call fastcc void @raise_exception_err(i32 13, i32 %198) #12
  unreachable

; <label>:228:                                    ; preds = %223
  %229 = trunc i32 %219 to i16
  %230 = icmp slt i16 %229, 0
  br i1 %230, label %232, label %231

; <label>:231:                                    ; preds = %228
  tail call fastcc void @raise_exception_err(i32 11, i32 %198) #12
  unreachable

; <label>:232:                                    ; preds = %228
  %233 = and i32 %219, 1024
  %234 = icmp eq i32 %233, 0
  %235 = icmp ult i32 %225, %181
  %236 = and i1 %234, %235
  br i1 %236, label %237, label %331

; <label>:237:                                    ; preds = %232
  %238 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %239 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %238, i64 0, i32 13, i32 3
  %240 = load i32, i32* %239, align 4
  %241 = trunc i32 %240 to i16
  %242 = icmp slt i16 %241, 0
  br i1 %242, label %244, label %243

; <label>:243:                                    ; preds = %237
  tail call void (%struct.CPUX86State*, i8*, ...) @cpu_abort(%struct.CPUX86State* %238, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.18, i64 0, i64 0)) #12
  unreachable

; <label>:244:                                    ; preds = %237
  %245 = lshr i32 %240, 11
  %246 = and i32 %245, 1
  %247 = shl nuw nsw i32 %225, 2
  %248 = or i32 %247, 2
  %249 = shl i32 %248, %246
  %250 = shl i32 4, %246
  %251 = add nsw i32 %250, -1
  %252 = add nsw i32 %251, %249
  %253 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %238, i64 0, i32 13, i32 2
  %254 = load i32, i32* %253, align 8
  %255 = icmp ugt i32 %252, %254
  br i1 %255, label %256, label %260

; <label>:256:                                    ; preds = %244
  %257 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %238, i64 0, i32 13, i32 0
  %258 = load i32, i32* %257, align 8
  %259 = and i32 %258, 65532
  tail call fastcc void @raise_exception_err(i32 10, i32 %259) #12
  unreachable

; <label>:260:                                    ; preds = %244
  %261 = icmp eq i32 %246, 0
  %262 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %238, i64 0, i32 13, i32 1
  %263 = load i32, i32* %262, align 4
  %264 = add i32 %263, %249
  br i1 %261, label %265, label %273

; <label>:265:                                    ; preds = %260
  %266 = tail call fastcc i32 @lduw_kernel(i32 %264)
  %267 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %268 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %267, i64 0, i32 13, i32 1
  %269 = load i32, i32* %268, align 4
  %270 = add nuw nsw i32 %249, 2
  %271 = add i32 %270, %269
  %272 = tail call fastcc i32 @lduw_kernel(i32 %271)
  br label %281

; <label>:273:                                    ; preds = %260
  %274 = tail call fastcc i32 @ldl_kernel(i32 %264)
  %275 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %276 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %275, i64 0, i32 13, i32 1
  %277 = load i32, i32* %276, align 4
  %278 = add nuw nsw i32 %249, 4
  %279 = add i32 %278, %277
  %280 = tail call fastcc i32 @lduw_kernel(i32 %279)
  br label %281

; <label>:281:                                    ; preds = %273, %265
  %282 = phi i32 [ %266, %265 ], [ %274, %273 ]
  %283 = phi i32 [ %272, %265 ], [ %280, %273 ]
  %284 = and i32 %283, 65532
  %285 = icmp eq i32 %284, 0
  br i1 %285, label %286, label %287

; <label>:286:                                    ; preds = %281
  tail call fastcc void @raise_exception_err(i32 10, i32 0) #12
  unreachable

; <label>:287:                                    ; preds = %281
  %288 = and i32 %283, 3
  %289 = icmp eq i32 %288, %225
  br i1 %289, label %291, label %290

; <label>:290:                                    ; preds = %287
  tail call fastcc void @raise_exception_err(i32 10, i32 %284) #12
  unreachable

; <label>:291:                                    ; preds = %287
  %292 = and i32 %283, 4
  %293 = icmp eq i32 %292, 0
  %294 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %295 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %294, i64 0, i32 12
  %296 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %294, i64 0, i32 14
  %297 = select i1 %293, %struct.SegmentCache* %296, %struct.SegmentCache* %295
  %298 = or i32 %283, 7
  %299 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %297, i64 0, i32 2
  %300 = load i32, i32* %299, align 4
  %301 = icmp ugt i32 %298, %300
  br i1 %301, label %302, label %303

; <label>:302:                                    ; preds = %291
  tail call fastcc void @raise_exception_err(i32 10, i32 %284) #12
  unreachable

; <label>:303:                                    ; preds = %291
  %304 = and i32 %283, -8
  %305 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %297, i64 0, i32 1
  %306 = load i32, i32* %305, align 4
  %307 = add i32 %306, %304
  %308 = tail call fastcc i32 @ldl_kernel(i32 %307)
  %309 = add i32 %307, 4
  %310 = tail call fastcc i32 @ldl_kernel(i32 %309)
  %311 = lshr i32 %310, 13
  %312 = and i32 %311, 3
  %313 = icmp eq i32 %312, %225
  br i1 %313, label %315, label %314

; <label>:314:                                    ; preds = %303
  tail call fastcc void @raise_exception_err(i32 10, i32 %284) #12
  unreachable

; <label>:315:                                    ; preds = %303
  %316 = and i32 %310, 6656
  %317 = icmp eq i32 %316, 4608
  br i1 %317, label %319, label %318

; <label>:318:                                    ; preds = %315
  tail call fastcc void @raise_exception_err(i32 10, i32 %284) #12
  unreachable

; <label>:319:                                    ; preds = %315
  %320 = trunc i32 %310 to i16
  %321 = icmp slt i16 %320, 0
  br i1 %321, label %323, label %322

; <label>:322:                                    ; preds = %319
  tail call fastcc void @raise_exception_err(i32 10, i32 %284) #12
  unreachable

; <label>:323:                                    ; preds = %319
  %324 = lshr i32 %308, 16
  %325 = shl i32 %310, 16
  %326 = and i32 %325, 16711680
  %327 = and i32 %310, -16777216
  %328 = or i32 %327, %324
  %329 = or i32 %328, %326
  %330 = and i32 %308, 65535
  br label %350

; <label>:331:                                    ; preds = %232
  %332 = icmp ne i32 %233, 0
  %333 = icmp eq i32 %225, %181
  %334 = or i1 %332, %333
  br i1 %334, label %335, label %349

; <label>:335:                                    ; preds = %331
  %336 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %337 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %336, i64 0, i32 7
  %338 = load i32, i32* %337, align 8
  %339 = and i32 %338, 131072
  %340 = icmp eq i32 %339, 0
  br i1 %340, label %342, label %341

; <label>:341:                                    ; preds = %335
  tail call fastcc void @raise_exception_err(i32 13, i32 %198) #12
  unreachable

; <label>:342:                                    ; preds = %335
  %343 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %336, i64 0, i32 11, i64 2, i32 3
  %344 = load i32, i32* %343, align 4
  %345 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %336, i64 0, i32 11, i64 2, i32 1
  %346 = load i32, i32* %345, align 4
  %347 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %336, i64 0, i32 0, i64 4
  %348 = load i32, i32* %347, align 16
  br label %350

; <label>:349:                                    ; preds = %331
  tail call fastcc void @raise_exception_err(i32 13, i32 %198) #12
  unreachable

; <label>:350:                                    ; preds = %342, %323
  %351 = phi i32 [ %283, %323 ], [ 0, %342 ]
  %352 = phi i32 [ %282, %323 ], [ %348, %342 ]
  %353 = phi i32 [ %330, %323 ], [ 0, %342 ]
  %354 = phi i32 [ %310, %323 ], [ 0, %342 ]
  %355 = phi i1 [ true, %323 ], [ false, %342 ]
  %356 = phi i32 [ %225, %323 ], [ %181, %342 ]
  %357 = phi i32 [ %310, %323 ], [ %344, %342 ]
  %358 = phi i32 [ %329, %323 ], [ %346, %342 ]
  %359 = lshr i32 %357, 6
  %360 = and i32 %359, 65536
  %361 = xor i32 %360, 65536
  %362 = add nsw i32 %361, -1
  %363 = and i32 %136, 24
  %364 = icmp eq i32 %363, 8
  br i1 %364, label %365, label %442

; <label>:365:                                    ; preds = %350
  br i1 %355, label %366, label %411

; <label>:366:                                    ; preds = %365
  %367 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %368 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %367, i64 0, i32 7
  %369 = load i32, i32* %368, align 8
  %370 = and i32 %369, 131072
  %371 = icmp eq i32 %370, 0
  br i1 %371, label %397, label %372

; <label>:372:                                    ; preds = %366
  %373 = add i32 %352, -4
  %374 = and i32 %362, %373
  %375 = add i32 %374, %358
  %376 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %367, i64 0, i32 11, i64 5, i32 0
  %377 = load i32, i32* %376, align 8
  tail call fastcc void @stl_kernel(i32 %375, i32 %377)
  %378 = add i32 %352, -8
  %379 = and i32 %362, %378
  %380 = add i32 %379, %358
  %381 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %382 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 4, i32 0
  %383 = load i32, i32* %382, align 8
  tail call fastcc void @stl_kernel(i32 %380, i32 %383)
  %384 = add i32 %352, -12
  %385 = and i32 %362, %384
  %386 = add i32 %385, %358
  %387 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %388 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %387, i64 0, i32 11, i64 3, i32 0
  %389 = load i32, i32* %388, align 8
  tail call fastcc void @stl_kernel(i32 %386, i32 %389)
  %390 = add i32 %352, -16
  %391 = and i32 %362, %390
  %392 = add i32 %391, %358
  %393 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %394 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %393, i64 0, i32 11, i64 0, i32 0
  %395 = load i32, i32* %394, align 8
  tail call fastcc void @stl_kernel(i32 %392, i32 %395)
  %396 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %397

; <label>:397:                                    ; preds = %372, %366
  %398 = phi %struct.CPUX86State* [ %367, %366 ], [ %396, %372 ]
  %399 = phi i32 [ %352, %366 ], [ %390, %372 ]
  %400 = add i32 %399, -4
  %401 = and i32 %400, %362
  %402 = add i32 %401, %358
  %403 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %398, i64 0, i32 11, i64 2, i32 0
  %404 = load i32, i32* %403, align 8
  tail call fastcc void @stl_kernel(i32 %402, i32 %404)
  %405 = add i32 %399, -8
  %406 = and i32 %405, %362
  %407 = add i32 %406, %358
  %408 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %409 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %408, i64 0, i32 0, i64 4
  %410 = load i32, i32* %409, align 16
  tail call fastcc void @stl_kernel(i32 %407, i32 %410)
  br label %411

; <label>:411:                                    ; preds = %397, %365
  %412 = phi i32 [ %405, %397 ], [ %352, %365 ]
  %413 = add i32 %412, -4
  %414 = and i32 %413, %362
  %415 = add i32 %414, %358
  %416 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %417 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %416, i64 0, i32 7
  %418 = load i32, i32* %417, align 8
  %419 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %416, i64 0, i32 1
  %420 = load i32, i32* %419, align 16
  %421 = tail call i32 @helper_cc_compute_all(i32 %420) #5
  %422 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %416, i64 0, i32 6
  %423 = load i32, i32* %422, align 4
  %424 = and i32 %423, 1024
  %425 = or i32 %418, %421
  %426 = or i32 %425, %424
  %427 = or i32 %426, 2
  tail call fastcc void @stl_kernel(i32 %415, i32 %427)
  %428 = add i32 %412, -8
  %429 = and i32 %428, %362
  %430 = add i32 %429, %358
  %431 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %432 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %431, i64 0, i32 11, i64 1, i32 0
  %433 = load i32, i32* %432, align 8
  tail call fastcc void @stl_kernel(i32 %430, i32 %433)
  %434 = add i32 %412, -12
  %435 = and i32 %434, %362
  %436 = add i32 %435, %358
  tail call fastcc void @stl_kernel(i32 %436, i32 %121)
  %437 = icmp eq i32 %115, 0
  br i1 %437, label %519, label %438

; <label>:438:                                    ; preds = %411
  %439 = add i32 %412, -16
  %440 = and i32 %439, %362
  %441 = add i32 %440, %358
  tail call fastcc void @stl_kernel(i32 %441, i32 %2)
  br label %519

; <label>:442:                                    ; preds = %350
  br i1 %355, label %443, label %488

; <label>:443:                                    ; preds = %442
  %444 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %445 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %444, i64 0, i32 7
  %446 = load i32, i32* %445, align 8
  %447 = and i32 %446, 131072
  %448 = icmp eq i32 %447, 0
  br i1 %448, label %474, label %449

; <label>:449:                                    ; preds = %443
  %450 = add i32 %352, -2
  %451 = and i32 %362, %450
  %452 = add i32 %451, %358
  %453 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %444, i64 0, i32 11, i64 5, i32 0
  %454 = load i32, i32* %453, align 8
  tail call fastcc void @stw_kernel(i32 %452, i32 %454)
  %455 = add i32 %352, -4
  %456 = and i32 %362, %455
  %457 = add i32 %456, %358
  %458 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %459 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %458, i64 0, i32 11, i64 4, i32 0
  %460 = load i32, i32* %459, align 8
  tail call fastcc void @stw_kernel(i32 %457, i32 %460)
  %461 = add i32 %352, -6
  %462 = and i32 %362, %461
  %463 = add i32 %462, %358
  %464 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %465 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %464, i64 0, i32 11, i64 3, i32 0
  %466 = load i32, i32* %465, align 8
  tail call fastcc void @stw_kernel(i32 %463, i32 %466)
  %467 = add i32 %352, -8
  %468 = and i32 %362, %467
  %469 = add i32 %468, %358
  %470 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %471 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %470, i64 0, i32 11, i64 0, i32 0
  %472 = load i32, i32* %471, align 8
  tail call fastcc void @stw_kernel(i32 %469, i32 %472)
  %473 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %474

; <label>:474:                                    ; preds = %449, %443
  %475 = phi %struct.CPUX86State* [ %444, %443 ], [ %473, %449 ]
  %476 = phi i32 [ %352, %443 ], [ %467, %449 ]
  %477 = add i32 %476, -2
  %478 = and i32 %477, %362
  %479 = add i32 %478, %358
  %480 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %475, i64 0, i32 11, i64 2, i32 0
  %481 = load i32, i32* %480, align 8
  tail call fastcc void @stw_kernel(i32 %479, i32 %481)
  %482 = add i32 %476, -4
  %483 = and i32 %482, %362
  %484 = add i32 %483, %358
  %485 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %486 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %485, i64 0, i32 0, i64 4
  %487 = load i32, i32* %486, align 16
  tail call fastcc void @stw_kernel(i32 %484, i32 %487)
  br label %488

; <label>:488:                                    ; preds = %474, %442
  %489 = phi i32 [ %482, %474 ], [ %352, %442 ]
  %490 = add i32 %489, -2
  %491 = and i32 %490, %362
  %492 = add i32 %491, %358
  %493 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %494 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %493, i64 0, i32 7
  %495 = load i32, i32* %494, align 8
  %496 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %493, i64 0, i32 1
  %497 = load i32, i32* %496, align 16
  %498 = tail call i32 @helper_cc_compute_all(i32 %497) #5
  %499 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %493, i64 0, i32 6
  %500 = load i32, i32* %499, align 4
  %501 = and i32 %500, 1024
  %502 = or i32 %495, %498
  %503 = or i32 %502, %501
  %504 = or i32 %503, 2
  tail call fastcc void @stw_kernel(i32 %492, i32 %504)
  %505 = add i32 %489, -4
  %506 = and i32 %505, %362
  %507 = add i32 %506, %358
  %508 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %509 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %508, i64 0, i32 11, i64 1, i32 0
  %510 = load i32, i32* %509, align 8
  tail call fastcc void @stw_kernel(i32 %507, i32 %510)
  %511 = add i32 %489, -6
  %512 = and i32 %511, %362
  %513 = add i32 %512, %358
  tail call fastcc void @stw_kernel(i32 %513, i32 %121)
  %514 = icmp eq i32 %115, 0
  br i1 %514, label %519, label %515

; <label>:515:                                    ; preds = %488
  %516 = add i32 %489, -8
  %517 = and i32 %516, %362
  %518 = add i32 %517, %358
  tail call fastcc void @stw_kernel(i32 %518, i32 %2)
  br label %519

; <label>:519:                                    ; preds = %515, %488, %438, %411
  %520 = phi i32 [ %434, %411 ], [ %439, %438 ], [ %511, %488 ], [ %516, %515 ]
  %521 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br i1 %355, label %527, label %522

; <label>:522:                                    ; preds = %519
  %523 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 8
  %524 = load i32, i32* %523, align 4
  %525 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 2, i32 3
  %526 = load i32, i32* %525, align 4
  br label %722

; <label>:527:                                    ; preds = %519
  %528 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 7
  %529 = bitcast i32* %528 to i64*
  %530 = load i64, i64* %529, align 8
  %531 = and i64 %530, 131072
  %532 = icmp eq i64 %531, 0
  %533 = lshr i64 %530, 32
  %534 = trunc i64 %533 to i32
  br i1 %532, label %535, label %538

; <label>:535:                                    ; preds = %527
  %536 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 2, i32 3
  %537 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 8
  br label %668

; <label>:538:                                    ; preds = %527
  %539 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 0, i32 0
  %540 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 8
  %541 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 2, i32 3
  %542 = bitcast i32* %539 to i8*
  br label %543

; <label>:543:                                    ; preds = %546, %538
  %544 = phi i64 [ 0, %538 ], [ %548, %546 ]
  %545 = icmp ult i64 %544, 16
  br i1 %545, label %546, label %549

; <label>:546:                                    ; preds = %543
  %547 = getelementptr i8, i8* %542, i64 %544
  store i8 0, i8* %547
  %548 = add i64 %544, 1
  br label %543

; <label>:549:                                    ; preds = %543
  %550 = load i32, i32* %541, align 4
  %551 = lshr i32 %550, 17
  %552 = and i32 %551, 32
  %553 = trunc i64 %533 to i16
  %554 = icmp slt i16 %553, 0
  %555 = or i32 %552, 64
  %556 = select i1 %554, i32 %552, i32 %555
  %557 = and i32 %534, -97
  %558 = or i32 %556, %557
  store i32 %558, i32* %540, align 4
  %559 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 3, i32 0
  %560 = bitcast i32* %559 to i8*
  br label %561

; <label>:561:                                    ; preds = %564, %549
  %562 = phi i64 [ 0, %549 ], [ %566, %564 ]
  %563 = icmp ult i64 %562, 16
  br i1 %563, label %564, label %567

; <label>:564:                                    ; preds = %561
  %565 = getelementptr i8, i8* %560, i64 %562
  store i8 0, i8* %565
  %566 = add i64 %562, 1
  br label %561

; <label>:567:                                    ; preds = %561
  br i1 %554, label %589, label %568

; <label>:568:                                    ; preds = %567
  %569 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 16, i64 0
  %570 = load i32, i32* %569, align 8
  %571 = and i32 %570, 1
  %572 = icmp eq i32 %571, 0
  br i1 %572, label %589, label %573

; <label>:573:                                    ; preds = %568
  %574 = load i64, i64* %529, align 8
  %575 = and i64 %574, 131072
  %576 = icmp ne i64 %575, 0
  %577 = and i32 %534, 16
  %578 = icmp eq i32 %577, 0
  %579 = or i1 %578, %576
  %580 = lshr i64 %574, 32
  %581 = trunc i64 %580 to i32
  br i1 %579, label %589, label %582

; <label>:582:                                    ; preds = %573
  %583 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 2, i32 1
  %584 = load i32, i32* %583, align 4
  %585 = icmp ne i32 %584, 0
  %586 = zext i1 %585 to i32
  %587 = shl nuw nsw i32 %586, 6
  %588 = or i32 %587, %552
  br label %589

; <label>:589:                                    ; preds = %568, %573, %582, %567
  %590 = phi i32 [ %558, %567 ], [ %581, %582 ], [ %581, %573 ], [ %558, %568 ]
  %591 = phi i32 [ %552, %567 ], [ %588, %582 ], [ %555, %573 ], [ %555, %568 ]
  %592 = and i32 %590, -97
  %593 = or i32 %592, %591
  store i32 %593, i32* %540, align 4
  %594 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 4, i32 0
  %595 = trunc i32 %593 to i16
  %596 = icmp slt i16 %595, 0
  %597 = bitcast i32* %594 to i8*
  br label %598

; <label>:598:                                    ; preds = %601, %589
  %599 = phi i64 [ 0, %589 ], [ %603, %601 ]
  %600 = icmp ult i64 %599, 16
  br i1 %600, label %601, label %604

; <label>:601:                                    ; preds = %598
  %602 = getelementptr i8, i8* %597, i64 %599
  store i8 0, i8* %602
  %603 = add i64 %599, 1
  br label %598

; <label>:604:                                    ; preds = %598
  br i1 %596, label %626, label %605

; <label>:605:                                    ; preds = %604
  %606 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 16, i64 0
  %607 = load i32, i32* %606, align 8
  %608 = and i32 %607, 1
  %609 = icmp eq i32 %608, 0
  br i1 %609, label %626, label %610

; <label>:610:                                    ; preds = %605
  %611 = load i64, i64* %529, align 8
  %612 = and i64 %611, 131072
  %613 = icmp ne i64 %612, 0
  %614 = and i32 %593, 16
  %615 = icmp eq i32 %614, 0
  %616 = or i1 %615, %613
  %617 = lshr i64 %611, 32
  %618 = trunc i64 %617 to i32
  br i1 %616, label %626, label %619

; <label>:619:                                    ; preds = %610
  %620 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 2, i32 1
  %621 = load i32, i32* %620, align 4
  %622 = icmp ne i32 %621, 0
  %623 = zext i1 %622 to i32
  %624 = shl nuw nsw i32 %623, 6
  %625 = or i32 %624, %552
  br label %626

; <label>:626:                                    ; preds = %605, %610, %619, %604
  %627 = phi i32 [ %593, %604 ], [ %618, %619 ], [ %618, %610 ], [ %593, %605 ]
  %628 = phi i32 [ %552, %604 ], [ %625, %619 ], [ %555, %610 ], [ %555, %605 ]
  %629 = and i32 %627, -97
  %630 = or i32 %629, %628
  store i32 %630, i32* %540, align 4
  %631 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 5, i32 0
  %632 = trunc i32 %630 to i16
  %633 = icmp slt i16 %632, 0
  %634 = bitcast i32* %631 to i8*
  br label %635

; <label>:635:                                    ; preds = %638, %626
  %636 = phi i64 [ 0, %626 ], [ %640, %638 ]
  %637 = icmp ult i64 %636, 16
  br i1 %637, label %638, label %641

; <label>:638:                                    ; preds = %635
  %639 = getelementptr i8, i8* %634, i64 %636
  store i8 0, i8* %639
  %640 = add i64 %636, 1
  br label %635

; <label>:641:                                    ; preds = %635
  br i1 %633, label %663, label %642

; <label>:642:                                    ; preds = %641
  %643 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 16, i64 0
  %644 = load i32, i32* %643, align 8
  %645 = and i32 %644, 1
  %646 = icmp eq i32 %645, 0
  br i1 %646, label %663, label %647

; <label>:647:                                    ; preds = %642
  %648 = load i64, i64* %529, align 8
  %649 = and i64 %648, 131072
  %650 = icmp ne i64 %649, 0
  %651 = and i32 %630, 16
  %652 = icmp eq i32 %651, 0
  %653 = or i1 %652, %650
  %654 = lshr i64 %648, 32
  %655 = trunc i64 %654 to i32
  br i1 %653, label %663, label %656

; <label>:656:                                    ; preds = %647
  %657 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 2, i32 1
  %658 = load i32, i32* %657, align 4
  %659 = icmp ne i32 %658, 0
  %660 = zext i1 %659 to i32
  %661 = shl nuw nsw i32 %660, 6
  %662 = or i32 %661, %552
  br label %663

; <label>:663:                                    ; preds = %642, %647, %656, %641
  %664 = phi i32 [ %630, %641 ], [ %655, %656 ], [ %655, %647 ], [ %630, %642 ]
  %665 = phi i32 [ %552, %641 ], [ %662, %656 ], [ %555, %647 ], [ %555, %642 ]
  %666 = and i32 %664, -97
  %667 = or i32 %666, %665
  store i32 %667, i32* %540, align 4
  br label %668

; <label>:668:                                    ; preds = %663, %535
  %669 = phi i32* [ %537, %535 ], [ %540, %663 ]
  %670 = phi i32* [ %536, %535 ], [ %541, %663 ]
  %671 = phi i32 [ %534, %535 ], [ %667, %663 ]
  %672 = and i32 %351, -4
  %673 = or i32 %356, %672
  %674 = and i32 %354, 983040
  %675 = or i32 %674, %353
  %676 = and i32 %354, 8388608
  %677 = icmp eq i32 %676, 0
  %678 = shl nuw i32 %675, 12
  %679 = or i32 %678, 4095
  %680 = select i1 %677, i32 %675, i32 %679
  %681 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 2, i32 0
  store i32 %673, i32* %681, align 4
  %682 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 2, i32 1
  store i32 %358, i32* %682, align 4
  %683 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 2, i32 2
  store i32 %680, i32* %683, align 4
  store i32 %354, i32* %670, align 4
  %684 = lshr i32 %354, 17
  %685 = and i32 %684, 32
  %686 = trunc i32 %671 to i16
  %687 = icmp slt i16 %686, 0
  br i1 %687, label %716, label %688

; <label>:688:                                    ; preds = %668
  %689 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 16, i64 0
  %690 = load i32, i32* %689, align 8
  %691 = and i32 %690, 1
  %692 = icmp eq i32 %691, 0
  br i1 %692, label %702, label %693

; <label>:693:                                    ; preds = %688
  %694 = load i64, i64* %529, align 8
  %695 = and i64 %694, 131072
  %696 = icmp ne i64 %695, 0
  %697 = and i32 %671, 16
  %698 = icmp eq i32 %697, 0
  %699 = or i1 %698, %696
  %700 = lshr i64 %694, 32
  %701 = trunc i64 %700 to i32
  br i1 %699, label %702, label %705

; <label>:702:                                    ; preds = %693, %688
  %703 = phi i32 [ %701, %693 ], [ %671, %688 ]
  %704 = or i32 %685, 64
  br label %716

; <label>:705:                                    ; preds = %693
  %706 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 3, i32 1
  %707 = load i32, i32* %706, align 4
  %708 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 0, i32 1
  %709 = load i32, i32* %708, align 4
  %710 = or i32 %707, %358
  %711 = or i32 %710, %709
  %712 = icmp ne i32 %711, 0
  %713 = zext i1 %712 to i32
  %714 = shl nuw nsw i32 %713, 6
  %715 = or i32 %714, %685
  br label %716

; <label>:716:                                    ; preds = %705, %702, %668
  %717 = phi i32 [ %671, %668 ], [ %703, %702 ], [ %701, %705 ]
  %718 = phi i32 [ %685, %668 ], [ %704, %702 ], [ %715, %705 ]
  %719 = and i32 %717, -97
  %720 = or i32 %719, %718
  store i32 %720, i32* %669, align 4
  %721 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 8
  br label %722

; <label>:722:                                    ; preds = %716, %522
  %723 = phi i32* [ %721, %716 ], [ %523, %522 ]
  %724 = phi i32 [ %354, %716 ], [ %526, %522 ]
  %725 = phi i32 [ %720, %716 ], [ %524, %522 ]
  %726 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 0, i64 4
  %727 = load i32, i32* %726, align 16
  %728 = sub nsw i32 0, %361
  %729 = and i32 %727, %728
  %730 = and i32 %520, %362
  %731 = or i32 %729, %730
  store i32 %731, i32* %726, align 16
  %732 = or i32 %356, %198
  %733 = lshr i32 %217, 16
  %734 = shl i32 %219, 16
  %735 = and i32 %734, 16711680
  %736 = and i32 %219, -16777216
  %737 = or i32 %736, %733
  %738 = or i32 %737, %735
  %739 = and i32 %217, 65535
  %740 = and i32 %219, 983040
  %741 = or i32 %740, %739
  %742 = and i32 %219, 8388608
  %743 = icmp eq i32 %742, 0
  %744 = shl nuw i32 %741, 12
  %745 = or i32 %744, 4095
  %746 = select i1 %743, i32 %741, i32 %745
  %747 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 1, i32 0
  store i32 %732, i32* %747, align 4
  %748 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 1, i32 1
  store i32 %738, i32* %748, align 4
  %749 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 1, i32 2
  store i32 %746, i32* %749, align 4
  %750 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 1, i32 3
  store i32 %219, i32* %750, align 4
  %751 = lshr i32 %219, 18
  %752 = and i32 %751, 16
  %753 = and i32 %725, -32785
  %754 = or i32 %753, %752
  store i32 %754, i32* %723, align 4
  %755 = lshr i32 %724, 17
  %756 = and i32 %755, 32
  %757 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 16, i64 0
  %758 = load i32, i32* %757, align 8
  %759 = and i32 %758, 1
  %760 = icmp eq i32 %759, 0
  br i1 %760, label %771, label %761

; <label>:761:                                    ; preds = %722
  %762 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 7
  %763 = bitcast i32* %762 to i64*
  %764 = load i64, i64* %763, align 8
  %765 = and i64 %764, 131072
  %766 = icmp ne i64 %765, 0
  %767 = icmp eq i32 %752, 0
  %768 = or i1 %767, %766
  %769 = lshr i64 %764, 32
  %770 = trunc i64 %769 to i32
  br i1 %768, label %771, label %774

; <label>:771:                                    ; preds = %761, %722
  %772 = phi i32 [ %770, %761 ], [ %754, %722 ]
  %773 = or i32 %756, 64
  br label %787

; <label>:774:                                    ; preds = %761
  %775 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 3, i32 1
  %776 = load i32, i32* %775, align 4
  %777 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 0, i32 1
  %778 = load i32, i32* %777, align 4
  %779 = or i32 %778, %776
  %780 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %521, i64 0, i32 11, i64 2, i32 1
  %781 = load i32, i32* %780, align 4
  %782 = or i32 %779, %781
  %783 = icmp ne i32 %782, 0
  %784 = zext i1 %783 to i32
  %785 = shl nuw nsw i32 %784, 6
  %786 = or i32 %785, %756
  br label %787

; <label>:787:                                    ; preds = %774, %771
  %788 = phi i32 [ %772, %771 ], [ %770, %774 ]
  %789 = phi i32 [ %773, %771 ], [ %786, %774 ]
  %790 = and i32 %788, -97
  %791 = or i32 %790, %789
  store i32 %791, i32* %723, align 4
  %792 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %793 = load i32, i32* %792, align 4
  %794 = icmp eq i32 %793, 0
  br i1 %794, label %800, label %795, !prof !2

; <label>:795:                                    ; preds = %787
  %796 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  %797 = and i32 %791, 3
  tail call void %796(i32 %797, i32 %356)
  %798 = load i32, i32* %723, align 4
  %799 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %800

; <label>:800:                                    ; preds = %795, %787
  %801 = phi %struct.CPUX86State* [ %799, %795 ], [ %521, %787 ]
  %802 = phi i32 [ %798, %795 ], [ %791, %787 ]
  %803 = and i32 %802, -4
  %804 = or i32 %803, %356
  store i32 %804, i32* %723, align 4
  %805 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %801, i64 0, i32 5
  store i32 %197, i32* %805, align 16
  %806 = and i32 %136, 1
  %807 = icmp eq i32 %806, 0
  %808 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %801, i64 0, i32 7
  %809 = load i32, i32* %808, align 8
  br i1 %807, label %810, label %812

; <label>:810:                                    ; preds = %800
  %811 = and i32 %809, -513
  store i32 %811, i32* %808, align 8
  br label %812

; <label>:812:                                    ; preds = %810, %800
  %813 = phi i32 [ %811, %810 ], [ %809, %800 ]
  %814 = and i32 %813, -213249
  store i32 %814, i32* %808, align 8
  %815 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %816 = load i32, i32* %815, align 4
  %817 = icmp eq i32 %816, 0
  br i1 %817, label %904, label %818, !prof !2

; <label>:818:                                    ; preds = %812
  %819 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  tail call void %819(i32 %181, i32 %356)
  br label %904

; <label>:820:                                    ; preds = %62
  br i1 %71, label %821, label %836

; <label>:821:                                    ; preds = %820
  %822 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %63, i64 0, i32 40
  %823 = load i64, i64* %822, align 8
  %824 = add i64 %823, 168
  %825 = tail call i32 @ldl_phys(i64 %824)
  %826 = icmp slt i32 %825, 0
  br i1 %826, label %836, label %827

; <label>:827:                                    ; preds = %821
  %828 = icmp eq i32 %1, 0
  %829 = select i1 %828, i32 768, i32 1024
  %830 = or i32 %829, %0
  %831 = or i32 %830, -2147483648
  %832 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %833 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %832, i64 0, i32 40
  %834 = load i64, i64* %833, align 8
  %835 = add i64 %834, 168
  tail call void @stl_phys(i64 %835, i32 %831)
  br label %836

; <label>:836:                                    ; preds = %827, %821, %820
  %837 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %838 = shl nsw i32 %0, 2
  %839 = or i32 %838, 3
  %840 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %837, i64 0, i32 15, i32 2
  %841 = load i32, i32* %840, align 4
  %842 = icmp ugt i32 %839, %841
  br i1 %842, label %843, label %846

; <label>:843:                                    ; preds = %836
  %844 = shl nsw i32 %0, 3
  %845 = or i32 %844, 2
  tail call fastcc void @raise_exception_err(i32 13, i32 %845) #12
  unreachable

; <label>:846:                                    ; preds = %836
  %847 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %837, i64 0, i32 15, i32 1
  %848 = load i32, i32* %847, align 4
  %849 = add i32 %848, %838
  %850 = tail call fastcc i32 @lduw_kernel(i32 %849)
  %851 = add i32 %849, 2
  %852 = tail call fastcc i32 @lduw_kernel(i32 %851)
  %853 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %854 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %853, i64 0, i32 0, i64 4
  %855 = load i32, i32* %854, align 16
  %856 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %853, i64 0, i32 11, i64 2, i32 1
  %857 = load i32, i32* %856, align 4
  %858 = icmp eq i32 %1, 0
  br i1 %858, label %862, label %859

; <label>:859:                                    ; preds = %846
  %860 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %853, i64 0, i32 6
  %861 = load i32, i32* %860, align 4
  br label %869

; <label>:862:                                    ; preds = %846
  %863 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %853, i64 0, i32 5
  %864 = bitcast i32* %863 to i64*
  %865 = load i64, i64* %864, align 16
  %866 = trunc i64 %865 to i32
  %867 = lshr i64 %865, 32
  %868 = trunc i64 %867 to i32
  br label %869

; <label>:869:                                    ; preds = %859, %862
  %870 = phi i32 [ %868, %862 ], [ %861, %859 ]
  %871 = phi i32 [ %866, %862 ], [ %3, %859 ]
  %872 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %853, i64 0, i32 11, i64 1, i32 0
  %873 = load i32, i32* %872, align 8
  %874 = add i32 %855, 65534
  %875 = and i32 %874, 65535
  %876 = add i32 %875, %857
  %877 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %853, i64 0, i32 7
  %878 = load i32, i32* %877, align 8
  %879 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %853, i64 0, i32 1
  %880 = load i32, i32* %879, align 16
  %881 = tail call i32 @helper_cc_compute_all(i32 %880) #5
  %882 = and i32 %870, 1024
  %883 = or i32 %882, %878
  %884 = or i32 %883, %881
  %885 = or i32 %884, 2
  tail call fastcc void @stw_kernel(i32 %876, i32 %885)
  %886 = add i32 %855, 65532
  %887 = and i32 %886, 65535
  %888 = add i32 %887, %857
  tail call fastcc void @stw_kernel(i32 %888, i32 %873)
  %889 = add i32 %855, 65530
  %890 = and i32 %889, 65535
  %891 = add i32 %890, %857
  tail call fastcc void @stw_kernel(i32 %891, i32 %871)
  %892 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %893 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %892, i64 0, i32 0, i64 4
  %894 = load i32, i32* %893, align 16
  %895 = and i32 %894, -65536
  %896 = or i32 %895, %890
  store i32 %896, i32* %893, align 16
  %897 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %892, i64 0, i32 5
  store i32 %850, i32* %897, align 16
  %898 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %892, i64 0, i32 11, i64 1, i32 0
  store i32 %852, i32* %898, align 8
  %899 = shl i32 %852, 4
  %900 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %892, i64 0, i32 11, i64 1, i32 1
  store i32 %899, i32* %900, align 4
  %901 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %892, i64 0, i32 7
  %902 = load i32, i32* %901, align 8
  %903 = and i32 %902, -328449
  store i32 %903, i32* %901, align 8
  br label %904

; <label>:904:                                    ; preds = %818, %812, %168, %143, %869
  %905 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %906 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %905, i64 0, i32 8
  %907 = load i32, i32* %906, align 4
  %908 = and i32 %907, 2097152
  %909 = icmp eq i32 %908, 0
  br i1 %909, label %920, label %910

; <label>:910:                                    ; preds = %904
  %911 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %905, i64 0, i32 40
  %912 = load i64, i64* %911, align 8
  %913 = add i64 %912, 168
  %914 = tail call i32 @ldl_phys(i64 %913)
  %915 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %916 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %915, i64 0, i32 40
  %917 = load i64, i64* %916, align 8
  %918 = add i64 %917, 168
  %919 = and i32 %914, 2147483647
  tail call void @stl_phys(i64 %918, i32 %919)
  br label %920

; <label>:920:                                    ; preds = %904, %910
  ret void
}

; Function Attrs: nounwind
declare i32 @fputc(i32, %struct._IO_FILE* nocapture) #5

; Function Attrs: uwtable
define internal fastcc i32 @ldl_kernel(i32) unnamed_addr #2 {
  %2 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %3 = load i32, i32* %2, align 4
  %4 = icmp eq i32 %3, 0
  br i1 %4, label %6, label %5

; <label>:5:                                      ; preds = %1
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %6

; <label>:6:                                      ; preds = %1, %5
  %7 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %8 = load i32, i32* %7, align 4
  %9 = icmp eq i32 %8, 0
  br i1 %9, label %12, label %10

; <label>:10:                                     ; preds = %6
  %11 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %12

; <label>:12:                                     ; preds = %6, %10
  %13 = phi i32 [ %11, %10 ], [ %0, %6 ]
  %14 = lshr i32 %13, 12
  %15 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %14, i32 0, i32 1048575, i32 0)
  %16 = and i32 %15, 1023
  %17 = zext i32 %16 to i64
  %18 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %19 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 77, i64 0, i64 %17, i32 0
  %20 = load i32, i32* %19, align 8
  %21 = and i32 %13, -4093
  %22 = icmp eq i32 %20, %21
  br i1 %22, label %25, label %23, !prof !2

; <label>:23:                                     ; preds = %12
  %24 = tail call i32 @__ldl_mmu(i32 %13, i32 0)
  br label %37

; <label>:25:                                     ; preds = %12
  %26 = zext i32 %13 to i64
  %27 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 77, i64 0, i64 %17, i32 4
  %28 = load i64, i64* %27, align 8
  %29 = add i64 %28, %26
  %30 = inttoptr i64 %29 to i32*
  %31 = load i32, i32* %30, align 4
  %32 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %33 = load i32, i32* %32, align 4
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %37, label %35

; <label>:35:                                     ; preds = %25
  %36 = zext i32 %31 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %13, i64 %36, i32 4, i32 0, i64 0)
  br label %37

; <label>:37:                                     ; preds = %25, %35, %23
  %38 = phi i32 [ %24, %23 ], [ %31, %35 ], [ %31, %25 ]
  ret i32 %38
}

; Function Attrs: uwtable
define internal fastcc void @switch_tss(i32, i32, i32, i32, i32) unnamed_addr #2 {
  %6 = alloca [8 x i32], align 16
  %7 = alloca [6 x i32], align 16
  %8 = bitcast [8 x i32]* %6 to i8*
  %9 = bitcast [6 x i32]* %7 to i8*
  %10 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 8
  %12 = load i32, i32* %11, align 4
  %13 = and i32 %12, 3
  %14 = lshr i32 %2, 8
  %15 = and i32 %14, 15
  %16 = icmp eq i32 %15, 5
  br i1 %16, label %17, label %54

; <label>:17:                                     ; preds = %5
  %18 = trunc i32 %2 to i16
  %19 = icmp slt i16 %18, 0
  br i1 %19, label %22, label %20

; <label>:20:                                     ; preds = %17
  %21 = and i32 %0, 65532
  tail call fastcc void @raise_exception_err(i32 11, i32 %21) #12
  unreachable

; <label>:22:                                     ; preds = %17
  %23 = lshr i32 %1, 16
  %24 = and i32 %23, 4
  %25 = icmp eq i32 %24, 0
  br i1 %25, label %28, label %26

; <label>:26:                                     ; preds = %22
  %27 = and i32 %23, 65532
  tail call fastcc void @raise_exception_err(i32 10, i32 %27) #12
  unreachable

; <label>:28:                                     ; preds = %22
  %29 = or i32 %23, 7
  %30 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 14, i32 2
  %31 = load i32, i32* %30, align 4
  %32 = icmp ugt i32 %29, %31
  br i1 %32, label %33, label %35

; <label>:33:                                     ; preds = %28
  %34 = and i32 %23, 65532
  tail call fastcc void @raise_exception_err(i32 13, i32 %34) #12
  unreachable

; <label>:35:                                     ; preds = %28
  %36 = and i32 %23, 65528
  %37 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 14, i32 1
  %38 = load i32, i32* %37, align 4
  %39 = add i32 %38, %36
  %40 = tail call fastcc i32 @ldl_kernel(i32 %39)
  %41 = add i32 %39, 4
  %42 = tail call fastcc i32 @ldl_kernel(i32 %41)
  %43 = and i32 %42, 4096
  %44 = icmp eq i32 %43, 0
  br i1 %44, label %47, label %45

; <label>:45:                                     ; preds = %35
  %46 = and i32 %23, 65532
  tail call fastcc void @raise_exception_err(i32 13, i32 %46) #12
  unreachable

; <label>:47:                                     ; preds = %35
  %48 = lshr i32 %42, 8
  %49 = and i32 %48, 15
  %50 = and i32 %48, 7
  %51 = icmp eq i32 %50, 1
  br i1 %51, label %54, label %52

; <label>:52:                                     ; preds = %47
  %53 = and i32 %23, 65532
  tail call fastcc void @raise_exception_err(i32 13, i32 %53) #12
  unreachable

; <label>:54:                                     ; preds = %47, %5
  %55 = phi i32 [ %40, %47 ], [ %1, %5 ]
  %56 = phi i32 [ %42, %47 ], [ %2, %5 ]
  %57 = phi i32 [ %49, %47 ], [ %15, %5 ]
  %58 = phi i32 [ %23, %47 ], [ %0, %5 ]
  %59 = trunc i32 %56 to i16
  %60 = icmp slt i16 %59, 0
  br i1 %60, label %63, label %61

; <label>:61:                                     ; preds = %54
  %62 = and i32 %58, 65532
  tail call fastcc void @raise_exception_err(i32 11, i32 %62) #12
  unreachable

; <label>:63:                                     ; preds = %54
  %64 = and i32 %57, 8
  %65 = icmp ne i32 %64, 0
  %66 = select i1 %65, i32 103, i32 43
  %67 = and i32 %55, 65535
  %68 = and i32 %56, 983040
  %69 = or i32 %68, %67
  %70 = and i32 %56, 8388608
  %71 = icmp eq i32 %70, 0
  %72 = shl nuw i32 %69, 12
  %73 = or i32 %72, 4095
  %74 = select i1 %71, i32 %69, i32 %73
  %75 = lshr i32 %55, 16
  %76 = shl i32 %56, 16
  %77 = and i32 %76, 16711680
  %78 = and i32 %56, -16777216
  %79 = or i32 %78, %75
  %80 = or i32 %79, %77
  %81 = and i32 %58, 4
  %82 = icmp ne i32 %81, 0
  %83 = icmp slt i32 %74, %66
  %84 = or i1 %82, %83
  br i1 %84, label %85, label %87

; <label>:85:                                     ; preds = %63
  %86 = and i32 %58, 65532
  tail call fastcc void @raise_exception_err(i32 10, i32 %86) #12
  unreachable

; <label>:87:                                     ; preds = %63
  %88 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %89 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 13, i32 3
  %90 = load i32, i32* %89, align 4
  %91 = and i32 %90, 2048
  %92 = icmp eq i32 %91, 0
  %93 = select i1 %92, i32 43, i32 103
  br i1 %65, label %94, label %147

; <label>:94:                                     ; preds = %87
  %95 = add i32 %80, 28
  %96 = tail call fastcc i32 @ldl_kernel(i32 %95)
  %97 = add i32 %80, 32
  %98 = tail call fastcc i32 @ldl_kernel(i32 %97)
  %99 = add i32 %80, 36
  %100 = tail call fastcc i32 @ldl_kernel(i32 %99)
  %101 = add i32 %80, 40
  %102 = tail call fastcc i32 @ldl_kernel(i32 %101)
  %103 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 0
  store i32 %102, i32* %103, align 16
  %104 = add i32 %80, 44
  %105 = tail call fastcc i32 @ldl_kernel(i32 %104)
  %106 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 1
  store i32 %105, i32* %106, align 4
  %107 = add i32 %80, 48
  %108 = tail call fastcc i32 @ldl_kernel(i32 %107)
  %109 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 2
  store i32 %108, i32* %109, align 8
  %110 = add i32 %80, 52
  %111 = tail call fastcc i32 @ldl_kernel(i32 %110)
  %112 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 3
  store i32 %111, i32* %112, align 4
  %113 = add i32 %80, 56
  %114 = tail call fastcc i32 @ldl_kernel(i32 %113)
  %115 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 4
  store i32 %114, i32* %115, align 16
  %116 = add i32 %80, 60
  %117 = tail call fastcc i32 @ldl_kernel(i32 %116)
  %118 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 5
  store i32 %117, i32* %118, align 4
  %119 = add i32 %80, 64
  %120 = tail call fastcc i32 @ldl_kernel(i32 %119)
  %121 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 6
  store i32 %120, i32* %121, align 8
  %122 = add i32 %80, 68
  %123 = tail call fastcc i32 @ldl_kernel(i32 %122)
  %124 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 7
  store i32 %123, i32* %124, align 4
  %125 = add i32 %80, 72
  %126 = tail call fastcc i32 @lduw_kernel(i32 %125)
  %127 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 0
  store i32 %126, i32* %127, align 16
  %128 = add i32 %80, 76
  %129 = tail call fastcc i32 @lduw_kernel(i32 %128)
  %130 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 1
  store i32 %129, i32* %130, align 4
  %131 = add i32 %80, 80
  %132 = tail call fastcc i32 @lduw_kernel(i32 %131)
  %133 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 2
  store i32 %132, i32* %133, align 8
  %134 = add i32 %80, 84
  %135 = tail call fastcc i32 @lduw_kernel(i32 %134)
  %136 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 3
  store i32 %135, i32* %136, align 4
  %137 = add i32 %80, 88
  %138 = tail call fastcc i32 @lduw_kernel(i32 %137)
  %139 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 4
  store i32 %138, i32* %139, align 16
  %140 = add i32 %80, 92
  %141 = tail call fastcc i32 @lduw_kernel(i32 %140)
  %142 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 5
  store i32 %141, i32* %142, align 4
  %143 = add i32 %80, 96
  %144 = tail call fastcc i32 @lduw_kernel(i32 %143)
  %145 = add i32 %80, 100
  %146 = tail call fastcc i32 @ldl_kernel(i32 %145)
  br label %200

; <label>:147:                                    ; preds = %87
  %148 = add i32 %80, 14
  %149 = tail call fastcc i32 @lduw_kernel(i32 %148)
  %150 = add i32 %80, 16
  %151 = tail call fastcc i32 @lduw_kernel(i32 %150)
  %152 = add i32 %80, 18
  %153 = tail call fastcc i32 @lduw_kernel(i32 %152)
  %154 = or i32 %153, -65536
  %155 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 0
  store i32 %154, i32* %155, align 16
  %156 = add i32 %80, 20
  %157 = tail call fastcc i32 @lduw_kernel(i32 %156)
  %158 = or i32 %157, -65536
  %159 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 1
  store i32 %158, i32* %159, align 4
  %160 = add i32 %80, 22
  %161 = tail call fastcc i32 @lduw_kernel(i32 %160)
  %162 = or i32 %161, -65536
  %163 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 2
  store i32 %162, i32* %163, align 8
  %164 = add i32 %80, 24
  %165 = tail call fastcc i32 @lduw_kernel(i32 %164)
  %166 = or i32 %165, -65536
  %167 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 3
  store i32 %166, i32* %167, align 4
  %168 = add i32 %80, 26
  %169 = tail call fastcc i32 @lduw_kernel(i32 %168)
  %170 = or i32 %169, -65536
  %171 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 4
  store i32 %170, i32* %171, align 16
  %172 = add i32 %80, 28
  %173 = tail call fastcc i32 @lduw_kernel(i32 %172)
  %174 = or i32 %173, -65536
  %175 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 5
  store i32 %174, i32* %175, align 4
  %176 = add i32 %80, 30
  %177 = tail call fastcc i32 @lduw_kernel(i32 %176)
  %178 = or i32 %177, -65536
  %179 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 6
  store i32 %178, i32* %179, align 8
  %180 = add i32 %80, 32
  %181 = tail call fastcc i32 @lduw_kernel(i32 %180)
  %182 = or i32 %181, -65536
  %183 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 7
  store i32 %182, i32* %183, align 4
  %184 = add i32 %80, 34
  %185 = tail call fastcc i32 @lduw_kernel(i32 %184)
  %186 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 0
  store i32 %185, i32* %186, align 16
  %187 = add i32 %80, 38
  %188 = tail call fastcc i32 @lduw_kernel(i32 %187)
  %189 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 1
  store i32 %188, i32* %189, align 4
  %190 = add i32 %80, 42
  %191 = tail call fastcc i32 @lduw_kernel(i32 %190)
  %192 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 2
  store i32 %191, i32* %192, align 8
  %193 = add i32 %80, 46
  %194 = tail call fastcc i32 @lduw_kernel(i32 %193)
  %195 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 3
  store i32 %194, i32* %195, align 4
  %196 = add i32 %80, 42
  %197 = tail call fastcc i32 @lduw_kernel(i32 %196)
  %198 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 4
  store i32 0, i32* %198, align 16
  %199 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 5
  store i32 0, i32* %199, align 4
  br label %200

; <label>:200:                                    ; preds = %147, %94
  %201 = phi i32 [ %144, %94 ], [ %197, %147 ]
  %202 = phi i32 [ %96, %94 ], [ 0, %147 ]
  %203 = phi i32 [ %98, %94 ], [ %149, %147 ]
  %204 = phi i32 [ %100, %94 ], [ %151, %147 ]
  %205 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %206 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %205, i64 0, i32 13, i32 1
  %207 = load i32, i32* %206, align 4
  %208 = tail call fastcc i32 @ldub_kernel(i32 %207)
  %209 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %210 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %209, i64 0, i32 13, i32 1
  %211 = load i32, i32* %210, align 4
  %212 = add i32 %211, %93
  %213 = tail call fastcc i32 @ldub_kernel(i32 %212)
  %214 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %215 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 13, i32 1
  %216 = load i32, i32* %215, align 4
  tail call fastcc void @stb_kernel(i32 %216, i32 %208)
  %217 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %218 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %217, i64 0, i32 13, i32 1
  %219 = load i32, i32* %218, align 4
  %220 = add i32 %219, %93
  tail call fastcc void @stb_kernel(i32 %220, i32 %213)
  %221 = icmp eq i32 %3, 1
  %222 = or i32 %3, 1
  %223 = icmp eq i32 %222, 1
  br i1 %223, label %224, label %235

; <label>:224:                                    ; preds = %200
  %225 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %226 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %225, i64 0, i32 14, i32 1
  %227 = load i32, i32* %226, align 4
  %228 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %225, i64 0, i32 13, i32 0
  %229 = load i32, i32* %228, align 8
  %230 = and i32 %229, -8
  %231 = add i32 %227, 4
  %232 = add i32 %231, %230
  %233 = tail call fastcc i32 @ldl_kernel(i32 %232)
  %234 = and i32 %233, -513
  tail call fastcc void @stl_kernel(i32 %232, i32 %234)
  br label %235

; <label>:235:                                    ; preds = %200, %224
  %236 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %237 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %236, i64 0, i32 7
  %238 = load i32, i32* %237, align 8
  %239 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %236, i64 0, i32 1
  %240 = load i32, i32* %239, align 16
  %241 = tail call i32 @helper_cc_compute_all(i32 %240) #5
  %242 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %236, i64 0, i32 6
  %243 = load i32, i32* %242, align 4
  %244 = and i32 %243, 1024
  %245 = or i32 %238, %241
  %246 = or i32 %245, %244
  %247 = or i32 %246, 2
  %248 = and i32 %247, -16385
  %249 = select i1 %221, i32 %248, i32 %247
  %250 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %236, i64 0, i32 13, i32 1
  %251 = load i32, i32* %250, align 4
  br i1 %65, label %252, label %342

; <label>:252:                                    ; preds = %235
  %253 = add i32 %251, 32
  tail call fastcc void @stl_kernel(i32 %253, i32 %4)
  %254 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %255 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %254, i64 0, i32 13, i32 1
  %256 = load i32, i32* %255, align 4
  %257 = add i32 %256, 36
  tail call fastcc void @stl_kernel(i32 %257, i32 %249)
  %258 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %259 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %258, i64 0, i32 13, i32 1
  %260 = load i32, i32* %259, align 4
  %261 = add i32 %260, 40
  %262 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %258, i64 0, i32 0, i64 0
  %263 = load i32, i32* %262, align 16
  tail call fastcc void @stl_kernel(i32 %261, i32 %263)
  %264 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %265 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %264, i64 0, i32 13, i32 1
  %266 = load i32, i32* %265, align 4
  %267 = add i32 %266, 44
  %268 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %264, i64 0, i32 0, i64 1
  %269 = load i32, i32* %268, align 4
  tail call fastcc void @stl_kernel(i32 %267, i32 %269)
  %270 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %271 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %270, i64 0, i32 13, i32 1
  %272 = load i32, i32* %271, align 4
  %273 = add i32 %272, 48
  %274 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %270, i64 0, i32 0, i64 2
  %275 = load i32, i32* %274, align 8
  tail call fastcc void @stl_kernel(i32 %273, i32 %275)
  %276 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %277 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %276, i64 0, i32 13, i32 1
  %278 = load i32, i32* %277, align 4
  %279 = add i32 %278, 52
  %280 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %276, i64 0, i32 0, i64 3
  %281 = load i32, i32* %280, align 4
  tail call fastcc void @stl_kernel(i32 %279, i32 %281)
  %282 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %283 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %282, i64 0, i32 13, i32 1
  %284 = load i32, i32* %283, align 4
  %285 = add i32 %284, 56
  %286 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %282, i64 0, i32 0, i64 4
  %287 = load i32, i32* %286, align 16
  tail call fastcc void @stl_kernel(i32 %285, i32 %287)
  %288 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %289 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %288, i64 0, i32 13, i32 1
  %290 = load i32, i32* %289, align 4
  %291 = add i32 %290, 60
  %292 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %288, i64 0, i32 0, i64 5
  %293 = load i32, i32* %292, align 4
  tail call fastcc void @stl_kernel(i32 %291, i32 %293)
  %294 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %295 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %294, i64 0, i32 13, i32 1
  %296 = load i32, i32* %295, align 4
  %297 = add i32 %296, 64
  %298 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %294, i64 0, i32 0, i64 6
  %299 = load i32, i32* %298, align 8
  tail call fastcc void @stl_kernel(i32 %297, i32 %299)
  %300 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %301 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %300, i64 0, i32 13, i32 1
  %302 = load i32, i32* %301, align 4
  %303 = add i32 %302, 68
  %304 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %300, i64 0, i32 0, i64 7
  %305 = load i32, i32* %304, align 4
  tail call fastcc void @stl_kernel(i32 %303, i32 %305)
  %306 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %307 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %306, i64 0, i32 13, i32 1
  %308 = load i32, i32* %307, align 4
  %309 = add i32 %308, 72
  %310 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %306, i64 0, i32 11, i64 0, i32 0
  %311 = load i32, i32* %310, align 8
  tail call fastcc void @stw_kernel(i32 %309, i32 %311)
  %312 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %313 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %312, i64 0, i32 13, i32 1
  %314 = load i32, i32* %313, align 4
  %315 = add i32 %314, 76
  %316 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %312, i64 0, i32 11, i64 1, i32 0
  %317 = load i32, i32* %316, align 8
  tail call fastcc void @stw_kernel(i32 %315, i32 %317)
  %318 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %319 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %318, i64 0, i32 13, i32 1
  %320 = load i32, i32* %319, align 4
  %321 = add i32 %320, 80
  %322 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %318, i64 0, i32 11, i64 2, i32 0
  %323 = load i32, i32* %322, align 8
  tail call fastcc void @stw_kernel(i32 %321, i32 %323)
  %324 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %325 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %324, i64 0, i32 13, i32 1
  %326 = load i32, i32* %325, align 4
  %327 = add i32 %326, 84
  %328 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %324, i64 0, i32 11, i64 3, i32 0
  %329 = load i32, i32* %328, align 8
  tail call fastcc void @stw_kernel(i32 %327, i32 %329)
  %330 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %331 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %330, i64 0, i32 13, i32 1
  %332 = load i32, i32* %331, align 4
  %333 = add i32 %332, 88
  %334 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %330, i64 0, i32 11, i64 4, i32 0
  %335 = load i32, i32* %334, align 8
  tail call fastcc void @stw_kernel(i32 %333, i32 %335)
  %336 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %337 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %336, i64 0, i32 13, i32 1
  %338 = load i32, i32* %337, align 4
  %339 = add i32 %338, 92
  %340 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %336, i64 0, i32 11, i64 5, i32 0
  %341 = load i32, i32* %340, align 8
  tail call fastcc void @stw_kernel(i32 %339, i32 %341)
  br label %420

; <label>:342:                                    ; preds = %235
  %343 = add i32 %251, 14
  tail call fastcc void @stw_kernel(i32 %343, i32 %4)
  %344 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %345 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %344, i64 0, i32 13, i32 1
  %346 = load i32, i32* %345, align 4
  %347 = add i32 %346, 16
  tail call fastcc void @stw_kernel(i32 %347, i32 %249)
  %348 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %349 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %348, i64 0, i32 13, i32 1
  %350 = load i32, i32* %349, align 4
  %351 = add i32 %350, 18
  %352 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %348, i64 0, i32 0, i64 0
  %353 = load i32, i32* %352, align 16
  tail call fastcc void @stw_kernel(i32 %351, i32 %353)
  %354 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %355 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %354, i64 0, i32 13, i32 1
  %356 = load i32, i32* %355, align 4
  %357 = add i32 %356, 20
  %358 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %354, i64 0, i32 0, i64 1
  %359 = load i32, i32* %358, align 4
  tail call fastcc void @stw_kernel(i32 %357, i32 %359)
  %360 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %361 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %360, i64 0, i32 13, i32 1
  %362 = load i32, i32* %361, align 4
  %363 = add i32 %362, 22
  %364 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %360, i64 0, i32 0, i64 2
  %365 = load i32, i32* %364, align 8
  tail call fastcc void @stw_kernel(i32 %363, i32 %365)
  %366 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %367 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %366, i64 0, i32 13, i32 1
  %368 = load i32, i32* %367, align 4
  %369 = add i32 %368, 24
  %370 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %366, i64 0, i32 0, i64 3
  %371 = load i32, i32* %370, align 4
  tail call fastcc void @stw_kernel(i32 %369, i32 %371)
  %372 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %373 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %372, i64 0, i32 13, i32 1
  %374 = load i32, i32* %373, align 4
  %375 = add i32 %374, 26
  %376 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %372, i64 0, i32 0, i64 4
  %377 = load i32, i32* %376, align 16
  tail call fastcc void @stw_kernel(i32 %375, i32 %377)
  %378 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %379 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %378, i64 0, i32 13, i32 1
  %380 = load i32, i32* %379, align 4
  %381 = add i32 %380, 28
  %382 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %378, i64 0, i32 0, i64 5
  %383 = load i32, i32* %382, align 4
  tail call fastcc void @stw_kernel(i32 %381, i32 %383)
  %384 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %385 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %384, i64 0, i32 13, i32 1
  %386 = load i32, i32* %385, align 4
  %387 = add i32 %386, 30
  %388 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %384, i64 0, i32 0, i64 6
  %389 = load i32, i32* %388, align 8
  tail call fastcc void @stw_kernel(i32 %387, i32 %389)
  %390 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %391 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %390, i64 0, i32 13, i32 1
  %392 = load i32, i32* %391, align 4
  %393 = add i32 %392, 32
  %394 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %390, i64 0, i32 0, i64 7
  %395 = load i32, i32* %394, align 4
  tail call fastcc void @stw_kernel(i32 %393, i32 %395)
  %396 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %397 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %396, i64 0, i32 13, i32 1
  %398 = load i32, i32* %397, align 4
  %399 = add i32 %398, 34
  %400 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %396, i64 0, i32 11, i64 0, i32 0
  %401 = load i32, i32* %400, align 8
  tail call fastcc void @stw_kernel(i32 %399, i32 %401)
  %402 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %403 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %402, i64 0, i32 13, i32 1
  %404 = load i32, i32* %403, align 4
  %405 = add i32 %404, 38
  %406 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %402, i64 0, i32 11, i64 1, i32 0
  %407 = load i32, i32* %406, align 8
  tail call fastcc void @stw_kernel(i32 %405, i32 %407)
  %408 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %409 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %408, i64 0, i32 13, i32 1
  %410 = load i32, i32* %409, align 4
  %411 = add i32 %410, 42
  %412 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %408, i64 0, i32 11, i64 2, i32 0
  %413 = load i32, i32* %412, align 8
  tail call fastcc void @stw_kernel(i32 %411, i32 %413)
  %414 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %415 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %414, i64 0, i32 13, i32 1
  %416 = load i32, i32* %415, align 4
  %417 = add i32 %416, 46
  %418 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %414, i64 0, i32 11, i64 3, i32 0
  %419 = load i32, i32* %418, align 8
  tail call fastcc void @stw_kernel(i32 %417, i32 %419)
  br label %420

; <label>:420:                                    ; preds = %342, %252
  %421 = icmp eq i32 %3, 2
  br i1 %421, label %422, label %427

; <label>:422:                                    ; preds = %420
  %423 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %424 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %423, i64 0, i32 13, i32 0
  %425 = load i32, i32* %424, align 8
  tail call fastcc void @stw_kernel(i32 %80, i32 %425)
  %426 = or i32 %204, 16384
  br label %430

; <label>:427:                                    ; preds = %420
  %428 = or i32 %3, 2
  %429 = icmp eq i32 %428, 2
  br i1 %429, label %430, label %440

; <label>:430:                                    ; preds = %422, %427
  %431 = phi i32 [ %426, %422 ], [ %204, %427 ]
  %432 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %433 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %432, i64 0, i32 14, i32 1
  %434 = load i32, i32* %433, align 4
  %435 = and i32 %58, -8
  %436 = or i32 %435, 4
  %437 = add i32 %436, %434
  %438 = tail call fastcc i32 @ldl_kernel(i32 %437)
  %439 = or i32 %438, 512
  tail call fastcc void @stl_kernel(i32 %437, i32 %439)
  br label %440

; <label>:440:                                    ; preds = %427, %430
  %441 = phi i32 [ %204, %427 ], [ %431, %430 ]
  %442 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %443 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %442, i64 0, i32 16, i64 0
  %444 = load i32, i32* %443, align 8
  %445 = or i32 %444, 8
  store i32 %445, i32* %443, align 8
  %446 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %442, i64 0, i32 8
  %447 = load i32, i32* %446, align 4
  %448 = or i32 %447, 2048
  store i32 %448, i32* %446, align 4
  %449 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %442, i64 0, i32 13, i32 0
  store i32 %58, i32* %449, align 8
  %450 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %442, i64 0, i32 13, i32 1
  store i32 %80, i32* %450, align 4
  %451 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %442, i64 0, i32 13, i32 2
  store i32 %74, i32* %451, align 8
  %452 = and i32 %56, -513
  %453 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %442, i64 0, i32 13, i32 3
  store i32 %452, i32* %453, align 4
  %454 = icmp slt i32 %445, 0
  %455 = and i1 %65, %454
  br i1 %455, label %456, label %458

; <label>:456:                                    ; preds = %440
  tail call void @cpu_x86_update_cr3(%struct.CPUX86State* %442, i32 %202)
  %457 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %458

; <label>:458:                                    ; preds = %456, %440
  %459 = phi %struct.CPUX86State* [ %457, %456 ], [ %442, %440 ]
  %460 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %459, i64 0, i32 5
  store i32 %203, i32* %460, align 16
  %461 = select i1 %65, i32 2585344, i32 29440
  %462 = and i32 %441, 2261
  %463 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %459, i64 0, i32 2
  store i32 %462, i32* %463, align 4
  %464 = lshr i32 %441, 9
  %465 = and i32 %464, 2
  %466 = xor i32 %465, 2
  %467 = add nsw i32 %466, -1
  %468 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %459, i64 0, i32 6
  store i32 %467, i32* %468, align 4
  %469 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %459, i64 0, i32 7
  %470 = load i32, i32* %469, align 8
  %471 = xor i32 %461, -1
  %472 = and i32 %470, %471
  %473 = and i32 %441, %461
  %474 = or i32 %472, %473
  store i32 %474, i32* %469, align 8
  %475 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 0
  %476 = load i32, i32* %475, align 16
  %477 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %459, i64 0, i32 0, i64 0
  store i32 %476, i32* %477, align 16
  %478 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 1
  %479 = load i32, i32* %478, align 4
  %480 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %481 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %480, i64 0, i32 0, i64 1
  store i32 %479, i32* %481, align 4
  %482 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 2
  %483 = load i32, i32* %482, align 8
  %484 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %485 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 0, i64 2
  store i32 %483, i32* %485, align 8
  %486 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 3
  %487 = load i32, i32* %486, align 4
  %488 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 0, i64 3
  store i32 %487, i32* %488, align 4
  %489 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 4
  %490 = load i32, i32* %489, align 16
  %491 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 0, i64 4
  store i32 %490, i32* %491, align 16
  %492 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 5
  %493 = load i32, i32* %492, align 4
  %494 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 0, i64 5
  store i32 %493, i32* %494, align 4
  %495 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 6
  %496 = load i32, i32* %495, align 8
  %497 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 0, i64 6
  store i32 %496, i32* %497, align 8
  %498 = getelementptr inbounds [8 x i32], [8 x i32]* %6, i64 0, i64 7
  %499 = load i32, i32* %498, align 4
  %500 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 0, i64 7
  store i32 %499, i32* %500, align 4
  %501 = and i32 %441, 131072
  %502 = icmp ne i32 %501, 0
  br i1 %502, label %503, label %586

; <label>:503:                                    ; preds = %458
  %504 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 11, i64 1, i32 3
  %505 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 8
  %506 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 11, i64 2, i32 3
  %507 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 16, i64 0
  %508 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 7
  %509 = bitcast i32* %508 to i64*
  %510 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 11, i64 3, i32 1
  %511 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 11, i64 0, i32 1
  %512 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 11, i64 2, i32 1
  br label %513

; <label>:513:                                    ; preds = %566, %503
  %514 = phi i64 [ 0, %503 ], [ %571, %566 ]
  %515 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 %514
  %516 = load i32, i32* %515, align 4
  %517 = and i32 %516, 65535
  %518 = shl nuw nsw i32 %517, 4
  %519 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 11, i64 %514, i32 0
  store i32 %517, i32* %519, align 4
  %520 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 11, i64 %514, i32 1
  store i32 %518, i32* %520, align 4
  %521 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 11, i64 %514, i32 2
  store i32 65535, i32* %521, align 4
  %522 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 11, i64 %514, i32 3
  store i32 0, i32* %522, align 4
  %523 = icmp eq i64 %514, 1
  br i1 %523, label %526, label %524

; <label>:524:                                    ; preds = %513
  %525 = load i32, i32* %505, align 4
  br label %533

; <label>:526:                                    ; preds = %513
  %527 = load i32, i32* %504, align 4
  %528 = lshr i32 %527, 18
  %529 = and i32 %528, 16
  %530 = load i32, i32* %505, align 4
  %531 = and i32 %530, -32785
  %532 = or i32 %531, %529
  store i32 %532, i32* %505, align 4
  br label %533

; <label>:533:                                    ; preds = %526, %524
  %534 = phi i32 [ %525, %524 ], [ %532, %526 ]
  %535 = load i32, i32* %506, align 4
  %536 = lshr i32 %535, 17
  %537 = and i32 %536, 32
  %538 = trunc i32 %534 to i16
  %539 = icmp slt i16 %538, 0
  br i1 %539, label %566, label %540

; <label>:540:                                    ; preds = %533
  %541 = load i32, i32* %507, align 8
  %542 = and i32 %541, 1
  %543 = icmp eq i32 %542, 0
  br i1 %543, label %553, label %544

; <label>:544:                                    ; preds = %540
  %545 = load i64, i64* %509, align 8
  %546 = and i64 %545, 131072
  %547 = icmp ne i64 %546, 0
  %548 = and i32 %534, 16
  %549 = icmp eq i32 %548, 0
  %550 = or i1 %549, %547
  %551 = lshr i64 %545, 32
  %552 = trunc i64 %551 to i32
  br i1 %550, label %553, label %556

; <label>:553:                                    ; preds = %544, %540
  %554 = phi i32 [ %552, %544 ], [ %534, %540 ]
  %555 = or i32 %537, 64
  br label %566

; <label>:556:                                    ; preds = %544
  %557 = load i32, i32* %510, align 4
  %558 = load i32, i32* %511, align 4
  %559 = or i32 %558, %557
  %560 = load i32, i32* %512, align 4
  %561 = or i32 %559, %560
  %562 = icmp ne i32 %561, 0
  %563 = zext i1 %562 to i32
  %564 = shl nuw nsw i32 %563, 6
  %565 = or i32 %564, %537
  br label %566

; <label>:566:                                    ; preds = %533, %553, %556
  %567 = phi i32 [ %534, %533 ], [ %554, %553 ], [ %552, %556 ]
  %568 = phi i32 [ %537, %533 ], [ %555, %553 ], [ %565, %556 ]
  %569 = and i32 %567, -97
  %570 = or i32 %569, %568
  store i32 %570, i32* %505, align 4
  %571 = add nuw nsw i64 %514, 1
  %572 = icmp eq i64 %571, 6
  br i1 %572, label %573, label %513

; <label>:573:                                    ; preds = %566
  %574 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %575 = load i32, i32* %574, align 4
  %576 = icmp eq i32 %575, 0
  br i1 %576, label %582, label %577, !prof !2

; <label>:577:                                    ; preds = %573
  %578 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  %579 = and i32 %570, 3
  tail call void %578(i32 %579, i32 3)
  %580 = load i32, i32* %505, align 4
  %581 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %582

; <label>:582:                                    ; preds = %573, %577
  %583 = phi %struct.CPUX86State* [ %581, %577 ], [ %484, %573 ]
  %584 = phi i32 [ %580, %577 ], [ %570, %573 ]
  %585 = or i32 %584, 3
  store i32 %585, i32* %505, align 4
  br label %674

; <label>:586:                                    ; preds = %458
  %587 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 1
  %588 = load i32, i32* %587, align 4
  %589 = and i32 %588, 3
  %590 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %591 = load i32, i32* %590, align 4
  %592 = icmp eq i32 %591, 0
  br i1 %592, label %593, label %595, !prof !2

; <label>:593:                                    ; preds = %586
  %594 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 8
  br label %601

; <label>:595:                                    ; preds = %586
  %596 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  %597 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %484, i64 0, i32 8
  %598 = load i32, i32* %597, align 4
  %599 = and i32 %598, 3
  tail call void %596(i32 %599, i32 %589)
  %600 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %601

; <label>:601:                                    ; preds = %593, %595
  %602 = phi %struct.CPUX86State* [ %484, %593 ], [ %600, %595 ]
  %603 = phi i32* [ %594, %593 ], [ %597, %595 ]
  %604 = load i32, i32* %603, align 4
  %605 = and i32 %604, -4
  %606 = or i32 %605, %589
  store i32 %606, i32* %603, align 4
  %607 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %602, i64 0, i32 11, i64 1, i32 3
  %608 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %602, i64 0, i32 8
  %609 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %602, i64 0, i32 11, i64 2, i32 3
  %610 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %602, i64 0, i32 16, i64 0
  %611 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %602, i64 0, i32 7
  %612 = bitcast i32* %611 to i64*
  %613 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %602, i64 0, i32 11, i64 3, i32 1
  %614 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %602, i64 0, i32 11, i64 0, i32 1
  %615 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %602, i64 0, i32 11, i64 2, i32 1
  br label %616

; <label>:616:                                    ; preds = %667, %601
  %617 = phi i64 [ 0, %601 ], [ %672, %667 ]
  %618 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 %617
  %619 = load i32, i32* %618, align 4
  %620 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %602, i64 0, i32 11, i64 %617, i32 0
  store i32 %619, i32* %620, align 4
  %621 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %602, i64 0, i32 11, i64 %617, i32 1
  store i32 0, i32* %621, align 4
  %622 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %602, i64 0, i32 11, i64 %617, i32 2
  store i32 0, i32* %622, align 4
  %623 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %602, i64 0, i32 11, i64 %617, i32 3
  store i32 0, i32* %623, align 4
  %624 = icmp eq i64 %617, 1
  br i1 %624, label %627, label %625

; <label>:625:                                    ; preds = %616
  %626 = load i32, i32* %608, align 4
  br label %634

; <label>:627:                                    ; preds = %616
  %628 = load i32, i32* %607, align 4
  %629 = lshr i32 %628, 18
  %630 = and i32 %629, 16
  %631 = load i32, i32* %608, align 4
  %632 = and i32 %631, -32785
  %633 = or i32 %632, %630
  store i32 %633, i32* %608, align 4
  br label %634

; <label>:634:                                    ; preds = %627, %625
  %635 = phi i32 [ %626, %625 ], [ %633, %627 ]
  %636 = load i32, i32* %609, align 4
  %637 = lshr i32 %636, 17
  %638 = and i32 %637, 32
  %639 = trunc i32 %635 to i16
  %640 = icmp slt i16 %639, 0
  br i1 %640, label %667, label %641

; <label>:641:                                    ; preds = %634
  %642 = load i32, i32* %610, align 8
  %643 = and i32 %642, 1
  %644 = icmp eq i32 %643, 0
  br i1 %644, label %654, label %645

; <label>:645:                                    ; preds = %641
  %646 = load i64, i64* %612, align 8
  %647 = and i64 %646, 131072
  %648 = icmp ne i64 %647, 0
  %649 = and i32 %635, 16
  %650 = icmp eq i32 %649, 0
  %651 = or i1 %650, %648
  %652 = lshr i64 %646, 32
  %653 = trunc i64 %652 to i32
  br i1 %651, label %654, label %657

; <label>:654:                                    ; preds = %645, %641
  %655 = phi i32 [ %653, %645 ], [ %635, %641 ]
  %656 = or i32 %638, 64
  br label %667

; <label>:657:                                    ; preds = %645
  %658 = load i32, i32* %613, align 4
  %659 = load i32, i32* %614, align 4
  %660 = or i32 %659, %658
  %661 = load i32, i32* %615, align 4
  %662 = or i32 %660, %661
  %663 = icmp ne i32 %662, 0
  %664 = zext i1 %663 to i32
  %665 = shl nuw nsw i32 %664, 6
  %666 = or i32 %665, %638
  br label %667

; <label>:667:                                    ; preds = %634, %654, %657
  %668 = phi i32 [ %635, %634 ], [ %655, %654 ], [ %653, %657 ]
  %669 = phi i32 [ %638, %634 ], [ %656, %654 ], [ %666, %657 ]
  %670 = and i32 %668, -97
  %671 = or i32 %670, %669
  store i32 %671, i32* %608, align 4
  %672 = add nuw nsw i64 %617, 1
  %673 = icmp eq i64 %672, 6
  br i1 %673, label %674, label %616

; <label>:674:                                    ; preds = %667, %582
  %675 = phi %struct.CPUX86State* [ %583, %582 ], [ %602, %667 ]
  %676 = phi i32 [ 3, %582 ], [ %589, %667 ]
  %677 = and i32 %201, -5
  %678 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %675, i64 0, i32 12, i32 0
  store i32 %677, i32* %678, align 8
  %679 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %675, i64 0, i32 12, i32 1
  store i32 0, i32* %679, align 4
  %680 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %675, i64 0, i32 12, i32 2
  store i32 0, i32* %680, align 8
  %681 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %675, i64 0, i32 12, i32 3
  store i32 0, i32* %681, align 4
  %682 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %683 = load i32, i32* %682, align 4
  %684 = icmp eq i32 %683, 0
  br i1 %684, label %687, label %685, !prof !2

; <label>:685:                                    ; preds = %674
  %686 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  tail call void %686(i32 %13, i32 %676)
  br label %687

; <label>:687:                                    ; preds = %674, %685
  %688 = and i32 %201, 4
  %689 = icmp eq i32 %688, 0
  %690 = and i32 %201, 65532
  br i1 %689, label %692, label %691

; <label>:691:                                    ; preds = %687
  tail call fastcc void @raise_exception_err(i32 10, i32 %690) #12
  unreachable

; <label>:692:                                    ; preds = %687
  %693 = icmp eq i32 %690, 0
  br i1 %693, label %735, label %694

; <label>:694:                                    ; preds = %692
  %695 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %696 = or i32 %201, 7
  %697 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %695, i64 0, i32 14, i32 2
  %698 = load i32, i32* %697, align 4
  %699 = icmp ugt i32 %696, %698
  br i1 %699, label %700, label %701

; <label>:700:                                    ; preds = %694
  tail call fastcc void @raise_exception_err(i32 10, i32 %690) #12
  unreachable

; <label>:701:                                    ; preds = %694
  %702 = and i32 %201, -8
  %703 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %695, i64 0, i32 14, i32 1
  %704 = load i32, i32* %703, align 4
  %705 = add i32 %704, %702
  %706 = tail call fastcc i32 @ldl_kernel(i32 %705)
  %707 = add i32 %705, 4
  %708 = tail call fastcc i32 @ldl_kernel(i32 %707)
  %709 = and i32 %708, 7936
  %710 = icmp eq i32 %709, 512
  br i1 %710, label %712, label %711

; <label>:711:                                    ; preds = %701
  tail call fastcc void @raise_exception_err(i32 10, i32 %690) #12
  unreachable

; <label>:712:                                    ; preds = %701
  %713 = trunc i32 %708 to i16
  %714 = icmp slt i16 %713, 0
  br i1 %714, label %716, label %715

; <label>:715:                                    ; preds = %712
  tail call fastcc void @raise_exception_err(i32 10, i32 %690) #12
  unreachable

; <label>:716:                                    ; preds = %712
  %717 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %718 = lshr i32 %706, 16
  %719 = shl i32 %708, 16
  %720 = and i32 %719, 16711680
  %721 = and i32 %708, -16777216
  %722 = or i32 %721, %718
  %723 = or i32 %722, %720
  %724 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %717, i64 0, i32 12, i32 1
  store i32 %723, i32* %724, align 4
  %725 = and i32 %706, 65535
  %726 = and i32 %708, 983040
  %727 = or i32 %726, %725
  %728 = and i32 %708, 8388608
  %729 = icmp eq i32 %728, 0
  %730 = shl nuw i32 %727, 12
  %731 = or i32 %730, 4095
  %732 = select i1 %729, i32 %727, i32 %731
  %733 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %717, i64 0, i32 12, i32 2
  store i32 %732, i32* %733, align 4
  %734 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %717, i64 0, i32 12, i32 3
  store i32 %708, i32* %734, align 4
  br label %735

; <label>:735:                                    ; preds = %692, %716
  br i1 %502, label %753, label %736

; <label>:736:                                    ; preds = %735
  %737 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 1
  %738 = load i32, i32* %737, align 4
  tail call fastcc void @tss_load_seg(i32 1, i32 %738)
  %739 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 2
  %740 = bitcast i32* %739 to i64*
  %741 = load i64, i64* %740, align 8
  %742 = trunc i64 %741 to i32
  tail call fastcc void @tss_load_seg(i32 2, i32 %742)
  %743 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 0
  %744 = load i32, i32* %743, align 16
  tail call fastcc void @tss_load_seg(i32 0, i32 %744)
  %745 = lshr i64 %741, 32
  %746 = trunc i64 %745 to i32
  tail call fastcc void @tss_load_seg(i32 3, i32 %746)
  %747 = getelementptr inbounds [6 x i32], [6 x i32]* %7, i64 0, i64 4
  %748 = bitcast i32* %747 to i64*
  %749 = load i64, i64* %748, align 16
  %750 = trunc i64 %749 to i32
  tail call fastcc void @tss_load_seg(i32 4, i32 %750)
  %751 = lshr i64 %749, 32
  %752 = trunc i64 %751 to i32
  tail call fastcc void @tss_load_seg(i32 5, i32 %752)
  br label %753

; <label>:753:                                    ; preds = %736, %735
  %754 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %755 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %754, i64 0, i32 11, i64 1, i32 2
  %756 = load i32, i32* %755, align 8
  %757 = icmp ugt i32 %203, %756
  br i1 %757, label %758, label %759

; <label>:758:                                    ; preds = %753
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:759:                                    ; preds = %753
  %760 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %754, i64 0, i32 61, i64 7
  %761 = load i32, i32* %760, align 4
  %762 = and i32 %761, 85
  %763 = icmp eq i32 %762, 0
  br i1 %763, label %776, label %764

; <label>:764:                                    ; preds = %759
  %765 = and i32 %761, 3
  %766 = icmp eq i32 %765, 1
  br i1 %766, label %767, label %771

; <label>:767:                                    ; preds = %764
  tail call void @hw_breakpoint_remove(%struct.CPUX86State* nonnull %754, i32 0)
  %768 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %769 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %768, i64 0, i32 61, i64 7
  %770 = load i32, i32* %769, align 4
  br label %771

; <label>:771:                                    ; preds = %764, %767
  %772 = phi i32 [ %761, %764 ], [ %770, %767 ]
  %773 = phi %struct.CPUX86State* [ %754, %764 ], [ %768, %767 ]
  %774 = and i32 %772, 12
  %775 = icmp eq i32 %774, 4
  br i1 %775, label %777, label %781

; <label>:776:                                    ; preds = %759, %799
  ret void

; <label>:777:                                    ; preds = %771
  tail call void @hw_breakpoint_remove(%struct.CPUX86State* %773, i32 1)
  %778 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %779 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %778, i64 0, i32 61, i64 7
  %780 = load i32, i32* %779, align 4
  br label %781

; <label>:781:                                    ; preds = %777, %771
  %782 = phi i32 [ %780, %777 ], [ %772, %771 ]
  %783 = phi %struct.CPUX86State* [ %778, %777 ], [ %773, %771 ]
  %784 = and i32 %782, 48
  %785 = icmp eq i32 %784, 16
  br i1 %785, label %786, label %790

; <label>:786:                                    ; preds = %781
  tail call void @hw_breakpoint_remove(%struct.CPUX86State* %783, i32 2)
  %787 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %788 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %787, i64 0, i32 61, i64 7
  %789 = load i32, i32* %788, align 4
  br label %790

; <label>:790:                                    ; preds = %786, %781
  %791 = phi i32 [ %789, %786 ], [ %782, %781 ]
  %792 = phi %struct.CPUX86State* [ %787, %786 ], [ %783, %781 ]
  %793 = and i32 %791, 192
  %794 = icmp eq i32 %793, 64
  br i1 %794, label %795, label %799

; <label>:795:                                    ; preds = %790
  tail call void @hw_breakpoint_remove(%struct.CPUX86State* %792, i32 3)
  %796 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %797 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %796, i64 0, i32 61, i64 7
  %798 = load i32, i32* %797, align 4
  br label %799

; <label>:799:                                    ; preds = %795, %790
  %800 = phi i32 [ %798, %795 ], [ %791, %790 ]
  %801 = phi %struct.CPUX86State* [ %796, %795 ], [ %792, %790 ]
  %802 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %801, i64 0, i32 61, i64 7
  %803 = and i32 %800, -86
  store i32 %803, i32* %802, align 4
  br label %776
}

; Function Attrs: uwtable
define internal fastcc void @stl_kernel(i32, i32) unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %8, label %6

; <label>:6:                                      ; preds = %2
  %7 = zext i32 %1 to i64
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 %7, i32 4, i32 1)
  br label %8

; <label>:8:                                      ; preds = %2, %6
  %9 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %10 = load i32, i32* %9, align 4
  %11 = icmp eq i32 %10, 0
  br i1 %11, label %14, label %12

; <label>:12:                                     ; preds = %8
  %13 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %14

; <label>:14:                                     ; preds = %8, %12
  %15 = phi i32 [ %13, %12 ], [ %0, %8 ]
  %16 = lshr i32 %15, 12
  %17 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %16, i32 0, i32 1048575, i32 0)
  %18 = and i32 %17, 1023
  %19 = zext i32 %18 to i64
  %20 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %21 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 0, i64 %19, i32 1
  %22 = load i32, i32* %21, align 4
  %23 = and i32 %15, -4093
  %24 = icmp eq i32 %22, %23
  br i1 %24, label %26, label %25, !prof !2

; <label>:25:                                     ; preds = %14
  tail call void @__stl_mmu(i32 %15, i32 %1, i32 0)
  br label %37

; <label>:26:                                     ; preds = %14
  %27 = zext i32 %15 to i64
  %28 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 0, i64 %19, i32 4
  %29 = load i64, i64* %28, align 8
  %30 = add i64 %29, %27
  %31 = inttoptr i64 %30 to i32*
  store i32 %1, i32* %31, align 4
  %32 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %33 = load i32, i32* %32, align 4
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %37, label %35

; <label>:35:                                     ; preds = %26
  %36 = zext i32 %1 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %15, i64 %36, i32 4, i32 2, i64 0)
  br label %37

; <label>:37:                                     ; preds = %26, %35, %25
  ret void
}

; Function Attrs: uwtable
define internal fastcc void @stw_kernel(i32, i32) unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %8, label %6

; <label>:6:                                      ; preds = %2
  %7 = zext i32 %1 to i64
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 %7, i32 4, i32 1)
  br label %8

; <label>:8:                                      ; preds = %2, %6
  %9 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %10 = load i32, i32* %9, align 4
  %11 = icmp eq i32 %10, 0
  br i1 %11, label %14, label %12

; <label>:12:                                     ; preds = %8
  %13 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %14

; <label>:14:                                     ; preds = %8, %12
  %15 = phi i32 [ %13, %12 ], [ %0, %8 ]
  %16 = lshr i32 %15, 12
  %17 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %16, i32 0, i32 1048575, i32 0)
  %18 = and i32 %17, 1023
  %19 = zext i32 %18 to i64
  %20 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %21 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 0, i64 %19, i32 1
  %22 = load i32, i32* %21, align 4
  %23 = and i32 %15, -4095
  %24 = icmp eq i32 %22, %23
  br i1 %24, label %27, label %25, !prof !2

; <label>:25:                                     ; preds = %14
  %26 = trunc i32 %1 to i16
  tail call void @__stw_mmu(i32 %15, i16 zeroext %26, i32 0)
  br label %39

; <label>:27:                                     ; preds = %14
  %28 = zext i32 %15 to i64
  %29 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 0, i64 %19, i32 4
  %30 = load i64, i64* %29, align 8
  %31 = add i64 %30, %28
  %32 = trunc i32 %1 to i16
  %33 = inttoptr i64 %31 to i16*
  store i16 %32, i16* %33, align 2
  %34 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %35 = load i32, i32* %34, align 4
  %36 = icmp eq i32 %35, 0
  br i1 %36, label %39, label %37

; <label>:37:                                     ; preds = %27
  %38 = zext i32 %1 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %15, i64 %38, i32 4, i32 2, i64 0)
  br label %39

; <label>:39:                                     ; preds = %27, %37, %25
  ret void
}

; Function Attrs: noreturn
declare void @cpu_abort(%struct.CPUX86State*, i8*, ...) local_unnamed_addr #7

; Function Attrs: argmemonly nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i32, i1) #8

; Function Attrs: uwtable
define void @__stw_mmu(i32, i16 zeroext, i32) local_unnamed_addr #2 {
  %4 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %5 = load i32, i32* %4, align 4
  %6 = icmp eq i32 %5, 0
  br i1 %6, label %9, label %7

; <label>:7:                                      ; preds = %3
  %8 = zext i16 %1 to i64
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 %8, i32 2, i32 1)
  br label %9

; <label>:9:                                      ; preds = %3, %7
  %10 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %11 = load i32, i32* %10, align 4
  %12 = icmp eq i32 %11, 0
  br i1 %12, label %15, label %13

; <label>:13:                                     ; preds = %9
  %14 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %15

; <label>:15:                                     ; preds = %9, %13
  %16 = phi i32 [ %14, %13 ], [ %0, %9 ]
  %17 = lshr i32 %16, 12
  %18 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %17, i32 0, i32 1048575, i32 0)
  %19 = and i32 %18, 1023
  %20 = zext i32 %19 to i64
  %21 = sext i32 %2 to i64
  %22 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 77, i64 %21, i64 %20, i32 1
  %24 = load i32, i32* %23, align 4
  %25 = and i32 %16, -4096
  %26 = and i32 %24, -4088
  %27 = icmp eq i32 %25, %26
  br i1 %27, label %30, label %28, !prof !2

; <label>:28:                                     ; preds = %15
  %29 = shl i32 %18, 12
  br label %166

; <label>:30:                                     ; preds = %166, %15
  %31 = phi %struct.CPUX86State* [ %22, %15 ], [ %168, %166 ]
  %32 = phi i32 [ %24, %15 ], [ %170, %166 ]
  %33 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 77, i64 %21, i64 %20
  %34 = and i32 %32, 4095
  %35 = icmp eq i32 %34, 0
  br i1 %35, label %64, label %36, !prof !2

; <label>:36:                                     ; preds = %30
  %37 = and i32 %16, 1
  %38 = icmp eq i32 %37, 0
  br i1 %38, label %39, label %68

; <label>:39:                                     ; preds = %36
  %40 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 78, i64 %21, i64 %20
  %41 = load i64, i64* %40, align 8
  %42 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 83
  store %struct.CPUTLBEntry* %33, %struct.CPUTLBEntry** %42, align 16
  %43 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %41)
  %44 = and i64 %41, -4096
  %45 = zext i32 %16 to i64
  %46 = add i64 %44, %45
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %45, i32 0)
  %47 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %48 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %47, i64 0, i32 72
  store i64 0, i64* %48, align 16
  %49 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %43, i32 1)
  br i1 %49, label %50, label %56

; <label>:50:                                     ; preds = %39
  %51 = and i64 %46, -4096
  %52 = tail call i64 @se_notdirty_mem_write(i64 %51)
  %53 = and i64 %46, 4095
  %54 = or i64 %52, %53
  %55 = inttoptr i64 %54 to i16*
  store i16 %1, i16* %55, align 2
  br label %57

; <label>:56:                                     ; preds = %39
  tail call void @io_writew_mmu(i64 %41, i16 zeroext %1, i32 %16, i8* null)
  br label %57

; <label>:57:                                     ; preds = %50, %56
  %58 = zext i16 %1 to i64
  %59 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %45, i64 %58, i32 2, i32 1)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %60 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %61 = load i32, i32* %60, align 4
  %62 = icmp eq i32 %61, 0
  br i1 %62, label %173, label %63

; <label>:63:                                     ; preds = %57
  tail call void @tcg_llvm_after_memory_access(i32 %16, i64 %58, i32 2, i32 3, i64 0)
  br label %173

; <label>:64:                                     ; preds = %30
  %65 = and i32 %16, 4095
  %66 = add nuw nsw i32 %65, 1
  %67 = icmp ugt i32 %66, 4095
  br i1 %67, label %68, label %155, !prof !3

; <label>:68:                                     ; preds = %36, %64
  %69 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %70 = load i32, i32* %69, align 4
  %71 = icmp eq i32 %70, 0
  br i1 %71, label %74, label %72

; <label>:72:                                     ; preds = %68
  %73 = zext i16 %1 to i64
  tail call void @tcg_llvm_before_memory_access(i32 %16, i64 %73, i32 2, i32 1)
  br label %74

; <label>:74:                                     ; preds = %72, %68
  %75 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %76 = load i32, i32* %75, align 4
  %77 = icmp eq i32 %76, 0
  br i1 %77, label %80, label %78

; <label>:78:                                     ; preds = %74
  %79 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %16, i32 0, i32 -1, i32 0)
  br label %80

; <label>:80:                                     ; preds = %78, %74
  %81 = phi i32 [ %79, %78 ], [ %16, %74 ]
  %82 = lshr i32 %81, 12
  %83 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %82, i32 0, i32 1048575, i32 0)
  %84 = and i32 %83, 1023
  %85 = zext i32 %84 to i64
  %86 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %87 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %86, i64 0, i32 77, i64 %21, i64 %85, i32 1
  %88 = load i32, i32* %87, align 4
  %89 = and i32 %81, -4096
  %90 = and i32 %88, -4088
  %91 = icmp eq i32 %89, %90
  br i1 %91, label %94, label %92

; <label>:92:                                     ; preds = %80
  %93 = shl i32 %83, 12
  br label %148

; <label>:94:                                     ; preds = %148, %80
  %95 = phi %struct.CPUX86State* [ %86, %80 ], [ %150, %148 ]
  %96 = phi i32 [ %88, %80 ], [ %152, %148 ]
  %97 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %95, i64 0, i32 77, i64 %21, i64 %85
  %98 = and i32 %96, 4095
  %99 = icmp eq i32 %98, 0
  br i1 %99, label %128, label %100

; <label>:100:                                    ; preds = %94
  %101 = and i32 %81, 1
  %102 = icmp eq i32 %101, 0
  br i1 %102, label %103, label %132

; <label>:103:                                    ; preds = %100
  %104 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %95, i64 0, i32 78, i64 %21, i64 %85
  %105 = load i64, i64* %104, align 8
  %106 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %95, i64 0, i32 83
  store %struct.CPUTLBEntry* %97, %struct.CPUTLBEntry** %106, align 16
  %107 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %105)
  %108 = and i64 %105, -4096
  %109 = zext i32 %81 to i64
  %110 = add i64 %108, %109
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %109, i32 0)
  %111 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %112 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %111, i64 0, i32 72
  store i64 0, i64* %112, align 16
  %113 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %107, i32 1)
  br i1 %113, label %114, label %120

; <label>:114:                                    ; preds = %103
  %115 = and i64 %110, -4096
  %116 = tail call i64 @se_notdirty_mem_write(i64 %115)
  %117 = and i64 %110, 4095
  %118 = or i64 %116, %117
  %119 = inttoptr i64 %118 to i16*
  store i16 %1, i16* %119, align 2
  br label %121

; <label>:120:                                    ; preds = %103
  tail call void @io_writew_mmu(i64 %105, i16 zeroext %1, i32 %81, i8* null)
  br label %121

; <label>:121:                                    ; preds = %120, %114
  %122 = zext i16 %1 to i64
  %123 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %109, i64 %122, i32 2, i32 1)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %124 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %125 = load i32, i32* %124, align 4
  %126 = icmp eq i32 %125, 0
  br i1 %126, label %173, label %127

; <label>:127:                                    ; preds = %121
  tail call void @tcg_llvm_after_memory_access(i32 %81, i64 %122, i32 2, i32 3, i64 0)
  br label %173

; <label>:128:                                    ; preds = %94
  %129 = and i32 %81, 4095
  %130 = add nuw nsw i32 %129, 1
  %131 = icmp ugt i32 %130, 4095
  br i1 %131, label %132, label %137

; <label>:132:                                    ; preds = %128, %100
  %133 = add i32 %81, 1
  %134 = lshr i16 %1, 8
  %135 = trunc i16 %134 to i8
  tail call fastcc void @slow_stb_mmu(i32 %133, i8 zeroext %135, i32 %2)
  %136 = trunc i16 %1 to i8
  tail call fastcc void @slow_stb_mmu(i32 %81, i8 zeroext %136, i32 %2)
  br label %173

; <label>:137:                                    ; preds = %128
  %138 = zext i32 %81 to i64
  %139 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %95, i64 0, i32 77, i64 %21, i64 %85, i32 4
  %140 = load i64, i64* %139, align 8
  %141 = add i64 %140, %138
  %142 = inttoptr i64 %141 to i16*
  store i16 %1, i16* %142, align 2
  %143 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %144 = load i32, i32* %143, align 4
  %145 = icmp eq i32 %144, 0
  br i1 %145, label %173, label %146

; <label>:146:                                    ; preds = %137
  %147 = zext i16 %1 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %81, i64 %147, i32 2, i32 2, i64 0)
  br label %173

; <label>:148:                                    ; preds = %148, %92
  %149 = phi %struct.CPUX86State* [ %86, %92 ], [ %150, %148 ]
  tail call void @tlb_fill(%struct.CPUX86State* %149, i32 %81, i32 %93, i32 1, i32 %2, i8* null)
  %150 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %151 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %150, i64 0, i32 77, i64 %21, i64 %85, i32 1
  %152 = load i32, i32* %151, align 4
  %153 = and i32 %152, -4088
  %154 = icmp eq i32 %89, %153
  br i1 %154, label %94, label %148

; <label>:155:                                    ; preds = %64
  %156 = zext i32 %16 to i64
  %157 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 77, i64 %21, i64 %20, i32 4
  %158 = load i64, i64* %157, align 8
  %159 = add i64 %158, %156
  %160 = inttoptr i64 %159 to i16*
  store i16 %1, i16* %160, align 2
  %161 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %162 = load i32, i32* %161, align 4
  %163 = icmp eq i32 %162, 0
  br i1 %163, label %173, label %164

; <label>:164:                                    ; preds = %155
  %165 = zext i16 %1 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %16, i64 %165, i32 2, i32 2, i64 0)
  br label %173

; <label>:166:                                    ; preds = %28, %166
  %167 = phi %struct.CPUX86State* [ %22, %28 ], [ %168, %166 ]
  tail call void @tlb_fill(%struct.CPUX86State* %167, i32 %16, i32 %29, i32 1, i32 %2, i8* null)
  %168 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %169 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %168, i64 0, i32 77, i64 %21, i64 %20, i32 1
  %170 = load i32, i32* %169, align 4
  %171 = and i32 %170, -4088
  %172 = icmp eq i32 %25, %171
  br i1 %172, label %30, label %166, !prof !2

; <label>:173:                                    ; preds = %146, %137, %132, %127, %121, %57, %155, %63, %164
  ret void
}

declare i64 @se_notdirty_mem_write(i64) local_unnamed_addr #6

declare void @io_writew_mmu(i64, i16 zeroext, i32, i8*) local_unnamed_addr #6

; Function Attrs: uwtable
define internal fastcc void @slow_stb_mmu(i32, i8 zeroext, i32) unnamed_addr #2 {
  %4 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %5 = load i32, i32* %4, align 4
  %6 = icmp eq i32 %5, 0
  br i1 %6, label %9, label %7

; <label>:7:                                      ; preds = %3
  %8 = zext i8 %1 to i64
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 %8, i32 1, i32 1)
  br label %9

; <label>:9:                                      ; preds = %3, %7
  %10 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %11 = load i32, i32* %10, align 4
  %12 = icmp eq i32 %11, 0
  br i1 %12, label %15, label %13

; <label>:13:                                     ; preds = %9
  %14 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %15

; <label>:15:                                     ; preds = %9, %13
  %16 = phi i32 [ %14, %13 ], [ %0, %9 ]
  %17 = lshr i32 %16, 12
  %18 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %17, i32 0, i32 1048575, i32 0)
  %19 = and i32 %18, 1023
  %20 = zext i32 %19 to i64
  %21 = sext i32 %2 to i64
  %22 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 77, i64 %21, i64 %20, i32 1
  %24 = load i32, i32* %23, align 4
  %25 = and i32 %16, -4096
  %26 = and i32 %24, -4088
  %27 = icmp eq i32 %25, %26
  br i1 %27, label %30, label %28

; <label>:28:                                     ; preds = %15
  %29 = shl i32 %18, 12
  br label %72

; <label>:30:                                     ; preds = %72, %15
  %31 = phi %struct.CPUX86State* [ %22, %15 ], [ %74, %72 ]
  %32 = phi i32 [ %24, %15 ], [ %76, %72 ]
  %33 = and i32 %32, 4095
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %61, label %35

; <label>:35:                                     ; preds = %30
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 77, i64 %21, i64 %20
  %37 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 78, i64 %21, i64 %20
  %38 = load i64, i64* %37, align 8
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 83
  store %struct.CPUTLBEntry* %36, %struct.CPUTLBEntry** %39, align 16
  %40 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %38)
  %41 = and i64 %38, -4096
  %42 = zext i32 %16 to i64
  %43 = add i64 %41, %42
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %42, i32 0)
  %44 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %45 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %44, i64 0, i32 72
  store i64 0, i64* %45, align 16
  %46 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %40, i32 1)
  br i1 %46, label %47, label %53

; <label>:47:                                     ; preds = %35
  %48 = and i64 %43, -4096
  %49 = tail call i64 @se_notdirty_mem_write(i64 %48)
  %50 = and i64 %43, 4095
  %51 = or i64 %49, %50
  %52 = inttoptr i64 %51 to i8*
  store i8 %1, i8* %52, align 1
  br label %54

; <label>:53:                                     ; preds = %35
  tail call void @io_writeb_mmu(i64 %38, i8 zeroext %1, i32 %16, i8* null)
  br label %54

; <label>:54:                                     ; preds = %47, %53
  %55 = zext i8 %1 to i64
  %56 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %42, i64 %55, i32 1, i32 1)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %57 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %58 = load i32, i32* %57, align 4
  %59 = icmp eq i32 %58, 0
  br i1 %59, label %79, label %60

; <label>:60:                                     ; preds = %54
  tail call void @tcg_llvm_after_memory_access(i32 %16, i64 %55, i32 1, i32 3, i64 0)
  br label %79

; <label>:61:                                     ; preds = %30
  %62 = zext i32 %16 to i64
  %63 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 77, i64 %21, i64 %20, i32 4
  %64 = load i64, i64* %63, align 8
  %65 = add i64 %64, %62
  %66 = inttoptr i64 %65 to i8*
  store i8 %1, i8* %66, align 1
  %67 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %68 = load i32, i32* %67, align 4
  %69 = icmp eq i32 %68, 0
  br i1 %69, label %79, label %70

; <label>:70:                                     ; preds = %61
  %71 = zext i8 %1 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %16, i64 %71, i32 1, i32 2, i64 0)
  br label %79

; <label>:72:                                     ; preds = %28, %72
  %73 = phi %struct.CPUX86State* [ %22, %28 ], [ %74, %72 ]
  tail call void @tlb_fill(%struct.CPUX86State* %73, i32 %16, i32 %29, i32 1, i32 %2, i8* null)
  %74 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %75 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %74, i64 0, i32 77, i64 %21, i64 %20, i32 1
  %76 = load i32, i32* %75, align 4
  %77 = and i32 %76, -4088
  %78 = icmp eq i32 %25, %77
  br i1 %78, label %30, label %72

; <label>:79:                                     ; preds = %54, %61, %60, %70
  ret void
}

declare void @io_writeb_mmu(i64, i8 zeroext, i32, i8*) local_unnamed_addr #6

; Function Attrs: uwtable
define void @__stl_mmu(i32, i32, i32) local_unnamed_addr #2 {
  %4 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %5 = load i32, i32* %4, align 4
  %6 = icmp eq i32 %5, 0
  br i1 %6, label %9, label %7

; <label>:7:                                      ; preds = %3
  %8 = zext i32 %1 to i64
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 %8, i32 4, i32 1)
  br label %9

; <label>:9:                                      ; preds = %3, %7
  %10 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %11 = load i32, i32* %10, align 4
  %12 = icmp eq i32 %11, 0
  br i1 %12, label %15, label %13

; <label>:13:                                     ; preds = %9
  %14 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %15

; <label>:15:                                     ; preds = %9, %13
  %16 = phi i32 [ %14, %13 ], [ %0, %9 ]
  %17 = lshr i32 %16, 12
  %18 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %17, i32 0, i32 1048575, i32 0)
  %19 = and i32 %18, 1023
  %20 = zext i32 %19 to i64
  %21 = sext i32 %2 to i64
  %22 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 77, i64 %21, i64 %20, i32 1
  %24 = load i32, i32* %23, align 4
  %25 = and i32 %16, -4096
  %26 = and i32 %24, -4088
  %27 = icmp eq i32 %25, %26
  br i1 %27, label %30, label %28, !prof !2

; <label>:28:                                     ; preds = %15
  %29 = shl i32 %18, 12
  br label %172

; <label>:30:                                     ; preds = %172, %15
  %31 = phi %struct.CPUX86State* [ %22, %15 ], [ %174, %172 ]
  %32 = phi i32 [ %24, %15 ], [ %176, %172 ]
  %33 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 77, i64 %21, i64 %20
  %34 = and i32 %32, 4095
  %35 = icmp eq i32 %34, 0
  br i1 %35, label %64, label %36, !prof !2

; <label>:36:                                     ; preds = %30
  %37 = and i32 %16, 3
  %38 = icmp eq i32 %37, 0
  br i1 %38, label %39, label %68

; <label>:39:                                     ; preds = %36
  %40 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 78, i64 %21, i64 %20
  %41 = load i64, i64* %40, align 8
  %42 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 83
  store %struct.CPUTLBEntry* %33, %struct.CPUTLBEntry** %42, align 16
  %43 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %41)
  %44 = and i64 %41, -4096
  %45 = zext i32 %16 to i64
  %46 = add i64 %44, %45
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %45, i32 0)
  %47 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %48 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %47, i64 0, i32 72
  store i64 0, i64* %48, align 16
  %49 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %43, i32 1)
  br i1 %49, label %50, label %56

; <label>:50:                                     ; preds = %39
  %51 = and i64 %46, -4096
  %52 = tail call i64 @se_notdirty_mem_write(i64 %51)
  %53 = and i64 %46, 4095
  %54 = or i64 %52, %53
  %55 = inttoptr i64 %54 to i32*
  store i32 %1, i32* %55, align 4
  br label %57

; <label>:56:                                     ; preds = %39
  tail call void @io_writel_mmu(i64 %41, i32 %1, i32 %16, i8* null)
  br label %57

; <label>:57:                                     ; preds = %50, %56
  %58 = zext i32 %1 to i64
  %59 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %45, i64 %58, i32 4, i32 1)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %60 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %61 = load i32, i32* %60, align 4
  %62 = icmp eq i32 %61, 0
  br i1 %62, label %179, label %63

; <label>:63:                                     ; preds = %57
  tail call void @tcg_llvm_after_memory_access(i32 %16, i64 %58, i32 4, i32 3, i64 0)
  br label %179

; <label>:64:                                     ; preds = %30
  %65 = and i32 %16, 4095
  %66 = add nuw nsw i32 %65, 3
  %67 = icmp ugt i32 %66, 4095
  br i1 %67, label %68, label %161, !prof !3

; <label>:68:                                     ; preds = %36, %64
  %69 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %70 = load i32, i32* %69, align 4
  %71 = icmp eq i32 %70, 0
  br i1 %71, label %74, label %72

; <label>:72:                                     ; preds = %68
  %73 = zext i32 %1 to i64
  tail call void @tcg_llvm_before_memory_access(i32 %16, i64 %73, i32 4, i32 1)
  br label %74

; <label>:74:                                     ; preds = %72, %68
  %75 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %76 = load i32, i32* %75, align 4
  %77 = icmp eq i32 %76, 0
  br i1 %77, label %80, label %78

; <label>:78:                                     ; preds = %74
  %79 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %16, i32 0, i32 -1, i32 0)
  br label %80

; <label>:80:                                     ; preds = %78, %74
  %81 = phi i32 [ %79, %78 ], [ %16, %74 ]
  %82 = lshr i32 %81, 12
  %83 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %82, i32 0, i32 1048575, i32 0)
  %84 = and i32 %83, 1023
  %85 = zext i32 %84 to i64
  %86 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %87 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %86, i64 0, i32 77, i64 %21, i64 %85, i32 1
  %88 = load i32, i32* %87, align 4
  %89 = and i32 %81, -4096
  %90 = and i32 %88, -4088
  %91 = icmp eq i32 %89, %90
  br i1 %91, label %94, label %92

; <label>:92:                                     ; preds = %80
  %93 = shl i32 %83, 12
  br label %154

; <label>:94:                                     ; preds = %154, %80
  %95 = phi %struct.CPUX86State* [ %86, %80 ], [ %156, %154 ]
  %96 = phi i32 [ %88, %80 ], [ %158, %154 ]
  %97 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %95, i64 0, i32 77, i64 %21, i64 %85
  %98 = and i32 %96, 4095
  %99 = icmp eq i32 %98, 0
  br i1 %99, label %128, label %100

; <label>:100:                                    ; preds = %94
  %101 = and i32 %81, 3
  %102 = icmp eq i32 %101, 0
  br i1 %102, label %103, label %132

; <label>:103:                                    ; preds = %100
  %104 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %95, i64 0, i32 78, i64 %21, i64 %85
  %105 = load i64, i64* %104, align 8
  %106 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %95, i64 0, i32 83
  store %struct.CPUTLBEntry* %97, %struct.CPUTLBEntry** %106, align 16
  %107 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %105)
  %108 = and i64 %105, -4096
  %109 = zext i32 %81 to i64
  %110 = add i64 %108, %109
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %109, i32 0)
  %111 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %112 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %111, i64 0, i32 72
  store i64 0, i64* %112, align 16
  %113 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %107, i32 1)
  br i1 %113, label %114, label %120

; <label>:114:                                    ; preds = %103
  %115 = and i64 %110, -4096
  %116 = tail call i64 @se_notdirty_mem_write(i64 %115)
  %117 = and i64 %110, 4095
  %118 = or i64 %116, %117
  %119 = inttoptr i64 %118 to i32*
  store i32 %1, i32* %119, align 4
  br label %121

; <label>:120:                                    ; preds = %103
  tail call void @io_writel_mmu(i64 %105, i32 %1, i32 %81, i8* null)
  br label %121

; <label>:121:                                    ; preds = %120, %114
  %122 = zext i32 %1 to i64
  %123 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %109, i64 %122, i32 4, i32 1)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %124 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %125 = load i32, i32* %124, align 4
  %126 = icmp eq i32 %125, 0
  br i1 %126, label %179, label %127

; <label>:127:                                    ; preds = %121
  tail call void @tcg_llvm_after_memory_access(i32 %81, i64 %122, i32 4, i32 3, i64 0)
  br label %179

; <label>:128:                                    ; preds = %94
  %129 = and i32 %81, 4095
  %130 = add nuw nsw i32 %129, 3
  %131 = icmp ugt i32 %130, 4095
  br i1 %131, label %132, label %143

; <label>:132:                                    ; preds = %128, %100
  %133 = add i32 %81, 3
  %134 = lshr i32 %1, 24
  %135 = trunc i32 %134 to i8
  tail call fastcc void @slow_stb_mmu(i32 %133, i8 zeroext %135, i32 %2)
  %136 = add i32 %81, 2
  %137 = lshr i32 %1, 16
  %138 = trunc i32 %137 to i8
  tail call fastcc void @slow_stb_mmu(i32 %136, i8 zeroext %138, i32 %2)
  %139 = add i32 %81, 1
  %140 = lshr i32 %1, 8
  %141 = trunc i32 %140 to i8
  tail call fastcc void @slow_stb_mmu(i32 %139, i8 zeroext %141, i32 %2)
  %142 = trunc i32 %1 to i8
  tail call fastcc void @slow_stb_mmu(i32 %81, i8 zeroext %142, i32 %2)
  br label %179

; <label>:143:                                    ; preds = %128
  %144 = zext i32 %81 to i64
  %145 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %95, i64 0, i32 77, i64 %21, i64 %85, i32 4
  %146 = load i64, i64* %145, align 8
  %147 = add i64 %146, %144
  %148 = inttoptr i64 %147 to i32*
  store i32 %1, i32* %148, align 4
  %149 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %150 = load i32, i32* %149, align 4
  %151 = icmp eq i32 %150, 0
  br i1 %151, label %179, label %152

; <label>:152:                                    ; preds = %143
  %153 = zext i32 %1 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %81, i64 %153, i32 4, i32 2, i64 0)
  br label %179

; <label>:154:                                    ; preds = %154, %92
  %155 = phi %struct.CPUX86State* [ %86, %92 ], [ %156, %154 ]
  tail call void @tlb_fill(%struct.CPUX86State* %155, i32 %81, i32 %93, i32 1, i32 %2, i8* null)
  %156 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %157 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %156, i64 0, i32 77, i64 %21, i64 %85, i32 1
  %158 = load i32, i32* %157, align 4
  %159 = and i32 %158, -4088
  %160 = icmp eq i32 %89, %159
  br i1 %160, label %94, label %154

; <label>:161:                                    ; preds = %64
  %162 = zext i32 %16 to i64
  %163 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 77, i64 %21, i64 %20, i32 4
  %164 = load i64, i64* %163, align 8
  %165 = add i64 %164, %162
  %166 = inttoptr i64 %165 to i32*
  store i32 %1, i32* %166, align 4
  %167 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %168 = load i32, i32* %167, align 4
  %169 = icmp eq i32 %168, 0
  br i1 %169, label %179, label %170

; <label>:170:                                    ; preds = %161
  %171 = zext i32 %1 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %16, i64 %171, i32 4, i32 2, i64 0)
  br label %179

; <label>:172:                                    ; preds = %28, %172
  %173 = phi %struct.CPUX86State* [ %22, %28 ], [ %174, %172 ]
  tail call void @tlb_fill(%struct.CPUX86State* %173, i32 %16, i32 %29, i32 1, i32 %2, i8* null)
  %174 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %175 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %174, i64 0, i32 77, i64 %21, i64 %20, i32 1
  %176 = load i32, i32* %175, align 4
  %177 = and i32 %176, -4088
  %178 = icmp eq i32 %25, %177
  br i1 %178, label %30, label %172, !prof !2

; <label>:179:                                    ; preds = %152, %143, %132, %127, %121, %57, %161, %63, %170
  ret void
}

declare void @io_writel_mmu(i64, i32, i32, i8*) local_unnamed_addr #6

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #8

; Function Attrs: uwtable
define internal fastcc i32 @ldub_kernel(i32) unnamed_addr #2 {
  %2 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %3 = load i32, i32* %2, align 4
  %4 = icmp eq i32 %3, 0
  br i1 %4, label %6, label %5

; <label>:5:                                      ; preds = %1
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %6

; <label>:6:                                      ; preds = %1, %5
  %7 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %8 = load i32, i32* %7, align 4
  %9 = icmp eq i32 %8, 0
  br i1 %9, label %12, label %10

; <label>:10:                                     ; preds = %6
  %11 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %12

; <label>:12:                                     ; preds = %6, %10
  %13 = phi i32 [ %11, %10 ], [ %0, %6 ]
  %14 = lshr i32 %13, 12
  %15 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %14, i32 0, i32 1048575, i32 0)
  %16 = and i32 %15, 1023
  %17 = zext i32 %16 to i64
  %18 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %19 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 77, i64 0, i64 %17, i32 0
  %20 = load i32, i32* %19, align 8
  %21 = and i32 %13, -4096
  %22 = icmp eq i32 %20, %21
  br i1 %22, label %26, label %23, !prof !2

; <label>:23:                                     ; preds = %12
  %24 = tail call zeroext i8 @__ldb_mmu(i32 %13, i32 0)
  %25 = zext i8 %24 to i32
  br label %39

; <label>:26:                                     ; preds = %12
  %27 = zext i32 %13 to i64
  %28 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 77, i64 0, i64 %17, i32 4
  %29 = load i64, i64* %28, align 8
  %30 = add i64 %29, %27
  %31 = inttoptr i64 %30 to i8*
  %32 = load i8, i8* %31, align 1
  %33 = zext i8 %32 to i32
  %34 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %35 = load i32, i32* %34, align 4
  %36 = icmp eq i32 %35, 0
  br i1 %36, label %39, label %37

; <label>:37:                                     ; preds = %26
  %38 = zext i8 %32 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %13, i64 %38, i32 4, i32 0, i64 0)
  br label %39

; <label>:39:                                     ; preds = %26, %37, %23
  %40 = phi i32 [ %25, %23 ], [ %33, %37 ], [ %33, %26 ]
  ret i32 %40
}

; Function Attrs: uwtable
define internal fastcc void @stb_kernel(i32, i32) unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %8, label %6

; <label>:6:                                      ; preds = %2
  %7 = zext i32 %1 to i64
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 %7, i32 4, i32 1)
  br label %8

; <label>:8:                                      ; preds = %2, %6
  %9 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %10 = load i32, i32* %9, align 4
  %11 = icmp eq i32 %10, 0
  br i1 %11, label %14, label %12

; <label>:12:                                     ; preds = %8
  %13 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %14

; <label>:14:                                     ; preds = %8, %12
  %15 = phi i32 [ %13, %12 ], [ %0, %8 ]
  %16 = lshr i32 %15, 12
  %17 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %16, i32 0, i32 1048575, i32 0)
  %18 = and i32 %17, 1023
  %19 = zext i32 %18 to i64
  %20 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %21 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 0, i64 %19, i32 1
  %22 = load i32, i32* %21, align 4
  %23 = and i32 %15, -4096
  %24 = icmp eq i32 %22, %23
  br i1 %24, label %27, label %25, !prof !2

; <label>:25:                                     ; preds = %14
  %26 = trunc i32 %1 to i8
  tail call void @__stb_mmu(i32 %15, i8 zeroext %26, i32 0)
  br label %39

; <label>:27:                                     ; preds = %14
  %28 = zext i32 %15 to i64
  %29 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 0, i64 %19, i32 4
  %30 = load i64, i64* %29, align 8
  %31 = add i64 %30, %28
  %32 = inttoptr i64 %31 to i8*
  %33 = trunc i32 %1 to i8
  store i8 %33, i8* %32, align 1
  %34 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %35 = load i32, i32* %34, align 4
  %36 = icmp eq i32 %35, 0
  br i1 %36, label %39, label %37

; <label>:37:                                     ; preds = %27
  %38 = zext i32 %1 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %15, i64 %38, i32 4, i32 2, i64 0)
  br label %39

; <label>:39:                                     ; preds = %27, %37, %25
  ret void
}

; Function Attrs: uwtable
define internal fastcc void @tss_load_seg(i32, i32) unnamed_addr #2 {
  %3 = and i32 %1, 65532
  %4 = icmp eq i32 %3, 0
  br i1 %4, label %145, label %5

; <label>:5:                                      ; preds = %2
  %6 = and i32 %1, 4
  %7 = icmp eq i32 %6, 0
  %8 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %9 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %8, i64 0, i32 12
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %8, i64 0, i32 14
  %11 = select i1 %7, %struct.SegmentCache* %10, %struct.SegmentCache* %9
  %12 = or i32 %1, 7
  %13 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %11, i64 0, i32 2
  %14 = load i32, i32* %13, align 4
  %15 = icmp ugt i32 %12, %14
  br i1 %15, label %16, label %17

; <label>:16:                                     ; preds = %5
  tail call fastcc void @raise_exception_err(i32 10, i32 %3) #12
  unreachable

; <label>:17:                                     ; preds = %5
  %18 = and i32 %1, -8
  %19 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %11, i64 0, i32 1
  %20 = load i32, i32* %19, align 4
  %21 = add i32 %20, %18
  %22 = tail call fastcc i32 @ldl_kernel(i32 %21)
  %23 = add i32 %21, 4
  %24 = tail call fastcc i32 @ldl_kernel(i32 %23)
  %25 = and i32 %24, 4096
  %26 = icmp eq i32 %25, 0
  br i1 %26, label %27, label %28

; <label>:27:                                     ; preds = %17
  tail call fastcc void @raise_exception_err(i32 10, i32 %3) #12
  unreachable

; <label>:28:                                     ; preds = %17
  %29 = and i32 %1, 3
  %30 = lshr i32 %24, 13
  %31 = and i32 %30, 3
  %32 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %33 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 8
  %34 = load i32, i32* %33, align 4
  %35 = and i32 %34, 3
  %36 = icmp eq i32 %0, 1
  br i1 %36, label %37, label %44

; <label>:37:                                     ; preds = %28
  %38 = and i32 %24, 2048
  %39 = icmp eq i32 %38, 0
  br i1 %39, label %40, label %41

; <label>:40:                                     ; preds = %37
  tail call fastcc void @raise_exception_err(i32 10, i32 %3) #12
  unreachable

; <label>:41:                                     ; preds = %37
  %42 = icmp eq i32 %31, %29
  br i1 %42, label %66, label %43

; <label>:43:                                     ; preds = %41
  tail call fastcc void @raise_exception_err(i32 10, i32 %3) #12
  unreachable

; <label>:44:                                     ; preds = %28
  %45 = icmp eq i32 %0, 2
  %46 = and i32 %24, 2560
  br i1 %45, label %47, label %55

; <label>:47:                                     ; preds = %44
  %48 = icmp eq i32 %46, 512
  br i1 %48, label %50, label %49

; <label>:49:                                     ; preds = %47
  tail call fastcc void @raise_exception_err(i32 10, i32 %3) #12
  unreachable

; <label>:50:                                     ; preds = %47
  %51 = icmp eq i32 %31, %35
  %52 = icmp eq i32 %31, %29
  %53 = and i1 %52, %51
  br i1 %53, label %66, label %54

; <label>:54:                                     ; preds = %50
  tail call fastcc void @raise_exception_err(i32 10, i32 %3) #12
  unreachable

; <label>:55:                                     ; preds = %44
  %56 = icmp eq i32 %46, 2048
  br i1 %56, label %57, label %58

; <label>:57:                                     ; preds = %55
  tail call fastcc void @raise_exception_err(i32 10, i32 %3) #12
  unreachable

; <label>:58:                                     ; preds = %55
  %59 = and i32 %24, 3072
  %60 = icmp ult i32 %59, 3072
  br i1 %60, label %61, label %66

; <label>:61:                                     ; preds = %58
  %62 = icmp ult i32 %31, %35
  %63 = icmp ult i32 %31, %29
  %64 = or i1 %63, %62
  br i1 %64, label %65, label %66

; <label>:65:                                     ; preds = %61
  tail call fastcc void @raise_exception_err(i32 10, i32 %3) #12
  unreachable

; <label>:66:                                     ; preds = %41, %61, %50, %58
  %67 = trunc i32 %24 to i16
  %68 = icmp slt i16 %67, 0
  br i1 %68, label %70, label %69

; <label>:69:                                     ; preds = %66
  tail call fastcc void @raise_exception_err(i32 11, i32 %3) #12
  unreachable

; <label>:70:                                     ; preds = %66
  %71 = lshr i32 %22, 16
  %72 = shl i32 %24, 16
  %73 = and i32 %72, 16711680
  %74 = and i32 %24, -16777216
  %75 = or i32 %74, %71
  %76 = or i32 %75, %73
  %77 = and i32 %22, 65535
  %78 = and i32 %24, 983040
  %79 = or i32 %78, %77
  %80 = and i32 %24, 8388608
  %81 = icmp eq i32 %80, 0
  %82 = shl nuw i32 %79, 12
  %83 = or i32 %82, 4095
  %84 = select i1 %81, i32 %79, i32 %83
  %85 = sext i32 %0 to i64
  %86 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 11, i64 %85, i32 0
  store i32 %1, i32* %86, align 4
  %87 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 11, i64 %85, i32 1
  store i32 %76, i32* %87, align 4
  %88 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 11, i64 %85, i32 2
  store i32 %84, i32* %88, align 4
  %89 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 11, i64 %85, i32 3
  store i32 %24, i32* %89, align 4
  br i1 %36, label %92, label %90

; <label>:90:                                     ; preds = %70
  %91 = load i32, i32* %33, align 4
  br label %100

; <label>:92:                                     ; preds = %70
  %93 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 11, i64 1, i32 3
  %94 = load i32, i32* %93, align 4
  %95 = lshr i32 %94, 18
  %96 = and i32 %95, 16
  %97 = load i32, i32* %33, align 4
  %98 = and i32 %97, -32785
  %99 = or i32 %98, %96
  store i32 %99, i32* %33, align 4
  br label %100

; <label>:100:                                    ; preds = %92, %90
  %101 = phi i32 [ %91, %90 ], [ %99, %92 ]
  %102 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 11, i64 2, i32 3
  %103 = load i32, i32* %102, align 4
  %104 = lshr i32 %103, 17
  %105 = and i32 %104, 32
  %106 = trunc i32 %101 to i16
  %107 = icmp slt i16 %106, 0
  br i1 %107, label %140, label %108

; <label>:108:                                    ; preds = %100
  %109 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 16, i64 0
  %110 = load i32, i32* %109, align 8
  %111 = and i32 %110, 1
  %112 = icmp eq i32 %111, 0
  br i1 %112, label %124, label %113

; <label>:113:                                    ; preds = %108
  %114 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 7
  %115 = bitcast i32* %114 to i64*
  %116 = load i64, i64* %115, align 8
  %117 = and i64 %116, 131072
  %118 = icmp ne i64 %117, 0
  %119 = and i32 %101, 16
  %120 = icmp eq i32 %119, 0
  %121 = or i1 %120, %118
  %122 = lshr i64 %116, 32
  %123 = trunc i64 %122 to i32
  br i1 %121, label %124, label %127

; <label>:124:                                    ; preds = %113, %108
  %125 = phi i32 [ %123, %113 ], [ %101, %108 ]
  %126 = or i32 %105, 64
  br label %140

; <label>:127:                                    ; preds = %113
  %128 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 11, i64 3, i32 1
  %129 = load i32, i32* %128, align 4
  %130 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 11, i64 0, i32 1
  %131 = load i32, i32* %130, align 4
  %132 = or i32 %131, %129
  %133 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 11, i64 2, i32 1
  %134 = load i32, i32* %133, align 4
  %135 = or i32 %132, %134
  %136 = icmp ne i32 %135, 0
  %137 = zext i1 %136 to i32
  %138 = shl nuw nsw i32 %137, 6
  %139 = or i32 %138, %105
  br label %140

; <label>:140:                                    ; preds = %100, %124, %127
  %141 = phi i32 [ %101, %100 ], [ %125, %124 ], [ %123, %127 ]
  %142 = phi i32 [ %105, %100 ], [ %126, %124 ], [ %139, %127 ]
  %143 = and i32 %141, -97
  %144 = or i32 %143, %142
  store i32 %144, i32* %33, align 4
  br label %149

; <label>:145:                                    ; preds = %2
  %146 = add i32 %0, -1
  %147 = icmp ult i32 %146, 2
  br i1 %147, label %148, label %149

; <label>:148:                                    ; preds = %145
  tail call fastcc void @raise_exception_err(i32 10, i32 0) #12
  unreachable

; <label>:149:                                    ; preds = %145, %140
  ret void
}

declare void @hw_breakpoint_remove(%struct.CPUX86State*, i32) local_unnamed_addr #6

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #8

; Function Attrs: uwtable
define void @__stb_mmu(i32, i8 zeroext, i32) local_unnamed_addr #2 {
  %4 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %5 = load i32, i32* %4, align 4
  %6 = icmp eq i32 %5, 0
  br i1 %6, label %9, label %7

; <label>:7:                                      ; preds = %3
  %8 = zext i8 %1 to i64
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 %8, i32 1, i32 1)
  br label %9

; <label>:9:                                      ; preds = %3, %7
  %10 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %11 = load i32, i32* %10, align 4
  %12 = icmp eq i32 %11, 0
  br i1 %12, label %15, label %13

; <label>:13:                                     ; preds = %9
  %14 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %15

; <label>:15:                                     ; preds = %9, %13
  %16 = phi i32 [ %14, %13 ], [ %0, %9 ]
  %17 = lshr i32 %16, 12
  %18 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %17, i32 0, i32 1048575, i32 0)
  %19 = and i32 %18, 1023
  %20 = zext i32 %19 to i64
  %21 = sext i32 %2 to i64
  %22 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 77, i64 %21, i64 %20, i32 1
  %24 = load i32, i32* %23, align 4
  %25 = and i32 %16, -4096
  %26 = and i32 %24, -4088
  %27 = icmp eq i32 %25, %26
  br i1 %27, label %30, label %28, !prof !2

; <label>:28:                                     ; preds = %15
  %29 = shl i32 %18, 12
  br label %72

; <label>:30:                                     ; preds = %72, %15
  %31 = phi %struct.CPUX86State* [ %22, %15 ], [ %74, %72 ]
  %32 = phi i32 [ %24, %15 ], [ %76, %72 ]
  %33 = and i32 %32, 4095
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %61, label %35, !prof !2

; <label>:35:                                     ; preds = %30
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 77, i64 %21, i64 %20
  %37 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 78, i64 %21, i64 %20
  %38 = load i64, i64* %37, align 8
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 83
  store %struct.CPUTLBEntry* %36, %struct.CPUTLBEntry** %39, align 16
  %40 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %38)
  %41 = and i64 %38, -4096
  %42 = zext i32 %16 to i64
  %43 = add i64 %41, %42
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %42, i32 0)
  %44 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %45 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %44, i64 0, i32 72
  store i64 0, i64* %45, align 16
  %46 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %40, i32 1)
  br i1 %46, label %47, label %53

; <label>:47:                                     ; preds = %35
  %48 = and i64 %43, -4096
  %49 = tail call i64 @se_notdirty_mem_write(i64 %48)
  %50 = and i64 %43, 4095
  %51 = or i64 %49, %50
  %52 = inttoptr i64 %51 to i8*
  store i8 %1, i8* %52, align 1
  br label %54

; <label>:53:                                     ; preds = %35
  tail call void @io_writeb_mmu(i64 %38, i8 zeroext %1, i32 %16, i8* null)
  br label %54

; <label>:54:                                     ; preds = %47, %53
  %55 = zext i8 %1 to i64
  %56 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %42, i64 %55, i32 1, i32 1)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %57 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %58 = load i32, i32* %57, align 4
  %59 = icmp eq i32 %58, 0
  br i1 %59, label %79, label %60

; <label>:60:                                     ; preds = %54
  tail call void @tcg_llvm_after_memory_access(i32 %16, i64 %55, i32 1, i32 3, i64 0)
  br label %79

; <label>:61:                                     ; preds = %30
  %62 = zext i32 %16 to i64
  %63 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 77, i64 %21, i64 %20, i32 4
  %64 = load i64, i64* %63, align 8
  %65 = add i64 %64, %62
  %66 = inttoptr i64 %65 to i8*
  store i8 %1, i8* %66, align 1
  %67 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %68 = load i32, i32* %67, align 4
  %69 = icmp eq i32 %68, 0
  br i1 %69, label %79, label %70

; <label>:70:                                     ; preds = %61
  %71 = zext i8 %1 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %16, i64 %71, i32 1, i32 2, i64 0)
  br label %79

; <label>:72:                                     ; preds = %28, %72
  %73 = phi %struct.CPUX86State* [ %22, %28 ], [ %74, %72 ]
  tail call void @tlb_fill(%struct.CPUX86State* %73, i32 %16, i32 %29, i32 1, i32 %2, i8* null)
  %74 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %75 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %74, i64 0, i32 77, i64 %21, i64 %20, i32 1
  %76 = load i32, i32* %75, align 4
  %77 = and i32 %76, -4088
  %78 = icmp eq i32 %25, %77
  br i1 %78, label %30, label %72, !prof !2

; <label>:79:                                     ; preds = %54, %61, %60, %70
  ret void
}

; Function Attrs: uwtable
define zeroext i8 @__ldb_mmu(i32, i32) local_unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %7, label %6

; <label>:6:                                      ; preds = %2
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %7

; <label>:7:                                      ; preds = %2, %6
  %8 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %9 = load i32, i32* %8, align 4
  %10 = icmp eq i32 %9, 0
  br i1 %10, label %13, label %11

; <label>:11:                                     ; preds = %7
  %12 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %13

; <label>:13:                                     ; preds = %7, %11
  %14 = phi i32 [ %12, %11 ], [ %0, %7 ]
  %15 = lshr i32 %14, 12
  %16 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %15, i32 0, i32 1048575, i32 0)
  %17 = and i32 %16, 1023
  %18 = zext i32 %17 to i64
  %19 = sext i32 %1 to i64
  %20 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %21 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 %19, i64 %18
  %22 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %21, i64 0, i32 0
  %23 = load i32, i32* %22, align 8
  %24 = and i32 %14, -4096
  %25 = and i32 %23, -4088
  %26 = icmp eq i32 %24, %25
  br i1 %26, label %29, label %27, !prof !2

; <label>:27:                                     ; preds = %13
  %28 = shl i32 %16, 12
  br label %77

; <label>:29:                                     ; preds = %77, %13
  %30 = phi %struct.CPUX86State* [ %20, %13 ], [ %79, %77 ]
  %31 = phi %struct.CPUTLBEntry* [ %21, %13 ], [ %80, %77 ]
  %32 = phi i32 [ %23, %13 ], [ %82, %77 ]
  %33 = and i32 %32, 4095
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %65, label %35, !prof !2

; <label>:35:                                     ; preds = %29
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 78, i64 %19, i64 %18
  %37 = load i64, i64* %36, align 8
  %38 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 83
  store %struct.CPUTLBEntry* %31, %struct.CPUTLBEntry** %38, align 16
  %39 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %37)
  %40 = and i64 %37, 4294963200
  %41 = zext i32 %14 to i64
  %42 = add nuw nsw i64 %40, %41
  %43 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %44 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %43, i64 0, i32 72
  store i64 0, i64* %44, align 16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %41, i32 0)
  %45 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %39, i32 0)
  br i1 %45, label %46, label %53

; <label>:46:                                     ; preds = %35
  %47 = and i64 %42, 4294963200
  %48 = tail call i64 @se_notdirty_mem_read(i64 %47)
  %49 = and i64 %42, 4095
  %50 = or i64 %48, %49
  %51 = inttoptr i64 %50 to i8*
  %52 = load i8, i8* %51, align 1
  br label %55

; <label>:53:                                     ; preds = %35
  %54 = tail call zeroext i8 @io_readb_mmu(i64 %37, i32 %14, i8* null)
  br label %55

; <label>:55:                                     ; preds = %46, %53
  %56 = phi i8 [ %52, %46 ], [ %54, %53 ]
  %57 = zext i8 %56 to i64
  %58 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %41, i64 %57, i32 1, i32 0)
  %59 = trunc i64 %58 to i8
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %60 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %61 = load i32, i32* %60, align 4
  %62 = icmp eq i32 %61, 0
  br i1 %62, label %85, label %63

; <label>:63:                                     ; preds = %55
  %64 = and i64 %58, 255
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %64, i32 1, i32 1, i64 0)
  br label %85

; <label>:65:                                     ; preds = %29
  %66 = zext i32 %14 to i64
  %67 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 77, i64 %19, i64 %18, i32 4
  %68 = load i64, i64* %67, align 8
  %69 = add i64 %68, %66
  %70 = inttoptr i64 %69 to i8*
  %71 = load i8, i8* %70, align 1
  %72 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %73 = load i32, i32* %72, align 4
  %74 = icmp eq i32 %73, 0
  br i1 %74, label %85, label %75

; <label>:75:                                     ; preds = %65
  %76 = zext i8 %71 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %76, i32 1, i32 0, i64 0)
  br label %85

; <label>:77:                                     ; preds = %27, %77
  %78 = phi %struct.CPUX86State* [ %20, %27 ], [ %79, %77 ]
  tail call void @tlb_fill(%struct.CPUX86State* %78, i32 %14, i32 %28, i32 0, i32 %1, i8* null)
  %79 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %80 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %79, i64 0, i32 77, i64 %19, i64 %18
  %81 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %80, i64 0, i32 0
  %82 = load i32, i32* %81, align 8
  %83 = and i32 %82, -4088
  %84 = icmp eq i32 %24, %83
  br i1 %84, label %29, label %77, !prof !2

; <label>:85:                                     ; preds = %55, %65, %63, %75
  %86 = phi i8 [ %59, %63 ], [ %59, %55 ], [ %71, %75 ], [ %71, %65 ]
  ret i8 %86
}

declare zeroext i8 @io_readb_mmu(i64, i32, i8*) local_unnamed_addr #6

; Function Attrs: uwtable
define i32 @__ldl_mmu(i32, i32) local_unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %7, label %6

; <label>:6:                                      ; preds = %2
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %7

; <label>:7:                                      ; preds = %2, %6
  %8 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %9 = load i32, i32* %8, align 4
  %10 = icmp eq i32 %9, 0
  br i1 %10, label %13, label %11

; <label>:11:                                     ; preds = %7
  %12 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %13

; <label>:13:                                     ; preds = %7, %11
  %14 = phi i32 [ %12, %11 ], [ %0, %7 ]
  %15 = lshr i32 %14, 12
  %16 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %15, i32 0, i32 1048575, i32 0)
  %17 = and i32 %16, 1023
  %18 = zext i32 %17 to i64
  %19 = sext i32 %1 to i64
  %20 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %21 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 %19, i64 %18
  %22 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %21, i64 0, i32 0
  %23 = load i32, i32* %22, align 8
  %24 = and i32 %14, -4096
  %25 = and i32 %23, -4088
  %26 = icmp eq i32 %24, %25
  br i1 %26, label %29, label %27, !prof !2

; <label>:27:                                     ; preds = %13
  %28 = shl i32 %16, 12
  br label %86

; <label>:29:                                     ; preds = %86, %13
  %30 = phi %struct.CPUX86State* [ %20, %13 ], [ %88, %86 ]
  %31 = phi %struct.CPUTLBEntry* [ %21, %13 ], [ %89, %86 ]
  %32 = phi i32 [ %23, %13 ], [ %91, %86 ]
  %33 = and i32 %32, 4095
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %68, label %35, !prof !2

; <label>:35:                                     ; preds = %29
  %36 = and i32 %14, 3
  %37 = icmp eq i32 %36, 0
  br i1 %37, label %38, label %72

; <label>:38:                                     ; preds = %35
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 78, i64 %19, i64 %18
  %40 = load i64, i64* %39, align 8
  %41 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 83
  store %struct.CPUTLBEntry* %31, %struct.CPUTLBEntry** %41, align 16
  %42 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %40)
  %43 = and i64 %40, 4294963200
  %44 = zext i32 %14 to i64
  %45 = add nuw nsw i64 %43, %44
  %46 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %47 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %46, i64 0, i32 72
  store i64 0, i64* %47, align 16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %44, i32 0)
  %48 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %42, i32 0)
  br i1 %48, label %49, label %56

; <label>:49:                                     ; preds = %38
  %50 = and i64 %45, 4294963200
  %51 = tail call i64 @se_notdirty_mem_read(i64 %50)
  %52 = and i64 %45, 4095
  %53 = or i64 %51, %52
  %54 = inttoptr i64 %53 to i32*
  %55 = load i32, i32* %54, align 4
  br label %58

; <label>:56:                                     ; preds = %38
  %57 = tail call i32 @io_readl_mmu(i64 %40, i32 %14, i8* null)
  br label %58

; <label>:58:                                     ; preds = %49, %56
  %59 = phi i32 [ %55, %49 ], [ %57, %56 ]
  %60 = zext i32 %59 to i64
  %61 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %44, i64 %60, i32 4, i32 0)
  %62 = trunc i64 %61 to i32
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %63 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %64 = load i32, i32* %63, align 4
  %65 = icmp eq i32 %64, 0
  br i1 %65, label %94, label %66

; <label>:66:                                     ; preds = %58
  %67 = and i64 %61, 4294967295
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %67, i32 4, i32 1, i64 0)
  br label %94

; <label>:68:                                     ; preds = %29
  %69 = and i32 %14, 4095
  %70 = add nuw nsw i32 %69, 3
  %71 = icmp ugt i32 %70, 4095
  br i1 %71, label %72, label %74, !prof !3

; <label>:72:                                     ; preds = %35, %68
  %73 = tail call fastcc i32 @slow_ldl_mmu(i32 %14, i32 %1)
  br label %94

; <label>:74:                                     ; preds = %68
  %75 = zext i32 %14 to i64
  %76 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 77, i64 %19, i64 %18, i32 4
  %77 = load i64, i64* %76, align 8
  %78 = add i64 %77, %75
  %79 = inttoptr i64 %78 to i32*
  %80 = load i32, i32* %79, align 4
  %81 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %82 = load i32, i32* %81, align 4
  %83 = icmp eq i32 %82, 0
  br i1 %83, label %94, label %84

; <label>:84:                                     ; preds = %74
  %85 = zext i32 %80 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %85, i32 4, i32 0, i64 0)
  br label %94

; <label>:86:                                     ; preds = %27, %86
  %87 = phi %struct.CPUX86State* [ %20, %27 ], [ %88, %86 ]
  tail call void @tlb_fill(%struct.CPUX86State* %87, i32 %14, i32 %28, i32 0, i32 %1, i8* null)
  %88 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %89 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 77, i64 %19, i64 %18
  %90 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %89, i64 0, i32 0
  %91 = load i32, i32* %90, align 8
  %92 = and i32 %91, -4088
  %93 = icmp eq i32 %24, %92
  br i1 %93, label %29, label %86, !prof !2

; <label>:94:                                     ; preds = %58, %74, %66, %84, %72
  %95 = phi i32 [ %73, %72 ], [ %62, %66 ], [ %62, %58 ], [ %80, %84 ], [ %80, %74 ]
  ret i32 %95
}

declare i32 @io_readl_mmu(i64, i32, i8*) local_unnamed_addr #6

; Function Attrs: uwtable
define internal fastcc i32 @slow_ldl_mmu(i32, i32) unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %7, label %6

; <label>:6:                                      ; preds = %2
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %7

; <label>:7:                                      ; preds = %2, %6
  %8 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %9 = load i32, i32* %8, align 4
  %10 = icmp eq i32 %9, 0
  br i1 %10, label %13, label %11

; <label>:11:                                     ; preds = %7
  %12 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %13

; <label>:13:                                     ; preds = %7, %11
  %14 = phi i32 [ %12, %11 ], [ %0, %7 ]
  %15 = lshr i32 %14, 12
  %16 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %15, i32 0, i32 1048575, i32 0)
  %17 = and i32 %16, 1023
  %18 = zext i32 %17 to i64
  %19 = sext i32 %1 to i64
  %20 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %21 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 %19, i64 %18
  %22 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %21, i64 0, i32 0
  %23 = load i32, i32* %22, align 8
  %24 = and i32 %14, -4096
  %25 = and i32 %23, -4088
  %26 = icmp eq i32 %24, %25
  br i1 %26, label %29, label %27

; <label>:27:                                     ; preds = %13
  %28 = shl i32 %16, 12
  br label %95

; <label>:29:                                     ; preds = %95, %13
  %30 = phi %struct.CPUX86State* [ %20, %13 ], [ %97, %95 ]
  %31 = phi %struct.CPUTLBEntry* [ %21, %13 ], [ %98, %95 ]
  %32 = phi i32 [ %23, %13 ], [ %100, %95 ]
  %33 = and i32 %32, 4095
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %68, label %35

; <label>:35:                                     ; preds = %29
  %36 = and i32 %14, 3
  %37 = icmp eq i32 %36, 0
  br i1 %37, label %38, label %72

; <label>:38:                                     ; preds = %35
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 78, i64 %19, i64 %18
  %40 = load i64, i64* %39, align 8
  %41 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 83
  store %struct.CPUTLBEntry* %31, %struct.CPUTLBEntry** %41, align 16
  %42 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %40)
  %43 = and i64 %40, 4294963200
  %44 = zext i32 %14 to i64
  %45 = add nuw nsw i64 %43, %44
  %46 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %47 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %46, i64 0, i32 72
  store i64 0, i64* %47, align 16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %44, i32 0)
  %48 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %42, i32 0)
  br i1 %48, label %49, label %56

; <label>:49:                                     ; preds = %38
  %50 = and i64 %45, 4294963200
  %51 = tail call i64 @se_notdirty_mem_read(i64 %50)
  %52 = and i64 %45, 4095
  %53 = or i64 %51, %52
  %54 = inttoptr i64 %53 to i32*
  %55 = load i32, i32* %54, align 4
  br label %58

; <label>:56:                                     ; preds = %38
  %57 = tail call i32 @io_readl_mmu(i64 %40, i32 %14, i8* null)
  br label %58

; <label>:58:                                     ; preds = %49, %56
  %59 = phi i32 [ %55, %49 ], [ %57, %56 ]
  %60 = zext i32 %59 to i64
  %61 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %44, i64 %60, i32 4, i32 0)
  %62 = trunc i64 %61 to i32
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %63 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %64 = load i32, i32* %63, align 4
  %65 = icmp eq i32 %64, 0
  br i1 %65, label %103, label %66

; <label>:66:                                     ; preds = %58
  %67 = and i64 %61, 4294967295
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %67, i32 4, i32 1, i64 0)
  br label %103

; <label>:68:                                     ; preds = %29
  %69 = and i32 %14, 4095
  %70 = add nuw nsw i32 %69, 3
  %71 = icmp ugt i32 %70, 4095
  br i1 %71, label %72, label %83

; <label>:72:                                     ; preds = %35, %68
  %73 = and i32 %14, -4
  %74 = add i32 %73, 4
  %75 = tail call fastcc i32 @slow_ldl_mmu(i32 %73, i32 %1)
  %76 = tail call fastcc i32 @slow_ldl_mmu(i32 %74, i32 %1)
  %77 = shl i32 %14, 3
  %78 = and i32 %77, 24
  %79 = lshr i32 %75, %78
  %80 = sub nsw i32 32, %78
  %81 = shl i32 %76, %80
  %82 = or i32 %81, %79
  ret i32 %82

; <label>:83:                                     ; preds = %68
  %84 = zext i32 %14 to i64
  %85 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 77, i64 %19, i64 %18, i32 4
  %86 = load i64, i64* %85, align 8
  %87 = add i64 %86, %84
  %88 = inttoptr i64 %87 to i32*
  %89 = load i32, i32* %88, align 4
  %90 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %91 = load i32, i32* %90, align 4
  %92 = icmp eq i32 %91, 0
  br i1 %92, label %103, label %93

; <label>:93:                                     ; preds = %83
  %94 = zext i32 %89 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %94, i32 4, i32 0, i64 0)
  br label %103

; <label>:95:                                     ; preds = %27, %95
  %96 = phi %struct.CPUX86State* [ %20, %27 ], [ %97, %95 ]
  tail call void @tlb_fill(%struct.CPUX86State* %96, i32 %14, i32 %28, i32 0, i32 %1, i8* null)
  %97 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %98 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %97, i64 0, i32 77, i64 %19, i64 %18
  %99 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %98, i64 0, i32 0
  %100 = load i32, i32* %99, align 8
  %101 = and i32 %100, -4088
  %102 = icmp eq i32 %24, %101
  br i1 %102, label %29, label %95

; <label>:103:                                    ; preds = %58, %83, %66, %93
  %104 = phi i32 [ %62, %66 ], [ %62, %58 ], [ %89, %93 ], [ %89, %83 ]
  ret i32 %104
}

; Function Attrs: uwtable
define void @do_interrupt(%struct.CPUX86State*) local_unnamed_addr #2 {
  %2 = load i64, i64* bitcast (%struct.CPUX86State** @env to i64*), align 8
  store %struct.CPUX86State* %0, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %0, i64 0, i32 92
  %4 = load i32, i32* %3, align 8
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %0, i64 0, i32 59
  %6 = load i32, i32* %5, align 4
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %0, i64 0, i32 58
  %8 = load i32, i32* %7, align 16
  %9 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %0, i64 0, i32 60
  %10 = load i32, i32* %9, align 8
  %11 = load void (i32, i32, i32, i64, i32)*, void (i32, i32, i32, i64, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 2, i32 9), align 8
  %12 = zext i32 %10 to i64
  tail call void %11(i32 %4, i32 %6, i32 %8, i64 %12, i32 0)
  %13 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %14 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %13, i64 0, i32 64
  store i32 -1, i32* %14, align 4
  store i64 %2, i64* bitcast (%struct.CPUX86State** @env to i64*), align 8
  ret void
}

; Function Attrs: uwtable
define void @do_interrupt_x86_hardirq(%struct.CPUX86State*, i32, i32) local_unnamed_addr #2 {
  %4 = load i64, i64* bitcast (%struct.CPUX86State** @env to i64*), align 8
  store %struct.CPUX86State* %0, %struct.CPUX86State** @env, align 8
  %5 = load void (i32, i32, i32, i64, i32)*, void (i32, i32, i32, i64, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 2, i32 9), align 8
  tail call void %5(i32 %1, i32 0, i32 0, i64 0, i32 %2)
  store i64 %4, i64* bitcast (%struct.CPUX86State** @env to i64*), align 8
  ret void
}

; Function Attrs: noreturn uwtable
define void @raise_exception_err_env(%struct.CPUX86State*, i32, i32) local_unnamed_addr #3 {
  store %struct.CPUX86State* %0, %struct.CPUX86State** @env, align 8
  tail call fastcc void @raise_interrupt(i32 %1, i32 0, i32 %2, i32 0) #12
  unreachable
}

; Function Attrs: noreturn uwtable
define void @raise_exception_env(i32, %struct.CPUX86State*) local_unnamed_addr #3 {
  store %struct.CPUX86State* %1, %struct.CPUX86State** @env, align 8
  tail call fastcc void @raise_exception(i32 %0) #12
  unreachable
}

; Function Attrs: noreturn uwtable
define internal fastcc void @raise_exception(i32) unnamed_addr #3 {
  tail call fastcc void @raise_interrupt(i32 %0, i32 0, i32 0, i32 0) #12
  unreachable
}

; Function Attrs: uwtable
define void @do_smm_enter(%struct.CPUX86State*) local_unnamed_addr #2 {
  %2 = load i64, i64* bitcast (%struct.CPUX86State** @env to i64*), align 8
  store %struct.CPUX86State* %0, %struct.CPUX86State** @env, align 8
  %3 = load i32, i32* @loglevel, align 4
  %4 = and i32 %3, 16
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %10, label %6

; <label>:6:                                      ; preds = %1
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %8 = tail call i64 @fwrite(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.4, i64 0, i64 0), i64 11, i64 1, %struct._IO_FILE* %7)
  %9 = load i32, i32* @loglevel, align 4
  br label %10

; <label>:10:                                     ; preds = %1, %6
  %11 = phi i32 [ %3, %1 ], [ %9, %6 ]
  %12 = and i32 %11, 16
  %13 = icmp eq i32 %12, 0
  br i1 %13, label %17, label %14

; <label>:14:                                     ; preds = %10
  %15 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  tail call void @cpu_dump_state(%struct.CPUX86State* %15, %struct._IO_FILE* %16, i32 (%struct._IO_FILE*, i8*, ...)* nonnull @fprintf, i32 2)
  br label %17

; <label>:17:                                     ; preds = %10, %14
  %18 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %19 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 8
  %20 = load i32, i32* %19, align 4
  %21 = or i32 %20, 524288
  store i32 %21, i32* %19, align 4
  tail call void @cpu_smm_update(%struct.CPUX86State* %18)
  %22 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 63
  %24 = load i32, i32* %23, align 16
  %25 = add i32 %24, 65532
  %26 = zext i32 %25 to i64
  %27 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 16, i64 0
  %28 = load i32, i32* %27, align 8
  tail call void @stl_phys(i64 %26, i32 %28)
  %29 = add i32 %24, 65528
  %30 = zext i32 %29 to i64
  %31 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %32 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 16, i64 3
  %33 = load i32, i32* %32, align 4
  tail call void @stl_phys(i64 %30, i32 %33)
  %34 = add i32 %24, 65524
  %35 = zext i32 %34 to i64
  %36 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %37 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %36, i64 0, i32 7
  %38 = load i32, i32* %37, align 8
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %36, i64 0, i32 1
  %40 = load i32, i32* %39, align 16
  %41 = tail call i32 @helper_cc_compute_all(i32 %40) #5
  %42 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %36, i64 0, i32 6
  %43 = load i32, i32* %42, align 4
  %44 = and i32 %43, 1024
  %45 = or i32 %38, %41
  %46 = or i32 %45, %44
  %47 = or i32 %46, 2
  tail call void @stl_phys(i64 %35, i32 %47)
  %48 = add i32 %24, 65520
  %49 = zext i32 %48 to i64
  %50 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %51 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %50, i64 0, i32 5
  %52 = load i32, i32* %51, align 16
  tail call void @stl_phys(i64 %49, i32 %52)
  %53 = add i32 %24, 65516
  %54 = zext i32 %53 to i64
  %55 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %56 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %55, i64 0, i32 0, i64 7
  %57 = load i32, i32* %56, align 4
  tail call void @stl_phys(i64 %54, i32 %57)
  %58 = add i32 %24, 65512
  %59 = zext i32 %58 to i64
  %60 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %61 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %60, i64 0, i32 0, i64 6
  %62 = load i32, i32* %61, align 8
  tail call void @stl_phys(i64 %59, i32 %62)
  %63 = add i32 %24, 65508
  %64 = zext i32 %63 to i64
  %65 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %66 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %65, i64 0, i32 0, i64 5
  %67 = load i32, i32* %66, align 4
  tail call void @stl_phys(i64 %64, i32 %67)
  %68 = add i32 %24, 65504
  %69 = zext i32 %68 to i64
  %70 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %71 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %70, i64 0, i32 0, i64 4
  %72 = load i32, i32* %71, align 16
  tail call void @stl_phys(i64 %69, i32 %72)
  %73 = add i32 %24, 65500
  %74 = zext i32 %73 to i64
  %75 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %76 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %75, i64 0, i32 0, i64 3
  %77 = load i32, i32* %76, align 4
  tail call void @stl_phys(i64 %74, i32 %77)
  %78 = add i32 %24, 65496
  %79 = zext i32 %78 to i64
  %80 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %81 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %80, i64 0, i32 0, i64 2
  %82 = load i32, i32* %81, align 8
  tail call void @stl_phys(i64 %79, i32 %82)
  %83 = add i32 %24, 65492
  %84 = zext i32 %83 to i64
  %85 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %86 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %85, i64 0, i32 0, i64 1
  %87 = load i32, i32* %86, align 4
  tail call void @stl_phys(i64 %84, i32 %87)
  %88 = add i32 %24, 65488
  %89 = zext i32 %88 to i64
  %90 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %91 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %90, i64 0, i32 0, i64 0
  %92 = load i32, i32* %91, align 16
  tail call void @stl_phys(i64 %89, i32 %92)
  %93 = add i32 %24, 65484
  %94 = zext i32 %93 to i64
  %95 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %96 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %95, i64 0, i32 61, i64 6
  %97 = load i32, i32* %96, align 4
  tail call void @stl_phys(i64 %94, i32 %97)
  %98 = add i32 %24, 65480
  %99 = zext i32 %98 to i64
  %100 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %101 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %100, i64 0, i32 61, i64 7
  %102 = load i32, i32* %101, align 4
  tail call void @stl_phys(i64 %99, i32 %102)
  %103 = add i32 %24, 65476
  %104 = zext i32 %103 to i64
  %105 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %106 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %105, i64 0, i32 13, i32 0
  %107 = load i32, i32* %106, align 8
  tail call void @stl_phys(i64 %104, i32 %107)
  %108 = add i32 %24, 65380
  %109 = zext i32 %108 to i64
  %110 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %111 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %110, i64 0, i32 13, i32 1
  %112 = load i32, i32* %111, align 4
  tail call void @stl_phys(i64 %109, i32 %112)
  %113 = add i32 %24, 65376
  %114 = zext i32 %113 to i64
  %115 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %116 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %115, i64 0, i32 13, i32 2
  %117 = load i32, i32* %116, align 8
  tail call void @stl_phys(i64 %114, i32 %117)
  %118 = add i32 %24, 65372
  %119 = zext i32 %118 to i64
  %120 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %121 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %120, i64 0, i32 13, i32 3
  %122 = load i32, i32* %121, align 4
  %123 = lshr i32 %122, 8
  %124 = and i32 %123, 61695
  tail call void @stl_phys(i64 %119, i32 %124)
  %125 = add i32 %24, 65472
  %126 = zext i32 %125 to i64
  %127 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %128 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %127, i64 0, i32 12, i32 0
  %129 = load i32, i32* %128, align 8
  tail call void @stl_phys(i64 %126, i32 %129)
  %130 = add i32 %24, 65408
  %131 = zext i32 %130 to i64
  %132 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %133 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %132, i64 0, i32 12, i32 1
  %134 = load i32, i32* %133, align 4
  tail call void @stl_phys(i64 %131, i32 %134)
  %135 = add i32 %24, 65404
  %136 = zext i32 %135 to i64
  %137 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %138 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %137, i64 0, i32 12, i32 2
  %139 = load i32, i32* %138, align 8
  tail call void @stl_phys(i64 %136, i32 %139)
  %140 = add i32 %24, 65400
  %141 = zext i32 %140 to i64
  %142 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %143 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %142, i64 0, i32 12, i32 3
  %144 = load i32, i32* %143, align 4
  %145 = lshr i32 %144, 8
  %146 = and i32 %145, 61695
  tail call void @stl_phys(i64 %141, i32 %146)
  %147 = add i32 %24, 65396
  %148 = zext i32 %147 to i64
  %149 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %150 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %149, i64 0, i32 14, i32 1
  %151 = load i32, i32* %150, align 4
  tail call void @stl_phys(i64 %148, i32 %151)
  %152 = add i32 %24, 65392
  %153 = zext i32 %152 to i64
  %154 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %155 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %154, i64 0, i32 14, i32 2
  %156 = load i32, i32* %155, align 8
  tail call void @stl_phys(i64 %153, i32 %156)
  %157 = add i32 %24, 65368
  %158 = zext i32 %157 to i64
  %159 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %160 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %159, i64 0, i32 15, i32 1
  %161 = load i32, i32* %160, align 4
  tail call void @stl_phys(i64 %158, i32 %161)
  %162 = add i32 %24, 65364
  %163 = zext i32 %162 to i64
  %164 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %165 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %164, i64 0, i32 15, i32 2
  %166 = load i32, i32* %165, align 8
  tail call void @stl_phys(i64 %163, i32 %166)
  %167 = add i32 %24, 65448
  %168 = add i32 %24, 32768
  br label %169

; <label>:169:                                    ; preds = %169, %17
  %170 = phi i64 [ 0, %17 ], [ %197, %169 ]
  %171 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %172 = icmp slt i64 %170, 3
  %173 = mul nuw nsw i64 %170, 12
  %174 = select i1 %172, i32 32644, i32 32520
  %175 = shl i64 %170, 2
  %176 = trunc i64 %175 to i32
  %177 = add i32 %167, %176
  %178 = zext i32 %177 to i64
  %179 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %171, i64 0, i32 11, i64 %170, i32 0
  %180 = load i32, i32* %179, align 4
  tail call void @stl_phys(i64 %178, i32 %180)
  %181 = trunc i64 %173 to i32
  %182 = add i32 %168, %181
  %183 = add i32 %182, %174
  %184 = add i32 %183, 8
  %185 = zext i32 %184 to i64
  %186 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %171, i64 0, i32 11, i64 %170, i32 1
  %187 = load i32, i32* %186, align 4
  tail call void @stl_phys(i64 %185, i32 %187)
  %188 = add i32 %183, 4
  %189 = zext i32 %188 to i64
  %190 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %171, i64 0, i32 11, i64 %170, i32 2
  %191 = load i32, i32* %190, align 4
  tail call void @stl_phys(i64 %189, i32 %191)
  %192 = zext i32 %183 to i64
  %193 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %171, i64 0, i32 11, i64 %170, i32 3
  %194 = load i32, i32* %193, align 4
  %195 = lshr i32 %194, 8
  %196 = and i32 %195, 61695
  tail call void @stl_phys(i64 %192, i32 %196)
  %197 = add nuw nsw i64 %170, 1
  %198 = icmp eq i64 %197, 6
  br i1 %198, label %199, label %169

; <label>:199:                                    ; preds = %169
  %200 = add i32 %24, 65300
  %201 = zext i32 %200 to i64
  %202 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %203 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %202, i64 0, i32 16, i64 4
  %204 = load i32, i32* %203, align 8
  tail call void @stl_phys(i64 %201, i32 %204)
  %205 = add i32 %24, 65276
  %206 = zext i32 %205 to i64
  tail call void @stl_phys(i64 %206, i32 131072)
  %207 = add i32 %24, 65272
  %208 = zext i32 %207 to i64
  %209 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %210 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %209, i64 0, i32 63
  %211 = load i32, i32* %210, align 16
  tail call void @stl_phys(i64 %208, i32 %211)
  %212 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %213 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 2
  store i32 0, i32* %213, align 4
  %214 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 6
  store i32 1, i32* %214, align 4
  %215 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 7
  %216 = bitcast i32* %215 to i64*
  %217 = load i64, i64* %216, align 8
  %218 = trunc i64 %217 to i32
  %219 = and i32 %218, 3285
  store i32 %219, i32* %215, align 8
  %220 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 5
  store i32 32768, i32* %220, align 16
  %221 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 63
  %222 = load i32, i32* %221, align 16
  %223 = lshr i32 %222, 4
  %224 = and i32 %223, 65535
  %225 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 1, i32 0
  store i32 %224, i32* %225, align 4
  %226 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 1, i32 1
  store i32 %222, i32* %226, align 4
  %227 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 1, i32 2
  store i32 -1, i32* %227, align 4
  %228 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 1, i32 3
  store i32 0, i32* %228, align 4
  %229 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 8
  %230 = lshr i64 %217, 32
  %231 = trunc i64 %230 to i32
  %232 = and i32 %231, -32785
  store i32 %232, i32* %229, align 4
  %233 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 2, i32 3
  %234 = load i32, i32* %233, align 4
  %235 = lshr i32 %234, 17
  %236 = and i32 %235, 32
  %237 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 16, i64 0
  %238 = load i32, i32* %237, align 8
  %239 = and i32 %238, 1
  %240 = icmp eq i32 %239, 0
  br i1 %240, label %245, label %241

; <label>:241:                                    ; preds = %199
  %242 = load i64, i64* %216, align 8
  %243 = lshr i64 %242, 32
  %244 = trunc i64 %243 to i32
  br label %245

; <label>:245:                                    ; preds = %199, %241
  %246 = phi i32 [ %244, %241 ], [ %232, %199 ]
  %247 = and i32 %246, -97
  %248 = or i32 %236, %247
  %249 = or i32 %248, 64
  store i32 %249, i32* %229, align 4
  %250 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 3, i32 0
  %251 = bitcast i32* %250 to <4 x i32>*
  store <4 x i32> <i32 0, i32 0, i32 -1, i32 0>, <4 x i32>* %251, align 4
  %252 = trunc i32 %246 to i16
  %253 = icmp slt i16 %252, 0
  br i1 %253, label %277, label %254

; <label>:254:                                    ; preds = %245
  br i1 %240, label %264, label %255

; <label>:255:                                    ; preds = %254
  %256 = load i64, i64* %216, align 8
  %257 = and i64 %256, 131072
  %258 = icmp ne i64 %257, 0
  %259 = and i32 %246, 16
  %260 = icmp eq i32 %259, 0
  %261 = or i1 %260, %258
  %262 = lshr i64 %256, 32
  %263 = trunc i64 %262 to i32
  br i1 %261, label %264, label %267

; <label>:264:                                    ; preds = %255, %254
  %265 = phi i32 [ %263, %255 ], [ %249, %254 ]
  %266 = or i32 %236, 64
  br label %277

; <label>:267:                                    ; preds = %255
  %268 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 0, i32 1
  %269 = load i32, i32* %268, align 4
  %270 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 2, i32 1
  %271 = load i32, i32* %270, align 4
  %272 = or i32 %269, %271
  %273 = icmp ne i32 %272, 0
  %274 = zext i1 %273 to i32
  %275 = shl nuw nsw i32 %274, 6
  %276 = or i32 %275, %236
  br label %277

; <label>:277:                                    ; preds = %245, %264, %267
  %278 = phi i32 [ %249, %245 ], [ %265, %264 ], [ %263, %267 ]
  %279 = phi i32 [ %236, %245 ], [ %266, %264 ], [ %276, %267 ]
  %280 = and i32 %278, -97
  %281 = or i32 %280, %279
  store i32 %281, i32* %229, align 4
  %282 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 0, i32 0
  %283 = bitcast i32* %282 to <4 x i32>*
  store <4 x i32> <i32 0, i32 0, i32 -1, i32 0>, <4 x i32>* %283, align 4
  %284 = trunc i32 %281 to i16
  %285 = icmp slt i16 %284, 0
  br i1 %285, label %306, label %286

; <label>:286:                                    ; preds = %277
  br i1 %240, label %296, label %287

; <label>:287:                                    ; preds = %286
  %288 = load i64, i64* %216, align 8
  %289 = and i64 %288, 131072
  %290 = icmp ne i64 %289, 0
  %291 = and i32 %281, 16
  %292 = icmp eq i32 %291, 0
  %293 = or i1 %292, %290
  %294 = lshr i64 %288, 32
  %295 = trunc i64 %294 to i32
  br i1 %293, label %296, label %299

; <label>:296:                                    ; preds = %287, %286
  %297 = phi i32 [ %295, %287 ], [ %281, %286 ]
  %298 = or i32 %236, 64
  br label %306

; <label>:299:                                    ; preds = %287
  %300 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 2, i32 1
  %301 = load i32, i32* %300, align 4
  %302 = icmp ne i32 %301, 0
  %303 = zext i1 %302 to i32
  %304 = shl nuw nsw i32 %303, 6
  %305 = or i32 %304, %236
  br label %306

; <label>:306:                                    ; preds = %277, %296, %299
  %307 = phi i32 [ %281, %277 ], [ %297, %296 ], [ %295, %299 ]
  %308 = phi i32 [ %236, %277 ], [ %298, %296 ], [ %305, %299 ]
  %309 = and i32 %307, -97
  %310 = or i32 %309, %308
  store i32 %310, i32* %229, align 4
  %311 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 2, i32 0
  %312 = bitcast i32* %311 to <4 x i32>*
  store <4 x i32> <i32 0, i32 0, i32 -1, i32 0>, <4 x i32>* %312, align 4
  %313 = trunc i32 %310 to i16
  %314 = icmp slt i16 %313, 0
  %315 = or i1 %314, %240
  %316 = select i1 %314, i32 0, i32 64
  br i1 %315, label %327, label %317

; <label>:317:                                    ; preds = %306
  %318 = load i64, i64* %216, align 8
  %319 = and i64 %318, 131072
  %320 = icmp ne i64 %319, 0
  %321 = and i32 %310, 16
  %322 = icmp eq i32 %321, 0
  %323 = or i1 %322, %320
  %324 = lshr i64 %318, 32
  %325 = trunc i64 %324 to i32
  %326 = select i1 %323, i32 64, i32 0
  br label %327

; <label>:327:                                    ; preds = %317, %306
  %328 = phi i32 [ %310, %306 ], [ %325, %317 ]
  %329 = phi i32 [ %316, %306 ], [ %326, %317 ]
  %330 = and i32 %328, -97
  %331 = or i32 %330, %329
  store i32 %331, i32* %229, align 4
  %332 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 4, i32 0
  %333 = bitcast i32* %332 to <4 x i32>*
  store <4 x i32> <i32 0, i32 0, i32 -1, i32 0>, <4 x i32>* %333, align 4
  %334 = trunc i32 %328 to i16
  %335 = icmp slt i16 %334, 0
  %336 = or i1 %335, %240
  %337 = select i1 %335, i32 0, i32 64
  br i1 %336, label %348, label %338

; <label>:338:                                    ; preds = %327
  %339 = load i64, i64* %216, align 8
  %340 = and i64 %339, 131072
  %341 = icmp ne i64 %340, 0
  %342 = and i32 %328, 16
  %343 = icmp eq i32 %342, 0
  %344 = or i1 %343, %341
  %345 = lshr i64 %339, 32
  %346 = trunc i64 %345 to i32
  %347 = select i1 %344, i32 64, i32 0
  br label %348

; <label>:348:                                    ; preds = %338, %327
  %349 = phi i32 [ %331, %327 ], [ %346, %338 ]
  %350 = phi i32 [ %337, %327 ], [ %347, %338 ]
  %351 = and i32 %349, -97
  %352 = or i32 %351, %350
  store i32 %352, i32* %229, align 4
  %353 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 11, i64 5, i32 0
  %354 = bitcast i32* %353 to <4 x i32>*
  store <4 x i32> <i32 0, i32 0, i32 -1, i32 0>, <4 x i32>* %354, align 4
  %355 = trunc i32 %349 to i16
  %356 = icmp slt i16 %355, 0
  %357 = or i1 %356, %240
  %358 = select i1 %356, i32 0, i32 64
  br i1 %357, label %369, label %359

; <label>:359:                                    ; preds = %348
  %360 = load i64, i64* %216, align 8
  %361 = and i64 %360, 131072
  %362 = icmp ne i64 %361, 0
  %363 = and i32 %349, 16
  %364 = icmp eq i32 %363, 0
  %365 = or i1 %364, %362
  %366 = lshr i64 %360, 32
  %367 = trunc i64 %366 to i32
  %368 = select i1 %365, i32 64, i32 0
  br label %369

; <label>:369:                                    ; preds = %359, %348
  %370 = phi i32 [ %352, %348 ], [ %367, %359 ]
  %371 = phi i32 [ %358, %348 ], [ %368, %359 ]
  %372 = and i32 %370, -97
  %373 = or i32 %372, %371
  store i32 %373, i32* %229, align 4
  %374 = and i32 %238, 2147483634
  tail call void @cpu_x86_update_cr0(%struct.CPUX86State* nonnull %212, i32 %374)
  %375 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @cpu_x86_update_cr4(%struct.CPUX86State* %375, i32 0)
  %376 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %377 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %376, i64 0, i32 61, i64 7
  store i32 1024, i32* %377, align 4
  %378 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %376, i64 0, i32 1
  store i32 1, i32* %378, align 16
  store i64 %2, i64* bitcast (%struct.CPUX86State** @env to i64*), align 8
  ret void
}

declare void @cpu_dump_state(%struct.CPUX86State*, %struct._IO_FILE*, i32 (%struct._IO_FILE*, i8*, ...)*, i32) local_unnamed_addr #6

declare void @cpu_smm_update(%struct.CPUX86State*) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_rsm() local_unnamed_addr #2 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 63
  %3 = load i32, i32* %2, align 16
  %4 = add i32 %3, 65532
  %5 = zext i32 %4 to i64
  %6 = tail call i32 @ldl_phys(i64 %5)
  tail call void @cpu_x86_update_cr0(%struct.CPUX86State* %1, i32 %6)
  %7 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %8 = add i32 %3, 65528
  %9 = zext i32 %8 to i64
  %10 = tail call i32 @ldl_phys(i64 %9)
  tail call void @cpu_x86_update_cr3(%struct.CPUX86State* %7, i32 %10)
  %11 = add i32 %3, 65524
  %12 = zext i32 %11 to i64
  %13 = tail call i32 @ldl_phys(i64 %12)
  %14 = and i32 %13, 2261
  %15 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %16 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %15, i64 0, i32 2
  store i32 %14, i32* %16, align 4
  %17 = lshr i32 %13, 9
  %18 = and i32 %17, 2
  %19 = xor i32 %18, 2
  %20 = add nsw i32 %19, -1
  %21 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %15, i64 0, i32 6
  store i32 %20, i32* %21, align 4
  %22 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %15, i64 0, i32 7
  %23 = load i32, i32* %22, align 8
  %24 = and i32 %23, 3285
  %25 = and i32 %13, -3286
  %26 = or i32 %24, %25
  store i32 %26, i32* %22, align 8
  %27 = add i32 %3, 65520
  %28 = zext i32 %27 to i64
  %29 = tail call i32 @ldl_phys(i64 %28)
  %30 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %31 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 5
  store i32 %29, i32* %31, align 16
  %32 = add i32 %3, 65516
  %33 = zext i32 %32 to i64
  %34 = tail call i32 @ldl_phys(i64 %33)
  %35 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 0, i64 7
  store i32 %34, i32* %36, align 4
  %37 = add i32 %3, 65512
  %38 = zext i32 %37 to i64
  %39 = tail call i32 @ldl_phys(i64 %38)
  %40 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %41 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %40, i64 0, i32 0, i64 6
  store i32 %39, i32* %41, align 8
  %42 = add i32 %3, 65508
  %43 = zext i32 %42 to i64
  %44 = tail call i32 @ldl_phys(i64 %43)
  %45 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %46 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %45, i64 0, i32 0, i64 5
  store i32 %44, i32* %46, align 4
  %47 = add i32 %3, 65504
  %48 = zext i32 %47 to i64
  %49 = tail call i32 @ldl_phys(i64 %48)
  %50 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %51 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %50, i64 0, i32 0, i64 4
  store i32 %49, i32* %51, align 16
  %52 = add i32 %3, 65500
  %53 = zext i32 %52 to i64
  %54 = tail call i32 @ldl_phys(i64 %53)
  %55 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %56 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %55, i64 0, i32 0, i64 3
  store i32 %54, i32* %56, align 4
  %57 = add i32 %3, 65496
  %58 = zext i32 %57 to i64
  %59 = tail call i32 @ldl_phys(i64 %58)
  %60 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %61 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %60, i64 0, i32 0, i64 2
  store i32 %59, i32* %61, align 8
  %62 = add i32 %3, 65492
  %63 = zext i32 %62 to i64
  %64 = tail call i32 @ldl_phys(i64 %63)
  %65 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %66 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %65, i64 0, i32 0, i64 1
  store i32 %64, i32* %66, align 4
  %67 = add i32 %3, 65488
  %68 = zext i32 %67 to i64
  %69 = tail call i32 @ldl_phys(i64 %68)
  %70 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %71 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %70, i64 0, i32 0, i64 0
  store i32 %69, i32* %71, align 16
  %72 = add i32 %3, 65484
  %73 = zext i32 %72 to i64
  %74 = tail call i32 @ldl_phys(i64 %73)
  %75 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %76 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %75, i64 0, i32 61, i64 6
  store i32 %74, i32* %76, align 4
  %77 = add i32 %3, 65480
  %78 = zext i32 %77 to i64
  %79 = tail call i32 @ldl_phys(i64 %78)
  %80 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %81 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %80, i64 0, i32 61, i64 7
  store i32 %79, i32* %81, align 4
  %82 = add i32 %3, 65476
  %83 = zext i32 %82 to i64
  %84 = tail call i32 @ldl_phys(i64 %83)
  %85 = and i32 %84, 65535
  %86 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %87 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %86, i64 0, i32 13, i32 0
  store i32 %85, i32* %87, align 8
  %88 = add i32 %3, 65380
  %89 = zext i32 %88 to i64
  %90 = tail call i32 @ldl_phys(i64 %89)
  %91 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %92 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %91, i64 0, i32 13, i32 1
  store i32 %90, i32* %92, align 4
  %93 = add i32 %3, 65376
  %94 = zext i32 %93 to i64
  %95 = tail call i32 @ldl_phys(i64 %94)
  %96 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %97 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %96, i64 0, i32 13, i32 2
  store i32 %95, i32* %97, align 8
  %98 = add i32 %3, 65372
  %99 = zext i32 %98 to i64
  %100 = tail call i32 @ldl_phys(i64 %99)
  %101 = shl i32 %100, 8
  %102 = and i32 %101, 15793920
  %103 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %104 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %103, i64 0, i32 13, i32 3
  store i32 %102, i32* %104, align 4
  %105 = add i32 %3, 65472
  %106 = zext i32 %105 to i64
  %107 = tail call i32 @ldl_phys(i64 %106)
  %108 = and i32 %107, 65535
  %109 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %110 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %109, i64 0, i32 12, i32 0
  store i32 %108, i32* %110, align 8
  %111 = add i32 %3, 65408
  %112 = zext i32 %111 to i64
  %113 = tail call i32 @ldl_phys(i64 %112)
  %114 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %115 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %114, i64 0, i32 12, i32 1
  store i32 %113, i32* %115, align 4
  %116 = add i32 %3, 65404
  %117 = zext i32 %116 to i64
  %118 = tail call i32 @ldl_phys(i64 %117)
  %119 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %120 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %119, i64 0, i32 12, i32 2
  store i32 %118, i32* %120, align 8
  %121 = add i32 %3, 65400
  %122 = zext i32 %121 to i64
  %123 = tail call i32 @ldl_phys(i64 %122)
  %124 = shl i32 %123, 8
  %125 = and i32 %124, 15793920
  %126 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %127 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %126, i64 0, i32 12, i32 3
  store i32 %125, i32* %127, align 4
  %128 = add i32 %3, 65396
  %129 = zext i32 %128 to i64
  %130 = tail call i32 @ldl_phys(i64 %129)
  %131 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %132 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %131, i64 0, i32 14, i32 1
  store i32 %130, i32* %132, align 4
  %133 = add i32 %3, 65392
  %134 = zext i32 %133 to i64
  %135 = tail call i32 @ldl_phys(i64 %134)
  %136 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %137 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %136, i64 0, i32 14, i32 2
  store i32 %135, i32* %137, align 8
  %138 = add i32 %3, 65368
  %139 = zext i32 %138 to i64
  %140 = tail call i32 @ldl_phys(i64 %139)
  %141 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %142 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %141, i64 0, i32 15, i32 1
  store i32 %140, i32* %142, align 4
  %143 = add i32 %3, 65364
  %144 = zext i32 %143 to i64
  %145 = tail call i32 @ldl_phys(i64 %144)
  %146 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %147 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %146, i64 0, i32 15, i32 2
  store i32 %145, i32* %147, align 8
  %148 = add i32 %3, 65448
  %149 = add i32 %3, 32768
  br label %150

; <label>:150:                                    ; preds = %233, %0
  %151 = phi %struct.CPUX86State* [ %146, %0 ], [ %240, %233 ]
  %152 = phi i64 [ 0, %0 ], [ %238, %233 ]
  %153 = icmp slt i64 %152, 3
  %154 = mul nuw nsw i64 %152, 12
  %155 = select i1 %153, i32 32644, i32 32520
  %156 = shl i64 %152, 2
  %157 = trunc i64 %156 to i32
  %158 = add i32 %148, %157
  %159 = zext i32 %158 to i64
  %160 = tail call i32 @ldl_phys(i64 %159)
  %161 = and i32 %160, 65535
  %162 = trunc i64 %154 to i32
  %163 = add i32 %149, %162
  %164 = add i32 %163, %155
  %165 = add i32 %164, 8
  %166 = zext i32 %165 to i64
  %167 = tail call i32 @ldl_phys(i64 %166)
  %168 = add i32 %164, 4
  %169 = zext i32 %168 to i64
  %170 = tail call i32 @ldl_phys(i64 %169)
  %171 = zext i32 %164 to i64
  %172 = tail call i32 @ldl_phys(i64 %171)
  %173 = shl i32 %172, 8
  %174 = and i32 %173, 15793920
  %175 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 11, i64 %152, i32 0
  store i32 %161, i32* %175, align 4
  %176 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 11, i64 %152, i32 1
  store i32 %167, i32* %176, align 4
  %177 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 11, i64 %152, i32 2
  store i32 %170, i32* %177, align 4
  %178 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 11, i64 %152, i32 3
  store i32 %174, i32* %178, align 4
  %179 = icmp eq i64 %152, 1
  br i1 %179, label %183, label %180

; <label>:180:                                    ; preds = %150
  %181 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 8
  %182 = load i32, i32* %181, align 4
  br label %192

; <label>:183:                                    ; preds = %150
  %184 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 11, i64 1, i32 3
  %185 = load i32, i32* %184, align 4
  %186 = lshr i32 %185, 18
  %187 = and i32 %186, 16
  %188 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 8
  %189 = load i32, i32* %188, align 4
  %190 = and i32 %189, -32785
  %191 = or i32 %190, %187
  store i32 %191, i32* %188, align 4
  br label %192

; <label>:192:                                    ; preds = %183, %180
  %193 = phi i32* [ %181, %180 ], [ %188, %183 ]
  %194 = phi i32 [ %182, %180 ], [ %191, %183 ]
  %195 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 11, i64 2, i32 3
  %196 = load i32, i32* %195, align 4
  %197 = lshr i32 %196, 17
  %198 = and i32 %197, 32
  %199 = trunc i32 %194 to i16
  %200 = icmp slt i16 %199, 0
  br i1 %200, label %233, label %201

; <label>:201:                                    ; preds = %192
  %202 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 16, i64 0
  %203 = load i32, i32* %202, align 8
  %204 = and i32 %203, 1
  %205 = icmp eq i32 %204, 0
  br i1 %205, label %217, label %206

; <label>:206:                                    ; preds = %201
  %207 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 7
  %208 = bitcast i32* %207 to i64*
  %209 = load i64, i64* %208, align 8
  %210 = and i64 %209, 131072
  %211 = icmp ne i64 %210, 0
  %212 = and i32 %194, 16
  %213 = icmp eq i32 %212, 0
  %214 = or i1 %213, %211
  %215 = lshr i64 %209, 32
  %216 = trunc i64 %215 to i32
  br i1 %214, label %217, label %220

; <label>:217:                                    ; preds = %206, %201
  %218 = phi i32 [ %216, %206 ], [ %194, %201 ]
  %219 = or i32 %198, 64
  br label %233

; <label>:220:                                    ; preds = %206
  %221 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 11, i64 3, i32 1
  %222 = load i32, i32* %221, align 4
  %223 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 11, i64 0, i32 1
  %224 = load i32, i32* %223, align 4
  %225 = or i32 %224, %222
  %226 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 11, i64 2, i32 1
  %227 = load i32, i32* %226, align 4
  %228 = or i32 %225, %227
  %229 = icmp ne i32 %228, 0
  %230 = zext i1 %229 to i32
  %231 = shl nuw nsw i32 %230, 6
  %232 = or i32 %231, %198
  br label %233

; <label>:233:                                    ; preds = %192, %217, %220
  %234 = phi i32 [ %194, %192 ], [ %218, %217 ], [ %216, %220 ]
  %235 = phi i32 [ %198, %192 ], [ %219, %217 ], [ %232, %220 ]
  %236 = and i32 %234, -97
  %237 = or i32 %236, %235
  store i32 %237, i32* %193, align 4
  %238 = add nuw nsw i64 %152, 1
  %239 = icmp eq i64 %238, 6
  %240 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br i1 %239, label %241, label %150

; <label>:241:                                    ; preds = %233
  %242 = add i32 %3, 65300
  %243 = zext i32 %242 to i64
  %244 = tail call i32 @ldl_phys(i64 %243)
  tail call void @cpu_x86_update_cr4(%struct.CPUX86State* %240, i32 %244)
  %245 = add i32 %3, 65276
  %246 = zext i32 %245 to i64
  %247 = tail call i32 @ldl_phys(i64 %246)
  %248 = and i32 %247, 131072
  %249 = icmp eq i32 %248, 0
  br i1 %249, label %250, label %252

; <label>:250:                                    ; preds = %241
  %251 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %259

; <label>:252:                                    ; preds = %241
  %253 = add i32 %3, 65272
  %254 = zext i32 %253 to i64
  %255 = tail call i32 @ldl_phys(i64 %254)
  %256 = and i32 %255, -32768
  %257 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %258 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %257, i64 0, i32 63
  store i32 %256, i32* %258, align 16
  br label %259

; <label>:259:                                    ; preds = %250, %252
  %260 = phi %struct.CPUX86State* [ %251, %250 ], [ %257, %252 ]
  %261 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %260, i64 0, i32 1
  store i32 1, i32* %261, align 16
  %262 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %260, i64 0, i32 8
  %263 = load i32, i32* %262, align 4
  %264 = and i32 %263, -524289
  store i32 %264, i32* %262, align 4
  tail call void @cpu_smm_update(%struct.CPUX86State* %260)
  %265 = load i32, i32* @loglevel, align 4
  %266 = and i32 %265, 16
  %267 = icmp eq i32 %266, 0
  br i1 %267, label %271, label %268

; <label>:268:                                    ; preds = %259
  %269 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %270 = tail call i64 @fwrite(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.5, i64 0, i64 0), i64 15, i64 1, %struct._IO_FILE* %269)
  br label %271

; <label>:271:                                    ; preds = %259, %268
  ret void
}

; Function Attrs: uwtable
define void @helper_divb_AL(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 0
  %4 = load i32, i32* %3, align 16
  %5 = and i32 %4, 65535
  %6 = and i32 %0, 255
  %7 = icmp eq i32 %6, 0
  br i1 %7, label %8, label %9

; <label>:8:                                      ; preds = %1
  tail call fastcc void @raise_exception(i32 0) #12
  unreachable

; <label>:9:                                      ; preds = %1
  %10 = udiv i32 %5, %6
  %11 = icmp ugt i32 %10, 255
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %9
  tail call fastcc void @raise_exception(i32 0) #12
  unreachable

; <label>:13:                                     ; preds = %9
  %14 = and i32 %10, 255
  %15 = urem i32 %5, %6
  %16 = and i32 %4, -65536
  %17 = shl nuw nsw i32 %15, 8
  %18 = and i32 %17, 65280
  %19 = or i32 %14, %16
  %20 = or i32 %19, %18
  store i32 %20, i32* %3, align 16
  ret void
}

; Function Attrs: uwtable
define void @helper_idivb_AL(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 0
  %4 = load i32, i32* %3, align 16
  %5 = shl i32 %4, 16
  %6 = ashr exact i32 %5, 16
  %7 = shl i32 %0, 24
  %8 = ashr exact i32 %7, 24
  %9 = icmp eq i32 %8, 0
  br i1 %9, label %10, label %11

; <label>:10:                                     ; preds = %1
  tail call fastcc void @raise_exception(i32 0) #12
  unreachable

; <label>:11:                                     ; preds = %1
  %12 = sdiv i32 %6, %8
  %13 = shl i32 %12, 24
  %14 = ashr exact i32 %13, 24
  %15 = icmp eq i32 %12, %14
  br i1 %15, label %17, label %16

; <label>:16:                                     ; preds = %11
  tail call fastcc void @raise_exception(i32 0) #12
  unreachable

; <label>:17:                                     ; preds = %11
  %18 = and i32 %12, 255
  %19 = srem i32 %6, %8
  %20 = and i32 %4, -65536
  %21 = shl i32 %19, 8
  %22 = and i32 %21, 65280
  %23 = or i32 %18, %20
  %24 = or i32 %23, %22
  store i32 %24, i32* %3, align 16
  ret void
}

; Function Attrs: uwtable
define void @helper_divw_AX(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 0
  %4 = load i32, i32* %3, align 16
  %5 = and i32 %4, 65535
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 2
  %7 = load i32, i32* %6, align 8
  %8 = shl i32 %7, 16
  %9 = or i32 %8, %5
  %10 = and i32 %0, 65535
  %11 = icmp eq i32 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  tail call fastcc void @raise_exception(i32 0) #12
  unreachable

; <label>:13:                                     ; preds = %1
  %14 = udiv i32 %9, %10
  %15 = icmp ugt i32 %14, 65535
  br i1 %15, label %16, label %17

; <label>:16:                                     ; preds = %13
  tail call fastcc void @raise_exception(i32 0) #12
  unreachable

; <label>:17:                                     ; preds = %13
  %18 = and i32 %14, 65535
  %19 = urem i32 %9, %10
  %20 = and i32 %19, 65535
  %21 = and i32 %4, -65536
  %22 = or i32 %18, %21
  store i32 %22, i32* %3, align 16
  %23 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %24 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %23, i64 0, i32 0, i64 2
  %25 = load i32, i32* %24, align 8
  %26 = and i32 %25, -65536
  %27 = or i32 %26, %20
  store i32 %27, i32* %24, align 8
  ret void
}

; Function Attrs: uwtable
define void @helper_idivw_AX(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 0
  %4 = load i32, i32* %3, align 16
  %5 = and i32 %4, 65535
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 2
  %7 = load i32, i32* %6, align 8
  %8 = shl i32 %7, 16
  %9 = or i32 %8, %5
  %10 = shl i32 %0, 16
  %11 = ashr exact i32 %10, 16
  %12 = icmp eq i32 %11, 0
  br i1 %12, label %13, label %14

; <label>:13:                                     ; preds = %1
  tail call fastcc void @raise_exception(i32 0) #12
  unreachable

; <label>:14:                                     ; preds = %1
  %15 = sdiv i32 %9, %11
  %16 = shl i32 %15, 16
  %17 = ashr exact i32 %16, 16
  %18 = icmp eq i32 %15, %17
  br i1 %18, label %20, label %19

; <label>:19:                                     ; preds = %14
  tail call fastcc void @raise_exception(i32 0) #12
  unreachable

; <label>:20:                                     ; preds = %14
  %21 = and i32 %15, 65535
  %22 = srem i32 %9, %11
  %23 = and i32 %22, 65535
  %24 = and i32 %4, -65536
  %25 = or i32 %21, %24
  store i32 %25, i32* %3, align 16
  %26 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %27 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 0, i64 2
  %28 = load i32, i32* %27, align 8
  %29 = and i32 %28, -65536
  %30 = or i32 %29, %23
  store i32 %30, i32* %27, align 8
  ret void
}

; Function Attrs: uwtable
define void @helper_divl_EAX(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 0
  %4 = load i32, i32* %3, align 16
  %5 = zext i32 %4 to i64
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 2
  %7 = load i32, i32* %6, align 8
  %8 = zext i32 %7 to i64
  %9 = shl nuw i64 %8, 32
  %10 = or i64 %9, %5
  %11 = icmp eq i32 %0, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  tail call fastcc void @raise_exception(i32 0) #12
  unreachable

; <label>:13:                                     ; preds = %1
  %14 = zext i32 %0 to i64
  %15 = udiv i64 %10, %14
  %16 = icmp ugt i64 %15, 4294967295
  br i1 %16, label %17, label %18

; <label>:17:                                     ; preds = %13
  tail call fastcc void @raise_exception(i32 0) #12
  unreachable

; <label>:18:                                     ; preds = %13
  %19 = urem i64 %10, %14
  %20 = trunc i64 %19 to i32
  %21 = trunc i64 %15 to i32
  store i32 %21, i32* %3, align 16
  %22 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %22, i64 0, i32 0, i64 2
  store i32 %20, i32* %23, align 8
  ret void
}

; Function Attrs: uwtable
define void @helper_idivl_EAX(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 0
  %4 = load i32, i32* %3, align 16
  %5 = zext i32 %4 to i64
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 2
  %7 = load i32, i32* %6, align 8
  %8 = zext i32 %7 to i64
  %9 = shl nuw i64 %8, 32
  %10 = or i64 %9, %5
  %11 = icmp eq i32 %0, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  tail call fastcc void @raise_exception(i32 0) #12
  unreachable

; <label>:13:                                     ; preds = %1
  %14 = sext i32 %0 to i64
  %15 = sdiv i64 %10, %14
  %16 = shl i64 %15, 32
  %17 = ashr exact i64 %16, 32
  %18 = icmp eq i64 %15, %17
  br i1 %18, label %20, label %19

; <label>:19:                                     ; preds = %13
  tail call fastcc void @raise_exception(i32 0) #12
  unreachable

; <label>:20:                                     ; preds = %13
  %21 = trunc i64 %15 to i32
  %22 = srem i64 %10, %14
  %23 = trunc i64 %22 to i32
  store i32 %21, i32* %3, align 16
  %24 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %25 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %24, i64 0, i32 0, i64 2
  store i32 %23, i32* %25, align 8
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_aam(i32) local_unnamed_addr #1 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 0
  %4 = load i32, i32* %3, align 16
  %5 = and i32 %4, 255
  %6 = sdiv i32 %5, %0
  %7 = srem i32 %5, %0
  %8 = and i32 %4, -65536
  %9 = or i32 %7, %8
  %10 = shl i32 %6, 8
  %11 = or i32 %9, %10
  store i32 %11, i32* %3, align 16
  %12 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %12, i64 0, i32 3
  store i32 %7, i32* %13, align 8
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_aad(i32) local_unnamed_addr #1 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 0
  %4 = load i32, i32* %3, align 16
  %5 = lshr i32 %4, 8
  %6 = and i32 %5, 255
  %7 = mul nsw i32 %6, %0
  %8 = add i32 %7, %4
  %9 = and i32 %8, 255
  %10 = and i32 %4, -65536
  %11 = or i32 %9, %10
  store i32 %11, i32* %3, align 16
  %12 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %12, i64 0, i32 3
  store i32 %9, i32* %13, align 8
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_aaa() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 1
  %3 = load i32, i32* %2, align 16
  %4 = tail call i32 @helper_cc_compute_all(i32 %3)
  %5 = and i32 %4, 16
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 0, i64 0
  %7 = load i32, i32* %6, align 16
  %8 = lshr i32 %7, 8
  %9 = and i32 %7, 15
  %10 = icmp ugt i32 %9, 9
  %11 = icmp ne i32 %5, 0
  %12 = or i1 %11, %10
  br i1 %12, label %13, label %22

; <label>:13:                                     ; preds = %0
  %14 = and i32 %7, 254
  %15 = icmp ugt i32 %14, 249
  %16 = zext i1 %15 to i32
  %17 = add i32 %7, 6
  %18 = and i32 %17, 15
  %19 = add nuw nsw i32 %8, 1
  %20 = add nuw nsw i32 %19, %16
  %21 = or i32 %4, 17
  br label %24

; <label>:22:                                     ; preds = %0
  %23 = and i32 %4, -18
  br label %24

; <label>:24:                                     ; preds = %22, %13
  %25 = phi i32 [ %20, %13 ], [ %8, %22 ]
  %26 = phi i32 [ %18, %13 ], [ %9, %22 ]
  %27 = phi i32 [ %21, %13 ], [ %23, %22 ]
  %28 = and i32 %7, -65536
  %29 = shl i32 %25, 8
  %30 = and i32 %29, 65280
  %31 = or i32 %30, %26
  %32 = or i32 %31, %28
  store i32 %32, i32* %6, align 16
  %33 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %34 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %33, i64 0, i32 2
  store i32 %27, i32* %34, align 4
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_aas() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 1
  %3 = load i32, i32* %2, align 16
  %4 = tail call i32 @helper_cc_compute_all(i32 %3)
  %5 = and i32 %4, 16
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 0, i64 0
  %7 = load i32, i32* %6, align 16
  %8 = lshr i32 %7, 8
  %9 = and i32 %7, 15
  %10 = icmp ugt i32 %9, 9
  %11 = icmp ne i32 %5, 0
  %12 = or i1 %11, %10
  br i1 %12, label %13, label %22

; <label>:13:                                     ; preds = %0
  %14 = and i32 %7, 254
  %15 = icmp ult i32 %14, 6
  %16 = add i32 %7, 10
  %17 = and i32 %16, 15
  %18 = add nsw i32 %8, -1
  %19 = sext i1 %15 to i32
  %20 = add nsw i32 %18, %19
  %21 = or i32 %4, 17
  br label %24

; <label>:22:                                     ; preds = %0
  %23 = and i32 %4, -18
  br label %24

; <label>:24:                                     ; preds = %22, %13
  %25 = phi i32 [ %20, %13 ], [ %8, %22 ]
  %26 = phi i32 [ %17, %13 ], [ %9, %22 ]
  %27 = phi i32 [ %21, %13 ], [ %23, %22 ]
  %28 = and i32 %7, -65536
  %29 = shl i32 %25, 8
  %30 = and i32 %29, 65280
  %31 = or i32 %30, %26
  %32 = or i32 %31, %28
  store i32 %32, i32* %6, align 16
  %33 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %34 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %33, i64 0, i32 2
  store i32 %27, i32* %34, align 4
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_daa() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 1
  %3 = load i32, i32* %2, align 16
  %4 = tail call i32 @helper_cc_compute_all(i32 %3)
  %5 = and i32 %4, 1
  %6 = and i32 %4, 16
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 0, i64 0
  %8 = load i32, i32* %7, align 16
  %9 = and i32 %8, 255
  %10 = and i32 %8, 14
  %11 = icmp ugt i32 %10, 9
  %12 = icmp ne i32 %6, 0
  %13 = or i1 %12, %11
  %14 = add i32 %8, 6
  %15 = and i32 %14, 255
  %16 = select i1 %13, i32 %15, i32 %9
  %17 = select i1 %13, i32 16, i32 0
  %18 = icmp ugt i32 %9, 153
  %19 = icmp ne i32 %5, 0
  %20 = or i1 %19, %18
  br i1 %20, label %21, label %25

; <label>:21:                                     ; preds = %0
  %22 = add nuw nsw i32 %16, 96
  %23 = and i32 %22, 255
  %24 = or i32 %17, 1
  br label %25

; <label>:25:                                     ; preds = %0, %21
  %26 = phi i32 [ %23, %21 ], [ %16, %0 ]
  %27 = phi i32 [ %24, %21 ], [ %17, %0 ]
  %28 = and i32 %8, -256
  %29 = or i32 %28, %26
  store i32 %29, i32* %7, align 16
  %30 = icmp eq i32 %26, 0
  %31 = zext i1 %30 to i32
  %32 = shl nuw nsw i32 %31, 6
  %33 = sext i32 %26 to i64
  %34 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %33
  %35 = load i8, i8* %34, align 1
  %36 = zext i8 %35 to i32
  %37 = and i32 %26, 128
  %38 = or i32 %37, %27
  %39 = or i32 %38, %32
  %40 = or i32 %39, %36
  %41 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %42 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %41, i64 0, i32 2
  store i32 %40, i32* %42, align 4
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_das() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 1
  %3 = load i32, i32* %2, align 16
  %4 = tail call i32 @helper_cc_compute_all(i32 %3)
  %5 = and i32 %4, 1
  %6 = and i32 %4, 16
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 0, i64 0
  %8 = load i32, i32* %7, align 16
  %9 = and i32 %8, 255
  %10 = and i32 %8, 14
  %11 = icmp ugt i32 %10, 9
  %12 = icmp ne i32 %6, 0
  %13 = or i1 %12, %11
  br i1 %13, label %14, label %21

; <label>:14:                                     ; preds = %0
  %15 = icmp ult i32 %9, 6
  %16 = icmp ne i32 %5, 0
  %17 = or i1 %16, %15
  %18 = select i1 %17, i32 17, i32 16
  %19 = add i32 %8, 250
  %20 = and i32 %19, 255
  br label %21

; <label>:21:                                     ; preds = %0, %14
  %22 = phi i32 [ %20, %14 ], [ %9, %0 ]
  %23 = phi i32 [ %18, %14 ], [ 0, %0 ]
  %24 = icmp ugt i32 %9, 153
  %25 = icmp ne i32 %5, 0
  %26 = or i1 %25, %24
  br i1 %26, label %27, label %31

; <label>:27:                                     ; preds = %21
  %28 = add nuw nsw i32 %22, 160
  %29 = and i32 %28, 255
  %30 = or i32 %23, 1
  br label %31

; <label>:31:                                     ; preds = %21, %27
  %32 = phi i32 [ %29, %27 ], [ %22, %21 ]
  %33 = phi i32 [ %30, %27 ], [ %23, %21 ]
  %34 = and i32 %8, -256
  %35 = or i32 %34, %32
  store i32 %35, i32* %7, align 16
  %36 = icmp eq i32 %32, 0
  %37 = zext i1 %36 to i32
  %38 = shl nuw nsw i32 %37, 6
  %39 = sext i32 %32 to i64
  %40 = getelementptr inbounds [256 x i8], [256 x i8]* @parity_table, i64 0, i64 %39
  %41 = load i8, i8* %40, align 1
  %42 = zext i8 %41 to i32
  %43 = and i32 %32, 128
  %44 = or i32 %43, %33
  %45 = or i32 %44, %38
  %46 = or i32 %45, %42
  %47 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %48 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %47, i64 0, i32 2
  store i32 %46, i32* %48, align 4
  ret void
}

; Function Attrs: uwtable
define void @helper_into(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 1
  %4 = load i32, i32* %3, align 16
  %5 = tail call i32 @helper_cc_compute_all(i32 %4)
  %6 = and i32 %5, 2048
  %7 = icmp eq i32 %6, 0
  br i1 %7, label %9, label %8

; <label>:8:                                      ; preds = %1
  tail call fastcc void @raise_interrupt(i32 4, i32 1, i32 0, i32 %0) #12
  unreachable

; <label>:9:                                      ; preds = %1
  ret void
}

; Function Attrs: uwtable
define void @helper_cmpxchg8b(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 1
  %4 = load i32, i32* %3, align 16
  %5 = tail call i32 @helper_cc_compute_all(i32 %4)
  %6 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %7 = load i32, i32* %6, align 4
  %8 = icmp eq i32 %7, 0
  br i1 %8, label %10, label %9

; <label>:9:                                      ; preds = %1
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %10

; <label>:10:                                     ; preds = %9, %1
  %11 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %12 = load i32, i32* %11, align 4
  %13 = icmp eq i32 %12, 0
  br i1 %13, label %16, label %14

; <label>:14:                                     ; preds = %10
  %15 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %16

; <label>:16:                                     ; preds = %14, %10
  %17 = phi i32 [ %15, %14 ], [ %0, %10 ]
  %18 = lshr i32 %17, 12
  %19 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %18, i32 0, i32 1048575, i32 0)
  %20 = and i32 %19, 1023
  %21 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %22 = getelementptr %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 8
  %23 = load i32, i32* %22, align 4
  %24 = and i32 %23, 3
  %25 = icmp eq i32 %24, 3
  %26 = zext i32 %20 to i64
  %27 = zext i1 %25 to i64
  %28 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 77, i64 %27, i64 %26, i32 0
  %29 = load i32, i32* %28, align 8
  %30 = and i32 %17, -4089
  %31 = icmp eq i32 %29, %30
  br i1 %31, label %35, label %32, !prof !2

; <label>:32:                                     ; preds = %16
  %33 = zext i1 %25 to i32
  %34 = tail call i64 @__ldq_mmu(i32 %17, i32 %33)
  br label %46

; <label>:35:                                     ; preds = %16
  %36 = zext i32 %17 to i64
  %37 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 77, i64 %27, i64 %26, i32 4
  %38 = load i64, i64* %37, align 8
  %39 = add i64 %38, %36
  %40 = inttoptr i64 %39 to i64*
  %41 = load i64, i64* %40, align 8
  %42 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %43 = load i32, i32* %42, align 4
  %44 = icmp eq i32 %43, 0
  br i1 %44, label %46, label %45

; <label>:45:                                     ; preds = %35
  tail call void @tcg_llvm_after_memory_access(i32 %17, i64 %41, i32 8, i32 0, i64 0)
  br label %46

; <label>:46:                                     ; preds = %32, %35, %45
  %47 = phi i64 [ %34, %32 ], [ %41, %45 ], [ %41, %35 ]
  %48 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %49 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %48, i64 0, i32 0, i64 2
  %50 = bitcast i32* %49 to i64*
  %51 = load i64, i64* %50, align 8
  %52 = shl i64 %51, 32
  %53 = bitcast %struct.CPUX86State* %48 to i64*
  %54 = load i64, i64* %53, align 16
  %55 = and i64 %54, 4294967295
  %56 = or i64 %52, %55
  %57 = icmp eq i64 %47, %56
  br i1 %57, label %58, label %63

; <label>:58:                                     ; preds = %46
  %59 = lshr i64 %51, 32
  %60 = and i64 %54, -4294967296
  %61 = or i64 %60, %59
  tail call fastcc void @stq_data(i32 %0, i64 %61)
  %62 = or i32 %5, 64
  br label %71

; <label>:63:                                     ; preds = %46
  tail call fastcc void @stq_data(i32 %0, i64 %47)
  %64 = lshr i64 %47, 32
  %65 = trunc i64 %64 to i32
  %66 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %67 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %66, i64 0, i32 0, i64 2
  store i32 %65, i32* %67, align 8
  %68 = trunc i64 %47 to i32
  %69 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %66, i64 0, i32 0, i64 0
  store i32 %68, i32* %69, align 16
  %70 = and i32 %5, -65
  br label %71

; <label>:71:                                     ; preds = %63, %58
  %72 = phi i32 [ %62, %58 ], [ %70, %63 ]
  %73 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %74 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %73, i64 0, i32 2
  store i32 %72, i32* %74, align 4
  ret void
}

; Function Attrs: uwtable
define i64 @__ldq_mmu(i32, i32) local_unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %7, label %6

; <label>:6:                                      ; preds = %2
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %7

; <label>:7:                                      ; preds = %2, %6
  %8 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %9 = load i32, i32* %8, align 4
  %10 = icmp eq i32 %9, 0
  br i1 %10, label %13, label %11

; <label>:11:                                     ; preds = %7
  %12 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %13

; <label>:13:                                     ; preds = %7, %11
  %14 = phi i32 [ %12, %11 ], [ %0, %7 ]
  %15 = lshr i32 %14, 12
  %16 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %15, i32 0, i32 1048575, i32 0)
  %17 = and i32 %16, 1023
  %18 = zext i32 %17 to i64
  %19 = sext i32 %1 to i64
  %20 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %21 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 %19, i64 %18
  %22 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %21, i64 0, i32 0
  %23 = load i32, i32* %22, align 8
  %24 = and i32 %14, -4096
  %25 = and i32 %23, -4088
  %26 = icmp eq i32 %24, %25
  br i1 %26, label %29, label %27, !prof !2

; <label>:27:                                     ; preds = %13
  %28 = shl i32 %16, 12
  br label %87

; <label>:29:                                     ; preds = %87, %13
  %30 = phi %struct.CPUX86State* [ %20, %13 ], [ %89, %87 ]
  %31 = phi %struct.CPUTLBEntry* [ %21, %13 ], [ %90, %87 ]
  %32 = phi i32 [ %23, %13 ], [ %92, %87 ]
  %33 = and i32 %32, 4095
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %70, label %35, !prof !2

; <label>:35:                                     ; preds = %29
  %36 = and i32 %14, 7
  %37 = icmp eq i32 %36, 0
  br i1 %37, label %38, label %74

; <label>:38:                                     ; preds = %35
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 78, i64 %19, i64 %18
  %40 = load i64, i64* %39, align 8
  %41 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 83
  store %struct.CPUTLBEntry* %31, %struct.CPUTLBEntry** %41, align 16
  %42 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %40)
  %43 = and i64 %40, 4294963200
  %44 = zext i32 %14 to i64
  %45 = add nuw nsw i64 %43, %44
  %46 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %47 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %46, i64 0, i32 72
  store i64 0, i64* %47, align 16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %44, i32 0)
  %48 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %42, i32 0)
  br i1 %48, label %49, label %61

; <label>:49:                                     ; preds = %38
  %50 = and i64 %45, 4294963200
  %51 = tail call i64 @se_notdirty_mem_read(i64 %50)
  %52 = and i64 %45, 4095
  %53 = or i64 %51, %52
  %54 = inttoptr i64 %53 to i64*
  %55 = load i64, i64* %54, align 8
  %56 = add i64 %53, 4
  %57 = inttoptr i64 %56 to i64*
  %58 = load i64, i64* %57, align 8
  %59 = shl i64 %58, 32
  %60 = or i64 %59, %55
  br label %63

; <label>:61:                                     ; preds = %38
  %62 = tail call i64 @io_readq_mmu(i64 %40, i32 %14, i8* null)
  br label %63

; <label>:63:                                     ; preds = %49, %61
  %64 = phi i64 [ %60, %49 ], [ %62, %61 ]
  %65 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %44, i64 %64, i32 8, i32 0)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %66 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %67 = load i32, i32* %66, align 4
  %68 = icmp eq i32 %67, 0
  br i1 %68, label %95, label %69

; <label>:69:                                     ; preds = %63
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %65, i32 8, i32 1, i64 0)
  br label %95

; <label>:70:                                     ; preds = %29
  %71 = and i32 %14, 4095
  %72 = add nuw nsw i32 %71, 7
  %73 = icmp ugt i32 %72, 4095
  br i1 %73, label %74, label %76, !prof !3

; <label>:74:                                     ; preds = %35, %70
  %75 = tail call fastcc i64 @slow_ldq_mmu(i32 %14, i32 %1)
  br label %95

; <label>:76:                                     ; preds = %70
  %77 = zext i32 %14 to i64
  %78 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 77, i64 %19, i64 %18, i32 4
  %79 = load i64, i64* %78, align 8
  %80 = add i64 %79, %77
  %81 = inttoptr i64 %80 to i64*
  %82 = load i64, i64* %81, align 8
  %83 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %84 = load i32, i32* %83, align 4
  %85 = icmp eq i32 %84, 0
  br i1 %85, label %95, label %86

; <label>:86:                                     ; preds = %76
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %82, i32 8, i32 0, i64 0)
  br label %95

; <label>:87:                                     ; preds = %27, %87
  %88 = phi %struct.CPUX86State* [ %20, %27 ], [ %89, %87 ]
  tail call void @tlb_fill(%struct.CPUX86State* %88, i32 %14, i32 %28, i32 0, i32 %1, i8* null)
  %89 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %90 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %89, i64 0, i32 77, i64 %19, i64 %18
  %91 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %90, i64 0, i32 0
  %92 = load i32, i32* %91, align 8
  %93 = and i32 %92, -4088
  %94 = icmp eq i32 %24, %93
  br i1 %94, label %29, label %87, !prof !2

; <label>:95:                                     ; preds = %63, %76, %69, %86, %74
  %96 = phi i64 [ %75, %74 ], [ %65, %69 ], [ %65, %63 ], [ %82, %86 ], [ %82, %76 ]
  ret i64 %96
}

; Function Attrs: uwtable
define internal fastcc void @stq_data(i32, i64) unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %7, label %6

; <label>:6:                                      ; preds = %2
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 %1, i32 8, i32 1)
  br label %7

; <label>:7:                                      ; preds = %2, %6
  %8 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %9 = load i32, i32* %8, align 4
  %10 = icmp eq i32 %9, 0
  br i1 %10, label %13, label %11

; <label>:11:                                     ; preds = %7
  %12 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %13

; <label>:13:                                     ; preds = %7, %11
  %14 = phi i32 [ %12, %11 ], [ %0, %7 ]
  %15 = lshr i32 %14, 12
  %16 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %15, i32 0, i32 1048575, i32 0)
  %17 = and i32 %16, 1023
  %18 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %19 = getelementptr %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 8
  %20 = load i32, i32* %19, align 4
  %21 = and i32 %20, 3
  %22 = icmp eq i32 %21, 3
  %23 = zext i32 %17 to i64
  %24 = zext i1 %22 to i64
  %25 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 77, i64 %24, i64 %23, i32 1
  %26 = load i32, i32* %25, align 4
  %27 = and i32 %14, -4089
  %28 = icmp eq i32 %26, %27
  br i1 %28, label %31, label %29, !prof !2

; <label>:29:                                     ; preds = %13
  %30 = zext i1 %22 to i32
  tail call void @__stq_mmu(i32 %14, i64 %1, i32 %30)
  br label %41

; <label>:31:                                     ; preds = %13
  %32 = zext i32 %14 to i64
  %33 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 77, i64 %24, i64 %23, i32 4
  %34 = load i64, i64* %33, align 8
  %35 = add i64 %34, %32
  %36 = inttoptr i64 %35 to i64*
  store i64 %1, i64* %36, align 8
  %37 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %38 = load i32, i32* %37, align 4
  %39 = icmp eq i32 %38, 0
  br i1 %39, label %41, label %40

; <label>:40:                                     ; preds = %31
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %1, i32 8, i32 2, i64 0)
  br label %41

; <label>:41:                                     ; preds = %31, %40, %29
  ret void
}

; Function Attrs: uwtable
define void @__stq_mmu(i32, i64, i32) local_unnamed_addr #2 {
  %4 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %5 = load i32, i32* %4, align 4
  %6 = icmp eq i32 %5, 0
  br i1 %6, label %8, label %7

; <label>:7:                                      ; preds = %3
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 %1, i32 8, i32 1)
  br label %8

; <label>:8:                                      ; preds = %3, %7
  %9 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %10 = load i32, i32* %9, align 4
  %11 = icmp eq i32 %10, 0
  br i1 %11, label %14, label %12

; <label>:12:                                     ; preds = %8
  %13 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %14

; <label>:14:                                     ; preds = %8, %12
  %15 = phi i32 [ %13, %12 ], [ %0, %8 ]
  %16 = lshr i32 %15, 12
  %17 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %16, i32 0, i32 1048575, i32 0)
  %18 = and i32 %17, 1023
  %19 = zext i32 %18 to i64
  %20 = sext i32 %2 to i64
  %21 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %22 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 77, i64 %20, i64 %19, i32 1
  %23 = load i32, i32* %22, align 4
  %24 = and i32 %15, -4096
  %25 = and i32 %23, -4088
  %26 = icmp eq i32 %24, %25
  br i1 %26, label %29, label %27, !prof !2

; <label>:27:                                     ; preds = %14
  %28 = shl i32 %17, 12
  br label %188

; <label>:29:                                     ; preds = %188, %14
  %30 = phi %struct.CPUX86State* [ %21, %14 ], [ %190, %188 ]
  %31 = phi i32 [ %23, %14 ], [ %192, %188 ]
  %32 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 77, i64 %20, i64 %19
  %33 = and i32 %31, 4095
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %67, label %35, !prof !2

; <label>:35:                                     ; preds = %29
  %36 = and i32 %15, 7
  %37 = icmp eq i32 %36, 0
  br i1 %37, label %38, label %71

; <label>:38:                                     ; preds = %35
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 78, i64 %20, i64 %19
  %40 = load i64, i64* %39, align 8
  %41 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 83
  store %struct.CPUTLBEntry* %32, %struct.CPUTLBEntry** %41, align 16
  %42 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %40)
  %43 = and i64 %40, -4096
  %44 = zext i32 %15 to i64
  %45 = add i64 %43, %44
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %44, i32 0)
  %46 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %47 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %46, i64 0, i32 72
  store i64 0, i64* %47, align 16
  %48 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %42, i32 1)
  br i1 %48, label %49, label %60

; <label>:49:                                     ; preds = %38
  %50 = and i64 %45, -4096
  %51 = tail call i64 @se_notdirty_mem_write(i64 %50)
  %52 = and i64 %45, 4095
  %53 = or i64 %51, %52
  %54 = trunc i64 %1 to i32
  %55 = inttoptr i64 %53 to i32*
  store i32 %54, i32* %55, align 4
  %56 = add i64 %53, 4
  %57 = lshr i64 %1, 32
  %58 = trunc i64 %57 to i32
  %59 = inttoptr i64 %56 to i32*
  store i32 %58, i32* %59, align 4
  br label %61

; <label>:60:                                     ; preds = %38
  tail call void @io_writeq_mmu(i64 %40, i64 %1, i32 %15, i8* null)
  br label %61

; <label>:61:                                     ; preds = %49, %60
  %62 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %44, i64 %1, i32 8, i32 1)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %63 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %64 = load i32, i32* %63, align 4
  %65 = icmp eq i32 %64, 0
  br i1 %65, label %195, label %66

; <label>:66:                                     ; preds = %61
  tail call void @tcg_llvm_after_memory_access(i32 %15, i64 %1, i32 8, i32 3, i64 0)
  br label %195

; <label>:67:                                     ; preds = %29
  %68 = and i32 %15, 4095
  %69 = add nuw nsw i32 %68, 7
  %70 = icmp ugt i32 %69, 4095
  br i1 %70, label %71, label %178, !prof !3

; <label>:71:                                     ; preds = %35, %67
  %72 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %73 = load i32, i32* %72, align 4
  %74 = icmp eq i32 %73, 0
  br i1 %74, label %76, label %75

; <label>:75:                                     ; preds = %71
  tail call void @tcg_llvm_before_memory_access(i32 %15, i64 %1, i32 8, i32 1)
  br label %76

; <label>:76:                                     ; preds = %75, %71
  %77 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %78 = load i32, i32* %77, align 4
  %79 = icmp eq i32 %78, 0
  br i1 %79, label %82, label %80

; <label>:80:                                     ; preds = %76
  %81 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %15, i32 0, i32 -1, i32 0)
  br label %82

; <label>:82:                                     ; preds = %80, %76
  %83 = phi i32 [ %81, %80 ], [ %15, %76 ]
  %84 = lshr i32 %83, 12
  %85 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %84, i32 0, i32 1048575, i32 0)
  %86 = and i32 %85, 1023
  %87 = zext i32 %86 to i64
  %88 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %89 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 77, i64 %20, i64 %87, i32 1
  %90 = load i32, i32* %89, align 4
  %91 = and i32 %83, -4096
  %92 = and i32 %90, -4088
  %93 = icmp eq i32 %91, %92
  br i1 %93, label %96, label %94

; <label>:94:                                     ; preds = %82
  %95 = shl i32 %85, 12
  br label %171

; <label>:96:                                     ; preds = %171, %82
  %97 = phi %struct.CPUX86State* [ %88, %82 ], [ %173, %171 ]
  %98 = phi i32 [ %90, %82 ], [ %175, %171 ]
  %99 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %97, i64 0, i32 77, i64 %20, i64 %87
  %100 = and i32 %98, 4095
  %101 = icmp eq i32 %100, 0
  br i1 %101, label %134, label %102

; <label>:102:                                    ; preds = %96
  %103 = and i32 %83, 7
  %104 = icmp eq i32 %103, 0
  br i1 %104, label %105, label %138

; <label>:105:                                    ; preds = %102
  %106 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %97, i64 0, i32 78, i64 %20, i64 %87
  %107 = load i64, i64* %106, align 8
  %108 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %97, i64 0, i32 83
  store %struct.CPUTLBEntry* %99, %struct.CPUTLBEntry** %108, align 16
  %109 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %107)
  %110 = and i64 %107, -4096
  %111 = zext i32 %83 to i64
  %112 = add i64 %110, %111
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %111, i32 0)
  %113 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %114 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %113, i64 0, i32 72
  store i64 0, i64* %114, align 16
  %115 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %109, i32 1)
  br i1 %115, label %116, label %127

; <label>:116:                                    ; preds = %105
  %117 = and i64 %112, -4096
  %118 = tail call i64 @se_notdirty_mem_write(i64 %117)
  %119 = and i64 %112, 4095
  %120 = or i64 %118, %119
  %121 = trunc i64 %1 to i32
  %122 = inttoptr i64 %120 to i32*
  store i32 %121, i32* %122, align 4
  %123 = add i64 %120, 4
  %124 = lshr i64 %1, 32
  %125 = trunc i64 %124 to i32
  %126 = inttoptr i64 %123 to i32*
  store i32 %125, i32* %126, align 4
  br label %128

; <label>:127:                                    ; preds = %105
  tail call void @io_writeq_mmu(i64 %107, i64 %1, i32 %83, i8* null)
  br label %128

; <label>:128:                                    ; preds = %127, %116
  %129 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %111, i64 %1, i32 8, i32 1)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %130 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %131 = load i32, i32* %130, align 4
  %132 = icmp eq i32 %131, 0
  br i1 %132, label %195, label %133

; <label>:133:                                    ; preds = %128
  tail call void @tcg_llvm_after_memory_access(i32 %83, i64 %1, i32 8, i32 3, i64 0)
  br label %195

; <label>:134:                                    ; preds = %96
  %135 = and i32 %83, 4095
  %136 = add nuw nsw i32 %135, 7
  %137 = icmp ugt i32 %136, 4095
  br i1 %137, label %138, label %161

; <label>:138:                                    ; preds = %134, %102
  %139 = add i32 %83, 7
  %140 = lshr i64 %1, 56
  %141 = trunc i64 %140 to i8
  tail call fastcc void @slow_stb_mmu(i32 %139, i8 zeroext %141, i32 %2)
  %142 = add i32 %83, 6
  %143 = lshr i64 %1, 48
  %144 = trunc i64 %143 to i8
  tail call fastcc void @slow_stb_mmu(i32 %142, i8 zeroext %144, i32 %2)
  %145 = add i32 %83, 5
  %146 = lshr i64 %1, 40
  %147 = trunc i64 %146 to i8
  tail call fastcc void @slow_stb_mmu(i32 %145, i8 zeroext %147, i32 %2)
  %148 = add i32 %83, 4
  %149 = lshr i64 %1, 32
  %150 = trunc i64 %149 to i8
  tail call fastcc void @slow_stb_mmu(i32 %148, i8 zeroext %150, i32 %2)
  %151 = add i32 %83, 3
  %152 = lshr i64 %1, 24
  %153 = trunc i64 %152 to i8
  tail call fastcc void @slow_stb_mmu(i32 %151, i8 zeroext %153, i32 %2)
  %154 = add i32 %83, 2
  %155 = lshr i64 %1, 16
  %156 = trunc i64 %155 to i8
  tail call fastcc void @slow_stb_mmu(i32 %154, i8 zeroext %156, i32 %2)
  %157 = add i32 %83, 1
  %158 = lshr i64 %1, 8
  %159 = trunc i64 %158 to i8
  tail call fastcc void @slow_stb_mmu(i32 %157, i8 zeroext %159, i32 %2)
  %160 = trunc i64 %1 to i8
  tail call fastcc void @slow_stb_mmu(i32 %83, i8 zeroext %160, i32 %2)
  br label %195

; <label>:161:                                    ; preds = %134
  %162 = zext i32 %83 to i64
  %163 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %97, i64 0, i32 77, i64 %20, i64 %87, i32 4
  %164 = load i64, i64* %163, align 8
  %165 = add i64 %164, %162
  %166 = inttoptr i64 %165 to i64*
  store i64 %1, i64* %166, align 8
  %167 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %168 = load i32, i32* %167, align 4
  %169 = icmp eq i32 %168, 0
  br i1 %169, label %195, label %170

; <label>:170:                                    ; preds = %161
  tail call void @tcg_llvm_after_memory_access(i32 %83, i64 %1, i32 8, i32 2, i64 0)
  br label %195

; <label>:171:                                    ; preds = %171, %94
  %172 = phi %struct.CPUX86State* [ %88, %94 ], [ %173, %171 ]
  tail call void @tlb_fill(%struct.CPUX86State* %172, i32 %83, i32 %95, i32 1, i32 %2, i8* null)
  %173 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %174 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %173, i64 0, i32 77, i64 %20, i64 %87, i32 1
  %175 = load i32, i32* %174, align 4
  %176 = and i32 %175, -4088
  %177 = icmp eq i32 %91, %176
  br i1 %177, label %96, label %171

; <label>:178:                                    ; preds = %67
  %179 = zext i32 %15 to i64
  %180 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 77, i64 %20, i64 %19, i32 4
  %181 = load i64, i64* %180, align 8
  %182 = add i64 %181, %179
  %183 = inttoptr i64 %182 to i64*
  store i64 %1, i64* %183, align 8
  %184 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %185 = load i32, i32* %184, align 4
  %186 = icmp eq i32 %185, 0
  br i1 %186, label %195, label %187

; <label>:187:                                    ; preds = %178
  tail call void @tcg_llvm_after_memory_access(i32 %15, i64 %1, i32 8, i32 2, i64 0)
  br label %195

; <label>:188:                                    ; preds = %27, %188
  %189 = phi %struct.CPUX86State* [ %21, %27 ], [ %190, %188 ]
  tail call void @tlb_fill(%struct.CPUX86State* %189, i32 %15, i32 %28, i32 1, i32 %2, i8* null)
  %190 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %191 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %190, i64 0, i32 77, i64 %20, i64 %19, i32 1
  %192 = load i32, i32* %191, align 4
  %193 = and i32 %192, -4088
  %194 = icmp eq i32 %24, %193
  br i1 %194, label %29, label %188, !prof !2

; <label>:195:                                    ; preds = %170, %161, %138, %133, %128, %61, %178, %66, %187
  ret void
}

declare void @io_writeq_mmu(i64, i64, i32, i8*) local_unnamed_addr #6

declare i64 @io_readq_mmu(i64, i32, i8*) local_unnamed_addr #6

; Function Attrs: uwtable
define internal fastcc i64 @slow_ldq_mmu(i32, i32) unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %7, label %6

; <label>:6:                                      ; preds = %2
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %7

; <label>:7:                                      ; preds = %2, %6
  %8 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %9 = load i32, i32* %8, align 4
  %10 = icmp eq i32 %9, 0
  br i1 %10, label %13, label %11

; <label>:11:                                     ; preds = %7
  %12 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %13

; <label>:13:                                     ; preds = %7, %11
  %14 = phi i32 [ %12, %11 ], [ %0, %7 ]
  %15 = lshr i32 %14, 12
  %16 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %15, i32 0, i32 1048575, i32 0)
  %17 = and i32 %16, 1023
  %18 = zext i32 %17 to i64
  %19 = sext i32 %1 to i64
  %20 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %21 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 77, i64 %19, i64 %18
  %22 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %21, i64 0, i32 0
  %23 = load i32, i32* %22, align 8
  %24 = and i32 %14, -4096
  %25 = and i32 %23, -4088
  %26 = icmp eq i32 %24, %25
  br i1 %26, label %29, label %27

; <label>:27:                                     ; preds = %13
  %28 = shl i32 %16, 12
  br label %98

; <label>:29:                                     ; preds = %98, %13
  %30 = phi %struct.CPUX86State* [ %20, %13 ], [ %100, %98 ]
  %31 = phi %struct.CPUTLBEntry* [ %21, %13 ], [ %101, %98 ]
  %32 = phi i32 [ %23, %13 ], [ %103, %98 ]
  %33 = and i32 %32, 4095
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %70, label %35

; <label>:35:                                     ; preds = %29
  %36 = and i32 %14, 7
  %37 = icmp eq i32 %36, 0
  br i1 %37, label %38, label %74

; <label>:38:                                     ; preds = %35
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 78, i64 %19, i64 %18
  %40 = load i64, i64* %39, align 8
  %41 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 83
  store %struct.CPUTLBEntry* %31, %struct.CPUTLBEntry** %41, align 16
  %42 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %40)
  %43 = and i64 %40, 4294963200
  %44 = zext i32 %14 to i64
  %45 = add nuw nsw i64 %43, %44
  %46 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %47 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %46, i64 0, i32 72
  store i64 0, i64* %47, align 16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %44, i32 0)
  %48 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %42, i32 0)
  br i1 %48, label %49, label %61

; <label>:49:                                     ; preds = %38
  %50 = and i64 %45, 4294963200
  %51 = tail call i64 @se_notdirty_mem_read(i64 %50)
  %52 = and i64 %45, 4095
  %53 = or i64 %51, %52
  %54 = inttoptr i64 %53 to i64*
  %55 = load i64, i64* %54, align 8
  %56 = add i64 %53, 4
  %57 = inttoptr i64 %56 to i64*
  %58 = load i64, i64* %57, align 8
  %59 = shl i64 %58, 32
  %60 = or i64 %59, %55
  br label %63

; <label>:61:                                     ; preds = %38
  %62 = tail call i64 @io_readq_mmu(i64 %40, i32 %14, i8* null)
  br label %63

; <label>:63:                                     ; preds = %49, %61
  %64 = phi i64 [ %60, %49 ], [ %62, %61 ]
  %65 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %44, i64 %64, i32 8, i32 0)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  %66 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %67 = load i32, i32* %66, align 4
  %68 = icmp eq i32 %67, 0
  br i1 %68, label %106, label %69

; <label>:69:                                     ; preds = %63
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %65, i32 8, i32 1, i64 0)
  br label %106

; <label>:70:                                     ; preds = %29
  %71 = and i32 %14, 4095
  %72 = add nuw nsw i32 %71, 7
  %73 = icmp ugt i32 %72, 4095
  br i1 %73, label %74, label %87

; <label>:74:                                     ; preds = %35, %70
  %75 = and i32 %14, -8
  %76 = add i32 %75, 8
  %77 = tail call fastcc i64 @slow_ldq_mmu(i32 %75, i32 %1)
  %78 = tail call fastcc i64 @slow_ldq_mmu(i32 %76, i32 %1)
  %79 = shl i32 %14, 3
  %80 = and i32 %79, 56
  %81 = zext i32 %80 to i64
  %82 = lshr i64 %77, %81
  %83 = sub nsw i32 64, %80
  %84 = zext i32 %83 to i64
  %85 = shl i64 %78, %84
  %86 = or i64 %85, %82
  ret i64 %86

; <label>:87:                                     ; preds = %70
  %88 = zext i32 %14 to i64
  %89 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 77, i64 %19, i64 %18, i32 4
  %90 = load i64, i64* %89, align 8
  %91 = add i64 %90, %88
  %92 = inttoptr i64 %91 to i64*
  %93 = load i64, i64* %92, align 8
  %94 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %95 = load i32, i32* %94, align 4
  %96 = icmp eq i32 %95, 0
  br i1 %96, label %106, label %97

; <label>:97:                                     ; preds = %87
  tail call void @tcg_llvm_after_memory_access(i32 %14, i64 %93, i32 8, i32 0, i64 0)
  br label %106

; <label>:98:                                     ; preds = %27, %98
  %99 = phi %struct.CPUX86State* [ %20, %27 ], [ %100, %98 ]
  tail call void @tlb_fill(%struct.CPUX86State* %99, i32 %14, i32 %28, i32 0, i32 %1, i8* null)
  %100 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %101 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %100, i64 0, i32 77, i64 %19, i64 %18
  %102 = getelementptr inbounds %struct.CPUTLBEntry, %struct.CPUTLBEntry* %101, i64 0, i32 0
  %103 = load i32, i32* %102, align 8
  %104 = and i32 %103, -4088
  %105 = icmp eq i32 %24, %104
  br i1 %105, label %29, label %98

; <label>:106:                                    ; preds = %63, %87, %69, %97
  %107 = phi i64 [ %65, %69 ], [ %65, %63 ], [ %93, %97 ], [ %93, %87 ]
  ret i64 %107
}

; Function Attrs: noreturn uwtable
define void @helper_single_step() local_unnamed_addr #3 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = tail call i32 @check_hw_breakpoints(%struct.CPUX86State* %1, i32 1)
  %3 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %4 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 61, i64 6
  %5 = load i32, i32* %4, align 4
  %6 = or i32 %5, 16384
  store i32 %6, i32* %4, align 4
  tail call fastcc void @raise_exception(i32 1) #12
  unreachable
}

declare i32 @check_hw_breakpoints(%struct.CPUX86State*, i32) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_cpuid() local_unnamed_addr #2 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = bitcast i32* %1 to i8*
  %6 = bitcast i32* %2 to i8*
  %7 = bitcast i32* %3 to i8*
  %8 = bitcast i32* %4 to i8*
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 8
  %11 = load i32, i32* %10, align 4
  %12 = and i32 %11, 2097152
  %13 = icmp eq i32 %12, 0
  br i1 %13, label %20, label %14, !prof !2

; <label>:14:                                     ; preds = %0
  %15 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 42
  %16 = load i64, i64* %15, align 8
  %17 = and i64 %16, 262144
  %18 = icmp eq i64 %17, 0
  br i1 %18, label %20, label %19

; <label>:19:                                     ; preds = %14
  tail call void @helper_vmexit(i32 114, i64 0)
  unreachable

; <label>:20:                                     ; preds = %0, %14
  %21 = bitcast %struct.CPUX86State* %9 to i64*
  %22 = load i64, i64* %21, align 16
  %23 = trunc i64 %22 to i32
  %24 = icmp eq i32 %23, 4
  br i1 %24, label %25, label %28

; <label>:25:                                     ; preds = %20
  %26 = lshr i64 %22, 32
  %27 = trunc i64 %26 to i32
  call void @cpu_x86_cpuid(%struct.CPUX86State* nonnull %9, i32 4, i32 %27, i32* nonnull %1, i32* nonnull %2, i32* nonnull %3, i32* nonnull %4)
  br label %29

; <label>:28:                                     ; preds = %20
  call void @cpu_x86_cpuid(%struct.CPUX86State* nonnull %9, i32 %23, i32 0, i32* nonnull %1, i32* nonnull %2, i32* nonnull %3, i32* nonnull %4)
  br label %29

; <label>:29:                                     ; preds = %28, %25
  %30 = load i32, i32* %1, align 4
  %31 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %32 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 0, i64 0
  store i32 %30, i32* %32, align 16
  %33 = load i32, i32* %2, align 4
  %34 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %35 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %34, i64 0, i32 0, i64 3
  store i32 %33, i32* %35, align 4
  %36 = load i32, i32* %3, align 4
  %37 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %34, i64 0, i32 0, i64 1
  store i32 %36, i32* %37, align 4
  %38 = load i32, i32* %4, align 4
  %39 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %40 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %39, i64 0, i32 0, i64 2
  store i32 %38, i32* %40, align 8
  ret void
}

declare void @cpu_x86_cpuid(%struct.CPUX86State*, i32, i32, i32*, i32*, i32*, i32*) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_enter_level(i32, i32, i32) local_unnamed_addr #2 {
  %4 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 11, i64 2, i32 3
  %6 = load i32, i32* %5, align 4
  %7 = lshr i32 %6, 6
  %8 = and i32 %7, 65536
  %9 = xor i32 %8, 65536
  %10 = add nsw i32 %9, -1
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 11, i64 2, i32 1
  %12 = load i32, i32* %11, align 4
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 0, i64 5
  %14 = load i32, i32* %13, align 4
  %15 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 0, i64 4
  %16 = load i32, i32* %15, align 16
  %17 = icmp eq i32 %1, 0
  %18 = add nsw i32 %0, -1
  %19 = icmp eq i32 %18, 0
  br i1 %17, label %44, label %20

; <label>:20:                                     ; preds = %3
  %21 = add i32 %16, -8
  br i1 %19, label %40, label %22

; <label>:22:                                     ; preds = %20
  %23 = shl i32 %0, 2
  br label %24

; <label>:24:                                     ; preds = %22, %24
  %25 = phi i32 [ %36, %24 ], [ %21, %22 ]
  %26 = phi i32 [ %34, %24 ], [ %18, %22 ]
  %27 = phi i32 [ %28, %24 ], [ %14, %22 ]
  %28 = add i32 %27, -4
  %29 = and i32 %25, %10
  %30 = add i32 %29, %12
  %31 = and i32 %28, %10
  %32 = add i32 %31, %12
  %33 = tail call fastcc i32 @ldl_data(i32 %32)
  tail call fastcc void @stl_data(i32 %30, i32 %33)
  %34 = add nsw i32 %26, -1
  %35 = icmp eq i32 %34, 0
  %36 = add i32 %25, -4
  br i1 %35, label %37, label %24

; <label>:37:                                     ; preds = %24
  %38 = add i32 %16, -4
  %39 = sub i32 %38, %23
  br label %40

; <label>:40:                                     ; preds = %37, %20
  %41 = phi i32 [ %21, %20 ], [ %39, %37 ]
  %42 = and i32 %41, %10
  %43 = add i32 %42, %12
  tail call fastcc void @stl_data(i32 %43, i32 %2)
  br label %112

; <label>:44:                                     ; preds = %3
  %45 = add i32 %16, -4
  br i1 %19, label %108, label %46

; <label>:46:                                     ; preds = %44
  %47 = shl i32 %0, 1
  br label %48

; <label>:48:                                     ; preds = %46, %100
  %49 = phi i32 [ %104, %100 ], [ %45, %46 ]
  %50 = phi i32 [ %102, %100 ], [ %18, %46 ]
  %51 = phi i32 [ %52, %100 ], [ %14, %46 ]
  %52 = add i32 %51, -2
  %53 = and i32 %49, %10
  %54 = add i32 %53, %12
  %55 = and i32 %52, %10
  %56 = add i32 %55, %12
  %57 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %58 = load i32, i32* %57, align 4
  %59 = icmp eq i32 %58, 0
  br i1 %59, label %61, label %60

; <label>:60:                                     ; preds = %48
  tail call void @tcg_llvm_before_memory_access(i32 %56, i64 0, i32 4, i32 0)
  br label %61

; <label>:61:                                     ; preds = %60, %48
  %62 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %63 = load i32, i32* %62, align 4
  %64 = icmp eq i32 %63, 0
  br i1 %64, label %67, label %65

; <label>:65:                                     ; preds = %61
  %66 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %56, i32 0, i32 -1, i32 0)
  br label %67

; <label>:67:                                     ; preds = %65, %61
  %68 = phi i32 [ %66, %65 ], [ %56, %61 ]
  %69 = lshr i32 %68, 12
  %70 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %69, i32 0, i32 1048575, i32 0)
  %71 = and i32 %70, 1023
  %72 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %73 = getelementptr %struct.CPUX86State, %struct.CPUX86State* %72, i64 0, i32 8
  %74 = load i32, i32* %73, align 4
  %75 = and i32 %74, 3
  %76 = icmp eq i32 %75, 3
  %77 = zext i32 %71 to i64
  %78 = zext i1 %76 to i64
  %79 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %72, i64 0, i32 77, i64 %78, i64 %77, i32 0
  %80 = load i32, i32* %79, align 8
  %81 = and i32 %68, -4095
  %82 = icmp eq i32 %80, %81
  br i1 %82, label %87, label %83, !prof !2

; <label>:83:                                     ; preds = %67
  %84 = zext i1 %76 to i32
  %85 = tail call zeroext i16 @__ldw_mmu(i32 %68, i32 %84)
  %86 = zext i16 %85 to i32
  br label %100

; <label>:87:                                     ; preds = %67
  %88 = zext i32 %68 to i64
  %89 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %72, i64 0, i32 77, i64 %78, i64 %77, i32 4
  %90 = load i64, i64* %89, align 8
  %91 = add i64 %90, %88
  %92 = inttoptr i64 %91 to i16*
  %93 = load i16, i16* %92, align 2
  %94 = zext i16 %93 to i32
  %95 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %96 = load i32, i32* %95, align 4
  %97 = icmp eq i32 %96, 0
  br i1 %97, label %100, label %98

; <label>:98:                                     ; preds = %87
  %99 = zext i16 %93 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %68, i64 %99, i32 4, i32 0, i64 0)
  br label %100

; <label>:100:                                    ; preds = %83, %87, %98
  %101 = phi i32 [ %86, %83 ], [ %94, %98 ], [ %94, %87 ]
  tail call fastcc void @stw_data(i32 %54, i32 %101)
  %102 = add nsw i32 %50, -1
  %103 = icmp eq i32 %102, 0
  %104 = add i32 %49, -2
  br i1 %103, label %105, label %48

; <label>:105:                                    ; preds = %100
  %106 = add i32 %16, -2
  %107 = sub i32 %106, %47
  br label %108

; <label>:108:                                    ; preds = %105, %44
  %109 = phi i32 [ %45, %44 ], [ %107, %105 ]
  %110 = and i32 %109, %10
  %111 = add i32 %110, %12
  tail call fastcc void @stw_data(i32 %111, i32 %2)
  br label %112

; <label>:112:                                    ; preds = %108, %40
  ret void
}

; Function Attrs: uwtable
define internal fastcc i32 @ldl_data(i32) unnamed_addr #2 {
  %2 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %3 = load i32, i32* %2, align 4
  %4 = icmp eq i32 %3, 0
  br i1 %4, label %6, label %5

; <label>:5:                                      ; preds = %1
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %6

; <label>:6:                                      ; preds = %1, %5
  %7 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %8 = load i32, i32* %7, align 4
  %9 = icmp eq i32 %8, 0
  br i1 %9, label %12, label %10

; <label>:10:                                     ; preds = %6
  %11 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %12

; <label>:12:                                     ; preds = %6, %10
  %13 = phi i32 [ %11, %10 ], [ %0, %6 ]
  %14 = lshr i32 %13, 12
  %15 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %14, i32 0, i32 1048575, i32 0)
  %16 = and i32 %15, 1023
  %17 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %18 = getelementptr %struct.CPUX86State, %struct.CPUX86State* %17, i64 0, i32 8
  %19 = load i32, i32* %18, align 4
  %20 = and i32 %19, 3
  %21 = icmp eq i32 %20, 3
  %22 = zext i32 %16 to i64
  %23 = zext i1 %21 to i64
  %24 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %17, i64 0, i32 77, i64 %23, i64 %22, i32 0
  %25 = load i32, i32* %24, align 8
  %26 = and i32 %13, -4093
  %27 = icmp eq i32 %25, %26
  br i1 %27, label %31, label %28, !prof !2

; <label>:28:                                     ; preds = %12
  %29 = zext i1 %21 to i32
  %30 = tail call i32 @__ldl_mmu(i32 %13, i32 %29)
  br label %43

; <label>:31:                                     ; preds = %12
  %32 = zext i32 %13 to i64
  %33 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %17, i64 0, i32 77, i64 %23, i64 %22, i32 4
  %34 = load i64, i64* %33, align 8
  %35 = add i64 %34, %32
  %36 = inttoptr i64 %35 to i32*
  %37 = load i32, i32* %36, align 4
  %38 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %39 = load i32, i32* %38, align 4
  %40 = icmp eq i32 %39, 0
  br i1 %40, label %43, label %41

; <label>:41:                                     ; preds = %31
  %42 = zext i32 %37 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %13, i64 %42, i32 4, i32 0, i64 0)
  br label %43

; <label>:43:                                     ; preds = %31, %41, %28
  %44 = phi i32 [ %30, %28 ], [ %37, %41 ], [ %37, %31 ]
  ret i32 %44
}

; Function Attrs: uwtable
define internal fastcc void @stl_data(i32, i32) unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %8, label %6

; <label>:6:                                      ; preds = %2
  %7 = zext i32 %1 to i64
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 %7, i32 4, i32 1)
  br label %8

; <label>:8:                                      ; preds = %2, %6
  %9 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %10 = load i32, i32* %9, align 4
  %11 = icmp eq i32 %10, 0
  br i1 %11, label %14, label %12

; <label>:12:                                     ; preds = %8
  %13 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %14

; <label>:14:                                     ; preds = %8, %12
  %15 = phi i32 [ %13, %12 ], [ %0, %8 ]
  %16 = lshr i32 %15, 12
  %17 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %16, i32 0, i32 1048575, i32 0)
  %18 = and i32 %17, 1023
  %19 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %20 = getelementptr %struct.CPUX86State, %struct.CPUX86State* %19, i64 0, i32 8
  %21 = load i32, i32* %20, align 4
  %22 = and i32 %21, 3
  %23 = icmp eq i32 %22, 3
  %24 = zext i32 %18 to i64
  %25 = zext i1 %23 to i64
  %26 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %19, i64 0, i32 77, i64 %25, i64 %24, i32 1
  %27 = load i32, i32* %26, align 4
  %28 = and i32 %15, -4093
  %29 = icmp eq i32 %27, %28
  br i1 %29, label %32, label %30, !prof !2

; <label>:30:                                     ; preds = %14
  %31 = zext i1 %23 to i32
  tail call void @__stl_mmu(i32 %15, i32 %1, i32 %31)
  br label %43

; <label>:32:                                     ; preds = %14
  %33 = zext i32 %15 to i64
  %34 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %19, i64 0, i32 77, i64 %25, i64 %24, i32 4
  %35 = load i64, i64* %34, align 8
  %36 = add i64 %35, %33
  %37 = inttoptr i64 %36 to i32*
  store i32 %1, i32* %37, align 4
  %38 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %39 = load i32, i32* %38, align 4
  %40 = icmp eq i32 %39, 0
  br i1 %40, label %43, label %41

; <label>:41:                                     ; preds = %32
  %42 = zext i32 %1 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %15, i64 %42, i32 4, i32 2, i64 0)
  br label %43

; <label>:43:                                     ; preds = %32, %41, %30
  ret void
}

; Function Attrs: uwtable
define internal fastcc void @stw_data(i32, i32) unnamed_addr #2 {
  %3 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %8, label %6

; <label>:6:                                      ; preds = %2
  %7 = zext i32 %1 to i64
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 %7, i32 4, i32 1)
  br label %8

; <label>:8:                                      ; preds = %2, %6
  %9 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %10 = load i32, i32* %9, align 4
  %11 = icmp eq i32 %10, 0
  br i1 %11, label %14, label %12

; <label>:12:                                     ; preds = %8
  %13 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %14

; <label>:14:                                     ; preds = %8, %12
  %15 = phi i32 [ %13, %12 ], [ %0, %8 ]
  %16 = lshr i32 %15, 12
  %17 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %16, i32 0, i32 1048575, i32 0)
  %18 = and i32 %17, 1023
  %19 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %20 = getelementptr %struct.CPUX86State, %struct.CPUX86State* %19, i64 0, i32 8
  %21 = load i32, i32* %20, align 4
  %22 = and i32 %21, 3
  %23 = icmp eq i32 %22, 3
  %24 = zext i32 %18 to i64
  %25 = zext i1 %23 to i64
  %26 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %19, i64 0, i32 77, i64 %25, i64 %24, i32 1
  %27 = load i32, i32* %26, align 4
  %28 = and i32 %15, -4095
  %29 = icmp eq i32 %27, %28
  br i1 %29, label %33, label %30, !prof !2

; <label>:30:                                     ; preds = %14
  %31 = zext i1 %23 to i32
  %32 = trunc i32 %1 to i16
  tail call void @__stw_mmu(i32 %15, i16 zeroext %32, i32 %31)
  br label %45

; <label>:33:                                     ; preds = %14
  %34 = zext i32 %15 to i64
  %35 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %19, i64 0, i32 77, i64 %25, i64 %24, i32 4
  %36 = load i64, i64* %35, align 8
  %37 = add i64 %36, %34
  %38 = trunc i32 %1 to i16
  %39 = inttoptr i64 %37 to i16*
  store i16 %38, i16* %39, align 2
  %40 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %41 = load i32, i32* %40, align 4
  %42 = icmp eq i32 %41, 0
  br i1 %42, label %45, label %43

; <label>:43:                                     ; preds = %33
  %44 = zext i32 %1 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %15, i64 %44, i32 4, i32 2, i64 0)
  br label %45

; <label>:45:                                     ; preds = %33, %43, %30
  ret void
}

; Function Attrs: uwtable
define void @helper_lldt(i32) local_unnamed_addr #2 {
  %2 = and i32 %0, 65535
  %3 = and i32 %0, 65532
  %4 = icmp eq i32 %3, 0
  br i1 %4, label %5, label %9

; <label>:5:                                      ; preds = %1
  %6 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %6, i64 0, i32 12, i32 1
  store i32 0, i32* %7, align 4
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %6, i64 0, i32 12, i32 2
  store i32 0, i32* %8, align 8
  br label %54

; <label>:9:                                      ; preds = %1
  %10 = and i32 %0, 4
  %11 = icmp eq i32 %10, 0
  br i1 %11, label %13, label %12

; <label>:12:                                     ; preds = %9
  tail call fastcc void @raise_exception_err(i32 13, i32 %3) #12
  unreachable

; <label>:13:                                     ; preds = %9
  %14 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %15 = and i32 %0, 65528
  %16 = or i32 %15, 7
  %17 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %14, i64 0, i32 14, i32 2
  %18 = load i32, i32* %17, align 4
  %19 = icmp ugt i32 %16, %18
  br i1 %19, label %20, label %21

; <label>:20:                                     ; preds = %13
  tail call fastcc void @raise_exception_err(i32 13, i32 %3) #12
  unreachable

; <label>:21:                                     ; preds = %13
  %22 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %14, i64 0, i32 14, i32 1
  %23 = load i32, i32* %22, align 4
  %24 = add i32 %23, %15
  %25 = tail call fastcc i32 @ldl_kernel(i32 %24)
  %26 = add i32 %24, 4
  %27 = tail call fastcc i32 @ldl_kernel(i32 %26)
  %28 = and i32 %27, 7936
  %29 = icmp eq i32 %28, 512
  br i1 %29, label %31, label %30

; <label>:30:                                     ; preds = %21
  tail call fastcc void @raise_exception_err(i32 13, i32 %3) #12
  unreachable

; <label>:31:                                     ; preds = %21
  %32 = trunc i32 %27 to i16
  %33 = icmp slt i16 %32, 0
  br i1 %33, label %35, label %34

; <label>:34:                                     ; preds = %31
  tail call fastcc void @raise_exception_err(i32 11, i32 %3) #12
  unreachable

; <label>:35:                                     ; preds = %31
  %36 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %37 = lshr i32 %25, 16
  %38 = shl i32 %27, 16
  %39 = and i32 %38, 16711680
  %40 = and i32 %27, -16777216
  %41 = or i32 %40, %37
  %42 = or i32 %41, %39
  %43 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %36, i64 0, i32 12, i32 1
  store i32 %42, i32* %43, align 4
  %44 = and i32 %25, 65535
  %45 = and i32 %27, 983040
  %46 = or i32 %45, %44
  %47 = and i32 %27, 8388608
  %48 = icmp eq i32 %47, 0
  %49 = shl nuw i32 %46, 12
  %50 = or i32 %49, 4095
  %51 = select i1 %48, i32 %46, i32 %50
  %52 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %36, i64 0, i32 12, i32 2
  store i32 %51, i32* %52, align 4
  %53 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %36, i64 0, i32 12, i32 3
  store i32 %27, i32* %53, align 4
  br label %54

; <label>:54:                                     ; preds = %35, %5
  %55 = phi %struct.CPUX86State* [ %36, %35 ], [ %6, %5 ]
  %56 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %55, i64 0, i32 12, i32 0
  store i32 %2, i32* %56, align 8
  ret void
}

; Function Attrs: uwtable
define void @helper_ltr(i32) local_unnamed_addr #2 {
  %2 = and i32 %0, 65535
  %3 = and i32 %0, 65532
  %4 = icmp eq i32 %3, 0
  br i1 %4, label %5, label %10

; <label>:5:                                      ; preds = %1
  %6 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %6, i64 0, i32 13, i32 1
  store i32 0, i32* %7, align 4
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %6, i64 0, i32 13, i32 2
  store i32 0, i32* %8, align 8
  %9 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %6, i64 0, i32 13, i32 3
  store i32 0, i32* %9, align 4
  br label %60

; <label>:10:                                     ; preds = %1
  %11 = and i32 %0, 4
  %12 = icmp eq i32 %11, 0
  br i1 %12, label %14, label %13

; <label>:13:                                     ; preds = %10
  tail call fastcc void @raise_exception_err(i32 13, i32 %3) #12
  unreachable

; <label>:14:                                     ; preds = %10
  %15 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %16 = and i32 %0, 65528
  %17 = or i32 %16, 7
  %18 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %15, i64 0, i32 14, i32 2
  %19 = load i32, i32* %18, align 4
  %20 = icmp ugt i32 %17, %19
  br i1 %20, label %21, label %22

; <label>:21:                                     ; preds = %14
  tail call fastcc void @raise_exception_err(i32 13, i32 %3) #12
  unreachable

; <label>:22:                                     ; preds = %14
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %15, i64 0, i32 14, i32 1
  %24 = load i32, i32* %23, align 4
  %25 = add i32 %24, %16
  %26 = tail call fastcc i32 @ldl_kernel(i32 %25)
  %27 = add i32 %25, 4
  %28 = tail call fastcc i32 @ldl_kernel(i32 %27)
  %29 = and i32 %28, 4096
  %30 = icmp eq i32 %29, 0
  br i1 %30, label %31, label %34

; <label>:31:                                     ; preds = %22
  %32 = lshr i32 %28, 8
  %33 = trunc i32 %32 to i4
  switch i4 %33, label %34 [
    i4 -7, label %35
    i4 1, label %35
  ]

; <label>:34:                                     ; preds = %31, %22
  tail call fastcc void @raise_exception_err(i32 13, i32 %3) #12
  unreachable

; <label>:35:                                     ; preds = %31, %31
  %36 = trunc i32 %28 to i16
  %37 = icmp slt i16 %36, 0
  br i1 %37, label %39, label %38

; <label>:38:                                     ; preds = %35
  tail call fastcc void @raise_exception_err(i32 11, i32 %3) #12
  unreachable

; <label>:39:                                     ; preds = %35
  %40 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %41 = lshr i32 %26, 16
  %42 = shl i32 %28, 16
  %43 = and i32 %42, 16711680
  %44 = and i32 %28, -16777216
  %45 = or i32 %44, %41
  %46 = or i32 %45, %43
  %47 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %40, i64 0, i32 13, i32 1
  store i32 %46, i32* %47, align 4
  %48 = and i32 %26, 65535
  %49 = and i32 %28, 983040
  %50 = or i32 %49, %48
  %51 = and i32 %28, 8388608
  %52 = icmp eq i32 %51, 0
  %53 = shl nuw i32 %50, 12
  %54 = or i32 %53, 4095
  %55 = select i1 %52, i32 %50, i32 %54
  %56 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %40, i64 0, i32 13, i32 2
  store i32 %55, i32* %56, align 4
  %57 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %40, i64 0, i32 13, i32 3
  store i32 %28, i32* %57, align 4
  %58 = or i32 %28, 512
  tail call fastcc void @stl_kernel(i32 %27, i32 %58)
  %59 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %60

; <label>:60:                                     ; preds = %39, %5
  %61 = phi %struct.CPUX86State* [ %59, %39 ], [ %6, %5 ]
  %62 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %61, i64 0, i32 13, i32 0
  store i32 %2, i32* %62, align 8
  ret void
}

; Function Attrs: uwtable
define void @helper_load_seg(i32, i32) local_unnamed_addr #2 {
  %3 = and i32 %1, 65535
  %4 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 8
  %6 = load i32, i32* %5, align 4
  %7 = and i32 %6, 3
  %8 = and i32 %1, 65532
  %9 = icmp eq i32 %8, 0
  br i1 %9, label %10, label %75

; <label>:10:                                     ; preds = %2
  %11 = icmp eq i32 %0, 2
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %10
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:13:                                     ; preds = %10
  %14 = sext i32 %0 to i64
  %15 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 11, i64 %14, i32 0
  store i32 %3, i32* %15, align 4
  %16 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 11, i64 %14, i32 1
  store i32 0, i32* %16, align 4
  %17 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 11, i64 %14, i32 2
  store i32 0, i32* %17, align 4
  %18 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 11, i64 %14, i32 3
  store i32 0, i32* %18, align 4
  %19 = icmp eq i32 %0, 1
  br i1 %19, label %22, label %20

; <label>:20:                                     ; preds = %13
  %21 = load i32, i32* %5, align 4
  br label %30

; <label>:22:                                     ; preds = %13
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 11, i64 1, i32 3
  %24 = load i32, i32* %23, align 4
  %25 = lshr i32 %24, 18
  %26 = and i32 %25, 16
  %27 = load i32, i32* %5, align 4
  %28 = and i32 %27, -32785
  %29 = or i32 %28, %26
  store i32 %29, i32* %5, align 4
  br label %30

; <label>:30:                                     ; preds = %22, %20
  %31 = phi i32 [ %21, %20 ], [ %29, %22 ]
  %32 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 11, i64 2, i32 3
  %33 = load i32, i32* %32, align 4
  %34 = lshr i32 %33, 17
  %35 = and i32 %34, 32
  %36 = trunc i32 %31 to i16
  %37 = icmp slt i16 %36, 0
  br i1 %37, label %70, label %38

; <label>:38:                                     ; preds = %30
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 16, i64 0
  %40 = load i32, i32* %39, align 8
  %41 = and i32 %40, 1
  %42 = icmp eq i32 %41, 0
  br i1 %42, label %54, label %43

; <label>:43:                                     ; preds = %38
  %44 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 7
  %45 = bitcast i32* %44 to i64*
  %46 = load i64, i64* %45, align 8
  %47 = and i64 %46, 131072
  %48 = icmp ne i64 %47, 0
  %49 = and i32 %31, 16
  %50 = icmp eq i32 %49, 0
  %51 = or i1 %50, %48
  %52 = lshr i64 %46, 32
  %53 = trunc i64 %52 to i32
  br i1 %51, label %54, label %57

; <label>:54:                                     ; preds = %43, %38
  %55 = phi i32 [ %53, %43 ], [ %31, %38 ]
  %56 = or i32 %35, 64
  br label %70

; <label>:57:                                     ; preds = %43
  %58 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 11, i64 3, i32 1
  %59 = load i32, i32* %58, align 4
  %60 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 11, i64 0, i32 1
  %61 = load i32, i32* %60, align 4
  %62 = or i32 %61, %59
  %63 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 11, i64 2, i32 1
  %64 = load i32, i32* %63, align 4
  %65 = or i32 %62, %64
  %66 = icmp ne i32 %65, 0
  %67 = zext i1 %66 to i32
  %68 = shl nuw nsw i32 %67, 6
  %69 = or i32 %68, %35
  br label %70

; <label>:70:                                     ; preds = %30, %54, %57
  %71 = phi i32 [ %31, %30 ], [ %55, %54 ], [ %53, %57 ]
  %72 = phi i32 [ %35, %30 ], [ %56, %54 ], [ %69, %57 ]
  %73 = and i32 %71, -97
  %74 = or i32 %73, %72
  store i32 %74, i32* %5, align 4
  br label %214

; <label>:75:                                     ; preds = %2
  %76 = and i32 %1, 4
  %77 = icmp eq i32 %76, 0
  %78 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 12
  %79 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 14
  %80 = select i1 %77, %struct.SegmentCache* %79, %struct.SegmentCache* %78
  %81 = and i32 %1, 65528
  %82 = or i32 %81, 7
  %83 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %80, i64 0, i32 2
  %84 = load i32, i32* %83, align 4
  %85 = icmp ugt i32 %82, %84
  br i1 %85, label %86, label %87

; <label>:86:                                     ; preds = %75
  tail call fastcc void @raise_exception_err(i32 13, i32 %8) #12
  unreachable

; <label>:87:                                     ; preds = %75
  %88 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %80, i64 0, i32 1
  %89 = load i32, i32* %88, align 4
  %90 = add i32 %89, %81
  %91 = tail call fastcc i32 @ldl_kernel(i32 %90)
  %92 = add i32 %90, 4
  %93 = tail call fastcc i32 @ldl_kernel(i32 %92)
  %94 = and i32 %93, 4096
  %95 = icmp eq i32 %94, 0
  br i1 %95, label %96, label %97

; <label>:96:                                     ; preds = %87
  tail call fastcc void @raise_exception_err(i32 13, i32 %8) #12
  unreachable

; <label>:97:                                     ; preds = %87
  %98 = and i32 %1, 3
  %99 = lshr i32 %93, 13
  %100 = and i32 %99, 3
  %101 = icmp eq i32 %0, 2
  %102 = and i32 %93, 2560
  br i1 %101, label %103, label %111

; <label>:103:                                    ; preds = %97
  %104 = icmp eq i32 %102, 512
  br i1 %104, label %106, label %105

; <label>:105:                                    ; preds = %103
  tail call fastcc void @raise_exception_err(i32 13, i32 %8) #12
  unreachable

; <label>:106:                                    ; preds = %103
  %107 = icmp eq i32 %98, %7
  %108 = icmp eq i32 %100, %7
  %109 = and i1 %107, %108
  br i1 %109, label %122, label %110

; <label>:110:                                    ; preds = %106
  tail call fastcc void @raise_exception_err(i32 13, i32 %8) #12
  unreachable

; <label>:111:                                    ; preds = %97
  %112 = icmp eq i32 %102, 2048
  br i1 %112, label %113, label %114

; <label>:113:                                    ; preds = %111
  tail call fastcc void @raise_exception_err(i32 13, i32 %8) #12
  unreachable

; <label>:114:                                    ; preds = %111
  %115 = and i32 %93, 3072
  %116 = icmp eq i32 %115, 3072
  br i1 %116, label %122, label %117

; <label>:117:                                    ; preds = %114
  %118 = icmp ult i32 %100, %7
  %119 = icmp ult i32 %100, %98
  %120 = or i1 %118, %119
  br i1 %120, label %121, label %122

; <label>:121:                                    ; preds = %117
  tail call fastcc void @raise_exception_err(i32 13, i32 %8) #12
  unreachable

; <label>:122:                                    ; preds = %114, %117, %106
  %123 = trunc i32 %93 to i16
  %124 = icmp slt i16 %123, 0
  br i1 %124, label %128, label %125

; <label>:125:                                    ; preds = %122
  br i1 %101, label %126, label %127

; <label>:126:                                    ; preds = %125
  tail call fastcc void @raise_exception_err(i32 12, i32 %8) #12
  unreachable

; <label>:127:                                    ; preds = %125
  tail call fastcc void @raise_exception_err(i32 11, i32 %8) #12
  unreachable

; <label>:128:                                    ; preds = %122
  %129 = and i32 %93, 256
  %130 = icmp eq i32 %129, 0
  br i1 %130, label %131, label %133

; <label>:131:                                    ; preds = %128
  %132 = or i32 %93, 256
  tail call fastcc void @stl_kernel(i32 %92, i32 %132)
  br label %133

; <label>:133:                                    ; preds = %128, %131
  %134 = phi i32 [ %93, %128 ], [ %132, %131 ]
  %135 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %136 = lshr i32 %91, 16
  %137 = shl i32 %134, 16
  %138 = and i32 %137, 16711680
  %139 = and i32 %134, -16777216
  %140 = or i32 %139, %136
  %141 = or i32 %140, %138
  %142 = and i32 %91, 65535
  %143 = and i32 %134, 983040
  %144 = or i32 %143, %142
  %145 = and i32 %134, 8388608
  %146 = icmp eq i32 %145, 0
  %147 = shl nuw i32 %144, 12
  %148 = or i32 %147, 4095
  %149 = select i1 %146, i32 %144, i32 %148
  %150 = sext i32 %0 to i64
  %151 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 11, i64 %150, i32 0
  store i32 %3, i32* %151, align 4
  %152 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 11, i64 %150, i32 1
  store i32 %141, i32* %152, align 4
  %153 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 11, i64 %150, i32 2
  store i32 %149, i32* %153, align 4
  %154 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 11, i64 %150, i32 3
  store i32 %134, i32* %154, align 4
  %155 = icmp eq i32 %0, 1
  br i1 %155, label %159, label %156

; <label>:156:                                    ; preds = %133
  %157 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 8
  %158 = load i32, i32* %157, align 4
  br label %168

; <label>:159:                                    ; preds = %133
  %160 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 11, i64 1, i32 3
  %161 = load i32, i32* %160, align 4
  %162 = lshr i32 %161, 18
  %163 = and i32 %162, 16
  %164 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 8
  %165 = load i32, i32* %164, align 4
  %166 = and i32 %165, -32785
  %167 = or i32 %166, %163
  store i32 %167, i32* %164, align 4
  br label %168

; <label>:168:                                    ; preds = %159, %156
  %169 = phi i32* [ %157, %156 ], [ %164, %159 ]
  %170 = phi i32 [ %158, %156 ], [ %167, %159 ]
  %171 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 11, i64 2, i32 3
  %172 = load i32, i32* %171, align 4
  %173 = lshr i32 %172, 17
  %174 = and i32 %173, 32
  %175 = trunc i32 %170 to i16
  %176 = icmp slt i16 %175, 0
  br i1 %176, label %209, label %177

; <label>:177:                                    ; preds = %168
  %178 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 16, i64 0
  %179 = load i32, i32* %178, align 8
  %180 = and i32 %179, 1
  %181 = icmp eq i32 %180, 0
  br i1 %181, label %193, label %182

; <label>:182:                                    ; preds = %177
  %183 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 7
  %184 = bitcast i32* %183 to i64*
  %185 = load i64, i64* %184, align 8
  %186 = and i64 %185, 131072
  %187 = icmp ne i64 %186, 0
  %188 = and i32 %170, 16
  %189 = icmp eq i32 %188, 0
  %190 = or i1 %189, %187
  %191 = lshr i64 %185, 32
  %192 = trunc i64 %191 to i32
  br i1 %190, label %193, label %196

; <label>:193:                                    ; preds = %182, %177
  %194 = phi i32 [ %192, %182 ], [ %170, %177 ]
  %195 = or i32 %174, 64
  br label %209

; <label>:196:                                    ; preds = %182
  %197 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 11, i64 3, i32 1
  %198 = load i32, i32* %197, align 4
  %199 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 11, i64 0, i32 1
  %200 = load i32, i32* %199, align 4
  %201 = or i32 %200, %198
  %202 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %135, i64 0, i32 11, i64 2, i32 1
  %203 = load i32, i32* %202, align 4
  %204 = or i32 %201, %203
  %205 = icmp ne i32 %204, 0
  %206 = zext i1 %205 to i32
  %207 = shl nuw nsw i32 %206, 6
  %208 = or i32 %207, %174
  br label %209

; <label>:209:                                    ; preds = %168, %193, %196
  %210 = phi i32 [ %170, %168 ], [ %194, %193 ], [ %192, %196 ]
  %211 = phi i32 [ %174, %168 ], [ %195, %193 ], [ %208, %196 ]
  %212 = and i32 %210, -97
  %213 = or i32 %212, %211
  store i32 %213, i32* %169, align 4
  br label %214

; <label>:214:                                    ; preds = %209, %70
  ret void
}

; Function Attrs: uwtable
define void @helper_ljmp_protected(i32, i32, i32) local_unnamed_addr #2 {
  %4 = and i32 %0, 65532
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %6, label %7

; <label>:6:                                      ; preds = %3
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:7:                                      ; preds = %3
  %8 = and i32 %0, 4
  %9 = icmp eq i32 %8, 0
  %10 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 12
  %12 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 14
  %13 = select i1 %9, %struct.SegmentCache* %12, %struct.SegmentCache* %11
  %14 = or i32 %0, 7
  %15 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %13, i64 0, i32 2
  %16 = load i32, i32* %15, align 4
  %17 = icmp ugt i32 %14, %16
  br i1 %17, label %18, label %19

; <label>:18:                                     ; preds = %7
  tail call fastcc void @raise_exception_err(i32 13, i32 %4) #12
  unreachable

; <label>:19:                                     ; preds = %7
  %20 = and i32 %0, -8
  %21 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %13, i64 0, i32 1
  %22 = load i32, i32* %21, align 4
  %23 = add i32 %22, %20
  %24 = tail call fastcc i32 @ldl_kernel(i32 %23)
  %25 = add i32 %23, 4
  %26 = tail call fastcc i32 @ldl_kernel(i32 %25)
  %27 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %28 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 8
  %29 = load i32, i32* %28, align 4
  %30 = and i32 %29, 3
  %31 = and i32 %26, 4096
  %32 = icmp eq i32 %31, 0
  br i1 %32, label %128, label %33

; <label>:33:                                     ; preds = %19
  %34 = and i32 %26, 2048
  %35 = icmp eq i32 %34, 0
  br i1 %35, label %36, label %37

; <label>:36:                                     ; preds = %33
  tail call fastcc void @raise_exception_err(i32 13, i32 %4) #12
  unreachable

; <label>:37:                                     ; preds = %33
  %38 = lshr i32 %26, 13
  %39 = and i32 %38, 3
  %40 = and i32 %26, 1024
  %41 = icmp eq i32 %40, 0
  br i1 %41, label %45, label %42

; <label>:42:                                     ; preds = %37
  %43 = icmp ugt i32 %39, %30
  br i1 %43, label %44, label %52

; <label>:44:                                     ; preds = %42
  tail call fastcc void @raise_exception_err(i32 13, i32 %4) #12
  unreachable

; <label>:45:                                     ; preds = %37
  %46 = and i32 %0, 3
  %47 = icmp ugt i32 %46, %30
  br i1 %47, label %48, label %49

; <label>:48:                                     ; preds = %45
  tail call fastcc void @raise_exception_err(i32 13, i32 %4) #12
  unreachable

; <label>:49:                                     ; preds = %45
  %50 = icmp eq i32 %39, %30
  br i1 %50, label %52, label %51

; <label>:51:                                     ; preds = %49
  tail call fastcc void @raise_exception_err(i32 13, i32 %4) #12
  unreachable

; <label>:52:                                     ; preds = %49, %42
  %53 = trunc i32 %26 to i16
  %54 = icmp slt i16 %53, 0
  br i1 %54, label %56, label %55

; <label>:55:                                     ; preds = %52
  tail call fastcc void @raise_exception_err(i32 11, i32 %4) #12
  unreachable

; <label>:56:                                     ; preds = %52
  %57 = and i32 %24, 65535
  %58 = and i32 %26, 983040
  %59 = or i32 %58, %57
  %60 = and i32 %26, 8388608
  %61 = icmp eq i32 %60, 0
  %62 = shl nuw i32 %59, 12
  %63 = or i32 %62, 4095
  %64 = select i1 %61, i32 %59, i32 %63
  %65 = icmp ult i32 %64, %1
  br i1 %65, label %66, label %72

; <label>:66:                                     ; preds = %56
  %67 = and i32 %29, 16384
  %68 = and i32 %26, 2097152
  %69 = or i32 %67, %68
  %70 = icmp eq i32 %69, 0
  br i1 %70, label %71, label %72

; <label>:71:                                     ; preds = %66
  tail call fastcc void @raise_exception_err(i32 13, i32 %4) #12
  unreachable

; <label>:72:                                     ; preds = %56, %66
  %73 = or i32 %30, %4
  %74 = lshr i32 %24, 16
  %75 = shl i32 %26, 16
  %76 = and i32 %75, 16711680
  %77 = and i32 %26, -16777216
  %78 = or i32 %77, %74
  %79 = or i32 %78, %76
  %80 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 11, i64 1, i32 0
  store i32 %73, i32* %80, align 4
  %81 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 11, i64 1, i32 1
  store i32 %79, i32* %81, align 4
  %82 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 11, i64 1, i32 2
  store i32 %64, i32* %82, align 4
  %83 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 11, i64 1, i32 3
  store i32 %26, i32* %83, align 4
  %84 = lshr i32 %26, 18
  %85 = and i32 %84, 16
  %86 = and i32 %29, -32785
  %87 = or i32 %86, %85
  store i32 %87, i32* %28, align 4
  %88 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 11, i64 2, i32 3
  %89 = load i32, i32* %88, align 4
  %90 = lshr i32 %89, 17
  %91 = and i32 %90, 32
  %92 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 16, i64 0
  %93 = load i32, i32* %92, align 8
  %94 = and i32 %93, 1
  %95 = icmp eq i32 %94, 0
  br i1 %95, label %106, label %96

; <label>:96:                                     ; preds = %72
  %97 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 7
  %98 = bitcast i32* %97 to i64*
  %99 = load i64, i64* %98, align 8
  %100 = and i64 %99, 131072
  %101 = icmp ne i64 %100, 0
  %102 = icmp eq i32 %85, 0
  %103 = or i1 %102, %101
  %104 = lshr i64 %99, 32
  %105 = trunc i64 %104 to i32
  br i1 %103, label %106, label %109

; <label>:106:                                    ; preds = %96, %72
  %107 = phi i32 [ %105, %96 ], [ %87, %72 ]
  %108 = or i32 %91, 64
  br label %122

; <label>:109:                                    ; preds = %96
  %110 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 11, i64 3, i32 1
  %111 = load i32, i32* %110, align 4
  %112 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 11, i64 0, i32 1
  %113 = load i32, i32* %112, align 4
  %114 = or i32 %113, %111
  %115 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 11, i64 2, i32 1
  %116 = load i32, i32* %115, align 4
  %117 = or i32 %114, %116
  %118 = icmp ne i32 %117, 0
  %119 = zext i1 %118 to i32
  %120 = shl nuw nsw i32 %119, 6
  %121 = or i32 %120, %91
  br label %122

; <label>:122:                                    ; preds = %106, %109
  %123 = phi i32 [ %107, %106 ], [ %105, %109 ]
  %124 = phi i32 [ %108, %106 ], [ %121, %109 ]
  %125 = and i32 %123, -97
  %126 = or i32 %125, %124
  store i32 %126, i32* %28, align 4
  %127 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 5
  store i32 %1, i32* %127, align 16
  br label %274

; <label>:128:                                    ; preds = %19
  %129 = lshr i32 %26, 13
  %130 = and i32 %129, 3
  %131 = and i32 %0, 3
  %132 = lshr i32 %26, 8
  %133 = and i32 %132, 15
  %134 = trunc i32 %132 to i4
  switch i4 %134, label %273 [
    i4 1, label %135
    i4 -7, label %135
    i4 5, label %135
    i4 4, label %146
    i4 -4, label %146
  ]

; <label>:135:                                    ; preds = %128, %128, %128
  %136 = icmp ult i32 %130, %30
  %137 = icmp ult i32 %130, %131
  %138 = or i1 %137, %136
  br i1 %138, label %139, label %140

; <label>:139:                                    ; preds = %135
  tail call fastcc void @raise_exception_err(i32 13, i32 %4) #12
  unreachable

; <label>:140:                                    ; preds = %135
  %141 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 5
  %142 = load i32, i32* %141, align 16
  %143 = add i32 %142, %2
  tail call fastcc void @switch_tss(i32 %0, i32 %24, i32 %26, i32 0, i32 %143)
  %144 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %145 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %144, i64 0, i32 1
  store i32 1, i32* %145, align 16
  br label %274

; <label>:146:                                    ; preds = %128, %128
  %147 = icmp ult i32 %130, %30
  %148 = icmp ult i32 %130, %131
  %149 = or i1 %148, %147
  br i1 %149, label %150, label %151

; <label>:150:                                    ; preds = %146
  tail call fastcc void @raise_exception_err(i32 13, i32 %4) #12
  unreachable

; <label>:151:                                    ; preds = %146
  %152 = trunc i32 %26 to i16
  %153 = icmp slt i16 %152, 0
  br i1 %153, label %155, label %154

; <label>:154:                                    ; preds = %151
  tail call fastcc void @raise_exception_err(i32 11, i32 %4) #12
  unreachable

; <label>:155:                                    ; preds = %151
  %156 = lshr i32 %24, 16
  %157 = and i32 %24, 65535
  %158 = icmp eq i32 %133, 12
  %159 = and i32 %26, -65536
  %160 = select i1 %158, i32 %159, i32 0
  %161 = or i32 %160, %157
  %162 = and i32 %156, 4
  %163 = icmp eq i32 %162, 0
  %164 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 12
  %165 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 14
  %166 = select i1 %163, %struct.SegmentCache* %165, %struct.SegmentCache* %164
  %167 = or i32 %156, 7
  %168 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %166, i64 0, i32 2
  %169 = load i32, i32* %168, align 4
  %170 = icmp ugt i32 %167, %169
  br i1 %170, label %171, label %173

; <label>:171:                                    ; preds = %155
  %172 = and i32 %156, 65532
  tail call fastcc void @raise_exception_err(i32 13, i32 %172) #12
  unreachable

; <label>:173:                                    ; preds = %155
  %174 = and i32 %156, 65528
  %175 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %166, i64 0, i32 1
  %176 = load i32, i32* %175, align 4
  %177 = add i32 %176, %174
  %178 = tail call fastcc i32 @ldl_kernel(i32 %177)
  %179 = add i32 %177, 4
  %180 = tail call fastcc i32 @ldl_kernel(i32 %179)
  %181 = lshr i32 %180, 13
  %182 = and i32 %181, 3
  %183 = and i32 %180, 6144
  %184 = icmp eq i32 %183, 6144
  br i1 %184, label %187, label %185

; <label>:185:                                    ; preds = %173
  %186 = and i32 %156, 65532
  tail call fastcc void @raise_exception_err(i32 13, i32 %186) #12
  unreachable

; <label>:187:                                    ; preds = %173
  %188 = and i32 %180, 1024
  %189 = icmp ne i32 %188, 0
  %190 = icmp ugt i32 %182, %30
  %191 = and i1 %189, %190
  %192 = icmp eq i32 %182, %30
  %193 = or i1 %189, %192
  %194 = xor i1 %191, %193
  br i1 %194, label %197, label %195

; <label>:195:                                    ; preds = %187
  %196 = and i32 %156, 65532
  tail call fastcc void @raise_exception_err(i32 13, i32 %196) #12
  unreachable

; <label>:197:                                    ; preds = %187
  %198 = trunc i32 %180 to i16
  %199 = icmp slt i16 %198, 0
  br i1 %199, label %202, label %200

; <label>:200:                                    ; preds = %197
  %201 = and i32 %156, 65532
  tail call fastcc void @raise_exception_err(i32 13, i32 %201) #12
  unreachable

; <label>:202:                                    ; preds = %197
  %203 = and i32 %178, 65535
  %204 = and i32 %180, 983040
  %205 = or i32 %204, %203
  %206 = and i32 %180, 8388608
  %207 = icmp eq i32 %206, 0
  %208 = shl nuw i32 %205, 12
  %209 = or i32 %208, 4095
  %210 = select i1 %207, i32 %205, i32 %209
  %211 = icmp ugt i32 %161, %210
  br i1 %211, label %212, label %213

; <label>:212:                                    ; preds = %202
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:213:                                    ; preds = %202
  %214 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %215 = and i32 %156, 65532
  %216 = or i32 %30, %215
  %217 = lshr i32 %178, 16
  %218 = shl i32 %180, 16
  %219 = and i32 %218, 16711680
  %220 = and i32 %180, -16777216
  %221 = or i32 %220, %217
  %222 = or i32 %221, %219
  %223 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 11, i64 1, i32 0
  store i32 %216, i32* %223, align 4
  %224 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 11, i64 1, i32 1
  store i32 %222, i32* %224, align 4
  %225 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 11, i64 1, i32 2
  store i32 %210, i32* %225, align 4
  %226 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 11, i64 1, i32 3
  store i32 %180, i32* %226, align 4
  %227 = lshr i32 %180, 18
  %228 = and i32 %227, 16
  %229 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 8
  %230 = load i32, i32* %229, align 4
  %231 = and i32 %230, -32785
  %232 = or i32 %231, %228
  store i32 %232, i32* %229, align 4
  %233 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 11, i64 2, i32 3
  %234 = load i32, i32* %233, align 4
  %235 = lshr i32 %234, 17
  %236 = and i32 %235, 32
  %237 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 16, i64 0
  %238 = load i32, i32* %237, align 8
  %239 = and i32 %238, 1
  %240 = icmp eq i32 %239, 0
  br i1 %240, label %251, label %241

; <label>:241:                                    ; preds = %213
  %242 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 7
  %243 = bitcast i32* %242 to i64*
  %244 = load i64, i64* %243, align 8
  %245 = and i64 %244, 131072
  %246 = icmp ne i64 %245, 0
  %247 = icmp eq i32 %228, 0
  %248 = or i1 %247, %246
  %249 = lshr i64 %244, 32
  %250 = trunc i64 %249 to i32
  br i1 %248, label %251, label %254

; <label>:251:                                    ; preds = %241, %213
  %252 = phi i32 [ %250, %241 ], [ %232, %213 ]
  %253 = or i32 %236, 64
  br label %267

; <label>:254:                                    ; preds = %241
  %255 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 11, i64 3, i32 1
  %256 = load i32, i32* %255, align 4
  %257 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 11, i64 0, i32 1
  %258 = load i32, i32* %257, align 4
  %259 = or i32 %258, %256
  %260 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 11, i64 2, i32 1
  %261 = load i32, i32* %260, align 4
  %262 = or i32 %259, %261
  %263 = icmp ne i32 %262, 0
  %264 = zext i1 %263 to i32
  %265 = shl nuw nsw i32 %264, 6
  %266 = or i32 %265, %236
  br label %267

; <label>:267:                                    ; preds = %251, %254
  %268 = phi i32 [ %252, %251 ], [ %250, %254 ]
  %269 = phi i32 [ %253, %251 ], [ %266, %254 ]
  %270 = and i32 %268, -97
  %271 = or i32 %270, %269
  store i32 %271, i32* %229, align 4
  %272 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %214, i64 0, i32 5
  store i32 %161, i32* %272, align 16
  br label %274

; <label>:273:                                    ; preds = %128
  tail call fastcc void @raise_exception_err(i32 13, i32 %4) #12
  unreachable

; <label>:274:                                    ; preds = %140, %267, %122
  ret void
}

; Function Attrs: uwtable
define void @helper_lcall_real(i32, i32, i32, i32) local_unnamed_addr #2 {
  %5 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 0, i64 4
  %7 = load i32, i32* %6, align 16
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 11, i64 2, i32 3
  %9 = load i32, i32* %8, align 4
  %10 = lshr i32 %9, 6
  %11 = and i32 %10, 65536
  %12 = xor i32 %11, 65536
  %13 = add nsw i32 %12, -1
  %14 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 11, i64 2, i32 1
  %15 = load i32, i32* %14, align 4
  %16 = icmp eq i32 %2, 0
  %17 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 11, i64 1, i32 0
  %18 = load i32, i32* %17, align 8
  br i1 %16, label %26, label %19

; <label>:19:                                     ; preds = %4
  %20 = add i32 %7, -4
  %21 = and i32 %13, %20
  %22 = add i32 %21, %15
  tail call fastcc void @stl_kernel(i32 %22, i32 %18)
  %23 = add i32 %7, -8
  %24 = and i32 %13, %23
  %25 = add i32 %24, %15
  tail call fastcc void @stl_kernel(i32 %25, i32 %3)
  br label %33

; <label>:26:                                     ; preds = %4
  %27 = add i32 %7, -2
  %28 = and i32 %13, %27
  %29 = add i32 %28, %15
  tail call fastcc void @stw_kernel(i32 %29, i32 %18)
  %30 = add i32 %7, -4
  %31 = and i32 %13, %30
  %32 = add i32 %31, %15
  tail call fastcc void @stw_kernel(i32 %32, i32 %3)
  br label %33

; <label>:33:                                     ; preds = %26, %19
  %34 = phi i32 [ %23, %19 ], [ %30, %26 ]
  %35 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 0, i64 4
  %37 = load i32, i32* %36, align 16
  %38 = sub nsw i32 0, %12
  %39 = and i32 %37, %38
  %40 = and i32 %34, %13
  %41 = or i32 %39, %40
  store i32 %41, i32* %36, align 16
  %42 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 5
  store i32 %1, i32* %42, align 16
  %43 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 1, i32 0
  store i32 %0, i32* %43, align 8
  %44 = shl i32 %0, 4
  %45 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 1, i32 1
  store i32 %44, i32* %45, align 4
  ret void
}

; Function Attrs: uwtable
define void @helper_lcall_protected(i32, i32, i32, i32) local_unnamed_addr #2 {
  %5 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 5
  %7 = load i32, i32* %6, align 16
  %8 = add i32 %7, %3
  %9 = and i32 %0, 65532
  %10 = icmp eq i32 %9, 0
  br i1 %10, label %11, label %12

; <label>:11:                                     ; preds = %4
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:12:                                     ; preds = %4
  %13 = and i32 %0, 4
  %14 = icmp eq i32 %13, 0
  %15 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 12
  %16 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 14
  %17 = select i1 %14, %struct.SegmentCache* %16, %struct.SegmentCache* %15
  %18 = or i32 %0, 7
  %19 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %17, i64 0, i32 2
  %20 = load i32, i32* %19, align 4
  %21 = icmp ugt i32 %18, %20
  br i1 %21, label %22, label %23

; <label>:22:                                     ; preds = %12
  tail call fastcc void @raise_exception_err(i32 13, i32 %9) #12
  unreachable

; <label>:23:                                     ; preds = %12
  %24 = and i32 %0, -8
  %25 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %17, i64 0, i32 1
  %26 = load i32, i32* %25, align 4
  %27 = add i32 %26, %24
  %28 = tail call fastcc i32 @ldl_kernel(i32 %27)
  %29 = add i32 %27, 4
  %30 = tail call fastcc i32 @ldl_kernel(i32 %29)
  %31 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %32 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 8
  %33 = load i32, i32* %32, align 4
  %34 = and i32 %33, 3
  %35 = and i32 %30, 4096
  %36 = icmp eq i32 %35, 0
  br i1 %36, label %165, label %37

; <label>:37:                                     ; preds = %23
  %38 = and i32 %30, 2048
  %39 = icmp eq i32 %38, 0
  br i1 %39, label %40, label %41

; <label>:40:                                     ; preds = %37
  tail call fastcc void @raise_exception_err(i32 13, i32 %9) #12
  unreachable

; <label>:41:                                     ; preds = %37
  %42 = lshr i32 %30, 13
  %43 = and i32 %42, 3
  %44 = and i32 %30, 1024
  %45 = icmp eq i32 %44, 0
  br i1 %45, label %49, label %46

; <label>:46:                                     ; preds = %41
  %47 = icmp ugt i32 %43, %34
  br i1 %47, label %48, label %56

; <label>:48:                                     ; preds = %46
  tail call fastcc void @raise_exception_err(i32 13, i32 %9) #12
  unreachable

; <label>:49:                                     ; preds = %41
  %50 = and i32 %0, 3
  %51 = icmp ugt i32 %50, %34
  br i1 %51, label %52, label %53

; <label>:52:                                     ; preds = %49
  tail call fastcc void @raise_exception_err(i32 13, i32 %9) #12
  unreachable

; <label>:53:                                     ; preds = %49
  %54 = icmp eq i32 %43, %34
  br i1 %54, label %56, label %55

; <label>:55:                                     ; preds = %53
  tail call fastcc void @raise_exception_err(i32 13, i32 %9) #12
  unreachable

; <label>:56:                                     ; preds = %53, %46
  %57 = trunc i32 %30 to i16
  %58 = icmp slt i16 %57, 0
  br i1 %58, label %60, label %59

; <label>:59:                                     ; preds = %56
  tail call fastcc void @raise_exception_err(i32 11, i32 %9) #12
  unreachable

; <label>:60:                                     ; preds = %56
  %61 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 0, i64 4
  %62 = load i32, i32* %61, align 16
  %63 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 11, i64 2, i32 3
  %64 = load i32, i32* %63, align 4
  %65 = lshr i32 %64, 6
  %66 = and i32 %65, 65536
  %67 = xor i32 %66, 65536
  %68 = add nsw i32 %67, -1
  %69 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 11, i64 2, i32 1
  %70 = load i32, i32* %69, align 4
  %71 = icmp eq i32 %2, 0
  %72 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 11, i64 1, i32 0
  %73 = load i32, i32* %72, align 8
  br i1 %71, label %81, label %74

; <label>:74:                                     ; preds = %60
  %75 = add i32 %62, -4
  %76 = and i32 %68, %75
  %77 = add i32 %76, %70
  tail call fastcc void @stl_kernel(i32 %77, i32 %73)
  %78 = add i32 %62, -8
  %79 = and i32 %68, %78
  %80 = add i32 %79, %70
  tail call fastcc void @stl_kernel(i32 %80, i32 %8)
  br label %88

; <label>:81:                                     ; preds = %60
  %82 = add i32 %62, -2
  %83 = and i32 %68, %82
  %84 = add i32 %83, %70
  tail call fastcc void @stw_kernel(i32 %84, i32 %73)
  %85 = add i32 %62, -4
  %86 = and i32 %68, %85
  %87 = add i32 %86, %70
  tail call fastcc void @stw_kernel(i32 %87, i32 %8)
  br label %88

; <label>:88:                                     ; preds = %81, %74
  %89 = phi i32 [ %85, %81 ], [ %78, %74 ]
  %90 = and i32 %28, 65535
  %91 = and i32 %30, 983040
  %92 = or i32 %91, %90
  %93 = and i32 %30, 8388608
  %94 = icmp eq i32 %93, 0
  %95 = shl nuw i32 %92, 12
  %96 = or i32 %95, 4095
  %97 = select i1 %94, i32 %92, i32 %96
  %98 = icmp ult i32 %97, %1
  br i1 %98, label %99, label %100

; <label>:99:                                     ; preds = %88
  tail call fastcc void @raise_exception_err(i32 13, i32 %9) #12
  unreachable

; <label>:100:                                    ; preds = %88
  %101 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %102 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 0, i64 4
  %103 = load i32, i32* %102, align 16
  %104 = sub nsw i32 0, %67
  %105 = and i32 %103, %104
  %106 = and i32 %89, %68
  %107 = or i32 %105, %106
  store i32 %107, i32* %102, align 16
  %108 = or i32 %34, %9
  %109 = lshr i32 %28, 16
  %110 = shl i32 %30, 16
  %111 = and i32 %110, 16711680
  %112 = and i32 %30, -16777216
  %113 = or i32 %112, %109
  %114 = or i32 %113, %111
  %115 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 11, i64 1, i32 0
  store i32 %108, i32* %115, align 4
  %116 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 11, i64 1, i32 1
  store i32 %114, i32* %116, align 4
  %117 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 11, i64 1, i32 2
  store i32 %97, i32* %117, align 4
  %118 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 11, i64 1, i32 3
  store i32 %30, i32* %118, align 4
  %119 = lshr i32 %30, 18
  %120 = and i32 %119, 16
  %121 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 8
  %122 = load i32, i32* %121, align 4
  %123 = and i32 %122, -32785
  %124 = or i32 %123, %120
  store i32 %124, i32* %121, align 4
  %125 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 11, i64 2, i32 3
  %126 = load i32, i32* %125, align 4
  %127 = lshr i32 %126, 17
  %128 = and i32 %127, 32
  %129 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 16, i64 0
  %130 = load i32, i32* %129, align 8
  %131 = and i32 %130, 1
  %132 = icmp eq i32 %131, 0
  br i1 %132, label %143, label %133

; <label>:133:                                    ; preds = %100
  %134 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 7
  %135 = bitcast i32* %134 to i64*
  %136 = load i64, i64* %135, align 8
  %137 = and i64 %136, 131072
  %138 = icmp ne i64 %137, 0
  %139 = icmp eq i32 %120, 0
  %140 = or i1 %139, %138
  %141 = lshr i64 %136, 32
  %142 = trunc i64 %141 to i32
  br i1 %140, label %143, label %146

; <label>:143:                                    ; preds = %133, %100
  %144 = phi i32 [ %142, %133 ], [ %124, %100 ]
  %145 = or i32 %128, 64
  br label %159

; <label>:146:                                    ; preds = %133
  %147 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 11, i64 3, i32 1
  %148 = load i32, i32* %147, align 4
  %149 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 11, i64 0, i32 1
  %150 = load i32, i32* %149, align 4
  %151 = or i32 %150, %148
  %152 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 11, i64 2, i32 1
  %153 = load i32, i32* %152, align 4
  %154 = or i32 %151, %153
  %155 = icmp ne i32 %154, 0
  %156 = zext i1 %155 to i32
  %157 = shl nuw nsw i32 %156, 6
  %158 = or i32 %157, %128
  br label %159

; <label>:159:                                    ; preds = %143, %146
  %160 = phi i32 [ %144, %143 ], [ %142, %146 ]
  %161 = phi i32 [ %145, %143 ], [ %158, %146 ]
  %162 = and i32 %160, -97
  %163 = or i32 %162, %161
  store i32 %163, i32* %121, align 4
  %164 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %101, i64 0, i32 5
  store i32 %1, i32* %164, align 16
  br label %617

; <label>:165:                                    ; preds = %23
  %166 = lshr i32 %30, 8
  %167 = lshr i32 %30, 13
  %168 = and i32 %167, 3
  %169 = and i32 %0, 3
  %170 = trunc i32 %166 to i5
  switch i5 %170, label %179 [
    i5 1, label %171
    i5 9, label %171
    i5 5, label %171
    i5 4, label %180
    i5 12, label %180
  ]

; <label>:171:                                    ; preds = %165, %165, %165
  %172 = icmp ult i32 %168, %34
  %173 = icmp ult i32 %168, %169
  %174 = or i1 %173, %172
  br i1 %174, label %175, label %176

; <label>:175:                                    ; preds = %171
  tail call fastcc void @raise_exception_err(i32 13, i32 %9) #12
  unreachable

; <label>:176:                                    ; preds = %171
  tail call fastcc void @switch_tss(i32 %0, i32 %28, i32 %30, i32 2, i32 %8)
  %177 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %178 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %177, i64 0, i32 1
  store i32 1, i32* %178, align 16
  br label %617

; <label>:179:                                    ; preds = %165
  tail call fastcc void @raise_exception_err(i32 13, i32 %9) #12
  unreachable

; <label>:180:                                    ; preds = %165, %165
  %181 = lshr i32 %30, 11
  %182 = and i32 %181, 3
  %183 = icmp ult i32 %168, %34
  %184 = icmp ult i32 %168, %169
  %185 = or i1 %184, %183
  br i1 %185, label %186, label %187

; <label>:186:                                    ; preds = %180
  tail call fastcc void @raise_exception_err(i32 13, i32 %9) #12
  unreachable

; <label>:187:                                    ; preds = %180
  %188 = trunc i32 %30 to i16
  %189 = icmp slt i16 %188, 0
  br i1 %189, label %191, label %190

; <label>:190:                                    ; preds = %187
  tail call fastcc void @raise_exception_err(i32 11, i32 %9) #12
  unreachable

; <label>:191:                                    ; preds = %187
  %192 = lshr i32 %28, 16
  %193 = and i32 %30, -65536
  %194 = and i32 %28, 65535
  %195 = or i32 %193, %194
  %196 = and i32 %30, 31
  %197 = and i32 %192, 65532
  %198 = icmp eq i32 %197, 0
  br i1 %198, label %199, label %200

; <label>:199:                                    ; preds = %191
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:200:                                    ; preds = %191
  %201 = and i32 %192, 4
  %202 = icmp eq i32 %201, 0
  %203 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 12
  %204 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 14
  %205 = select i1 %202, %struct.SegmentCache* %204, %struct.SegmentCache* %203
  %206 = or i32 %192, 7
  %207 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %205, i64 0, i32 2
  %208 = load i32, i32* %207, align 4
  %209 = icmp ugt i32 %206, %208
  br i1 %209, label %210, label %211

; <label>:210:                                    ; preds = %200
  tail call fastcc void @raise_exception_err(i32 13, i32 %197) #12
  unreachable

; <label>:211:                                    ; preds = %200
  %212 = and i32 %192, 65528
  %213 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %205, i64 0, i32 1
  %214 = load i32, i32* %213, align 4
  %215 = add i32 %214, %212
  %216 = tail call fastcc i32 @ldl_kernel(i32 %215)
  %217 = add i32 %215, 4
  %218 = tail call fastcc i32 @ldl_kernel(i32 %217)
  %219 = and i32 %218, 6144
  %220 = icmp eq i32 %219, 6144
  br i1 %220, label %222, label %221

; <label>:221:                                    ; preds = %211
  tail call fastcc void @raise_exception_err(i32 13, i32 %197) #12
  unreachable

; <label>:222:                                    ; preds = %211
  %223 = lshr i32 %218, 13
  %224 = and i32 %223, 3
  %225 = icmp ugt i32 %224, %34
  br i1 %225, label %226, label %227

; <label>:226:                                    ; preds = %222
  tail call fastcc void @raise_exception_err(i32 13, i32 %197) #12
  unreachable

; <label>:227:                                    ; preds = %222
  %228 = trunc i32 %218 to i16
  %229 = icmp slt i16 %228, 0
  br i1 %229, label %231, label %230

; <label>:230:                                    ; preds = %227
  tail call fastcc void @raise_exception_err(i32 11, i32 %197) #12
  unreachable

; <label>:231:                                    ; preds = %227
  %232 = and i32 %218, 1024
  %233 = icmp eq i32 %232, 0
  %234 = icmp ult i32 %224, %34
  %235 = and i1 %233, %234
  %236 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br i1 %235, label %237, label %401

; <label>:237:                                    ; preds = %231
  %238 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %236, i64 0, i32 13, i32 3
  %239 = load i32, i32* %238, align 4
  %240 = trunc i32 %239 to i16
  %241 = icmp slt i16 %240, 0
  br i1 %241, label %243, label %242

; <label>:242:                                    ; preds = %237
  tail call void (%struct.CPUX86State*, i8*, ...) @cpu_abort(%struct.CPUX86State* nonnull %236, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.18, i64 0, i64 0)) #12
  unreachable

; <label>:243:                                    ; preds = %237
  %244 = lshr i32 %239, 11
  %245 = and i32 %244, 1
  %246 = shl nuw nsw i32 %224, 2
  %247 = or i32 %246, 2
  %248 = shl i32 %247, %245
  %249 = shl i32 4, %245
  %250 = add nsw i32 %249, -1
  %251 = add nsw i32 %250, %248
  %252 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %236, i64 0, i32 13, i32 2
  %253 = load i32, i32* %252, align 8
  %254 = icmp ugt i32 %251, %253
  br i1 %254, label %255, label %259

; <label>:255:                                    ; preds = %243
  %256 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %236, i64 0, i32 13, i32 0
  %257 = load i32, i32* %256, align 8
  %258 = and i32 %257, 65532
  tail call fastcc void @raise_exception_err(i32 10, i32 %258) #12
  unreachable

; <label>:259:                                    ; preds = %243
  %260 = icmp eq i32 %245, 0
  %261 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %236, i64 0, i32 13, i32 1
  %262 = load i32, i32* %261, align 4
  %263 = add i32 %262, %248
  br i1 %260, label %264, label %272

; <label>:264:                                    ; preds = %259
  %265 = tail call fastcc i32 @lduw_kernel(i32 %263)
  %266 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %267 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %266, i64 0, i32 13, i32 1
  %268 = load i32, i32* %267, align 4
  %269 = add nuw nsw i32 %248, 2
  %270 = add i32 %269, %268
  %271 = tail call fastcc i32 @lduw_kernel(i32 %270)
  br label %280

; <label>:272:                                    ; preds = %259
  %273 = tail call fastcc i32 @ldl_kernel(i32 %263)
  %274 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %275 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %274, i64 0, i32 13, i32 1
  %276 = load i32, i32* %275, align 4
  %277 = add nuw nsw i32 %248, 4
  %278 = add i32 %277, %276
  %279 = tail call fastcc i32 @lduw_kernel(i32 %278)
  br label %280

; <label>:280:                                    ; preds = %264, %272
  %281 = phi i32 [ %265, %264 ], [ %273, %272 ]
  %282 = phi i32 [ %271, %264 ], [ %279, %272 ]
  %283 = and i32 %282, 65532
  %284 = icmp eq i32 %283, 0
  br i1 %284, label %285, label %286

; <label>:285:                                    ; preds = %280
  tail call fastcc void @raise_exception_err(i32 10, i32 0) #12
  unreachable

; <label>:286:                                    ; preds = %280
  %287 = and i32 %282, 3
  %288 = icmp eq i32 %287, %224
  br i1 %288, label %290, label %289

; <label>:289:                                    ; preds = %286
  tail call fastcc void @raise_exception_err(i32 10, i32 %283) #12
  unreachable

; <label>:290:                                    ; preds = %286
  %291 = and i32 %282, 4
  %292 = icmp eq i32 %291, 0
  %293 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %294 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %293, i64 0, i32 12
  %295 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %293, i64 0, i32 14
  %296 = select i1 %292, %struct.SegmentCache* %295, %struct.SegmentCache* %294
  %297 = or i32 %282, 7
  %298 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %296, i64 0, i32 2
  %299 = load i32, i32* %298, align 4
  %300 = icmp ugt i32 %297, %299
  br i1 %300, label %301, label %302

; <label>:301:                                    ; preds = %290
  tail call fastcc void @raise_exception_err(i32 10, i32 %283) #12
  unreachable

; <label>:302:                                    ; preds = %290
  %303 = and i32 %282, -8
  %304 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %296, i64 0, i32 1
  %305 = load i32, i32* %304, align 4
  %306 = add i32 %305, %303
  %307 = tail call fastcc i32 @ldl_kernel(i32 %306)
  %308 = add i32 %306, 4
  %309 = tail call fastcc i32 @ldl_kernel(i32 %308)
  %310 = lshr i32 %309, 13
  %311 = and i32 %310, 3
  %312 = icmp eq i32 %311, %224
  br i1 %312, label %314, label %313

; <label>:313:                                    ; preds = %302
  tail call fastcc void @raise_exception_err(i32 10, i32 %283) #12
  unreachable

; <label>:314:                                    ; preds = %302
  %315 = and i32 %309, 6656
  %316 = icmp eq i32 %315, 4608
  br i1 %316, label %318, label %317

; <label>:317:                                    ; preds = %314
  tail call fastcc void @raise_exception_err(i32 10, i32 %283) #12
  unreachable

; <label>:318:                                    ; preds = %314
  %319 = trunc i32 %309 to i16
  %320 = icmp slt i16 %319, 0
  br i1 %320, label %322, label %321

; <label>:321:                                    ; preds = %318
  tail call fastcc void @raise_exception_err(i32 10, i32 %283) #12
  unreachable

; <label>:322:                                    ; preds = %318
  %323 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %324 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %323, i64 0, i32 11, i64 2, i32 3
  %325 = load i32, i32* %324, align 4
  %326 = lshr i32 %325, 6
  %327 = and i32 %326, 65536
  %328 = xor i32 %327, 65536
  %329 = add nsw i32 %328, -1
  %330 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %323, i64 0, i32 11, i64 2, i32 1
  %331 = load i32, i32* %330, align 4
  %332 = lshr i32 %309, 6
  %333 = and i32 %332, 65536
  %334 = xor i32 %333, 65536
  %335 = add nsw i32 %334, -1
  %336 = lshr i32 %307, 16
  %337 = shl i32 %309, 16
  %338 = and i32 %337, 16711680
  %339 = and i32 %309, -16777216
  %340 = or i32 %339, %336
  %341 = or i32 %340, %338
  %342 = icmp eq i32 %182, 0
  br i1 %342, label %372, label %343

; <label>:343:                                    ; preds = %322
  %344 = add i32 %281, -4
  %345 = and i32 %335, %344
  %346 = add i32 %345, %341
  %347 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %323, i64 0, i32 11, i64 2, i32 0
  %348 = load i32, i32* %347, align 8
  tail call fastcc void @stl_kernel(i32 %346, i32 %348)
  %349 = add i32 %281, -8
  %350 = and i32 %335, %349
  %351 = add i32 %350, %341
  %352 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %353 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %352, i64 0, i32 0, i64 4
  %354 = load i32, i32* %353, align 16
  tail call fastcc void @stl_kernel(i32 %351, i32 %354)
  %355 = icmp eq i32 %196, 0
  br i1 %355, label %421, label %356

; <label>:356:                                    ; preds = %343, %356
  %357 = phi i32 [ %359, %356 ], [ %196, %343 ]
  %358 = phi i32 [ %368, %356 ], [ %349, %343 ]
  %359 = add nsw i32 %357, -1
  %360 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %361 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %360, i64 0, i32 0, i64 4
  %362 = load i32, i32* %361, align 16
  %363 = shl i32 %359, 2
  %364 = add i32 %362, %363
  %365 = and i32 %364, %329
  %366 = add i32 %365, %331
  %367 = tail call fastcc i32 @ldl_kernel(i32 %366)
  %368 = add i32 %358, -4
  %369 = and i32 %368, %335
  %370 = add i32 %369, %341
  tail call fastcc void @stl_kernel(i32 %370, i32 %367)
  %371 = icmp sgt i32 %357, 1
  br i1 %371, label %356, label %412

; <label>:372:                                    ; preds = %322
  %373 = add i32 %281, -2
  %374 = and i32 %335, %373
  %375 = add i32 %374, %341
  %376 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %323, i64 0, i32 11, i64 2, i32 0
  %377 = load i32, i32* %376, align 8
  tail call fastcc void @stw_kernel(i32 %375, i32 %377)
  %378 = add i32 %281, -4
  %379 = and i32 %335, %378
  %380 = add i32 %379, %341
  %381 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %382 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 0, i64 4
  %383 = load i32, i32* %382, align 16
  tail call fastcc void @stw_kernel(i32 %380, i32 %383)
  %384 = icmp eq i32 %196, 0
  br i1 %384, label %438, label %385

; <label>:385:                                    ; preds = %372, %385
  %386 = phi i32 [ %388, %385 ], [ %196, %372 ]
  %387 = phi i32 [ %397, %385 ], [ %378, %372 ]
  %388 = add nsw i32 %386, -1
  %389 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %390 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %389, i64 0, i32 0, i64 4
  %391 = load i32, i32* %390, align 16
  %392 = shl i32 %388, 1
  %393 = add i32 %391, %392
  %394 = and i32 %393, %329
  %395 = add i32 %394, %331
  %396 = tail call fastcc i32 @lduw_kernel(i32 %395)
  %397 = add i32 %387, -2
  %398 = and i32 %397, %335
  %399 = add i32 %398, %341
  tail call fastcc void @stw_kernel(i32 %399, i32 %396)
  %400 = icmp sgt i32 %386, 1
  br i1 %400, label %385, label %412

; <label>:401:                                    ; preds = %231
  %402 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %236, i64 0, i32 0, i64 4
  %403 = load i32, i32* %402, align 16
  %404 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %236, i64 0, i32 11, i64 2, i32 3
  %405 = load i32, i32* %404, align 4
  %406 = lshr i32 %405, 6
  %407 = and i32 %406, 65536
  %408 = xor i32 %407, 65536
  %409 = add nsw i32 %408, -1
  %410 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %236, i64 0, i32 11, i64 2, i32 1
  %411 = load i32, i32* %410, align 4
  br label %412

; <label>:412:                                    ; preds = %356, %385, %401
  %413 = phi i32 [ 0, %401 ], [ %282, %385 ], [ %282, %356 ]
  %414 = phi i32 [ 0, %401 ], [ %307, %385 ], [ %307, %356 ]
  %415 = phi i32 [ 0, %401 ], [ %309, %385 ], [ %309, %356 ]
  %416 = phi i32 [ %403, %401 ], [ %397, %385 ], [ %368, %356 ]
  %417 = phi i32 [ %409, %401 ], [ %335, %385 ], [ %335, %356 ]
  %418 = phi i32 [ %411, %401 ], [ %341, %385 ], [ %341, %356 ]
  %419 = phi i32 [ 0, %401 ], [ 1, %385 ], [ 1, %356 ]
  %420 = icmp eq i32 %182, 0
  br i1 %420, label %438, label %421

; <label>:421:                                    ; preds = %343, %412
  %422 = phi i32 [ %419, %412 ], [ 1, %343 ]
  %423 = phi i32 [ %418, %412 ], [ %341, %343 ]
  %424 = phi i32 [ %417, %412 ], [ %335, %343 ]
  %425 = phi i32 [ %416, %412 ], [ %349, %343 ]
  %426 = phi i32 [ %415, %412 ], [ %309, %343 ]
  %427 = phi i32 [ %414, %412 ], [ %307, %343 ]
  %428 = phi i32 [ %413, %412 ], [ %282, %343 ]
  %429 = add i32 %425, -4
  %430 = and i32 %429, %424
  %431 = add i32 %423, %430
  %432 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %433 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %432, i64 0, i32 11, i64 1, i32 0
  %434 = load i32, i32* %433, align 8
  tail call fastcc void @stl_kernel(i32 %431, i32 %434)
  %435 = add i32 %425, -8
  %436 = and i32 %435, %424
  %437 = add i32 %423, %436
  tail call fastcc void @stl_kernel(i32 %437, i32 %8)
  br label %455

; <label>:438:                                    ; preds = %372, %412
  %439 = phi i32 [ %419, %412 ], [ 1, %372 ]
  %440 = phi i32 [ %418, %412 ], [ %341, %372 ]
  %441 = phi i32 [ %417, %412 ], [ %335, %372 ]
  %442 = phi i32 [ %416, %412 ], [ %378, %372 ]
  %443 = phi i32 [ %415, %412 ], [ %309, %372 ]
  %444 = phi i32 [ %414, %412 ], [ %307, %372 ]
  %445 = phi i32 [ %413, %412 ], [ %282, %372 ]
  %446 = add i32 %442, -2
  %447 = and i32 %446, %441
  %448 = add i32 %440, %447
  %449 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %450 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %449, i64 0, i32 11, i64 1, i32 0
  %451 = load i32, i32* %450, align 8
  tail call fastcc void @stw_kernel(i32 %448, i32 %451)
  %452 = add i32 %442, -4
  %453 = and i32 %452, %441
  %454 = add i32 %440, %453
  tail call fastcc void @stw_kernel(i32 %454, i32 %8)
  br label %455

; <label>:455:                                    ; preds = %438, %421
  %456 = phi i32 [ %439, %438 ], [ %422, %421 ]
  %457 = phi i32 [ %440, %438 ], [ %423, %421 ]
  %458 = phi i32 [ %441, %438 ], [ %424, %421 ]
  %459 = phi i32 [ %443, %438 ], [ %426, %421 ]
  %460 = phi i32 [ %444, %438 ], [ %427, %421 ]
  %461 = phi i32 [ %445, %438 ], [ %428, %421 ]
  %462 = phi i32 [ %452, %438 ], [ %435, %421 ]
  %463 = icmp eq i32 %456, 0
  br i1 %463, label %464, label %470

; <label>:464:                                    ; preds = %455
  %465 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %466 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %465, i64 0, i32 8
  %467 = load i32, i32* %466, align 4
  %468 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %465, i64 0, i32 11, i64 2, i32 3
  %469 = load i32, i32* %468, align 4
  br label %527

; <label>:470:                                    ; preds = %455
  %471 = and i32 %461, -4
  %472 = or i32 %471, %224
  %473 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %474 = and i32 %460, 65535
  %475 = and i32 %459, 983040
  %476 = or i32 %475, %474
  %477 = and i32 %459, 8388608
  %478 = icmp eq i32 %477, 0
  %479 = shl nuw i32 %476, 12
  %480 = or i32 %479, 4095
  %481 = select i1 %478, i32 %476, i32 %480
  %482 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %473, i64 0, i32 11, i64 2, i32 0
  store i32 %472, i32* %482, align 4
  %483 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %473, i64 0, i32 11, i64 2, i32 1
  store i32 %457, i32* %483, align 4
  %484 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %473, i64 0, i32 11, i64 2, i32 2
  store i32 %481, i32* %484, align 4
  %485 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %473, i64 0, i32 11, i64 2, i32 3
  store i32 %459, i32* %485, align 4
  %486 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %473, i64 0, i32 8
  %487 = load i32, i32* %486, align 4
  %488 = lshr i32 %459, 17
  %489 = and i32 %488, 32
  %490 = trunc i32 %487 to i16
  %491 = icmp slt i16 %490, 0
  br i1 %491, label %522, label %492

; <label>:492:                                    ; preds = %470
  %493 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %473, i64 0, i32 16, i64 0
  %494 = load i32, i32* %493, align 8
  %495 = and i32 %494, 1
  %496 = icmp eq i32 %495, 0
  br i1 %496, label %508, label %497

; <label>:497:                                    ; preds = %492
  %498 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %473, i64 0, i32 7
  %499 = bitcast i32* %498 to i64*
  %500 = load i64, i64* %499, align 8
  %501 = and i64 %500, 131072
  %502 = icmp ne i64 %501, 0
  %503 = and i32 %487, 16
  %504 = icmp eq i32 %503, 0
  %505 = or i1 %504, %502
  %506 = lshr i64 %500, 32
  %507 = trunc i64 %506 to i32
  br i1 %505, label %508, label %511

; <label>:508:                                    ; preds = %497, %492
  %509 = phi i32 [ %507, %497 ], [ %487, %492 ]
  %510 = or i32 %489, 64
  br label %522

; <label>:511:                                    ; preds = %497
  %512 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %473, i64 0, i32 11, i64 3, i32 1
  %513 = load i32, i32* %512, align 4
  %514 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %473, i64 0, i32 11, i64 0, i32 1
  %515 = load i32, i32* %514, align 4
  %516 = or i32 %515, %513
  %517 = or i32 %516, %457
  %518 = icmp ne i32 %517, 0
  %519 = zext i1 %518 to i32
  %520 = shl nuw nsw i32 %519, 6
  %521 = or i32 %520, %489
  br label %522

; <label>:522:                                    ; preds = %470, %508, %511
  %523 = phi i32 [ %487, %470 ], [ %509, %508 ], [ %507, %511 ]
  %524 = phi i32 [ %489, %470 ], [ %510, %508 ], [ %521, %511 ]
  %525 = and i32 %523, -97
  %526 = or i32 %525, %524
  store i32 %526, i32* %486, align 4
  br label %527

; <label>:527:                                    ; preds = %464, %522
  %528 = phi i32 [ %469, %464 ], [ %459, %522 ]
  %529 = phi i32 [ %467, %464 ], [ %526, %522 ]
  %530 = phi %struct.CPUX86State* [ %465, %464 ], [ %473, %522 ]
  %531 = or i32 %224, %197
  %532 = lshr i32 %216, 16
  %533 = shl i32 %218, 16
  %534 = and i32 %533, 16711680
  %535 = and i32 %218, -16777216
  %536 = or i32 %535, %532
  %537 = or i32 %536, %534
  %538 = and i32 %216, 65535
  %539 = and i32 %218, 983040
  %540 = or i32 %539, %538
  %541 = and i32 %218, 8388608
  %542 = icmp eq i32 %541, 0
  %543 = shl nuw i32 %540, 12
  %544 = or i32 %543, 4095
  %545 = select i1 %542, i32 %540, i32 %544
  %546 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %530, i64 0, i32 11, i64 1, i32 0
  store i32 %531, i32* %546, align 4
  %547 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %530, i64 0, i32 11, i64 1, i32 1
  store i32 %537, i32* %547, align 4
  %548 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %530, i64 0, i32 11, i64 1, i32 2
  store i32 %545, i32* %548, align 4
  %549 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %530, i64 0, i32 11, i64 1, i32 3
  store i32 %218, i32* %549, align 4
  %550 = lshr i32 %218, 18
  %551 = and i32 %550, 16
  %552 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %530, i64 0, i32 8
  %553 = and i32 %529, -32785
  %554 = or i32 %553, %551
  store i32 %554, i32* %552, align 4
  %555 = lshr i32 %528, 17
  %556 = and i32 %555, 32
  %557 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %530, i64 0, i32 16, i64 0
  %558 = load i32, i32* %557, align 8
  %559 = and i32 %558, 1
  %560 = icmp eq i32 %559, 0
  br i1 %560, label %571, label %561

; <label>:561:                                    ; preds = %527
  %562 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %530, i64 0, i32 7
  %563 = bitcast i32* %562 to i64*
  %564 = load i64, i64* %563, align 8
  %565 = and i64 %564, 131072
  %566 = icmp ne i64 %565, 0
  %567 = icmp eq i32 %551, 0
  %568 = or i1 %567, %566
  %569 = lshr i64 %564, 32
  %570 = trunc i64 %569 to i32
  br i1 %568, label %571, label %574

; <label>:571:                                    ; preds = %561, %527
  %572 = phi i32 [ %570, %561 ], [ %554, %527 ]
  %573 = or i32 %556, 64
  br label %587

; <label>:574:                                    ; preds = %561
  %575 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %530, i64 0, i32 11, i64 3, i32 1
  %576 = load i32, i32* %575, align 4
  %577 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %530, i64 0, i32 11, i64 0, i32 1
  %578 = load i32, i32* %577, align 4
  %579 = or i32 %578, %576
  %580 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %530, i64 0, i32 11, i64 2, i32 1
  %581 = load i32, i32* %580, align 4
  %582 = or i32 %579, %581
  %583 = icmp ne i32 %582, 0
  %584 = zext i1 %583 to i32
  %585 = shl nuw nsw i32 %584, 6
  %586 = or i32 %585, %556
  br label %587

; <label>:587:                                    ; preds = %571, %574
  %588 = phi i32 [ %572, %571 ], [ %570, %574 ]
  %589 = phi i32 [ %573, %571 ], [ %586, %574 ]
  %590 = and i32 %588, -97
  %591 = or i32 %590, %589
  store i32 %591, i32* %552, align 4
  %592 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %593 = load i32, i32* %592, align 4
  %594 = icmp eq i32 %593, 0
  br i1 %594, label %600, label %595, !prof !2

; <label>:595:                                    ; preds = %587
  %596 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  %597 = and i32 %591, 3
  tail call void %596(i32 %597, i32 %224)
  %598 = load i32, i32* %552, align 4
  %599 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %600

; <label>:600:                                    ; preds = %587, %595
  %601 = phi %struct.CPUX86State* [ %599, %595 ], [ %530, %587 ]
  %602 = phi i32 [ %598, %595 ], [ %591, %587 ]
  %603 = and i32 %602, -4
  %604 = or i32 %603, %224
  store i32 %604, i32* %552, align 4
  %605 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %601, i64 0, i32 0, i64 4
  %606 = load i32, i32* %605, align 16
  %607 = xor i32 %458, -1
  %608 = and i32 %606, %607
  %609 = and i32 %462, %458
  %610 = or i32 %608, %609
  store i32 %610, i32* %605, align 16
  %611 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %601, i64 0, i32 5
  store i32 %195, i32* %611, align 16
  %612 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %613 = load i32, i32* %612, align 4
  %614 = icmp eq i32 %613, 0
  br i1 %614, label %617, label %615, !prof !2

; <label>:615:                                    ; preds = %600
  %616 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  tail call void %616(i32 %34, i32 %224)
  br label %617

; <label>:617:                                    ; preds = %159, %615, %600, %176
  ret void
}

; Function Attrs: uwtable
define void @helper_iret_real(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 4
  %4 = load i32, i32* %3, align 16
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 11, i64 2, i32 1
  %6 = load i32, i32* %5, align 4
  %7 = icmp eq i32 %0, 1
  %8 = and i32 %4, 65535
  %9 = add i32 %6, %8
  br i1 %7, label %10, label %22

; <label>:10:                                     ; preds = %1
  %11 = tail call fastcc i32 @ldl_kernel(i32 %9)
  %12 = add i32 %4, 4
  %13 = and i32 %12, 65535
  %14 = add i32 %13, %6
  %15 = tail call fastcc i32 @ldl_kernel(i32 %14)
  %16 = add i32 %4, 8
  %17 = and i32 %15, 65535
  %18 = and i32 %16, 65535
  %19 = add i32 %18, %6
  %20 = tail call fastcc i32 @ldl_kernel(i32 %19)
  %21 = add i32 %4, 12
  br label %33

; <label>:22:                                     ; preds = %1
  %23 = tail call fastcc i32 @lduw_kernel(i32 %9)
  %24 = add i32 %4, 2
  %25 = and i32 %24, 65535
  %26 = add i32 %25, %6
  %27 = tail call fastcc i32 @lduw_kernel(i32 %26)
  %28 = add i32 %4, 4
  %29 = and i32 %28, 65535
  %30 = add i32 %29, %6
  %31 = tail call fastcc i32 @lduw_kernel(i32 %30)
  %32 = add i32 %4, 6
  br label %33

; <label>:33:                                     ; preds = %22, %10
  %34 = phi i32 [ %11, %10 ], [ %23, %22 ]
  %35 = phi i32 [ %20, %10 ], [ %31, %22 ]
  %36 = phi i32 [ %17, %10 ], [ %27, %22 ]
  %37 = phi i32 [ %21, %10 ], [ %32, %22 ]
  %38 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %38, i64 0, i32 0, i64 4
  %40 = load i32, i32* %39, align 16
  %41 = and i32 %40, -65536
  %42 = and i32 %37, 65535
  %43 = or i32 %41, %42
  store i32 %43, i32* %39, align 16
  %44 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %38, i64 0, i32 11, i64 1, i32 0
  store i32 %36, i32* %44, align 8
  %45 = shl i32 %36, 4
  %46 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %38, i64 0, i32 11, i64 1, i32 1
  store i32 %45, i32* %46, align 4
  %47 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %38, i64 0, i32 5
  store i32 %34, i32* %47, align 16
  %48 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %38, i64 0, i32 7
  %49 = load i32, i32* %48, align 8
  %50 = and i32 %49, 131072
  %51 = icmp eq i32 %50, 0
  %52 = select i1 %51, i32 2454272, i32 2441984
  %53 = icmp eq i32 %0, 0
  %54 = and i32 %52, 29440
  %55 = select i1 %53, i32 %54, i32 %52
  %56 = and i32 %35, 2261
  %57 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %38, i64 0, i32 2
  store i32 %56, i32* %57, align 4
  %58 = lshr i32 %35, 9
  %59 = and i32 %58, 2
  %60 = xor i32 %59, 2
  %61 = add nsw i32 %60, -1
  %62 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %38, i64 0, i32 6
  store i32 %61, i32* %62, align 4
  %63 = xor i32 %55, -1
  %64 = and i32 %49, %63
  %65 = and i32 %55, %35
  %66 = or i32 %64, %65
  store i32 %66, i32* %48, align 8
  %67 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %38, i64 0, i32 9
  %68 = load i32, i32* %67, align 16
  %69 = and i32 %68, -5
  store i32 %69, i32* %67, align 16
  ret void
}

; Function Attrs: uwtable
define void @helper_iret_protected(i32, i32) local_unnamed_addr #2 {
  %3 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %4 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 7
  %5 = bitcast i32* %4 to i64*
  %6 = load i64, i64* %5, align 8
  %7 = and i64 %6, 16384
  %8 = icmp eq i64 %7, 0
  br i1 %8, label %38, label %9

; <label>:9:                                      ; preds = %2
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 13, i32 1
  %11 = load i32, i32* %10, align 4
  %12 = tail call fastcc i32 @lduw_kernel(i32 %11)
  %13 = and i32 %12, 4
  %14 = icmp eq i32 %13, 0
  br i1 %14, label %17, label %15

; <label>:15:                                     ; preds = %9
  %16 = and i32 %12, 65532
  tail call fastcc void @raise_exception_err(i32 10, i32 %16) #12
  unreachable

; <label>:17:                                     ; preds = %9
  %18 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %19 = or i32 %12, 7
  %20 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 14, i32 2
  %21 = load i32, i32* %20, align 4
  %22 = icmp ugt i32 %19, %21
  br i1 %22, label %23, label %25

; <label>:23:                                     ; preds = %17
  %24 = and i32 %12, 65532
  tail call fastcc void @raise_exception_err(i32 10, i32 %24) #12
  unreachable

; <label>:25:                                     ; preds = %17
  %26 = and i32 %12, -8
  %27 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %18, i64 0, i32 14, i32 1
  %28 = load i32, i32* %27, align 4
  %29 = add i32 %28, %26
  %30 = tail call fastcc i32 @ldl_kernel(i32 %29)
  %31 = add i32 %29, 4
  %32 = tail call fastcc i32 @ldl_kernel(i32 %31)
  %33 = and i32 %32, 5888
  %34 = icmp eq i32 %33, 768
  br i1 %34, label %37, label %35

; <label>:35:                                     ; preds = %25
  %36 = and i32 %12, 65532
  tail call fastcc void @raise_exception_err(i32 10, i32 %36) #12
  unreachable

; <label>:37:                                     ; preds = %25
  tail call fastcc void @switch_tss(i32 %12, i32 %30, i32 %32, i32 1, i32 %1)
  br label %954

; <label>:38:                                     ; preds = %2
  %39 = lshr i64 %6, 32
  %40 = trunc i64 %39 to i32
  %41 = and i32 %40, 3
  %42 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 11, i64 2, i32 3
  %43 = load i32, i32* %42, align 4
  %44 = lshr i32 %43, 6
  %45 = and i32 %44, 65536
  %46 = xor i32 %45, 65536
  %47 = add nsw i32 %46, -1
  %48 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 0, i64 4
  %49 = load i32, i32* %48, align 16
  %50 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 11, i64 2, i32 1
  %51 = load i32, i32* %50, align 4
  %52 = icmp eq i32 %0, 1
  %53 = and i32 %47, %49
  %54 = add i32 %53, %51
  br i1 %52, label %55, label %69

; <label>:55:                                     ; preds = %38
  %56 = tail call fastcc i32 @ldl_kernel(i32 %54)
  %57 = add i32 %49, 4
  %58 = and i32 %47, %57
  %59 = add i32 %58, %51
  %60 = tail call fastcc i32 @ldl_kernel(i32 %59)
  %61 = add i32 %49, 8
  %62 = and i32 %60, 65535
  %63 = and i32 %47, %61
  %64 = add i32 %63, %51
  %65 = tail call fastcc i32 @ldl_kernel(i32 %64)
  %66 = add i32 %49, 12
  %67 = and i32 %65, 131072
  %68 = icmp eq i32 %67, 0
  br i1 %68, label %80, label %686

; <label>:69:                                     ; preds = %38
  %70 = tail call fastcc i32 @lduw_kernel(i32 %54)
  %71 = add i32 %49, 2
  %72 = and i32 %47, %71
  %73 = add i32 %72, %51
  %74 = tail call fastcc i32 @lduw_kernel(i32 %73)
  %75 = add i32 %49, 4
  %76 = and i32 %47, %75
  %77 = add i32 %76, %51
  %78 = tail call fastcc i32 @lduw_kernel(i32 %77)
  %79 = add i32 %49, 6
  br label %80

; <label>:80:                                     ; preds = %69, %55
  %81 = phi i32 [ %65, %55 ], [ %78, %69 ]
  %82 = phi i32 [ %62, %55 ], [ %74, %69 ]
  %83 = phi i32 [ %66, %55 ], [ %79, %69 ]
  %84 = phi i32 [ %56, %55 ], [ %70, %69 ]
  %85 = and i32 %82, 65532
  %86 = icmp eq i32 %85, 0
  br i1 %86, label %87, label %88

; <label>:87:                                     ; preds = %80
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:88:                                     ; preds = %80
  %89 = and i32 %82, 4
  %90 = icmp eq i32 %89, 0
  %91 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %92 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %91, i64 0, i32 12
  %93 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %91, i64 0, i32 14
  %94 = select i1 %90, %struct.SegmentCache* %93, %struct.SegmentCache* %92
  %95 = or i32 %82, 7
  %96 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %94, i64 0, i32 2
  %97 = load i32, i32* %96, align 4
  %98 = icmp ugt i32 %95, %97
  br i1 %98, label %99, label %100

; <label>:99:                                     ; preds = %88
  tail call fastcc void @raise_exception_err(i32 13, i32 %85) #12
  unreachable

; <label>:100:                                    ; preds = %88
  %101 = and i32 %82, -8
  %102 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %94, i64 0, i32 1
  %103 = load i32, i32* %102, align 4
  %104 = add i32 %103, %101
  %105 = tail call fastcc i32 @ldl_kernel(i32 %104)
  %106 = add i32 %104, 4
  %107 = tail call fastcc i32 @ldl_kernel(i32 %106)
  %108 = and i32 %107, 6144
  %109 = icmp eq i32 %108, 6144
  br i1 %109, label %111, label %110

; <label>:110:                                    ; preds = %100
  tail call fastcc void @raise_exception_err(i32 13, i32 %85) #12
  unreachable

; <label>:111:                                    ; preds = %100
  %112 = and i32 %82, 3
  %113 = icmp ult i32 %112, %41
  br i1 %113, label %114, label %115

; <label>:114:                                    ; preds = %111
  tail call fastcc void @raise_exception_err(i32 13, i32 %85) #12
  unreachable

; <label>:115:                                    ; preds = %111
  %116 = lshr i32 %107, 13
  %117 = and i32 %116, 3
  %118 = and i32 %107, 1024
  %119 = icmp eq i32 %118, 0
  br i1 %119, label %123, label %120

; <label>:120:                                    ; preds = %115
  %121 = icmp ugt i32 %117, %112
  br i1 %121, label %122, label %126

; <label>:122:                                    ; preds = %120
  tail call fastcc void @raise_exception_err(i32 13, i32 %85) #12
  unreachable

; <label>:123:                                    ; preds = %115
  %124 = icmp eq i32 %117, %112
  br i1 %124, label %126, label %125

; <label>:125:                                    ; preds = %123
  tail call fastcc void @raise_exception_err(i32 13, i32 %85) #12
  unreachable

; <label>:126:                                    ; preds = %123, %120
  %127 = trunc i32 %107 to i16
  %128 = icmp slt i16 %127, 0
  br i1 %128, label %130, label %129

; <label>:129:                                    ; preds = %126
  tail call fastcc void @raise_exception_err(i32 11, i32 %85) #12
  unreachable

; <label>:130:                                    ; preds = %126
  %131 = icmp eq i32 %112, %41
  br i1 %131, label %132, label %200

; <label>:132:                                    ; preds = %130
  %133 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %134 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %133, i64 0, i32 8
  %135 = load i32, i32* %134, align 4
  %136 = trunc i32 %135 to i16
  %137 = icmp sgt i16 %136, -1
  br i1 %137, label %138, label %200

; <label>:138:                                    ; preds = %132
  %139 = lshr i32 %105, 16
  %140 = shl i32 %107, 16
  %141 = and i32 %140, 16711680
  %142 = and i32 %107, -16777216
  %143 = or i32 %142, %139
  %144 = or i32 %143, %141
  %145 = and i32 %105, 65535
  %146 = and i32 %107, 983040
  %147 = or i32 %146, %145
  %148 = and i32 %107, 8388608
  %149 = icmp eq i32 %148, 0
  %150 = shl nuw i32 %147, 12
  %151 = or i32 %150, 4095
  %152 = select i1 %149, i32 %147, i32 %151
  %153 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %133, i64 0, i32 11, i64 1, i32 0
  store i32 %82, i32* %153, align 4
  %154 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %133, i64 0, i32 11, i64 1, i32 1
  store i32 %144, i32* %154, align 4
  %155 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %133, i64 0, i32 11, i64 1, i32 2
  store i32 %152, i32* %155, align 4
  %156 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %133, i64 0, i32 11, i64 1, i32 3
  store i32 %107, i32* %156, align 4
  %157 = lshr i32 %107, 18
  %158 = and i32 %157, 16
  %159 = and i32 %135, -32785
  %160 = or i32 %159, %158
  store i32 %160, i32* %134, align 4
  %161 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %133, i64 0, i32 11, i64 2, i32 3
  %162 = load i32, i32* %161, align 4
  %163 = lshr i32 %162, 17
  %164 = and i32 %163, 32
  %165 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %133, i64 0, i32 16, i64 0
  %166 = load i32, i32* %165, align 8
  %167 = and i32 %166, 1
  %168 = icmp eq i32 %167, 0
  br i1 %168, label %179, label %169

; <label>:169:                                    ; preds = %138
  %170 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %133, i64 0, i32 7
  %171 = bitcast i32* %170 to i64*
  %172 = load i64, i64* %171, align 8
  %173 = and i64 %172, 131072
  %174 = icmp ne i64 %173, 0
  %175 = icmp eq i32 %158, 0
  %176 = or i1 %175, %174
  %177 = lshr i64 %172, 32
  %178 = trunc i64 %177 to i32
  br i1 %176, label %179, label %182

; <label>:179:                                    ; preds = %169, %138
  %180 = phi i32 [ %178, %169 ], [ %160, %138 ]
  %181 = or i32 %164, 64
  br label %195

; <label>:182:                                    ; preds = %169
  %183 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %133, i64 0, i32 11, i64 3, i32 1
  %184 = load i32, i32* %183, align 4
  %185 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %133, i64 0, i32 11, i64 0, i32 1
  %186 = load i32, i32* %185, align 4
  %187 = or i32 %186, %184
  %188 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %133, i64 0, i32 11, i64 2, i32 1
  %189 = load i32, i32* %188, align 4
  %190 = or i32 %187, %189
  %191 = icmp ne i32 %190, 0
  %192 = zext i1 %191 to i32
  %193 = shl nuw nsw i32 %192, 6
  %194 = or i32 %193, %164
  br label %195

; <label>:195:                                    ; preds = %182, %179
  %196 = phi i32 [ %180, %179 ], [ %178, %182 ]
  %197 = phi i32 [ %181, %179 ], [ %194, %182 ]
  %198 = and i32 %196, -97
  %199 = or i32 %198, %197
  store i32 %199, i32* %134, align 4
  br label %646

; <label>:200:                                    ; preds = %132, %130
  %201 = and i32 %83, %47
  %202 = add i32 %201, %51
  br i1 %52, label %203, label %210

; <label>:203:                                    ; preds = %200
  %204 = tail call fastcc i32 @ldl_kernel(i32 %202)
  %205 = add i32 %83, 4
  %206 = and i32 %205, %47
  %207 = add i32 %206, %51
  %208 = tail call fastcc i32 @ldl_kernel(i32 %207)
  %209 = and i32 %208, 65535
  br label %216

; <label>:210:                                    ; preds = %200
  %211 = tail call fastcc i32 @lduw_kernel(i32 %202)
  %212 = add i32 %83, 2
  %213 = and i32 %212, %47
  %214 = add i32 %213, %51
  %215 = tail call fastcc i32 @lduw_kernel(i32 %214)
  br label %216

; <label>:216:                                    ; preds = %210, %203
  %217 = phi i32 [ %209, %203 ], [ %215, %210 ]
  %218 = phi i32 [ %204, %203 ], [ %211, %210 ]
  %219 = and i32 %217, 65532
  %220 = icmp eq i32 %219, 0
  br i1 %220, label %221, label %222

; <label>:221:                                    ; preds = %216
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:222:                                    ; preds = %216
  %223 = and i32 %217, 3
  %224 = icmp eq i32 %223, %112
  br i1 %224, label %226, label %225

; <label>:225:                                    ; preds = %222
  tail call fastcc void @raise_exception_err(i32 13, i32 %219) #12
  unreachable

; <label>:226:                                    ; preds = %222
  %227 = and i32 %217, 4
  %228 = icmp eq i32 %227, 0
  %229 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %230 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %229, i64 0, i32 12
  %231 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %229, i64 0, i32 14
  %232 = select i1 %228, %struct.SegmentCache* %231, %struct.SegmentCache* %230
  %233 = or i32 %217, 7
  %234 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %232, i64 0, i32 2
  %235 = load i32, i32* %234, align 4
  %236 = icmp ugt i32 %233, %235
  br i1 %236, label %237, label %238

; <label>:237:                                    ; preds = %226
  tail call fastcc void @raise_exception_err(i32 13, i32 %219) #12
  unreachable

; <label>:238:                                    ; preds = %226
  %239 = and i32 %217, -8
  %240 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %232, i64 0, i32 1
  %241 = load i32, i32* %240, align 4
  %242 = add i32 %241, %239
  %243 = tail call fastcc i32 @ldl_kernel(i32 %242)
  %244 = add i32 %242, 4
  %245 = tail call fastcc i32 @ldl_kernel(i32 %244)
  %246 = and i32 %245, 6656
  %247 = icmp eq i32 %246, 4608
  br i1 %247, label %249, label %248

; <label>:248:                                    ; preds = %238
  tail call fastcc void @raise_exception_err(i32 13, i32 %219) #12
  unreachable

; <label>:249:                                    ; preds = %238
  %250 = lshr i32 %245, 13
  %251 = and i32 %250, 3
  %252 = icmp eq i32 %251, %112
  br i1 %252, label %254, label %253

; <label>:253:                                    ; preds = %249
  tail call fastcc void @raise_exception_err(i32 13, i32 %219) #12
  unreachable

; <label>:254:                                    ; preds = %249
  %255 = trunc i32 %245 to i16
  %256 = icmp slt i16 %255, 0
  br i1 %256, label %258, label %257

; <label>:257:                                    ; preds = %254
  tail call fastcc void @raise_exception_err(i32 11, i32 %219) #12
  unreachable

; <label>:258:                                    ; preds = %254
  %259 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %260 = lshr i32 %243, 16
  %261 = shl i32 %245, 16
  %262 = and i32 %261, 16711680
  %263 = and i32 %245, -16777216
  %264 = or i32 %263, %260
  %265 = or i32 %264, %262
  %266 = and i32 %243, 65535
  %267 = and i32 %245, 983040
  %268 = or i32 %267, %266
  %269 = and i32 %245, 8388608
  %270 = icmp eq i32 %269, 0
  %271 = shl nuw i32 %268, 12
  %272 = or i32 %271, 4095
  %273 = select i1 %270, i32 %268, i32 %272
  %274 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 11, i64 2, i32 0
  store i32 %217, i32* %274, align 4
  %275 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 11, i64 2, i32 1
  store i32 %265, i32* %275, align 4
  %276 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 11, i64 2, i32 2
  store i32 %273, i32* %276, align 4
  %277 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 11, i64 2, i32 3
  store i32 %245, i32* %277, align 4
  %278 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 8
  %279 = load i32, i32* %278, align 4
  %280 = lshr i32 %245, 17
  %281 = and i32 %280, 32
  %282 = trunc i32 %279 to i16
  %283 = icmp slt i16 %282, 0
  %284 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 16, i64 0
  %285 = load i32, i32* %284, align 8
  %286 = and i32 %285, 1
  br i1 %283, label %314, label %287

; <label>:287:                                    ; preds = %258
  %288 = icmp eq i32 %286, 0
  br i1 %288, label %300, label %289

; <label>:289:                                    ; preds = %287
  %290 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 7
  %291 = bitcast i32* %290 to i64*
  %292 = load i64, i64* %291, align 8
  %293 = and i64 %292, 131072
  %294 = icmp ne i64 %293, 0
  %295 = and i32 %279, 16
  %296 = icmp eq i32 %295, 0
  %297 = or i1 %296, %294
  %298 = lshr i64 %292, 32
  %299 = trunc i64 %298 to i32
  br i1 %297, label %300, label %303

; <label>:300:                                    ; preds = %289, %287
  %301 = phi i32 [ %299, %289 ], [ %279, %287 ]
  %302 = or i32 %281, 64
  br label %314

; <label>:303:                                    ; preds = %289
  %304 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 11, i64 3, i32 1
  %305 = load i32, i32* %304, align 4
  %306 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 11, i64 0, i32 1
  %307 = load i32, i32* %306, align 4
  %308 = or i32 %305, %265
  %309 = or i32 %308, %307
  %310 = icmp ne i32 %309, 0
  %311 = zext i1 %310 to i32
  %312 = shl nuw nsw i32 %311, 6
  %313 = or i32 %312, %281
  br label %314

; <label>:314:                                    ; preds = %258, %303, %300
  %315 = phi i32 [ 1, %303 ], [ %286, %300 ], [ %286, %258 ]
  %316 = phi i32 [ %299, %303 ], [ %301, %300 ], [ %279, %258 ]
  %317 = phi i32 [ %313, %303 ], [ %302, %300 ], [ %281, %258 ]
  %318 = and i32 %316, -32881
  %319 = lshr i32 %105, 16
  %320 = shl i32 %107, 16
  %321 = and i32 %320, 16711680
  %322 = and i32 %107, -16777216
  %323 = or i32 %322, %319
  %324 = or i32 %323, %321
  %325 = and i32 %105, 65535
  %326 = and i32 %107, 983040
  %327 = or i32 %326, %325
  %328 = and i32 %107, 8388608
  %329 = icmp eq i32 %328, 0
  %330 = shl nuw i32 %327, 12
  %331 = or i32 %330, 4095
  %332 = select i1 %329, i32 %327, i32 %331
  %333 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 11, i64 1, i32 0
  store i32 %82, i32* %333, align 4
  %334 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 11, i64 1, i32 1
  store i32 %324, i32* %334, align 4
  %335 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 11, i64 1, i32 2
  store i32 %332, i32* %335, align 4
  %336 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 11, i64 1, i32 3
  store i32 %107, i32* %336, align 4
  %337 = lshr i32 %107, 18
  %338 = and i32 %337, 16
  %339 = and i32 %317, -32785
  %340 = or i32 %318, %338
  %341 = or i32 %340, %339
  store i32 %341, i32* %278, align 4
  %342 = icmp eq i32 %315, 0
  br i1 %342, label %353, label %343

; <label>:343:                                    ; preds = %314
  %344 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 7
  %345 = bitcast i32* %344 to i64*
  %346 = load i64, i64* %345, align 8
  %347 = and i64 %346, 131072
  %348 = icmp ne i64 %347, 0
  %349 = icmp eq i32 %338, 0
  %350 = or i1 %349, %348
  %351 = lshr i64 %346, 32
  %352 = trunc i64 %351 to i32
  br i1 %350, label %353, label %356

; <label>:353:                                    ; preds = %343, %314
  %354 = phi i32 [ %352, %343 ], [ %341, %314 ]
  %355 = or i32 %281, 64
  br label %367

; <label>:356:                                    ; preds = %343
  %357 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 11, i64 3, i32 1
  %358 = load i32, i32* %357, align 4
  %359 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %259, i64 0, i32 11, i64 0, i32 1
  %360 = load i32, i32* %359, align 4
  %361 = or i32 %358, %265
  %362 = or i32 %361, %360
  %363 = icmp ne i32 %362, 0
  %364 = zext i1 %363 to i32
  %365 = shl nuw nsw i32 %364, 6
  %366 = or i32 %365, %281
  br label %367

; <label>:367:                                    ; preds = %356, %353
  %368 = phi i32 [ %354, %353 ], [ %352, %356 ]
  %369 = phi i32 [ %355, %353 ], [ %366, %356 ]
  %370 = and i32 %368, -97
  %371 = or i32 %370, %369
  store i32 %371, i32* %278, align 4
  %372 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %373 = load i32, i32* %372, align 4
  %374 = icmp eq i32 %373, 0
  br i1 %374, label %380, label %375, !prof !2

; <label>:375:                                    ; preds = %367
  %376 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  %377 = and i32 %371, 3
  tail call void %376(i32 %377, i32 %112)
  %378 = load i32, i32* %278, align 4
  %379 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %380

; <label>:380:                                    ; preds = %375, %367
  %381 = phi %struct.CPUX86State* [ %379, %375 ], [ %259, %367 ]
  %382 = phi i32 [ %378, %375 ], [ %371, %367 ]
  %383 = and i32 %382, -4
  %384 = or i32 %383, %112
  store i32 %384, i32* %278, align 4
  %385 = lshr i32 %245, 6
  %386 = and i32 %385, 65536
  %387 = xor i32 %386, 65536
  %388 = add nsw i32 %387, -1
  %389 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 0, i32 3
  %390 = load i32, i32* %389, align 4
  %391 = lshr i32 %390, 13
  %392 = and i32 %391, 3
  %393 = and i32 %390, 3072
  %394 = icmp ne i32 %393, 3072
  %395 = icmp ult i32 %392, %112
  %396 = and i1 %394, %395
  br i1 %396, label %397, label %449

; <label>:397:                                    ; preds = %380
  %398 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 0, i32 0
  %399 = bitcast i32* %398 to i8*
  br label %400

; <label>:400:                                    ; preds = %403, %397
  %401 = phi i64 [ 0, %397 ], [ %405, %403 ]
  %402 = icmp ult i64 %401, 16
  br i1 %402, label %403, label %406

; <label>:403:                                    ; preds = %400
  %404 = getelementptr i8, i8* %399, i64 %401
  store i8 0, i8* %404
  %405 = add i64 %401, 1
  br label %400

; <label>:406:                                    ; preds = %400
  %407 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 8
  %408 = load i32, i32* %407, align 4
  %409 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 2, i32 3
  %410 = load i32, i32* %409, align 4
  %411 = lshr i32 %410, 17
  %412 = and i32 %411, 32
  %413 = trunc i32 %408 to i16
  %414 = icmp slt i16 %413, 0
  br i1 %414, label %444, label %415

; <label>:415:                                    ; preds = %406
  %416 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 16, i64 0
  %417 = load i32, i32* %416, align 8
  %418 = and i32 %417, 1
  %419 = icmp eq i32 %418, 0
  br i1 %419, label %431, label %420

; <label>:420:                                    ; preds = %415
  %421 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 7
  %422 = bitcast i32* %421 to i64*
  %423 = load i64, i64* %422, align 8
  %424 = and i64 %423, 131072
  %425 = icmp ne i64 %424, 0
  %426 = and i32 %408, 16
  %427 = icmp eq i32 %426, 0
  %428 = or i1 %427, %425
  %429 = lshr i64 %423, 32
  %430 = trunc i64 %429 to i32
  br i1 %428, label %431, label %434

; <label>:431:                                    ; preds = %420, %415
  %432 = phi i32 [ %430, %420 ], [ %408, %415 ]
  %433 = or i32 %412, 64
  br label %444

; <label>:434:                                    ; preds = %420
  %435 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 3, i32 1
  %436 = load i32, i32* %435, align 4
  %437 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 2, i32 1
  %438 = load i32, i32* %437, align 4
  %439 = or i32 %438, %436
  %440 = icmp ne i32 %439, 0
  %441 = zext i1 %440 to i32
  %442 = shl nuw nsw i32 %441, 6
  %443 = or i32 %442, %412
  br label %444

; <label>:444:                                    ; preds = %434, %431, %406
  %445 = phi i32 [ %408, %406 ], [ %432, %431 ], [ %430, %434 ]
  %446 = phi i32 [ %412, %406 ], [ %433, %431 ], [ %443, %434 ]
  %447 = and i32 %445, -97
  %448 = or i32 %447, %446
  store i32 %448, i32* %407, align 4
  br label %449

; <label>:449:                                    ; preds = %444, %380
  %450 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 3, i32 3
  %451 = load i32, i32* %450, align 4
  %452 = lshr i32 %451, 13
  %453 = and i32 %452, 3
  %454 = and i32 %451, 3072
  %455 = icmp ne i32 %454, 3072
  %456 = icmp ult i32 %453, %112
  %457 = and i1 %455, %456
  br i1 %457, label %458, label %510

; <label>:458:                                    ; preds = %449
  %459 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 3, i32 0
  %460 = bitcast i32* %459 to i8*
  br label %461

; <label>:461:                                    ; preds = %464, %458
  %462 = phi i64 [ 0, %458 ], [ %466, %464 ]
  %463 = icmp ult i64 %462, 16
  br i1 %463, label %464, label %467

; <label>:464:                                    ; preds = %461
  %465 = getelementptr i8, i8* %460, i64 %462
  store i8 0, i8* %465
  %466 = add i64 %462, 1
  br label %461

; <label>:467:                                    ; preds = %461
  %468 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 8
  %469 = load i32, i32* %468, align 4
  %470 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 2, i32 3
  %471 = load i32, i32* %470, align 4
  %472 = lshr i32 %471, 17
  %473 = and i32 %472, 32
  %474 = trunc i32 %469 to i16
  %475 = icmp slt i16 %474, 0
  br i1 %475, label %505, label %476

; <label>:476:                                    ; preds = %467
  %477 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 16, i64 0
  %478 = load i32, i32* %477, align 8
  %479 = and i32 %478, 1
  %480 = icmp eq i32 %479, 0
  br i1 %480, label %492, label %481

; <label>:481:                                    ; preds = %476
  %482 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 7
  %483 = bitcast i32* %482 to i64*
  %484 = load i64, i64* %483, align 8
  %485 = and i64 %484, 131072
  %486 = icmp ne i64 %485, 0
  %487 = and i32 %469, 16
  %488 = icmp eq i32 %487, 0
  %489 = or i1 %488, %486
  %490 = lshr i64 %484, 32
  %491 = trunc i64 %490 to i32
  br i1 %489, label %492, label %495

; <label>:492:                                    ; preds = %481, %476
  %493 = phi i32 [ %491, %481 ], [ %469, %476 ]
  %494 = or i32 %473, 64
  br label %505

; <label>:495:                                    ; preds = %481
  %496 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 0, i32 1
  %497 = load i32, i32* %496, align 4
  %498 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 2, i32 1
  %499 = load i32, i32* %498, align 4
  %500 = or i32 %499, %497
  %501 = icmp ne i32 %500, 0
  %502 = zext i1 %501 to i32
  %503 = shl nuw nsw i32 %502, 6
  %504 = or i32 %503, %473
  br label %505

; <label>:505:                                    ; preds = %495, %492, %467
  %506 = phi i32 [ %469, %467 ], [ %493, %492 ], [ %491, %495 ]
  %507 = phi i32 [ %473, %467 ], [ %494, %492 ], [ %504, %495 ]
  %508 = and i32 %506, -97
  %509 = or i32 %508, %507
  store i32 %509, i32* %468, align 4
  br label %510

; <label>:510:                                    ; preds = %505, %449
  %511 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 4, i32 0
  %512 = load i32, i32* %511, align 8
  %513 = and i32 %512, 65532
  %514 = icmp eq i32 %513, 0
  br i1 %514, label %578, label %515

; <label>:515:                                    ; preds = %510
  %516 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 4, i32 3
  %517 = load i32, i32* %516, align 4
  %518 = lshr i32 %517, 13
  %519 = and i32 %518, 3
  %520 = and i32 %517, 3072
  %521 = icmp ne i32 %520, 3072
  %522 = icmp ult i32 %519, %112
  %523 = and i1 %521, %522
  br i1 %523, label %524, label %578

; <label>:524:                                    ; preds = %515
  %525 = bitcast i32* %511 to i8*
  br label %526

; <label>:526:                                    ; preds = %529, %524
  %527 = phi i64 [ 0, %524 ], [ %531, %529 ]
  %528 = icmp ult i64 %527, 16
  br i1 %528, label %529, label %532

; <label>:529:                                    ; preds = %526
  %530 = getelementptr i8, i8* %525, i64 %527
  store i8 0, i8* %530
  %531 = add i64 %527, 1
  br label %526

; <label>:532:                                    ; preds = %526
  %533 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 8
  %534 = load i32, i32* %533, align 4
  %535 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 2, i32 3
  %536 = load i32, i32* %535, align 4
  %537 = lshr i32 %536, 17
  %538 = and i32 %537, 32
  %539 = trunc i32 %534 to i16
  %540 = icmp slt i16 %539, 0
  br i1 %540, label %573, label %541

; <label>:541:                                    ; preds = %532
  %542 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 16, i64 0
  %543 = load i32, i32* %542, align 8
  %544 = and i32 %543, 1
  %545 = icmp eq i32 %544, 0
  br i1 %545, label %557, label %546

; <label>:546:                                    ; preds = %541
  %547 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 7
  %548 = bitcast i32* %547 to i64*
  %549 = load i64, i64* %548, align 8
  %550 = and i64 %549, 131072
  %551 = icmp ne i64 %550, 0
  %552 = and i32 %534, 16
  %553 = icmp eq i32 %552, 0
  %554 = or i1 %553, %551
  %555 = lshr i64 %549, 32
  %556 = trunc i64 %555 to i32
  br i1 %554, label %557, label %560

; <label>:557:                                    ; preds = %546, %541
  %558 = phi i32 [ %556, %546 ], [ %534, %541 ]
  %559 = or i32 %538, 64
  br label %573

; <label>:560:                                    ; preds = %546
  %561 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 3, i32 1
  %562 = load i32, i32* %561, align 4
  %563 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 0, i32 1
  %564 = load i32, i32* %563, align 4
  %565 = or i32 %564, %562
  %566 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 2, i32 1
  %567 = load i32, i32* %566, align 4
  %568 = or i32 %565, %567
  %569 = icmp ne i32 %568, 0
  %570 = zext i1 %569 to i32
  %571 = shl nuw nsw i32 %570, 6
  %572 = or i32 %571, %538
  br label %573

; <label>:573:                                    ; preds = %560, %557, %532
  %574 = phi i32 [ %534, %532 ], [ %558, %557 ], [ %556, %560 ]
  %575 = phi i32 [ %538, %532 ], [ %559, %557 ], [ %572, %560 ]
  %576 = and i32 %574, -97
  %577 = or i32 %576, %575
  store i32 %577, i32* %533, align 4
  br label %578

; <label>:578:                                    ; preds = %573, %515, %510
  %579 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 5, i32 0
  %580 = load i32, i32* %579, align 8
  %581 = and i32 %580, 65532
  %582 = icmp eq i32 %581, 0
  br i1 %582, label %646, label %583

; <label>:583:                                    ; preds = %578
  %584 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 5, i32 3
  %585 = load i32, i32* %584, align 4
  %586 = lshr i32 %585, 13
  %587 = and i32 %586, 3
  %588 = and i32 %585, 3072
  %589 = icmp ne i32 %588, 3072
  %590 = icmp ult i32 %587, %112
  %591 = and i1 %589, %590
  br i1 %591, label %592, label %646

; <label>:592:                                    ; preds = %583
  %593 = bitcast i32* %579 to i8*
  br label %594

; <label>:594:                                    ; preds = %597, %592
  %595 = phi i64 [ 0, %592 ], [ %599, %597 ]
  %596 = icmp ult i64 %595, 16
  br i1 %596, label %597, label %600

; <label>:597:                                    ; preds = %594
  %598 = getelementptr i8, i8* %593, i64 %595
  store i8 0, i8* %598
  %599 = add i64 %595, 1
  br label %594

; <label>:600:                                    ; preds = %594
  %601 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 8
  %602 = load i32, i32* %601, align 4
  %603 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 2, i32 3
  %604 = load i32, i32* %603, align 4
  %605 = lshr i32 %604, 17
  %606 = and i32 %605, 32
  %607 = trunc i32 %602 to i16
  %608 = icmp slt i16 %607, 0
  br i1 %608, label %641, label %609

; <label>:609:                                    ; preds = %600
  %610 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 16, i64 0
  %611 = load i32, i32* %610, align 8
  %612 = and i32 %611, 1
  %613 = icmp eq i32 %612, 0
  br i1 %613, label %625, label %614

; <label>:614:                                    ; preds = %609
  %615 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 7
  %616 = bitcast i32* %615 to i64*
  %617 = load i64, i64* %616, align 8
  %618 = and i64 %617, 131072
  %619 = icmp ne i64 %618, 0
  %620 = and i32 %602, 16
  %621 = icmp eq i32 %620, 0
  %622 = or i1 %621, %619
  %623 = lshr i64 %617, 32
  %624 = trunc i64 %623 to i32
  br i1 %622, label %625, label %628

; <label>:625:                                    ; preds = %614, %609
  %626 = phi i32 [ %624, %614 ], [ %602, %609 ]
  %627 = or i32 %606, 64
  br label %641

; <label>:628:                                    ; preds = %614
  %629 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 3, i32 1
  %630 = load i32, i32* %629, align 4
  %631 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 0, i32 1
  %632 = load i32, i32* %631, align 4
  %633 = or i32 %632, %630
  %634 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %381, i64 0, i32 11, i64 2, i32 1
  %635 = load i32, i32* %634, align 4
  %636 = or i32 %633, %635
  %637 = icmp ne i32 %636, 0
  %638 = zext i1 %637 to i32
  %639 = shl nuw nsw i32 %638, 6
  %640 = or i32 %639, %606
  br label %641

; <label>:641:                                    ; preds = %628, %625, %600
  %642 = phi i32 [ %602, %600 ], [ %626, %625 ], [ %624, %628 ]
  %643 = phi i32 [ %606, %600 ], [ %627, %625 ], [ %640, %628 ]
  %644 = and i32 %642, -97
  %645 = or i32 %644, %643
  store i32 %645, i32* %601, align 4
  br label %646

; <label>:646:                                    ; preds = %578, %583, %641, %195
  %647 = phi %struct.CPUX86State* [ %133, %195 ], [ %381, %641 ], [ %381, %583 ], [ %381, %578 ]
  %648 = phi i32 [ %83, %195 ], [ %218, %641 ], [ %218, %583 ], [ %218, %578 ]
  %649 = phi i32 [ %47, %195 ], [ %388, %641 ], [ %388, %583 ], [ %388, %578 ]
  %650 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %647, i64 0, i32 0, i64 4
  %651 = load i32, i32* %650, align 16
  %652 = xor i32 %649, -1
  %653 = and i32 %651, %652
  %654 = and i32 %649, %648
  %655 = or i32 %653, %654
  store i32 %655, i32* %650, align 16
  %656 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %647, i64 0, i32 5
  store i32 %84, i32* %656, align 16
  %657 = icmp eq i32 %41, 0
  %658 = select i1 %657, i32 2453760, i32 2441472
  %659 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %647, i64 0, i32 7
  %660 = load i32, i32* %659, align 8
  %661 = lshr i32 %660, 12
  %662 = and i32 %661, 3
  %663 = icmp ugt i32 %41, %662
  %664 = or i32 %658, 512
  %665 = select i1 %663, i32 %658, i32 %664
  %666 = icmp eq i32 %0, 0
  %667 = and i32 %665, 29440
  %668 = select i1 %666, i32 %667, i32 %665
  %669 = and i32 %81, 2261
  %670 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %647, i64 0, i32 2
  store i32 %669, i32* %670, align 4
  %671 = lshr i32 %81, 9
  %672 = and i32 %671, 2
  %673 = xor i32 %672, 2
  %674 = add nsw i32 %673, -1
  %675 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %647, i64 0, i32 6
  store i32 %674, i32* %675, align 4
  %676 = xor i32 %668, -1
  %677 = and i32 %660, %676
  %678 = and i32 %668, %81
  %679 = or i32 %677, %678
  store i32 %679, i32* %659, align 8
  br i1 %131, label %954, label %680

; <label>:680:                                    ; preds = %646
  %681 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %682 = load i32, i32* %681, align 4
  %683 = icmp eq i32 %682, 0
  br i1 %683, label %954, label %684, !prof !2

; <label>:684:                                    ; preds = %680
  %685 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  tail call void %685(i32 %41, i32 %112)
  br label %954

; <label>:686:                                    ; preds = %55
  %687 = and i32 %47, %66
  %688 = add i32 %687, %51
  %689 = tail call fastcc i32 @ldl_kernel(i32 %688)
  %690 = add i32 %49, 16
  %691 = and i32 %47, %690
  %692 = add i32 %691, %51
  %693 = tail call fastcc i32 @ldl_kernel(i32 %692)
  %694 = add i32 %49, 20
  %695 = and i32 %47, %694
  %696 = add i32 %695, %51
  %697 = tail call fastcc i32 @ldl_kernel(i32 %696)
  %698 = add i32 %49, 24
  %699 = and i32 %47, %698
  %700 = add i32 %699, %51
  %701 = tail call fastcc i32 @ldl_kernel(i32 %700)
  %702 = add i32 %49, 28
  %703 = and i32 %47, %702
  %704 = add i32 %703, %51
  %705 = tail call fastcc i32 @ldl_kernel(i32 %704)
  %706 = add i32 %49, 32
  %707 = and i32 %47, %706
  %708 = add i32 %707, %51
  %709 = tail call fastcc i32 @ldl_kernel(i32 %708)
  %710 = and i32 %65, 2261
  %711 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %712 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %711, i64 0, i32 2
  store i32 %710, i32* %712, align 4
  %713 = lshr i32 %65, 9
  %714 = and i32 %713, 2
  %715 = xor i32 %714, 2
  %716 = add nsw i32 %715, -1
  %717 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %711, i64 0, i32 6
  store i32 %716, i32* %717, align 4
  %718 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %711, i64 0, i32 7
  %719 = bitcast i32* %718 to i64*
  %720 = load i64, i64* %719, align 8
  %721 = trunc i64 %720 to i32
  %722 = and i32 %721, -4092673
  %723 = and i32 %65, 4092672
  %724 = or i32 %722, %723
  store i32 %724, i32* %718, align 8
  %725 = shl nuw nsw i32 %62, 4
  %726 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %711, i64 0, i32 11, i64 1, i32 0
  store i32 %62, i32* %726, align 4
  %727 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %711, i64 0, i32 11, i64 1, i32 1
  store i32 %725, i32* %727, align 4
  %728 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %711, i64 0, i32 11, i64 1, i32 2
  store i32 65535, i32* %728, align 4
  %729 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %711, i64 0, i32 11, i64 1, i32 3
  store i32 0, i32* %729, align 4
  %730 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %711, i64 0, i32 8
  %731 = lshr i64 %720, 32
  %732 = trunc i64 %731 to i32
  %733 = and i32 %732, -32785
  store i32 %733, i32* %730, align 4
  %734 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %711, i64 0, i32 11, i64 2, i32 3
  %735 = load i32, i32* %734, align 4
  %736 = lshr i32 %735, 17
  %737 = and i32 %736, 32
  %738 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %711, i64 0, i32 16, i64 0
  %739 = load i32, i32* %738, align 8
  %740 = and i32 %739, 1
  %741 = icmp eq i32 %740, 0
  br i1 %741, label %746, label %742

; <label>:742:                                    ; preds = %686
  %743 = load i64, i64* %719, align 8
  %744 = lshr i64 %743, 32
  %745 = trunc i64 %744 to i32
  br label %746

; <label>:746:                                    ; preds = %742, %686
  %747 = phi i32 [ %745, %742 ], [ %733, %686 ]
  %748 = and i32 %747, -97
  %749 = or i32 %737, %748
  %750 = or i32 %749, 64
  store i32 %750, i32* %730, align 4
  %751 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %752 = load i32, i32* %751, align 4
  %753 = icmp eq i32 %752, 0
  br i1 %753, label %759, label %754, !prof !2

; <label>:754:                                    ; preds = %746
  %755 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  %756 = and i32 %747, 3
  tail call void %755(i32 %756, i32 3)
  %757 = load i32, i32* %730, align 4
  %758 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %759

; <label>:759:                                    ; preds = %754, %746
  %760 = phi %struct.CPUX86State* [ %758, %754 ], [ %711, %746 ]
  %761 = phi i32 [ %757, %754 ], [ %750, %746 ]
  %762 = or i32 %761, 3
  store i32 %762, i32* %730, align 4
  %763 = and i32 %693, 65535
  %764 = shl nuw nsw i32 %763, 4
  %765 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 2, i32 0
  store i32 %763, i32* %765, align 4
  %766 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 2, i32 1
  store i32 %764, i32* %766, align 4
  %767 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 2, i32 2
  store i32 65535, i32* %767, align 4
  %768 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 2, i32 3
  store i32 0, i32* %768, align 4
  %769 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 8
  %770 = load i32, i32* %769, align 4
  %771 = trunc i32 %770 to i16
  %772 = icmp slt i16 %771, 0
  br i1 %772, label %799, label %773

; <label>:773:                                    ; preds = %759
  %774 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 16, i64 0
  %775 = load i32, i32* %774, align 8
  %776 = and i32 %775, 1
  %777 = icmp eq i32 %776, 0
  br i1 %777, label %799, label %778

; <label>:778:                                    ; preds = %773
  %779 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 7
  %780 = bitcast i32* %779 to i64*
  %781 = load i64, i64* %780, align 8
  %782 = and i64 %781, 131072
  %783 = icmp ne i64 %782, 0
  %784 = and i32 %770, 16
  %785 = icmp eq i32 %784, 0
  %786 = or i1 %785, %783
  %787 = lshr i64 %781, 32
  %788 = trunc i64 %787 to i32
  br i1 %786, label %799, label %789

; <label>:789:                                    ; preds = %778
  %790 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 3, i32 1
  %791 = load i32, i32* %790, align 4
  %792 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 0, i32 1
  %793 = load i32, i32* %792, align 4
  %794 = or i32 %791, %764
  %795 = or i32 %794, %793
  %796 = icmp ne i32 %795, 0
  %797 = zext i1 %796 to i32
  %798 = shl nuw nsw i32 %797, 6
  br label %799

; <label>:799:                                    ; preds = %789, %778, %773, %759
  %800 = phi i32 [ %770, %759 ], [ %788, %789 ], [ %788, %778 ], [ %770, %773 ]
  %801 = phi i32 [ 0, %759 ], [ %798, %789 ], [ 64, %778 ], [ 64, %773 ]
  %802 = and i32 %800, -97
  %803 = or i32 %802, %801
  store i32 %803, i32* %769, align 4
  %804 = and i32 %697, 65535
  %805 = shl nuw nsw i32 %804, 4
  %806 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 0, i32 0
  store i32 %804, i32* %806, align 4
  %807 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 0, i32 1
  store i32 %805, i32* %807, align 4
  %808 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 0, i32 2
  store i32 65535, i32* %808, align 4
  %809 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 0, i32 3
  store i32 0, i32* %809, align 4
  %810 = trunc i32 %803 to i16
  %811 = icmp slt i16 %810, 0
  br i1 %811, label %836, label %812

; <label>:812:                                    ; preds = %799
  %813 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 16, i64 0
  %814 = load i32, i32* %813, align 8
  %815 = and i32 %814, 1
  %816 = icmp eq i32 %815, 0
  br i1 %816, label %836, label %817

; <label>:817:                                    ; preds = %812
  %818 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 7
  %819 = bitcast i32* %818 to i64*
  %820 = load i64, i64* %819, align 8
  %821 = and i64 %820, 131072
  %822 = icmp ne i64 %821, 0
  %823 = and i32 %800, 16
  %824 = icmp eq i32 %823, 0
  %825 = or i1 %824, %822
  %826 = lshr i64 %820, 32
  %827 = trunc i64 %826 to i32
  br i1 %825, label %836, label %828

; <label>:828:                                    ; preds = %817
  %829 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 3, i32 1
  %830 = load i32, i32* %829, align 4
  %831 = or i32 %805, %764
  %832 = or i32 %831, %830
  %833 = icmp ne i32 %832, 0
  %834 = zext i1 %833 to i32
  %835 = shl nuw nsw i32 %834, 6
  br label %836

; <label>:836:                                    ; preds = %828, %817, %812, %799
  %837 = phi i32 [ %803, %799 ], [ %827, %828 ], [ %827, %817 ], [ %803, %812 ]
  %838 = phi i32 [ 0, %799 ], [ %835, %828 ], [ 64, %817 ], [ 64, %812 ]
  %839 = and i32 %837, -97
  %840 = or i32 %839, %838
  store i32 %840, i32* %769, align 4
  %841 = and i32 %701, 65535
  %842 = shl nuw nsw i32 %841, 4
  %843 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 3, i32 0
  store i32 %841, i32* %843, align 4
  %844 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 3, i32 1
  store i32 %842, i32* %844, align 4
  %845 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 3, i32 2
  store i32 65535, i32* %845, align 4
  %846 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 3, i32 3
  store i32 0, i32* %846, align 4
  %847 = trunc i32 %840 to i16
  %848 = icmp slt i16 %847, 0
  br i1 %848, label %871, label %849

; <label>:849:                                    ; preds = %836
  %850 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 16, i64 0
  %851 = load i32, i32* %850, align 8
  %852 = and i32 %851, 1
  %853 = icmp eq i32 %852, 0
  br i1 %853, label %871, label %854

; <label>:854:                                    ; preds = %849
  %855 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 7
  %856 = bitcast i32* %855 to i64*
  %857 = load i64, i64* %856, align 8
  %858 = and i64 %857, 131072
  %859 = icmp ne i64 %858, 0
  %860 = and i32 %837, 16
  %861 = icmp eq i32 %860, 0
  %862 = or i1 %861, %859
  %863 = lshr i64 %857, 32
  %864 = trunc i64 %863 to i32
  br i1 %862, label %871, label %865

; <label>:865:                                    ; preds = %854
  %866 = or i32 %805, %764
  %867 = or i32 %866, %842
  %868 = icmp ne i32 %867, 0
  %869 = zext i1 %868 to i32
  %870 = shl nuw nsw i32 %869, 6
  br label %871

; <label>:871:                                    ; preds = %865, %854, %849, %836
  %872 = phi i32 [ %840, %836 ], [ %864, %865 ], [ %864, %854 ], [ %840, %849 ]
  %873 = phi i32 [ 0, %836 ], [ %870, %865 ], [ 64, %854 ], [ 64, %849 ]
  %874 = and i32 %872, -97
  %875 = or i32 %874, %873
  store i32 %875, i32* %769, align 4
  %876 = and i32 %705, 65535
  %877 = shl nuw nsw i32 %876, 4
  %878 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 4, i32 0
  store i32 %876, i32* %878, align 4
  %879 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 4, i32 1
  store i32 %877, i32* %879, align 4
  %880 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 4, i32 2
  store i32 65535, i32* %880, align 4
  %881 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 4, i32 3
  store i32 0, i32* %881, align 4
  %882 = trunc i32 %875 to i16
  %883 = icmp slt i16 %882, 0
  br i1 %883, label %906, label %884

; <label>:884:                                    ; preds = %871
  %885 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 16, i64 0
  %886 = load i32, i32* %885, align 8
  %887 = and i32 %886, 1
  %888 = icmp eq i32 %887, 0
  br i1 %888, label %906, label %889

; <label>:889:                                    ; preds = %884
  %890 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 7
  %891 = bitcast i32* %890 to i64*
  %892 = load i64, i64* %891, align 8
  %893 = and i64 %892, 131072
  %894 = icmp ne i64 %893, 0
  %895 = and i32 %872, 16
  %896 = icmp eq i32 %895, 0
  %897 = or i1 %896, %894
  %898 = lshr i64 %892, 32
  %899 = trunc i64 %898 to i32
  br i1 %897, label %906, label %900

; <label>:900:                                    ; preds = %889
  %901 = or i32 %805, %764
  %902 = or i32 %901, %842
  %903 = icmp ne i32 %902, 0
  %904 = zext i1 %903 to i32
  %905 = shl nuw nsw i32 %904, 6
  br label %906

; <label>:906:                                    ; preds = %900, %889, %884, %871
  %907 = phi i32 [ %875, %871 ], [ %899, %900 ], [ %899, %889 ], [ %875, %884 ]
  %908 = phi i32 [ 0, %871 ], [ %905, %900 ], [ 64, %889 ], [ 64, %884 ]
  %909 = and i32 %907, -97
  %910 = or i32 %909, %908
  store i32 %910, i32* %769, align 4
  %911 = and i32 %709, 65535
  %912 = shl nuw nsw i32 %911, 4
  %913 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 5, i32 0
  store i32 %911, i32* %913, align 4
  %914 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 5, i32 1
  store i32 %912, i32* %914, align 4
  %915 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 5, i32 2
  store i32 65535, i32* %915, align 4
  %916 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 11, i64 5, i32 3
  store i32 0, i32* %916, align 4
  %917 = trunc i32 %910 to i16
  %918 = icmp slt i16 %917, 0
  br i1 %918, label %941, label %919

; <label>:919:                                    ; preds = %906
  %920 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 16, i64 0
  %921 = load i32, i32* %920, align 8
  %922 = and i32 %921, 1
  %923 = icmp eq i32 %922, 0
  br i1 %923, label %941, label %924

; <label>:924:                                    ; preds = %919
  %925 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 7
  %926 = bitcast i32* %925 to i64*
  %927 = load i64, i64* %926, align 8
  %928 = and i64 %927, 131072
  %929 = icmp ne i64 %928, 0
  %930 = and i32 %907, 16
  %931 = icmp eq i32 %930, 0
  %932 = or i1 %931, %929
  %933 = lshr i64 %927, 32
  %934 = trunc i64 %933 to i32
  br i1 %932, label %941, label %935

; <label>:935:                                    ; preds = %924
  %936 = or i32 %805, %764
  %937 = or i32 %936, %842
  %938 = icmp ne i32 %937, 0
  %939 = zext i1 %938 to i32
  %940 = shl nuw nsw i32 %939, 6
  br label %941

; <label>:941:                                    ; preds = %935, %924, %919, %906
  %942 = phi i32 [ %910, %906 ], [ %934, %935 ], [ %934, %924 ], [ %910, %919 ]
  %943 = phi i32 [ 0, %906 ], [ %940, %935 ], [ 64, %924 ], [ 64, %919 ]
  %944 = and i32 %942, -97
  %945 = or i32 %944, %943
  store i32 %945, i32* %769, align 4
  %946 = and i32 %56, 65535
  %947 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 5
  store i32 %946, i32* %947, align 16
  %948 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %760, i64 0, i32 0, i64 4
  store i32 %689, i32* %948, align 16
  %949 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %950 = load i32, i32* %949, align 4
  %951 = icmp eq i32 %950, 0
  br i1 %951, label %954, label %952, !prof !2

; <label>:952:                                    ; preds = %941
  %953 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  tail call void %953(i32 %41, i32 3)
  br label %954

; <label>:954:                                    ; preds = %952, %941, %684, %680, %646, %37
  %955 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %956 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %955, i64 0, i32 9
  %957 = load i32, i32* %956, align 16
  %958 = and i32 %957, -5
  store i32 %958, i32* %956, align 16
  ret void
}

; Function Attrs: uwtable
define void @helper_lret_protected(i32, i32) local_unnamed_addr #2 {
  %3 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %4 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 8
  %5 = load i32, i32* %4, align 4
  %6 = and i32 %5, 3
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 11, i64 2, i32 3
  %8 = load i32, i32* %7, align 4
  %9 = lshr i32 %8, 6
  %10 = and i32 %9, 65536
  %11 = xor i32 %10, 65536
  %12 = add nsw i32 %11, -1
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 0, i64 4
  %14 = load i32, i32* %13, align 16
  %15 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 11, i64 2, i32 1
  %16 = load i32, i32* %15, align 4
  %17 = icmp eq i32 %0, 1
  %18 = and i32 %12, %14
  %19 = add i32 %18, %16
  br i1 %17, label %20, label %28

; <label>:20:                                     ; preds = %2
  %21 = tail call fastcc i32 @ldl_kernel(i32 %19)
  %22 = add i32 %14, 4
  %23 = and i32 %12, %22
  %24 = add i32 %23, %16
  %25 = tail call fastcc i32 @ldl_kernel(i32 %24)
  %26 = add i32 %14, 8
  %27 = and i32 %25, 65535
  br label %35

; <label>:28:                                     ; preds = %2
  %29 = tail call fastcc i32 @lduw_kernel(i32 %19)
  %30 = add i32 %14, 2
  %31 = and i32 %12, %30
  %32 = add i32 %31, %16
  %33 = tail call fastcc i32 @lduw_kernel(i32 %32)
  %34 = add i32 %14, 4
  br label %35

; <label>:35:                                     ; preds = %28, %20
  %36 = phi i32 [ %27, %20 ], [ %33, %28 ]
  %37 = phi i32 [ %26, %20 ], [ %34, %28 ]
  %38 = phi i32 [ %21, %20 ], [ %29, %28 ]
  %39 = and i32 %36, 65532
  %40 = icmp eq i32 %39, 0
  br i1 %40, label %41, label %42

; <label>:41:                                     ; preds = %35
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:42:                                     ; preds = %35
  %43 = and i32 %36, 4
  %44 = icmp eq i32 %43, 0
  %45 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %46 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %45, i64 0, i32 12
  %47 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %45, i64 0, i32 14
  %48 = select i1 %44, %struct.SegmentCache* %47, %struct.SegmentCache* %46
  %49 = or i32 %36, 7
  %50 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %48, i64 0, i32 2
  %51 = load i32, i32* %50, align 4
  %52 = icmp ugt i32 %49, %51
  br i1 %52, label %53, label %54

; <label>:53:                                     ; preds = %42
  tail call fastcc void @raise_exception_err(i32 13, i32 %39) #12
  unreachable

; <label>:54:                                     ; preds = %42
  %55 = and i32 %36, -8
  %56 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %48, i64 0, i32 1
  %57 = load i32, i32* %56, align 4
  %58 = add i32 %57, %55
  %59 = tail call fastcc i32 @ldl_kernel(i32 %58)
  %60 = add i32 %58, 4
  %61 = tail call fastcc i32 @ldl_kernel(i32 %60)
  %62 = and i32 %61, 6144
  %63 = icmp eq i32 %62, 6144
  br i1 %63, label %65, label %64

; <label>:64:                                     ; preds = %54
  tail call fastcc void @raise_exception_err(i32 13, i32 %39) #12
  unreachable

; <label>:65:                                     ; preds = %54
  %66 = and i32 %36, 3
  %67 = icmp ult i32 %66, %6
  br i1 %67, label %68, label %69

; <label>:68:                                     ; preds = %65
  tail call fastcc void @raise_exception_err(i32 13, i32 %39) #12
  unreachable

; <label>:69:                                     ; preds = %65
  %70 = lshr i32 %61, 13
  %71 = and i32 %70, 3
  %72 = and i32 %61, 1024
  %73 = icmp eq i32 %72, 0
  br i1 %73, label %77, label %74

; <label>:74:                                     ; preds = %69
  %75 = icmp ugt i32 %71, %66
  br i1 %75, label %76, label %80

; <label>:76:                                     ; preds = %74
  tail call fastcc void @raise_exception_err(i32 13, i32 %39) #12
  unreachable

; <label>:77:                                     ; preds = %69
  %78 = icmp eq i32 %71, %66
  br i1 %78, label %80, label %79

; <label>:79:                                     ; preds = %77
  tail call fastcc void @raise_exception_err(i32 13, i32 %39) #12
  unreachable

; <label>:80:                                     ; preds = %77, %74
  %81 = trunc i32 %61 to i16
  %82 = icmp slt i16 %81, 0
  br i1 %82, label %84, label %83

; <label>:83:                                     ; preds = %80
  tail call fastcc void @raise_exception_err(i32 11, i32 %39) #12
  unreachable

; <label>:84:                                     ; preds = %80
  %85 = add i32 %37, %1
  %86 = icmp eq i32 %66, %6
  br i1 %86, label %87, label %152

; <label>:87:                                     ; preds = %84
  %88 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %89 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 8
  %90 = load i32, i32* %89, align 4
  %91 = lshr i32 %59, 16
  %92 = shl i32 %61, 16
  %93 = and i32 %92, 16711680
  %94 = and i32 %61, -16777216
  %95 = or i32 %94, %91
  %96 = or i32 %95, %93
  %97 = and i32 %59, 65535
  %98 = and i32 %61, 983040
  %99 = or i32 %98, %97
  %100 = and i32 %61, 8388608
  %101 = icmp eq i32 %100, 0
  %102 = shl nuw i32 %99, 12
  %103 = or i32 %102, 4095
  %104 = select i1 %101, i32 %99, i32 %103
  %105 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 11, i64 1, i32 0
  store i32 %36, i32* %105, align 4
  %106 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 11, i64 1, i32 1
  store i32 %96, i32* %106, align 4
  %107 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 11, i64 1, i32 2
  store i32 %104, i32* %107, align 4
  %108 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 11, i64 1, i32 3
  store i32 %61, i32* %108, align 4
  %109 = lshr i32 %61, 18
  %110 = and i32 %109, 16
  %111 = and i32 %90, -32785
  %112 = or i32 %111, %110
  store i32 %112, i32* %89, align 4
  %113 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 11, i64 2, i32 3
  %114 = load i32, i32* %113, align 4
  %115 = lshr i32 %114, 17
  %116 = and i32 %115, 32
  %117 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 16, i64 0
  %118 = load i32, i32* %117, align 8
  %119 = and i32 %118, 1
  %120 = icmp eq i32 %119, 0
  br i1 %120, label %131, label %121

; <label>:121:                                    ; preds = %87
  %122 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 7
  %123 = bitcast i32* %122 to i64*
  %124 = load i64, i64* %123, align 8
  %125 = and i64 %124, 131072
  %126 = icmp ne i64 %125, 0
  %127 = icmp eq i32 %110, 0
  %128 = or i1 %127, %126
  %129 = lshr i64 %124, 32
  %130 = trunc i64 %129 to i32
  br i1 %128, label %131, label %134

; <label>:131:                                    ; preds = %121, %87
  %132 = phi i32 [ %130, %121 ], [ %112, %87 ]
  %133 = or i32 %116, 64
  br label %147

; <label>:134:                                    ; preds = %121
  %135 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 11, i64 3, i32 1
  %136 = load i32, i32* %135, align 4
  %137 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 11, i64 0, i32 1
  %138 = load i32, i32* %137, align 4
  %139 = or i32 %138, %136
  %140 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %88, i64 0, i32 11, i64 2, i32 1
  %141 = load i32, i32* %140, align 4
  %142 = or i32 %139, %141
  %143 = icmp ne i32 %142, 0
  %144 = zext i1 %143 to i32
  %145 = shl nuw nsw i32 %144, 6
  %146 = or i32 %145, %116
  br label %147

; <label>:147:                                    ; preds = %134, %131
  %148 = phi i32 [ %132, %131 ], [ %130, %134 ]
  %149 = phi i32 [ %133, %131 ], [ %146, %134 ]
  %150 = and i32 %148, -97
  %151 = or i32 %150, %149
  store i32 %151, i32* %89, align 4
  br label %600

; <label>:152:                                    ; preds = %84
  %153 = and i32 %85, %12
  %154 = add i32 %153, %16
  br i1 %17, label %155, label %162

; <label>:155:                                    ; preds = %152
  %156 = tail call fastcc i32 @ldl_kernel(i32 %154)
  %157 = add i32 %85, 4
  %158 = and i32 %157, %12
  %159 = add i32 %158, %16
  %160 = tail call fastcc i32 @ldl_kernel(i32 %159)
  %161 = and i32 %160, 65535
  br label %168

; <label>:162:                                    ; preds = %152
  %163 = tail call fastcc i32 @lduw_kernel(i32 %154)
  %164 = add i32 %85, 2
  %165 = and i32 %164, %12
  %166 = add i32 %165, %16
  %167 = tail call fastcc i32 @lduw_kernel(i32 %166)
  br label %168

; <label>:168:                                    ; preds = %162, %155
  %169 = phi i32 [ %161, %155 ], [ %167, %162 ]
  %170 = phi i32 [ %156, %155 ], [ %163, %162 ]
  %171 = and i32 %169, 65532
  %172 = icmp eq i32 %171, 0
  br i1 %172, label %173, label %174

; <label>:173:                                    ; preds = %168
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:174:                                    ; preds = %168
  %175 = and i32 %169, 3
  %176 = icmp eq i32 %175, %66
  br i1 %176, label %178, label %177

; <label>:177:                                    ; preds = %174
  tail call fastcc void @raise_exception_err(i32 13, i32 %171) #12
  unreachable

; <label>:178:                                    ; preds = %174
  %179 = and i32 %169, 4
  %180 = icmp eq i32 %179, 0
  %181 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %182 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %181, i64 0, i32 12
  %183 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %181, i64 0, i32 14
  %184 = select i1 %180, %struct.SegmentCache* %183, %struct.SegmentCache* %182
  %185 = or i32 %169, 7
  %186 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %184, i64 0, i32 2
  %187 = load i32, i32* %186, align 4
  %188 = icmp ugt i32 %185, %187
  br i1 %188, label %189, label %190

; <label>:189:                                    ; preds = %178
  tail call fastcc void @raise_exception_err(i32 13, i32 %171) #12
  unreachable

; <label>:190:                                    ; preds = %178
  %191 = and i32 %169, -8
  %192 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %184, i64 0, i32 1
  %193 = load i32, i32* %192, align 4
  %194 = add i32 %193, %191
  %195 = tail call fastcc i32 @ldl_kernel(i32 %194)
  %196 = add i32 %194, 4
  %197 = tail call fastcc i32 @ldl_kernel(i32 %196)
  %198 = and i32 %197, 6656
  %199 = icmp eq i32 %198, 4608
  br i1 %199, label %201, label %200

; <label>:200:                                    ; preds = %190
  tail call fastcc void @raise_exception_err(i32 13, i32 %171) #12
  unreachable

; <label>:201:                                    ; preds = %190
  %202 = lshr i32 %197, 13
  %203 = and i32 %202, 3
  %204 = icmp eq i32 %203, %66
  br i1 %204, label %206, label %205

; <label>:205:                                    ; preds = %201
  tail call fastcc void @raise_exception_err(i32 13, i32 %171) #12
  unreachable

; <label>:206:                                    ; preds = %201
  %207 = trunc i32 %197 to i16
  %208 = icmp slt i16 %207, 0
  br i1 %208, label %210, label %209

; <label>:209:                                    ; preds = %206
  tail call fastcc void @raise_exception_err(i32 11, i32 %171) #12
  unreachable

; <label>:210:                                    ; preds = %206
  %211 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %212 = lshr i32 %195, 16
  %213 = shl i32 %197, 16
  %214 = and i32 %213, 16711680
  %215 = and i32 %197, -16777216
  %216 = or i32 %215, %212
  %217 = or i32 %216, %214
  %218 = and i32 %195, 65535
  %219 = and i32 %197, 983040
  %220 = or i32 %219, %218
  %221 = and i32 %197, 8388608
  %222 = icmp eq i32 %221, 0
  %223 = shl nuw i32 %220, 12
  %224 = or i32 %223, 4095
  %225 = select i1 %222, i32 %220, i32 %224
  %226 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 11, i64 2, i32 0
  store i32 %169, i32* %226, align 4
  %227 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 11, i64 2, i32 1
  store i32 %217, i32* %227, align 4
  %228 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 11, i64 2, i32 2
  store i32 %225, i32* %228, align 4
  %229 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 11, i64 2, i32 3
  store i32 %197, i32* %229, align 4
  %230 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 8
  %231 = load i32, i32* %230, align 4
  %232 = lshr i32 %197, 17
  %233 = and i32 %232, 32
  %234 = trunc i32 %231 to i16
  %235 = icmp slt i16 %234, 0
  %236 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 16, i64 0
  %237 = load i32, i32* %236, align 8
  %238 = and i32 %237, 1
  br i1 %235, label %266, label %239

; <label>:239:                                    ; preds = %210
  %240 = icmp eq i32 %238, 0
  br i1 %240, label %252, label %241

; <label>:241:                                    ; preds = %239
  %242 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 7
  %243 = bitcast i32* %242 to i64*
  %244 = load i64, i64* %243, align 8
  %245 = and i64 %244, 131072
  %246 = icmp ne i64 %245, 0
  %247 = and i32 %231, 16
  %248 = icmp eq i32 %247, 0
  %249 = or i1 %248, %246
  %250 = lshr i64 %244, 32
  %251 = trunc i64 %250 to i32
  br i1 %249, label %252, label %255

; <label>:252:                                    ; preds = %241, %239
  %253 = phi i32 [ %251, %241 ], [ %231, %239 ]
  %254 = or i32 %233, 64
  br label %266

; <label>:255:                                    ; preds = %241
  %256 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 11, i64 3, i32 1
  %257 = load i32, i32* %256, align 4
  %258 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 11, i64 0, i32 1
  %259 = load i32, i32* %258, align 4
  %260 = or i32 %257, %217
  %261 = or i32 %260, %259
  %262 = icmp ne i32 %261, 0
  %263 = zext i1 %262 to i32
  %264 = shl nuw nsw i32 %263, 6
  %265 = or i32 %264, %233
  br label %266

; <label>:266:                                    ; preds = %210, %255, %252
  %267 = phi i32 [ 1, %255 ], [ %238, %252 ], [ %238, %210 ]
  %268 = phi i32 [ %251, %255 ], [ %253, %252 ], [ %231, %210 ]
  %269 = phi i32 [ %265, %255 ], [ %254, %252 ], [ %233, %210 ]
  %270 = and i32 %268, -32881
  %271 = lshr i32 %59, 16
  %272 = shl i32 %61, 16
  %273 = and i32 %272, 16711680
  %274 = and i32 %61, -16777216
  %275 = or i32 %274, %271
  %276 = or i32 %275, %273
  %277 = and i32 %59, 65535
  %278 = and i32 %61, 983040
  %279 = or i32 %278, %277
  %280 = and i32 %61, 8388608
  %281 = icmp eq i32 %280, 0
  %282 = shl nuw i32 %279, 12
  %283 = or i32 %282, 4095
  %284 = select i1 %281, i32 %279, i32 %283
  %285 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 11, i64 1, i32 0
  store i32 %36, i32* %285, align 4
  %286 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 11, i64 1, i32 1
  store i32 %276, i32* %286, align 4
  %287 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 11, i64 1, i32 2
  store i32 %284, i32* %287, align 4
  %288 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 11, i64 1, i32 3
  store i32 %61, i32* %288, align 4
  %289 = lshr i32 %61, 18
  %290 = and i32 %289, 16
  %291 = and i32 %269, -32785
  %292 = or i32 %270, %290
  %293 = or i32 %292, %291
  store i32 %293, i32* %230, align 4
  %294 = icmp eq i32 %267, 0
  br i1 %294, label %305, label %295

; <label>:295:                                    ; preds = %266
  %296 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 7
  %297 = bitcast i32* %296 to i64*
  %298 = load i64, i64* %297, align 8
  %299 = and i64 %298, 131072
  %300 = icmp ne i64 %299, 0
  %301 = icmp eq i32 %290, 0
  %302 = or i1 %301, %300
  %303 = lshr i64 %298, 32
  %304 = trunc i64 %303 to i32
  br i1 %302, label %305, label %308

; <label>:305:                                    ; preds = %295, %266
  %306 = phi i32 [ %304, %295 ], [ %293, %266 ]
  %307 = or i32 %233, 64
  br label %319

; <label>:308:                                    ; preds = %295
  %309 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 11, i64 3, i32 1
  %310 = load i32, i32* %309, align 4
  %311 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %211, i64 0, i32 11, i64 0, i32 1
  %312 = load i32, i32* %311, align 4
  %313 = or i32 %310, %217
  %314 = or i32 %313, %312
  %315 = icmp ne i32 %314, 0
  %316 = zext i1 %315 to i32
  %317 = shl nuw nsw i32 %316, 6
  %318 = or i32 %317, %233
  br label %319

; <label>:319:                                    ; preds = %308, %305
  %320 = phi i32 [ %306, %305 ], [ %304, %308 ]
  %321 = phi i32 [ %307, %305 ], [ %318, %308 ]
  %322 = and i32 %320, -97
  %323 = or i32 %322, %321
  store i32 %323, i32* %230, align 4
  %324 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %325 = load i32, i32* %324, align 4
  %326 = icmp eq i32 %325, 0
  br i1 %326, label %332, label %327, !prof !2

; <label>:327:                                    ; preds = %319
  %328 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  %329 = and i32 %323, 3
  tail call void %328(i32 %329, i32 %66)
  %330 = load i32, i32* %230, align 4
  %331 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %332

; <label>:332:                                    ; preds = %327, %319
  %333 = phi %struct.CPUX86State* [ %331, %327 ], [ %211, %319 ]
  %334 = phi i32 [ %330, %327 ], [ %323, %319 ]
  %335 = and i32 %334, -4
  %336 = or i32 %335, %66
  store i32 %336, i32* %230, align 4
  %337 = lshr i32 %197, 6
  %338 = and i32 %337, 65536
  %339 = xor i32 %338, 65536
  %340 = add nsw i32 %339, -1
  %341 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 0, i32 3
  %342 = load i32, i32* %341, align 4
  %343 = lshr i32 %342, 13
  %344 = and i32 %343, 3
  %345 = and i32 %342, 3072
  %346 = icmp ne i32 %345, 3072
  %347 = icmp ult i32 %344, %66
  %348 = and i1 %346, %347
  br i1 %348, label %349, label %401

; <label>:349:                                    ; preds = %332
  %350 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 0, i32 0
  %351 = bitcast i32* %350 to i8*
  br label %352

; <label>:352:                                    ; preds = %355, %349
  %353 = phi i64 [ 0, %349 ], [ %357, %355 ]
  %354 = icmp ult i64 %353, 16
  br i1 %354, label %355, label %358

; <label>:355:                                    ; preds = %352
  %356 = getelementptr i8, i8* %351, i64 %353
  store i8 0, i8* %356
  %357 = add i64 %353, 1
  br label %352

; <label>:358:                                    ; preds = %352
  %359 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 8
  %360 = load i32, i32* %359, align 4
  %361 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 2, i32 3
  %362 = load i32, i32* %361, align 4
  %363 = lshr i32 %362, 17
  %364 = and i32 %363, 32
  %365 = trunc i32 %360 to i16
  %366 = icmp slt i16 %365, 0
  br i1 %366, label %396, label %367

; <label>:367:                                    ; preds = %358
  %368 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 16, i64 0
  %369 = load i32, i32* %368, align 8
  %370 = and i32 %369, 1
  %371 = icmp eq i32 %370, 0
  br i1 %371, label %383, label %372

; <label>:372:                                    ; preds = %367
  %373 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 7
  %374 = bitcast i32* %373 to i64*
  %375 = load i64, i64* %374, align 8
  %376 = and i64 %375, 131072
  %377 = icmp ne i64 %376, 0
  %378 = and i32 %360, 16
  %379 = icmp eq i32 %378, 0
  %380 = or i1 %379, %377
  %381 = lshr i64 %375, 32
  %382 = trunc i64 %381 to i32
  br i1 %380, label %383, label %386

; <label>:383:                                    ; preds = %372, %367
  %384 = phi i32 [ %382, %372 ], [ %360, %367 ]
  %385 = or i32 %364, 64
  br label %396

; <label>:386:                                    ; preds = %372
  %387 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 3, i32 1
  %388 = load i32, i32* %387, align 4
  %389 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 2, i32 1
  %390 = load i32, i32* %389, align 4
  %391 = or i32 %390, %388
  %392 = icmp ne i32 %391, 0
  %393 = zext i1 %392 to i32
  %394 = shl nuw nsw i32 %393, 6
  %395 = or i32 %394, %364
  br label %396

; <label>:396:                                    ; preds = %386, %383, %358
  %397 = phi i32 [ %360, %358 ], [ %384, %383 ], [ %382, %386 ]
  %398 = phi i32 [ %364, %358 ], [ %385, %383 ], [ %395, %386 ]
  %399 = and i32 %397, -97
  %400 = or i32 %399, %398
  store i32 %400, i32* %359, align 4
  br label %401

; <label>:401:                                    ; preds = %396, %332
  %402 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 3, i32 3
  %403 = load i32, i32* %402, align 4
  %404 = lshr i32 %403, 13
  %405 = and i32 %404, 3
  %406 = and i32 %403, 3072
  %407 = icmp ne i32 %406, 3072
  %408 = icmp ult i32 %405, %66
  %409 = and i1 %407, %408
  br i1 %409, label %410, label %462

; <label>:410:                                    ; preds = %401
  %411 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 3, i32 0
  %412 = bitcast i32* %411 to i8*
  br label %413

; <label>:413:                                    ; preds = %416, %410
  %414 = phi i64 [ 0, %410 ], [ %418, %416 ]
  %415 = icmp ult i64 %414, 16
  br i1 %415, label %416, label %419

; <label>:416:                                    ; preds = %413
  %417 = getelementptr i8, i8* %412, i64 %414
  store i8 0, i8* %417
  %418 = add i64 %414, 1
  br label %413

; <label>:419:                                    ; preds = %413
  %420 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 8
  %421 = load i32, i32* %420, align 4
  %422 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 2, i32 3
  %423 = load i32, i32* %422, align 4
  %424 = lshr i32 %423, 17
  %425 = and i32 %424, 32
  %426 = trunc i32 %421 to i16
  %427 = icmp slt i16 %426, 0
  br i1 %427, label %457, label %428

; <label>:428:                                    ; preds = %419
  %429 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 16, i64 0
  %430 = load i32, i32* %429, align 8
  %431 = and i32 %430, 1
  %432 = icmp eq i32 %431, 0
  br i1 %432, label %444, label %433

; <label>:433:                                    ; preds = %428
  %434 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 7
  %435 = bitcast i32* %434 to i64*
  %436 = load i64, i64* %435, align 8
  %437 = and i64 %436, 131072
  %438 = icmp ne i64 %437, 0
  %439 = and i32 %421, 16
  %440 = icmp eq i32 %439, 0
  %441 = or i1 %440, %438
  %442 = lshr i64 %436, 32
  %443 = trunc i64 %442 to i32
  br i1 %441, label %444, label %447

; <label>:444:                                    ; preds = %433, %428
  %445 = phi i32 [ %443, %433 ], [ %421, %428 ]
  %446 = or i32 %425, 64
  br label %457

; <label>:447:                                    ; preds = %433
  %448 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 0, i32 1
  %449 = load i32, i32* %448, align 4
  %450 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 2, i32 1
  %451 = load i32, i32* %450, align 4
  %452 = or i32 %451, %449
  %453 = icmp ne i32 %452, 0
  %454 = zext i1 %453 to i32
  %455 = shl nuw nsw i32 %454, 6
  %456 = or i32 %455, %425
  br label %457

; <label>:457:                                    ; preds = %447, %444, %419
  %458 = phi i32 [ %421, %419 ], [ %445, %444 ], [ %443, %447 ]
  %459 = phi i32 [ %425, %419 ], [ %446, %444 ], [ %456, %447 ]
  %460 = and i32 %458, -97
  %461 = or i32 %460, %459
  store i32 %461, i32* %420, align 4
  br label %462

; <label>:462:                                    ; preds = %457, %401
  %463 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 4, i32 0
  %464 = load i32, i32* %463, align 8
  %465 = and i32 %464, 65532
  %466 = icmp eq i32 %465, 0
  br i1 %466, label %530, label %467

; <label>:467:                                    ; preds = %462
  %468 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 4, i32 3
  %469 = load i32, i32* %468, align 4
  %470 = lshr i32 %469, 13
  %471 = and i32 %470, 3
  %472 = and i32 %469, 3072
  %473 = icmp ne i32 %472, 3072
  %474 = icmp ult i32 %471, %66
  %475 = and i1 %473, %474
  br i1 %475, label %476, label %530

; <label>:476:                                    ; preds = %467
  %477 = bitcast i32* %463 to i8*
  br label %478

; <label>:478:                                    ; preds = %481, %476
  %479 = phi i64 [ 0, %476 ], [ %483, %481 ]
  %480 = icmp ult i64 %479, 16
  br i1 %480, label %481, label %484

; <label>:481:                                    ; preds = %478
  %482 = getelementptr i8, i8* %477, i64 %479
  store i8 0, i8* %482
  %483 = add i64 %479, 1
  br label %478

; <label>:484:                                    ; preds = %478
  %485 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 8
  %486 = load i32, i32* %485, align 4
  %487 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 2, i32 3
  %488 = load i32, i32* %487, align 4
  %489 = lshr i32 %488, 17
  %490 = and i32 %489, 32
  %491 = trunc i32 %486 to i16
  %492 = icmp slt i16 %491, 0
  br i1 %492, label %525, label %493

; <label>:493:                                    ; preds = %484
  %494 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 16, i64 0
  %495 = load i32, i32* %494, align 8
  %496 = and i32 %495, 1
  %497 = icmp eq i32 %496, 0
  br i1 %497, label %509, label %498

; <label>:498:                                    ; preds = %493
  %499 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 7
  %500 = bitcast i32* %499 to i64*
  %501 = load i64, i64* %500, align 8
  %502 = and i64 %501, 131072
  %503 = icmp ne i64 %502, 0
  %504 = and i32 %486, 16
  %505 = icmp eq i32 %504, 0
  %506 = or i1 %505, %503
  %507 = lshr i64 %501, 32
  %508 = trunc i64 %507 to i32
  br i1 %506, label %509, label %512

; <label>:509:                                    ; preds = %498, %493
  %510 = phi i32 [ %508, %498 ], [ %486, %493 ]
  %511 = or i32 %490, 64
  br label %525

; <label>:512:                                    ; preds = %498
  %513 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 3, i32 1
  %514 = load i32, i32* %513, align 4
  %515 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 0, i32 1
  %516 = load i32, i32* %515, align 4
  %517 = or i32 %516, %514
  %518 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 2, i32 1
  %519 = load i32, i32* %518, align 4
  %520 = or i32 %517, %519
  %521 = icmp ne i32 %520, 0
  %522 = zext i1 %521 to i32
  %523 = shl nuw nsw i32 %522, 6
  %524 = or i32 %523, %490
  br label %525

; <label>:525:                                    ; preds = %512, %509, %484
  %526 = phi i32 [ %486, %484 ], [ %510, %509 ], [ %508, %512 ]
  %527 = phi i32 [ %490, %484 ], [ %511, %509 ], [ %524, %512 ]
  %528 = and i32 %526, -97
  %529 = or i32 %528, %527
  store i32 %529, i32* %485, align 4
  br label %530

; <label>:530:                                    ; preds = %525, %467, %462
  %531 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 5, i32 0
  %532 = load i32, i32* %531, align 8
  %533 = and i32 %532, 65532
  %534 = icmp eq i32 %533, 0
  br i1 %534, label %598, label %535

; <label>:535:                                    ; preds = %530
  %536 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 5, i32 3
  %537 = load i32, i32* %536, align 4
  %538 = lshr i32 %537, 13
  %539 = and i32 %538, 3
  %540 = and i32 %537, 3072
  %541 = icmp ne i32 %540, 3072
  %542 = icmp ult i32 %539, %66
  %543 = and i1 %541, %542
  br i1 %543, label %544, label %598

; <label>:544:                                    ; preds = %535
  %545 = bitcast i32* %531 to i8*
  br label %546

; <label>:546:                                    ; preds = %549, %544
  %547 = phi i64 [ 0, %544 ], [ %551, %549 ]
  %548 = icmp ult i64 %547, 16
  br i1 %548, label %549, label %552

; <label>:549:                                    ; preds = %546
  %550 = getelementptr i8, i8* %545, i64 %547
  store i8 0, i8* %550
  %551 = add i64 %547, 1
  br label %546

; <label>:552:                                    ; preds = %546
  %553 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 8
  %554 = load i32, i32* %553, align 4
  %555 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 2, i32 3
  %556 = load i32, i32* %555, align 4
  %557 = lshr i32 %556, 17
  %558 = and i32 %557, 32
  %559 = trunc i32 %554 to i16
  %560 = icmp slt i16 %559, 0
  br i1 %560, label %593, label %561

; <label>:561:                                    ; preds = %552
  %562 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 16, i64 0
  %563 = load i32, i32* %562, align 8
  %564 = and i32 %563, 1
  %565 = icmp eq i32 %564, 0
  br i1 %565, label %577, label %566

; <label>:566:                                    ; preds = %561
  %567 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 7
  %568 = bitcast i32* %567 to i64*
  %569 = load i64, i64* %568, align 8
  %570 = and i64 %569, 131072
  %571 = icmp ne i64 %570, 0
  %572 = and i32 %554, 16
  %573 = icmp eq i32 %572, 0
  %574 = or i1 %573, %571
  %575 = lshr i64 %569, 32
  %576 = trunc i64 %575 to i32
  br i1 %574, label %577, label %580

; <label>:577:                                    ; preds = %566, %561
  %578 = phi i32 [ %576, %566 ], [ %554, %561 ]
  %579 = or i32 %558, 64
  br label %593

; <label>:580:                                    ; preds = %566
  %581 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 3, i32 1
  %582 = load i32, i32* %581, align 4
  %583 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 0, i32 1
  %584 = load i32, i32* %583, align 4
  %585 = or i32 %584, %582
  %586 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %333, i64 0, i32 11, i64 2, i32 1
  %587 = load i32, i32* %586, align 4
  %588 = or i32 %585, %587
  %589 = icmp ne i32 %588, 0
  %590 = zext i1 %589 to i32
  %591 = shl nuw nsw i32 %590, 6
  %592 = or i32 %591, %558
  br label %593

; <label>:593:                                    ; preds = %580, %577, %552
  %594 = phi i32 [ %554, %552 ], [ %578, %577 ], [ %576, %580 ]
  %595 = phi i32 [ %558, %552 ], [ %579, %577 ], [ %592, %580 ]
  %596 = and i32 %594, -97
  %597 = or i32 %596, %595
  store i32 %597, i32* %553, align 4
  br label %598

; <label>:598:                                    ; preds = %593, %535, %530
  %599 = add i32 %170, %1
  br label %600

; <label>:600:                                    ; preds = %598, %147
  %601 = phi %struct.CPUX86State* [ %333, %598 ], [ %88, %147 ]
  %602 = phi i32 [ %599, %598 ], [ %85, %147 ]
  %603 = phi i32 [ %340, %598 ], [ %12, %147 ]
  %604 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %601, i64 0, i32 0, i64 4
  %605 = load i32, i32* %604, align 16
  %606 = xor i32 %603, -1
  %607 = and i32 %605, %606
  %608 = and i32 %603, %602
  %609 = or i32 %607, %608
  store i32 %609, i32* %604, align 16
  %610 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %601, i64 0, i32 5
  store i32 %38, i32* %610, align 16
  br i1 %86, label %617, label %611

; <label>:611:                                    ; preds = %600
  %612 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %613 = load i32, i32* %612, align 4
  %614 = icmp eq i32 %613, 0
  br i1 %614, label %617, label %615, !prof !2

; <label>:615:                                    ; preds = %611
  %616 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  tail call void %616(i32 %6, i32 %66)
  br label %617

; <label>:617:                                    ; preds = %600, %611, %615
  ret void
}

; Function Attrs: uwtable
define void @helper_sysenter() local_unnamed_addr #2 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 8
  %3 = load i32, i32* %2, align 4
  %4 = and i32 %3, 3
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 34
  %6 = load i32, i32* %5, align 16
  %7 = icmp eq i32 %6, 0
  br i1 %7, label %8, label %9

; <label>:8:                                      ; preds = %0
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:9:                                      ; preds = %0
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 7
  %11 = bitcast i32* %10 to i64*
  %12 = load i64, i64* %11, align 8
  %13 = trunc i64 %12 to i32
  %14 = and i32 %13, -197121
  store i32 %14, i32* %10, align 8
  %15 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %16 = load i32, i32* %15, align 4
  %17 = icmp eq i32 %16, 0
  %18 = lshr i64 %12, 32
  %19 = trunc i64 %18 to i32
  br i1 %17, label %25, label %20, !prof !2

; <label>:20:                                     ; preds = %9
  %21 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  %22 = and i32 %19, 3
  tail call void %21(i32 %22, i32 0)
  %23 = load i32, i32* %2, align 4
  %24 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %25

; <label>:25:                                     ; preds = %9, %20
  %26 = phi %struct.CPUX86State* [ %24, %20 ], [ %1, %9 ]
  %27 = phi i32 [ %23, %20 ], [ %19, %9 ]
  %28 = and i32 %27, -4
  store i32 %28, i32* %2, align 4
  %29 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 34
  %30 = bitcast i32* %29 to i64*
  %31 = load i64, i64* %30, align 16
  %32 = trunc i64 %31 to i32
  %33 = and i32 %32, 65532
  %34 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 1, i32 0
  store i32 %33, i32* %34, align 4
  %35 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 1, i32 1
  store i32 0, i32* %35, align 4
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 1, i32 2
  store i32 -1, i32* %36, align 4
  %37 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 1, i32 3
  store i32 12622592, i32* %37, align 4
  %38 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 8
  %39 = load i32, i32* %38, align 4
  %40 = and i32 %39, -32785
  %41 = or i32 %40, 16
  store i32 %41, i32* %38, align 4
  %42 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 2, i32 3
  %43 = load i32, i32* %42, align 4
  %44 = lshr i32 %43, 17
  %45 = and i32 %44, 32
  %46 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 16, i64 0
  %47 = load i32, i32* %46, align 8
  %48 = and i32 %47, 1
  %49 = icmp eq i32 %48, 0
  %50 = lshr i64 %31, 32
  %51 = trunc i64 %50 to i32
  br i1 %49, label %60, label %52

; <label>:52:                                     ; preds = %25
  %53 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 7
  %54 = bitcast i32* %53 to i64*
  %55 = load i64, i64* %54, align 8
  %56 = and i64 %55, 131072
  %57 = icmp eq i64 %56, 0
  %58 = lshr i64 %55, 32
  %59 = trunc i64 %58 to i32
  br i1 %57, label %64, label %60

; <label>:60:                                     ; preds = %52, %25
  %61 = phi i32 [ %59, %52 ], [ %41, %25 ]
  %62 = or i32 %45, 64
  %63 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 2, i32 1
  br label %77

; <label>:64:                                     ; preds = %52
  %65 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 3, i32 1
  %66 = load i32, i32* %65, align 4
  %67 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 0, i32 1
  %68 = load i32, i32* %67, align 4
  %69 = or i32 %68, %66
  %70 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 2, i32 1
  %71 = load i32, i32* %70, align 4
  %72 = or i32 %69, %71
  %73 = icmp ne i32 %72, 0
  %74 = zext i1 %73 to i32
  %75 = shl nuw nsw i32 %74, 6
  %76 = or i32 %75, %45
  br label %77

; <label>:77:                                     ; preds = %60, %64
  %78 = phi i32* [ %63, %60 ], [ %70, %64 ]
  %79 = phi i32 [ %61, %60 ], [ %59, %64 ]
  %80 = phi i32 [ %62, %60 ], [ %76, %64 ]
  %81 = and i32 %79, -97
  %82 = or i32 %81, %80
  store i32 %82, i32* %38, align 4
  %83 = add i32 %32, 8
  %84 = and i32 %83, 65532
  %85 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 2, i32 0
  store i32 %84, i32* %85, align 4
  store i32 0, i32* %78, align 4
  %86 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 2, i32 2
  store i32 -1, i32* %86, align 4
  store i32 12620544, i32* %42, align 4
  %87 = trunc i32 %82 to i16
  %88 = icmp slt i16 %87, 0
  %89 = or i1 %88, %49
  %90 = select i1 %88, i32 32, i32 96
  br i1 %89, label %112, label %91

; <label>:91:                                     ; preds = %77
  %92 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 7
  %93 = bitcast i32* %92 to i64*
  %94 = load i64, i64* %93, align 8
  %95 = and i64 %94, 131072
  %96 = icmp ne i64 %95, 0
  %97 = and i32 %82, 16
  %98 = icmp eq i32 %97, 0
  %99 = or i1 %98, %96
  %100 = lshr i64 %94, 32
  %101 = trunc i64 %100 to i32
  br i1 %99, label %112, label %102

; <label>:102:                                    ; preds = %91
  %103 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 3, i32 1
  %104 = load i32, i32* %103, align 4
  %105 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 11, i64 0, i32 1
  %106 = load i32, i32* %105, align 4
  %107 = or i32 %106, %104
  %108 = icmp ne i32 %107, 0
  %109 = zext i1 %108 to i32
  %110 = shl nuw nsw i32 %109, 6
  %111 = or i32 %110, 32
  br label %112

; <label>:112:                                    ; preds = %77, %91, %102
  %113 = phi i32 [ %82, %77 ], [ %101, %102 ], [ %101, %91 ]
  %114 = phi i32 [ %90, %77 ], [ %111, %102 ], [ 96, %91 ]
  %115 = and i32 %113, -97
  %116 = or i32 %115, %114
  store i32 %116, i32* %38, align 4
  %117 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 0, i64 4
  store i32 %51, i32* %117, align 16
  %118 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 36
  %119 = load i32, i32* %118, align 8
  %120 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 5
  store i32 %119, i32* %120, align 16
  %121 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %122 = load i32, i32* %121, align 4
  %123 = icmp eq i32 %122, 0
  br i1 %123, label %126, label %124, !prof !2

; <label>:124:                                    ; preds = %112
  %125 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  tail call void %125(i32 %4, i32 0)
  br label %126

; <label>:126:                                    ; preds = %112, %124
  ret void
}

; Function Attrs: uwtable
define void @helper_sysexit(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 8
  %4 = load i32, i32* %3, align 4
  %5 = and i32 %4, 3
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 34
  %7 = load i32, i32* %6, align 16
  %8 = icmp eq i32 %7, 0
  %9 = icmp ne i32 %5, 0
  %10 = or i1 %8, %9
  br i1 %10, label %11, label %12

; <label>:11:                                     ; preds = %1
  tail call fastcc void @raise_exception_err(i32 13, i32 0) #12
  unreachable

; <label>:12:                                     ; preds = %1
  %13 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %14 = load i32, i32* %13, align 4
  %15 = icmp eq i32 %14, 0
  br i1 %15, label %20, label %16, !prof !2

; <label>:16:                                     ; preds = %12
  %17 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  tail call void %17(i32 0, i32 3)
  %18 = load i32, i32* %3, align 4
  %19 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %20

; <label>:20:                                     ; preds = %16, %12
  %21 = phi %struct.CPUX86State* [ %19, %16 ], [ %2, %12 ]
  %22 = phi i32 [ %18, %16 ], [ %4, %12 ]
  %23 = or i32 %22, 3
  store i32 %23, i32* %3, align 4
  %24 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 34
  %25 = load i32, i32* %24, align 16
  %26 = add i32 %25, 16
  %27 = and i32 %26, 65532
  %28 = or i32 %27, 3
  %29 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 1, i32 0
  store i32 %28, i32* %29, align 4
  %30 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 1, i32 1
  store i32 0, i32* %30, align 4
  %31 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 1, i32 2
  store i32 -1, i32* %31, align 4
  %32 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 1, i32 3
  store i32 12647168, i32* %32, align 4
  %33 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 8
  %34 = load i32, i32* %33, align 4
  %35 = and i32 %34, -32785
  %36 = or i32 %35, 16
  store i32 %36, i32* %33, align 4
  %37 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 2, i32 3
  %38 = load i32, i32* %37, align 4
  %39 = lshr i32 %38, 17
  %40 = and i32 %39, 32
  %41 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 16, i64 0
  %42 = load i32, i32* %41, align 8
  %43 = and i32 %42, 1
  %44 = icmp eq i32 %43, 0
  br i1 %44, label %53, label %45

; <label>:45:                                     ; preds = %20
  %46 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 7
  %47 = bitcast i32* %46 to i64*
  %48 = load i64, i64* %47, align 8
  %49 = and i64 %48, 131072
  %50 = icmp eq i64 %49, 0
  %51 = lshr i64 %48, 32
  %52 = trunc i64 %51 to i32
  br i1 %50, label %57, label %53

; <label>:53:                                     ; preds = %45, %20
  %54 = phi i32 [ %52, %45 ], [ %36, %20 ]
  %55 = or i32 %40, 64
  %56 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 2, i32 1
  br label %70

; <label>:57:                                     ; preds = %45
  %58 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 3, i32 1
  %59 = load i32, i32* %58, align 4
  %60 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 0, i32 1
  %61 = load i32, i32* %60, align 4
  %62 = or i32 %61, %59
  %63 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 2, i32 1
  %64 = load i32, i32* %63, align 4
  %65 = or i32 %62, %64
  %66 = icmp ne i32 %65, 0
  %67 = zext i1 %66 to i32
  %68 = shl nuw nsw i32 %67, 6
  %69 = or i32 %68, %40
  br label %70

; <label>:70:                                     ; preds = %53, %57
  %71 = phi i32* [ %56, %53 ], [ %63, %57 ]
  %72 = phi i32 [ %54, %53 ], [ %52, %57 ]
  %73 = phi i32 [ %55, %53 ], [ %69, %57 ]
  %74 = and i32 %72, -97
  %75 = or i32 %74, %73
  store i32 %75, i32* %33, align 4
  %76 = add i32 %25, 24
  %77 = and i32 %76, 65532
  %78 = or i32 %77, 3
  %79 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 2, i32 0
  store i32 %78, i32* %79, align 4
  store i32 0, i32* %71, align 4
  %80 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 2, i32 2
  store i32 -1, i32* %80, align 4
  store i32 12645120, i32* %37, align 4
  %81 = trunc i32 %75 to i16
  %82 = icmp slt i16 %81, 0
  %83 = or i1 %82, %44
  %84 = select i1 %82, i32 32, i32 96
  br i1 %83, label %106, label %85

; <label>:85:                                     ; preds = %70
  %86 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 7
  %87 = bitcast i32* %86 to i64*
  %88 = load i64, i64* %87, align 8
  %89 = and i64 %88, 131072
  %90 = icmp ne i64 %89, 0
  %91 = and i32 %75, 16
  %92 = icmp eq i32 %91, 0
  %93 = or i1 %92, %90
  %94 = lshr i64 %88, 32
  %95 = trunc i64 %94 to i32
  br i1 %93, label %106, label %96

; <label>:96:                                     ; preds = %85
  %97 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 3, i32 1
  %98 = load i32, i32* %97, align 4
  %99 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 11, i64 0, i32 1
  %100 = load i32, i32* %99, align 4
  %101 = or i32 %100, %98
  %102 = icmp ne i32 %101, 0
  %103 = zext i1 %102 to i32
  %104 = shl nuw nsw i32 %103, 6
  %105 = or i32 %104, 32
  br label %106

; <label>:106:                                    ; preds = %70, %85, %96
  %107 = phi i32 [ %75, %70 ], [ %95, %96 ], [ %95, %85 ]
  %108 = phi i32 [ %84, %70 ], [ %105, %96 ], [ 96, %85 ]
  %109 = and i32 %107, -97
  %110 = or i32 %109, %108
  store i32 %110, i32* %33, align 4
  %111 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 0, i64 1
  %112 = load i32, i32* %111, align 4
  %113 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 0, i64 4
  store i32 %112, i32* %113, align 16
  %114 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 0, i64 2
  %115 = load i32, i32* %114, align 8
  %116 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 5
  store i32 %115, i32* %116, align 16
  %117 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %118 = load i32, i32* %117, align 4
  %119 = icmp eq i32 %118, 0
  br i1 %119, label %122, label %120, !prof !2

; <label>:120:                                    ; preds = %106
  %121 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  tail call void %121(i32 0, i32 3)
  br label %122

; <label>:122:                                    ; preds = %106, %120
  ret void
}

; Function Attrs: uwtable
define i32 @helper_read_crN(i32) local_unnamed_addr #2 {
  tail call void @helper_svm_check_intercept_param(i32 %0, i64 0)
  %2 = icmp eq i32 %0, 8
  br i1 %2, label %8, label %3

; <label>:3:                                      ; preds = %1
  %4 = sext i32 %0 to i64
  %5 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 16, i64 %4
  %7 = load i32, i32* %6, align 4
  br label %23

; <label>:8:                                      ; preds = %1
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 9
  %11 = load i32, i32* %10, align 16
  %12 = and i32 %11, 8
  %13 = icmp eq i32 %12, 0
  br i1 %13, label %14, label %19

; <label>:14:                                     ; preds = %8
  %15 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 129
  %16 = load %struct.DeviceState*, %struct.DeviceState** %15, align 16
  %17 = tail call zeroext i8 @cpu_get_apic_tpr(%struct.DeviceState* %16)
  %18 = zext i8 %17 to i32
  br label %23

; <label>:19:                                     ; preds = %8
  %20 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 48
  %21 = load i8, i8* %20, align 4
  %22 = zext i8 %21 to i32
  br label %23

; <label>:23:                                     ; preds = %14, %19, %3
  %24 = phi i32 [ %22, %19 ], [ %18, %14 ], [ %7, %3 ]
  ret i32 %24
}

declare zeroext i8 @cpu_get_apic_tpr(%struct.DeviceState*) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_write_crN(i32, i32) local_unnamed_addr #2 {
  %3 = add nsw i32 %0, 16
  tail call void @helper_svm_check_intercept_param(i32 %3, i64 0)
  switch i32 %0, label %28 [
    i32 0, label %4
    i32 3, label %6
    i32 4, label %8
    i32 8, label %10
  ]

; <label>:4:                                      ; preds = %2
  %5 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @cpu_x86_update_cr0(%struct.CPUX86State* %5, i32 %1)
  br label %32

; <label>:6:                                      ; preds = %2
  %7 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @cpu_x86_update_cr3(%struct.CPUX86State* %7, i32 %1)
  br label %32

; <label>:8:                                      ; preds = %2
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @cpu_x86_update_cr4(%struct.CPUX86State* %9, i32 %1)
  br label %32

; <label>:10:                                     ; preds = %2
  %11 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %12 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %11, i64 0, i32 9
  %13 = load i32, i32* %12, align 16
  %14 = and i32 %13, 8
  %15 = icmp eq i32 %14, 0
  br i1 %15, label %18, label %16

; <label>:16:                                     ; preds = %10
  %17 = trunc i32 %1 to i8
  br label %23

; <label>:18:                                     ; preds = %10
  %19 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %11, i64 0, i32 129
  %20 = load %struct.DeviceState*, %struct.DeviceState** %19, align 16
  %21 = trunc i32 %1 to i8
  tail call void @cpu_set_apic_tpr(%struct.DeviceState* %20, i8 zeroext %21)
  %22 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %23

; <label>:23:                                     ; preds = %16, %18
  %24 = phi i8 [ %17, %16 ], [ %21, %18 ]
  %25 = phi %struct.CPUX86State* [ %11, %16 ], [ %22, %18 ]
  %26 = and i8 %24, 15
  %27 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %25, i64 0, i32 48
  store i8 %26, i8* %27, align 4
  br label %32

; <label>:28:                                     ; preds = %2
  %29 = sext i32 %0 to i64
  %30 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %31 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %30, i64 0, i32 16, i64 %29
  store i32 %1, i32* %31, align 4
  br label %32

; <label>:32:                                     ; preds = %28, %23, %8, %6, %4
  ret void
}

declare void @cpu_set_apic_tpr(%struct.DeviceState*, i8 zeroext) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_movl_drN_T0(i32, i32) local_unnamed_addr #2 {
  %3 = icmp slt i32 %0, 4
  br i1 %3, label %4, label %9

; <label>:4:                                      ; preds = %2
  %5 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @hw_breakpoint_remove(%struct.CPUX86State* %5, i32 %0)
  %6 = sext i32 %0 to i64
  %7 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %7, i64 0, i32 61, i64 %6
  store i32 %1, i32* %8, align 4
  tail call void @hw_breakpoint_insert(%struct.CPUX86State* %7, i32 %0)
  br label %25

; <label>:9:                                      ; preds = %2
  %10 = icmp eq i32 %0, 7
  br i1 %10, label %11, label %21

; <label>:11:                                     ; preds = %9
  %12 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @hw_breakpoint_remove(%struct.CPUX86State* %12, i32 0)
  %13 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @hw_breakpoint_remove(%struct.CPUX86State* %13, i32 1)
  %14 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @hw_breakpoint_remove(%struct.CPUX86State* %14, i32 2)
  %15 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @hw_breakpoint_remove(%struct.CPUX86State* %15, i32 3)
  %16 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %17 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %16, i64 0, i32 61, i64 7
  store i32 %1, i32* %17, align 4
  tail call void @hw_breakpoint_insert(%struct.CPUX86State* %16, i32 0)
  %18 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @hw_breakpoint_insert(%struct.CPUX86State* %18, i32 1)
  %19 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @hw_breakpoint_insert(%struct.CPUX86State* %19, i32 2)
  %20 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @hw_breakpoint_insert(%struct.CPUX86State* %20, i32 3)
  br label %25

; <label>:21:                                     ; preds = %9
  %22 = sext i32 %0 to i64
  %23 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %24 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %23, i64 0, i32 61, i64 %22
  store i32 %1, i32* %24, align 4
  br label %25

; <label>:25:                                     ; preds = %11, %21, %4
  ret void
}

declare void @hw_breakpoint_insert(%struct.CPUX86State*, i32) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_lmsw(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 16, i64 0
  %4 = load i32, i32* %3, align 8
  %5 = and i32 %4, -15
  %6 = and i32 %0, 15
  %7 = or i32 %5, %6
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 8
  %9 = load i32, i32* %8, align 4
  %10 = and i32 %9, 2097152
  %11 = icmp eq i32 %10, 0
  br i1 %11, label %18, label %12, !prof !2

; <label>:12:                                     ; preds = %1
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 44
  %14 = load i16, i16* %13, align 2
  %15 = and i16 %14, 1
  %16 = icmp eq i16 %15, 0
  br i1 %16, label %18, label %17

; <label>:17:                                     ; preds = %12
  tail call void @helper_vmexit(i32 16, i64 0)
  unreachable

; <label>:18:                                     ; preds = %1, %12
  tail call void @cpu_x86_update_cr0(%struct.CPUX86State* %2, i32 %7)
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_clts() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 16, i64 0
  %3 = load i32, i32* %2, align 8
  %4 = and i32 %3, -9
  store i32 %4, i32* %2, align 8
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 8
  %6 = load i32, i32* %5, align 4
  %7 = and i32 %6, -2049
  store i32 %7, i32* %5, align 4
  ret void
}

; Function Attrs: uwtable
define void @helper_invlpg(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 8
  %4 = load i32, i32* %3, align 4
  %5 = and i32 %4, 2097152
  %6 = icmp eq i32 %5, 0
  br i1 %6, label %13, label %7, !prof !2

; <label>:7:                                      ; preds = %1
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 42
  %9 = load i64, i64* %8, align 8
  %10 = and i64 %9, 33554432
  %11 = icmp eq i64 %10, 0
  br i1 %11, label %13, label %12

; <label>:12:                                     ; preds = %7
  tail call void @helper_vmexit(i32 121, i64 0)
  unreachable

; <label>:13:                                     ; preds = %1, %7
  tail call void @tlb_flush_page(%struct.CPUX86State* %2, i32 %0)
  ret void
}

declare void @tlb_flush_page(%struct.CPUX86State*, i32) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_rdtsc() local_unnamed_addr #2 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 16, i64 4
  %3 = load i32, i32* %2, align 8
  %4 = and i32 %3, 4
  %5 = icmp eq i32 %4, 0
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 8
  %7 = load i32, i32* %6, align 4
  %8 = and i32 %7, 3
  %9 = icmp eq i32 %8, 0
  %10 = or i1 %5, %9
  br i1 %10, label %12, label %11

; <label>:11:                                     ; preds = %0
  tail call fastcc void @raise_exception(i32 13) #12
  unreachable

; <label>:12:                                     ; preds = %0
  %13 = and i32 %7, 2097152
  %14 = icmp eq i32 %13, 0
  br i1 %14, label %21, label %15, !prof !2

; <label>:15:                                     ; preds = %12
  %16 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 42
  %17 = load i64, i64* %16, align 8
  %18 = and i64 %17, 16384
  %19 = icmp eq i64 %18, 0
  br i1 %19, label %21, label %20

; <label>:20:                                     ; preds = %15
  tail call void @helper_vmexit(i32 110, i64 0)
  unreachable

; <label>:21:                                     ; preds = %12, %15
  %22 = tail call i64 @cpu_get_tsc(%struct.CPUX86State* nonnull %1)
  %23 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %24 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %23, i64 0, i32 41
  %25 = load i64, i64* %24, align 16
  %26 = add i64 %25, %22
  %27 = trunc i64 %26 to i32
  %28 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %23, i64 0, i32 0, i64 0
  store i32 %27, i32* %28, align 16
  %29 = lshr i64 %26, 32
  %30 = trunc i64 %29 to i32
  %31 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %32 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 0, i64 2
  store i32 %30, i32* %32, align 8
  ret void
}

declare i64 @cpu_get_tsc(%struct.CPUX86State*) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_rdtscp() local_unnamed_addr #2 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 16, i64 4
  %3 = load i32, i32* %2, align 8
  %4 = and i32 %3, 4
  %5 = icmp eq i32 %4, 0
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 8
  %7 = load i32, i32* %6, align 4
  %8 = and i32 %7, 3
  %9 = icmp eq i32 %8, 0
  %10 = or i1 %5, %9
  br i1 %10, label %12, label %11

; <label>:11:                                     ; preds = %0
  tail call fastcc void @raise_exception(i32 13) #12
  unreachable

; <label>:12:                                     ; preds = %0
  %13 = and i32 %7, 2097152
  %14 = icmp eq i32 %13, 0
  br i1 %14, label %21, label %15, !prof !2

; <label>:15:                                     ; preds = %12
  %16 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 42
  %17 = load i64, i64* %16, align 8
  %18 = and i64 %17, 16384
  %19 = icmp eq i64 %18, 0
  br i1 %19, label %21, label %20

; <label>:20:                                     ; preds = %15
  tail call void @helper_vmexit(i32 110, i64 0)
  unreachable

; <label>:21:                                     ; preds = %12, %15
  %22 = tail call i64 @cpu_get_tsc(%struct.CPUX86State* nonnull %1)
  %23 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %24 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %23, i64 0, i32 41
  %25 = load i64, i64* %24, align 16
  %26 = add i64 %25, %22
  %27 = trunc i64 %26 to i32
  %28 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %23, i64 0, i32 0, i64 0
  store i32 %27, i32* %28, align 16
  %29 = lshr i64 %26, 32
  %30 = trunc i64 %29 to i32
  %31 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %32 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 0, i64 2
  store i32 %30, i32* %32, align 8
  %33 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 133
  %34 = load i64, i64* %33, align 8
  %35 = trunc i64 %34 to i32
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %31, i64 0, i32 0, i64 1
  store i32 %35, i32* %36, align 4
  ret void
}

; Function Attrs: noreturn uwtable
define void @helper_rdpmc() local_unnamed_addr #3 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 16, i64 4
  %3 = load i32, i32* %2, align 8
  %4 = and i32 %3, 256
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %12, label %6

; <label>:6:                                      ; preds = %0
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 8
  %8 = load i32, i32* %7, align 4
  %9 = and i32 %8, 3
  %10 = icmp eq i32 %9, 0
  br i1 %10, label %12, label %11

; <label>:11:                                     ; preds = %6
  tail call fastcc void @raise_exception(i32 13) #12
  unreachable

; <label>:12:                                     ; preds = %6, %0
  tail call void @helper_svm_check_intercept_param(i32 111, i64 0)
  tail call fastcc void @raise_exception_err(i32 6, i32 0) #12
  unreachable
}

; Function Attrs: uwtable
define void @helper_wrmsr_v(i32, i64) local_unnamed_addr #2 {
  switch i32 %0, label %114 [
    i32 372, label %3
    i32 373, label %8
    i32 374, label %12
    i32 27, label %16
    i32 -1073741696, label %20
    i32 -1073741695, label %59
    i32 631, label %62
    i32 -1073676009, label %65
    i32 512, label %68
    i32 514, label %68
    i32 516, label %68
    i32 518, label %68
    i32 520, label %68
    i32 522, label %68
    i32 524, label %68
    i32 526, label %68
    i32 513, label %74
    i32 515, label %74
    i32 517, label %74
    i32 519, label %74
    i32 521, label %74
    i32 523, label %74
    i32 525, label %74
    i32 527, label %74
    i32 592, label %80
    i32 600, label %83
    i32 601, label %83
    i32 616, label %88
    i32 617, label %88
    i32 618, label %88
    i32 619, label %88
    i32 620, label %88
    i32 621, label %88
    i32 622, label %88
    i32 623, label %88
    i32 767, label %93
    i32 378, label %96
    i32 379, label %99
    i32 -1073741565, label %108
    i32 416, label %111
  ]

; <label>:3:                                      ; preds = %2
  %4 = trunc i64 %1 to i32
  %5 = and i32 %4, 65535
  %6 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %6, i64 0, i32 34
  store i32 %5, i32* %7, align 16
  br label %133

; <label>:8:                                      ; preds = %2
  %9 = trunc i64 %1 to i32
  %10 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 35
  store i32 %9, i32* %11, align 4
  br label %133

; <label>:12:                                     ; preds = %2
  %13 = trunc i64 %1 to i32
  %14 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %15 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %14, i64 0, i32 36
  store i32 %13, i32* %15, align 8
  br label %133

; <label>:16:                                     ; preds = %2
  %17 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %18 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %17, i64 0, i32 129
  %19 = load %struct.DeviceState*, %struct.DeviceState** %18, align 16
  tail call void @cpu_set_apic_base(%struct.DeviceState* %19, i64 %1)
  br label %133

; <label>:20:                                     ; preds = %2
  %21 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %22 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 115
  %23 = bitcast i32* %22 to i64*
  %24 = load i64, i64* %23, align 16
  %25 = trunc i64 %24 to i32
  %26 = lshr i32 %25, 11
  %27 = and i32 %26, 1
  %28 = lshr i32 %25, 21
  %29 = and i32 %28, 256
  %30 = or i32 %29, %27
  %31 = lshr i32 %25, 11
  %32 = and i32 %31, 16384
  %33 = or i32 %30, %32
  %34 = lshr i32 %25, 9
  %35 = and i32 %34, 2048
  %36 = or i32 %33, %35
  %37 = zext i32 %36 to i64
  %38 = lshr i64 %24, 22
  %39 = and i64 %38, 4096
  %40 = zext i32 %32 to i64
  %41 = or i64 %39, %40
  %42 = or i64 %41, %37
  %43 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 37
  %44 = load i64, i64* %43, align 16
  %45 = xor i64 %42, -1
  %46 = and i64 %44, %45
  %47 = and i64 %42, %1
  %48 = or i64 %46, %47
  store i64 %48, i64* %43, align 16
  %49 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %21, i64 0, i32 8
  %50 = load i32, i32* %49, align 4
  %51 = and i32 %50, -1064961
  %52 = trunc i64 %48 to i32
  %53 = shl i32 %52, 4
  %54 = and i32 %53, 16384
  %55 = or i32 %54, %51
  %56 = shl i32 %52, 8
  %57 = and i32 %56, 1048576
  %58 = or i32 %55, %57
  store i32 %58, i32* %49, align 4
  br label %133

; <label>:59:                                     ; preds = %2
  %60 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %61 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %60, i64 0, i32 38
  store i64 %1, i64* %61, align 8
  br label %133

; <label>:62:                                     ; preds = %2
  %63 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %64 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %63, i64 0, i32 105
  store i64 %1, i64* %64, align 8
  br label %133

; <label>:65:                                     ; preds = %2
  %66 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %67 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %66, i64 0, i32 39
  store i64 %1, i64* %67, align 16
  br label %133

; <label>:68:                                     ; preds = %2, %2, %2, %2, %2, %2, %2, %2
  %69 = add i32 %0, -512
  %70 = lshr i32 %69, 1
  %71 = zext i32 %70 to i64
  %72 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %73 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %72, i64 0, i32 123, i64 %71, i32 0
  store i64 %1, i64* %73, align 8
  br label %133

; <label>:74:                                     ; preds = %2, %2, %2, %2, %2, %2, %2, %2
  %75 = add i32 %0, -513
  %76 = lshr i32 %75, 1
  %77 = zext i32 %76 to i64
  %78 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %79 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %78, i64 0, i32 123, i64 %77, i32 1
  store i64 %1, i64* %79, align 8
  br label %133

; <label>:80:                                     ; preds = %2
  %81 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %82 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %81, i64 0, i32 121, i64 0
  store i64 %1, i64* %82, align 8
  br label %133

; <label>:83:                                     ; preds = %2, %2
  %84 = add i32 %0, -599
  %85 = zext i32 %84 to i64
  %86 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %87 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %86, i64 0, i32 121, i64 %85
  store i64 %1, i64* %87, align 8
  br label %133

; <label>:88:                                     ; preds = %2, %2, %2, %2, %2, %2, %2, %2
  %89 = add i32 %0, -613
  %90 = zext i32 %89 to i64
  %91 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %92 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %91, i64 0, i32 121, i64 %90
  store i64 %1, i64* %92, align 8
  br label %133

; <label>:93:                                     ; preds = %2
  %94 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %95 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %94, i64 0, i32 122
  store i64 %1, i64* %95, align 16
  br label %133

; <label>:96:                                     ; preds = %2
  %97 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %98 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %97, i64 0, i32 56
  store i64 %1, i64* %98, align 16
  br label %133

; <label>:99:                                     ; preds = %2
  %100 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %101 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %100, i64 0, i32 130
  %102 = load i64, i64* %101, align 8
  %103 = and i64 %102, 256
  %104 = icmp eq i64 %103, 0
  br i1 %104, label %133, label %105

; <label>:105:                                    ; preds = %99
  switch i64 %1, label %133 [
    i64 -1, label %106
    i64 0, label %106
  ]

; <label>:106:                                    ; preds = %105, %105
  %107 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %100, i64 0, i32 131
  store i64 %1, i64* %107, align 16
  br label %133

; <label>:108:                                    ; preds = %2
  %109 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %110 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %109, i64 0, i32 133
  store i64 %1, i64* %110, align 8
  br label %133

; <label>:111:                                    ; preds = %2
  %112 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %113 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %112, i64 0, i32 57
  store i64 %1, i64* %113, align 8
  br label %133

; <label>:114:                                    ; preds = %2
  %115 = icmp ugt i32 %0, 1023
  br i1 %115, label %116, label %133

; <label>:116:                                    ; preds = %114
  %117 = zext i32 %0 to i64
  %118 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %119 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %118, i64 0, i32 130
  %120 = load i64, i64* %119, align 8
  %121 = shl i64 %120, 2
  %122 = and i64 %121, 252
  %123 = or i64 %122, 1024
  %124 = icmp ult i64 %117, %123
  br i1 %124, label %125, label %133

; <label>:125:                                    ; preds = %116
  %126 = add i32 %0, -1024
  %127 = and i32 %126, 3
  %128 = icmp eq i32 %127, 0
  br i1 %128, label %129, label %130

; <label>:129:                                    ; preds = %125
  switch i64 %1, label %133 [
    i64 -1, label %130
    i64 0, label %130
  ]

; <label>:130:                                    ; preds = %125, %129, %129
  %131 = zext i32 %126 to i64
  %132 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %118, i64 0, i32 132, i64 %131
  store i64 %1, i64* %132, align 8
  br label %133

; <label>:133:                                    ; preds = %130, %129, %105, %99, %114, %116, %106, %111, %108, %96, %93, %88, %83, %80, %74, %68, %65, %62, %59, %20, %16, %12, %8, %3
  ret void
}

declare void @cpu_set_apic_base(%struct.DeviceState*, i64) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_wrmsr() local_unnamed_addr #2 {
  tail call void @helper_svm_check_intercept_param(i32 124, i64 1)
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = bitcast %struct.CPUX86State* %1 to i64*
  %3 = load i64, i64* %2, align 16
  %4 = and i64 %3, 4294967295
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 0, i64 2
  %6 = load i32, i32* %5, align 8
  %7 = zext i32 %6 to i64
  %8 = shl nuw i64 %7, 32
  %9 = or i64 %8, %4
  %10 = lshr i64 %3, 32
  %11 = trunc i64 %10 to i32
  tail call void @helper_wrmsr_v(i32 %11, i64 %9)
  ret void
}

; Function Attrs: uwtable
define i64 @helper_rdmsr_v(i64) local_unnamed_addr #2 {
  %2 = trunc i64 %0 to i32
  switch i32 %2, label %101 [
    i32 372, label %3
    i32 373, label %8
    i32 374, label %13
    i32 27, label %18
    i32 -1073741696, label %23
    i32 -1073741695, label %27
    i32 631, label %31
    i32 -1073676009, label %35
    i32 408, label %117
    i32 512, label %39
    i32 514, label %39
    i32 516, label %39
    i32 518, label %39
    i32 520, label %39
    i32 522, label %39
    i32 524, label %39
    i32 526, label %39
    i32 513, label %46
    i32 515, label %46
    i32 517, label %46
    i32 519, label %46
    i32 521, label %46
    i32 523, label %46
    i32 525, label %46
    i32 527, label %46
    i32 592, label %53
    i32 600, label %57
    i32 601, label %57
    i32 616, label %63
    i32 617, label %63
    i32 618, label %63
    i32 619, label %63
    i32 620, label %63
    i32 621, label %63
    i32 622, label %63
    i32 623, label %63
    i32 767, label %69
    i32 254, label %73
    i32 377, label %80
    i32 379, label %84
    i32 378, label %93
    i32 416, label %97
  ]

; <label>:3:                                      ; preds = %1
  %4 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 34
  %6 = load i32, i32* %5, align 16
  %7 = zext i32 %6 to i64
  br label %117

; <label>:8:                                      ; preds = %1
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 35
  %11 = load i32, i32* %10, align 4
  %12 = zext i32 %11 to i64
  br label %117

; <label>:13:                                     ; preds = %1
  %14 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %15 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %14, i64 0, i32 36
  %16 = load i32, i32* %15, align 8
  %17 = zext i32 %16 to i64
  br label %117

; <label>:18:                                     ; preds = %1
  %19 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %20 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %19, i64 0, i32 129
  %21 = load %struct.DeviceState*, %struct.DeviceState** %20, align 16
  %22 = tail call i64 @cpu_get_apic_base(%struct.DeviceState* %21)
  br label %117

; <label>:23:                                     ; preds = %1
  %24 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %25 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %24, i64 0, i32 37
  %26 = load i64, i64* %25, align 16
  br label %117

; <label>:27:                                     ; preds = %1
  %28 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %29 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %28, i64 0, i32 38
  %30 = load i64, i64* %29, align 8
  br label %117

; <label>:31:                                     ; preds = %1
  %32 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %33 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %32, i64 0, i32 105
  %34 = load i64, i64* %33, align 8
  br label %117

; <label>:35:                                     ; preds = %1
  %36 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %37 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %36, i64 0, i32 39
  %38 = load i64, i64* %37, align 16
  br label %117

; <label>:39:                                     ; preds = %1, %1, %1, %1, %1, %1, %1, %1
  %40 = add i64 %0, 4294966784
  %41 = lshr i64 %40, 1
  %42 = and i64 %41, 2147483647
  %43 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %44 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %43, i64 0, i32 123, i64 %42, i32 0
  %45 = load i64, i64* %44, align 8
  br label %117

; <label>:46:                                     ; preds = %1, %1, %1, %1, %1, %1, %1, %1
  %47 = add i64 %0, 4294966783
  %48 = lshr i64 %47, 1
  %49 = and i64 %48, 2147483647
  %50 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %51 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %50, i64 0, i32 123, i64 %49, i32 1
  %52 = load i64, i64* %51, align 8
  br label %117

; <label>:53:                                     ; preds = %1
  %54 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %55 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %54, i64 0, i32 121, i64 0
  %56 = load i64, i64* %55, align 8
  br label %117

; <label>:57:                                     ; preds = %1, %1
  %58 = add i64 %0, 4294966697
  %59 = and i64 %58, 4294967295
  %60 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %61 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %60, i64 0, i32 121, i64 %59
  %62 = load i64, i64* %61, align 8
  br label %117

; <label>:63:                                     ; preds = %1, %1, %1, %1, %1, %1, %1, %1
  %64 = add i64 %0, 4294966683
  %65 = and i64 %64, 4294967295
  %66 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %67 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %66, i64 0, i32 121, i64 %65
  %68 = load i64, i64* %67, align 8
  br label %117

; <label>:69:                                     ; preds = %1
  %70 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %71 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %70, i64 0, i32 122
  %72 = load i64, i64* %71, align 16
  br label %117

; <label>:73:                                     ; preds = %1
  %74 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %75 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %74, i64 0, i32 111
  %76 = load i32, i32* %75, align 4
  %77 = and i32 %76, 4096
  %78 = icmp eq i32 %77, 0
  %79 = select i1 %78, i64 0, i64 1288
  br label %117

; <label>:80:                                     ; preds = %1
  %81 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %82 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %81, i64 0, i32 130
  %83 = load i64, i64* %82, align 8
  br label %117

; <label>:84:                                     ; preds = %1
  %85 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %86 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %85, i64 0, i32 130
  %87 = load i64, i64* %86, align 8
  %88 = and i64 %87, 256
  %89 = icmp eq i64 %88, 0
  br i1 %89, label %117, label %90

; <label>:90:                                     ; preds = %84
  %91 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %85, i64 0, i32 131
  %92 = load i64, i64* %91, align 16
  br label %117

; <label>:93:                                     ; preds = %1
  %94 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %95 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %94, i64 0, i32 56
  %96 = load i64, i64* %95, align 16
  br label %117

; <label>:97:                                     ; preds = %1
  %98 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %99 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %98, i64 0, i32 57
  %100 = load i64, i64* %99, align 8
  br label %117

; <label>:101:                                    ; preds = %1
  %102 = icmp ugt i32 %2, 1023
  br i1 %102, label %103, label %117

; <label>:103:                                    ; preds = %101
  %104 = and i64 %0, 4294967295
  %105 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %106 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %105, i64 0, i32 130
  %107 = load i64, i64* %106, align 8
  %108 = shl i64 %107, 2
  %109 = and i64 %108, 252
  %110 = or i64 %109, 1024
  %111 = icmp ult i64 %104, %110
  br i1 %111, label %112, label %117

; <label>:112:                                    ; preds = %103
  %113 = add i64 %0, 4294966272
  %114 = and i64 %113, 4294967295
  %115 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %105, i64 0, i32 132, i64 %114
  %116 = load i64, i64* %115, align 8
  br label %117

; <label>:117:                                    ; preds = %101, %103, %84, %73, %1, %90, %112, %97, %93, %80, %69, %63, %57, %53, %46, %39, %35, %31, %27, %23, %18, %13, %8, %3
  %118 = phi i64 [ %116, %112 ], [ %100, %97 ], [ %96, %93 ], [ %92, %90 ], [ %83, %80 ], [ %72, %69 ], [ %68, %63 ], [ %62, %57 ], [ %56, %53 ], [ %52, %46 ], [ %45, %39 ], [ %38, %35 ], [ %34, %31 ], [ %30, %27 ], [ %26, %23 ], [ %22, %18 ], [ %17, %13
  ret i64 %118
}

declare i64 @cpu_get_apic_base(%struct.DeviceState*) local_unnamed_addr #6

; Function Attrs: uwtable
define void @helper_rdmsr() local_unnamed_addr #2 {
  tail call void @helper_svm_check_intercept_param(i32 124, i64 0)
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 0, i64 1
  %3 = load i32, i32* %2, align 4
  %4 = zext i32 %3 to i64
  %5 = tail call i64 @helper_rdmsr_v(i64 %4)
  %6 = trunc i64 %5 to i32
  %7 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %7, i64 0, i32 0, i64 0
  store i32 %6, i32* %8, align 16
  %9 = lshr i64 %5, 32
  %10 = trunc i64 %9 to i32
  %11 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %12 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %11, i64 0, i32 0, i64 2
  store i32 %10, i32* %12, align 8
  ret void
}

; Function Attrs: uwtable
define i32 @helper_lsl(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 1
  %4 = load i32, i32* %3, align 16
  %5 = tail call i32 @helper_cc_compute_all(i32 %4)
  %6 = and i32 %0, 65532
  %7 = icmp eq i32 %6, 0
  br i1 %7, label %49, label %8

; <label>:8:                                      ; preds = %1
  %9 = and i32 %0, 65528
  %10 = and i32 %0, 4
  %11 = icmp eq i32 %10, 0
  %12 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 12
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 14
  %14 = select i1 %11, %struct.SegmentCache* %13, %struct.SegmentCache* %12
  %15 = or i32 %9, 7
  %16 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %14, i64 0, i32 2
  %17 = load i32, i32* %16, align 4
  %18 = icmp ugt i32 %15, %17
  br i1 %18, label %49, label %19

; <label>:19:                                     ; preds = %8
  %20 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %14, i64 0, i32 1
  %21 = load i32, i32* %20, align 4
  %22 = add i32 %21, %9
  %23 = tail call fastcc i32 @ldl_kernel(i32 %22)
  %24 = add i32 %22, 4
  %25 = tail call fastcc i32 @ldl_kernel(i32 %24)
  %26 = and i32 %0, 3
  %27 = lshr i32 %25, 13
  %28 = and i32 %27, 3
  %29 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %30 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %29, i64 0, i32 8
  %31 = load i32, i32* %30, align 4
  %32 = and i32 %31, 3
  %33 = and i32 %25, 4096
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %42, label %35

; <label>:35:                                     ; preds = %19
  %36 = and i32 %25, 3072
  %37 = icmp eq i32 %36, 3072
  br i1 %37, label %53, label %38

; <label>:38:                                     ; preds = %35
  %39 = icmp ult i32 %28, %32
  %40 = icmp ult i32 %28, %26
  %41 = or i1 %40, %39
  br i1 %41, label %49, label %53

; <label>:42:                                     ; preds = %19
  %43 = lshr i32 %25, 8
  %44 = trunc i32 %43 to i4
  switch i4 %44, label %49 [
    i4 1, label %45
    i4 2, label %45
    i4 3, label %45
    i4 -7, label %45
    i4 -5, label %45
  ]

; <label>:45:                                     ; preds = %42, %42, %42, %42, %42
  %46 = icmp ult i32 %28, %32
  %47 = icmp ult i32 %28, %26
  %48 = or i1 %47, %46
  br i1 %48, label %49, label %53

; <label>:49:                                     ; preds = %8, %45, %42, %38, %1
  %50 = phi %struct.CPUX86State* [ %2, %8 ], [ %29, %45 ], [ %29, %42 ], [ %29, %38 ], [ %2, %1 ]
  %51 = and i32 %5, -65
  %52 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %50, i64 0, i32 2
  store i32 %51, i32* %52, align 4
  br label %64

; <label>:53:                                     ; preds = %35, %45, %38
  %54 = and i32 %23, 65535
  %55 = and i32 %25, 983040
  %56 = or i32 %55, %54
  %57 = and i32 %25, 8388608
  %58 = icmp eq i32 %57, 0
  %59 = shl nuw i32 %56, 12
  %60 = or i32 %59, 4095
  %61 = select i1 %58, i32 %56, i32 %60
  %62 = or i32 %5, 64
  %63 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %29, i64 0, i32 2
  store i32 %62, i32* %63, align 4
  br label %64

; <label>:64:                                     ; preds = %53, %49
  %65 = phi i32 [ 0, %49 ], [ %61, %53 ]
  ret i32 %65
}

; Function Attrs: uwtable
define i32 @helper_lar(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 1
  %4 = load i32, i32* %3, align 16
  %5 = tail call i32 @helper_cc_compute_all(i32 %4)
  %6 = and i32 %0, 65532
  %7 = icmp eq i32 %6, 0
  br i1 %7, label %49, label %8

; <label>:8:                                      ; preds = %1
  %9 = and i32 %0, 65528
  %10 = and i32 %0, 4
  %11 = icmp eq i32 %10, 0
  %12 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 12
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 14
  %14 = select i1 %11, %struct.SegmentCache* %13, %struct.SegmentCache* %12
  %15 = or i32 %9, 7
  %16 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %14, i64 0, i32 2
  %17 = load i32, i32* %16, align 4
  %18 = icmp ugt i32 %15, %17
  br i1 %18, label %49, label %19

; <label>:19:                                     ; preds = %8
  %20 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %14, i64 0, i32 1
  %21 = load i32, i32* %20, align 4
  %22 = add i32 %21, %9
  %23 = tail call fastcc i32 @ldl_kernel(i32 %22)
  %24 = add i32 %22, 4
  %25 = tail call fastcc i32 @ldl_kernel(i32 %24)
  %26 = and i32 %0, 3
  %27 = lshr i32 %25, 13
  %28 = and i32 %27, 3
  %29 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %30 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %29, i64 0, i32 8
  %31 = load i32, i32* %30, align 4
  %32 = and i32 %31, 3
  %33 = and i32 %25, 4096
  %34 = icmp eq i32 %33, 0
  br i1 %34, label %42, label %35

; <label>:35:                                     ; preds = %19
  %36 = and i32 %25, 3072
  %37 = icmp eq i32 %36, 3072
  br i1 %37, label %53, label %38

; <label>:38:                                     ; preds = %35
  %39 = icmp ult i32 %28, %32
  %40 = icmp ult i32 %28, %26
  %41 = or i1 %40, %39
  br i1 %41, label %49, label %53

; <label>:42:                                     ; preds = %19
  %43 = lshr i32 %25, 8
  %44 = trunc i32 %43 to i4
  switch i4 %44, label %49 [
    i4 1, label %45
    i4 2, label %45
    i4 3, label %45
    i4 4, label %45
    i4 5, label %45
    i4 -7, label %45
    i4 -5, label %45
    i4 -4, label %45
  ]

; <label>:45:                                     ; preds = %42, %42, %42, %42, %42, %42, %42, %42
  %46 = icmp ult i32 %28, %32
  %47 = icmp ult i32 %28, %26
  %48 = or i1 %47, %46
  br i1 %48, label %49, label %53

; <label>:49:                                     ; preds = %8, %45, %42, %38, %1
  %50 = phi %struct.CPUX86State* [ %2, %8 ], [ %29, %45 ], [ %29, %42 ], [ %29, %38 ], [ %2, %1 ]
  %51 = and i32 %5, -65
  %52 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %50, i64 0, i32 2
  store i32 %51, i32* %52, align 4
  br label %57

; <label>:53:                                     ; preds = %35, %45, %38
  %54 = or i32 %5, 64
  %55 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %29, i64 0, i32 2
  store i32 %54, i32* %55, align 4
  %56 = and i32 %25, 15793920
  br label %57

; <label>:57:                                     ; preds = %53, %49
  %58 = phi i32 [ 0, %49 ], [ %56, %53 ]
  ret i32 %58
}

; Function Attrs: uwtable
define void @helper_verr(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 1
  %4 = load i32, i32* %3, align 16
  %5 = tail call i32 @helper_cc_compute_all(i32 %4)
  %6 = and i32 %0, 65532
  %7 = icmp eq i32 %6, 0
  br i1 %7, label %54, label %8

; <label>:8:                                      ; preds = %1
  %9 = and i32 %0, 65528
  %10 = and i32 %0, 4
  %11 = icmp eq i32 %10, 0
  %12 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 12
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 14
  %14 = select i1 %11, %struct.SegmentCache* %13, %struct.SegmentCache* %12
  %15 = or i32 %9, 7
  %16 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %14, i64 0, i32 2
  %17 = load i32, i32* %16, align 4
  %18 = icmp ugt i32 %15, %17
  br i1 %18, label %54, label %19

; <label>:19:                                     ; preds = %8
  %20 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %14, i64 0, i32 1
  %21 = load i32, i32* %20, align 4
  %22 = add i32 %21, %9
  %23 = tail call fastcc i32 @ldl_kernel(i32 %22)
  %24 = add i32 %22, 4
  %25 = tail call fastcc i32 @ldl_kernel(i32 %24)
  %26 = and i32 %25, 4096
  %27 = icmp eq i32 %26, 0
  br i1 %27, label %28, label %30

; <label>:28:                                     ; preds = %19
  %29 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %54

; <label>:30:                                     ; preds = %19
  %31 = and i32 %0, 3
  %32 = lshr i32 %25, 13
  %33 = and i32 %32, 3
  %34 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %35 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %34, i64 0, i32 8
  %36 = load i32, i32* %35, align 4
  %37 = and i32 %36, 3
  %38 = and i32 %25, 2048
  %39 = icmp eq i32 %38, 0
  br i1 %39, label %50, label %40

; <label>:40:                                     ; preds = %30
  %41 = and i32 %25, 512
  %42 = icmp eq i32 %41, 0
  br i1 %42, label %54, label %43

; <label>:43:                                     ; preds = %40
  %44 = and i32 %25, 1024
  %45 = icmp eq i32 %44, 0
  br i1 %45, label %46, label %58

; <label>:46:                                     ; preds = %43
  %47 = icmp ult i32 %33, %37
  %48 = icmp ult i32 %33, %31
  %49 = or i1 %48, %47
  br i1 %49, label %54, label %58

; <label>:50:                                     ; preds = %30
  %51 = icmp ult i32 %33, %37
  %52 = icmp ult i32 %33, %31
  %53 = or i1 %52, %51
  br i1 %53, label %54, label %58

; <label>:54:                                     ; preds = %28, %8, %40, %50, %46, %1
  %55 = phi %struct.CPUX86State* [ %29, %28 ], [ %2, %8 ], [ %34, %40 ], [ %34, %50 ], [ %34, %46 ], [ %2, %1 ]
  %56 = and i32 %5, -65
  %57 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %55, i64 0, i32 2
  store i32 %56, i32* %57, align 4
  br label %61

; <label>:58:                                     ; preds = %50, %46, %43
  %59 = or i32 %5, 64
  %60 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %34, i64 0, i32 2
  store i32 %59, i32* %60, align 4
  br label %61

; <label>:61:                                     ; preds = %58, %54
  ret void
}

; Function Attrs: uwtable
define void @helper_verw(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 1
  %4 = load i32, i32* %3, align 16
  %5 = tail call i32 @helper_cc_compute_all(i32 %4)
  %6 = and i32 %0, 65532
  %7 = icmp eq i32 %6, 0
  br i1 %7, label %47, label %8

; <label>:8:                                      ; preds = %1
  %9 = and i32 %0, 65528
  %10 = and i32 %0, 4
  %11 = icmp eq i32 %10, 0
  %12 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 12
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 14
  %14 = select i1 %11, %struct.SegmentCache* %13, %struct.SegmentCache* %12
  %15 = or i32 %9, 7
  %16 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %14, i64 0, i32 2
  %17 = load i32, i32* %16, align 4
  %18 = icmp ugt i32 %15, %17
  br i1 %18, label %47, label %19

; <label>:19:                                     ; preds = %8
  %20 = getelementptr inbounds %struct.SegmentCache, %struct.SegmentCache* %14, i64 0, i32 1
  %21 = load i32, i32* %20, align 4
  %22 = add i32 %21, %9
  %23 = tail call fastcc i32 @ldl_kernel(i32 %22)
  %24 = add i32 %22, 4
  %25 = tail call fastcc i32 @ldl_kernel(i32 %24)
  %26 = and i32 %25, 4096
  %27 = icmp eq i32 %26, 0
  br i1 %27, label %28, label %30

; <label>:28:                                     ; preds = %19
  %29 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %47

; <label>:30:                                     ; preds = %19
  %31 = lshr i32 %25, 13
  %32 = and i32 %31, 3
  %33 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %34 = and i32 %25, 2048
  %35 = icmp eq i32 %34, 0
  br i1 %35, label %36, label %47

; <label>:36:                                     ; preds = %30
  %37 = and i32 %0, 3
  %38 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %33, i64 0, i32 8
  %39 = load i32, i32* %38, align 4
  %40 = and i32 %39, 3
  %41 = icmp ult i32 %32, %40
  %42 = icmp ult i32 %32, %37
  %43 = or i1 %42, %41
  %44 = and i32 %25, 512
  %45 = icmp eq i32 %44, 0
  %46 = or i1 %45, %43
  br i1 %46, label %47, label %51

; <label>:47:                                     ; preds = %28, %8, %30, %36, %1
  %48 = phi %struct.CPUX86State* [ %29, %28 ], [ %2, %8 ], [ %33, %30 ], [ %33, %36 ], [ %2, %1 ]
  %49 = and i32 %5, -65
  %50 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %48, i64 0, i32 2
  store i32 %49, i32* %50, align 4
  br label %54

; <label>:51:                                     ; preds = %36
  %52 = or i32 %5, 64
  %53 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %33, i64 0, i32 2
  store i32 %52, i32* %53, align 4
  br label %54

; <label>:54:                                     ; preds = %51, %47
  ret void
}

; Function Attrs: noreturn uwtable
define void @helper_hlt(i32) local_unnamed_addr #3 {
  tail call void @helper_svm_check_intercept_param(i32 120, i64 0)
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 5
  %4 = load i32, i32* %3, align 16
  %5 = add i32 %4, %0
  store i32 %5, i32* %3, align 16
  tail call fastcc void @do_hlt()
  unreachable
}

; Function Attrs: noreturn uwtable
define internal fastcc void @do_hlt() unnamed_addr #3 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 8
  %3 = load i32, i32* %2, align 4
  %4 = and i32 %3, -9
  store i32 %4, i32* %2, align 4
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 74
  store i32 1, i32* %5, align 4
  %6 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 92
  store i32 65537, i32* %6, align 8
  tail call void @cpu_loop_exit(%struct.CPUX86State* %1) #12
  unreachable
}

; Function Attrs: uwtable
define void @helper_monitor(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 1
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %7, label %6

; <label>:6:                                      ; preds = %1
  tail call fastcc void @raise_exception(i32 13) #12
  unreachable

; <label>:7:                                      ; preds = %1
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 8
  %9 = load i32, i32* %8, align 4
  %10 = and i32 %9, 2097152
  %11 = icmp eq i32 %10, 0
  br i1 %11, label %18, label %12, !prof !2

; <label>:12:                                     ; preds = %7
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 42
  %14 = load i64, i64* %13, align 8
  %15 = and i64 %14, 4398046511104
  %16 = icmp eq i64 %15, 0
  br i1 %16, label %18, label %17

; <label>:17:                                     ; preds = %12
  tail call void @helper_vmexit(i32 138, i64 0)
  unreachable

; <label>:18:                                     ; preds = %7, %12
  ret void
}

; Function Attrs: uwtable
define void @helper_mwait(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 1
  %4 = load i32, i32* %3, align 4
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %7, label %6

; <label>:6:                                      ; preds = %1
  tail call fastcc void @raise_exception(i32 13) #12
  unreachable

; <label>:7:                                      ; preds = %1
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 8
  %9 = load i32, i32* %8, align 4
  %10 = and i32 %9, 2097152
  %11 = icmp eq i32 %10, 0
  br i1 %11, label %18, label %12, !prof !2

; <label>:12:                                     ; preds = %7
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 42
  %14 = load i64, i64* %13, align 8
  %15 = and i64 %14, 8796093022208
  %16 = icmp eq i64 %15, 0
  br i1 %16, label %18, label %17

; <label>:17:                                     ; preds = %12
  tail call void @helper_vmexit(i32 139, i64 0)
  unreachable

; <label>:18:                                     ; preds = %7, %12
  %19 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 5
  %20 = load i32, i32* %19, align 16
  %21 = add i32 %20, %0
  store i32 %21, i32* %19, align 16
  %22 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 94
  %23 = load i32, i32* %22, align 8
  %24 = icmp eq i32 %23, 0
  br i1 %24, label %25, label %30

; <label>:25:                                     ; preds = %18
  %26 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 93
  %27 = load %struct.CPUX86State*, %struct.CPUX86State** %26, align 16
  %28 = icmp eq %struct.CPUX86State* %27, null
  br i1 %28, label %29, label %30

; <label>:29:                                     ; preds = %25
  tail call fastcc void @do_hlt()
  unreachable

; <label>:30:                                     ; preds = %25, %18
  ret void
}

; Function Attrs: noreturn uwtable
define void @helper_debug() local_unnamed_addr #3 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 92
  store i32 65538, i32* %2, align 8
  tail call void @cpu_loop_exit(%struct.CPUX86State* %1) #12
  unreachable
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_reset_rf() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 7
  %3 = load i32, i32* %2, align 8
  %4 = and i32 %3, -65537
  store i32 %4, i32* %2, align 8
  ret void
}

; Function Attrs: noreturn uwtable
define void @helper_raise_interrupt(i32, i32) local_unnamed_addr #3 {
  tail call fastcc void @raise_interrupt(i32 %0, i32 1, i32 0, i32 %1) #12
  unreachable
}

; Function Attrs: noreturn uwtable
define void @helper_raise_exception(i32) local_unnamed_addr #3 {
  tail call fastcc void @raise_exception(i32 %0) #12
  unreachable
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_cli() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 7
  %3 = load i32, i32* %2, align 8
  %4 = and i32 %3, -513
  store i32 %4, i32* %2, align 8
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_sti() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 7
  %3 = load i32, i32* %2, align 8
  %4 = or i32 %3, 512
  store i32 %4, i32* %2, align 8
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_set_inhibit_irq() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 8
  %3 = load i32, i32* %2, align 4
  %4 = or i32 %3, 8
  store i32 %4, i32* %2, align 4
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_reset_inhibit_irq() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 8
  %3 = load i32, i32* %2, align 4
  %4 = and i32 %3, -9
  store i32 %4, i32* %2, align 4
  ret void
}

; Function Attrs: uwtable
define void @helper_boundw(i32, i32) local_unnamed_addr #2 {
  %3 = tail call fastcc i32 @ldsw_data(i32 %0)
  %4 = add i32 %0, 2
  %5 = tail call fastcc i32 @ldsw_data(i32 %4)
  %6 = shl i32 %1, 16
  %7 = ashr exact i32 %6, 16
  %8 = icmp slt i32 %7, %3
  %9 = icmp sgt i32 %7, %5
  %10 = or i1 %8, %9
  br i1 %10, label %11, label %12

; <label>:11:                                     ; preds = %2
  tail call fastcc void @raise_exception(i32 5) #12
  unreachable

; <label>:12:                                     ; preds = %2
  ret void
}

; Function Attrs: uwtable
define internal fastcc i32 @ldsw_data(i32) unnamed_addr #2 {
  %2 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 0), align 8
  %3 = load i32, i32* %2, align 4
  %4 = icmp eq i32 %3, 0
  br i1 %4, label %6, label %5

; <label>:5:                                      ; preds = %1
  tail call void @tcg_llvm_before_memory_access(i32 %0, i64 0, i32 4, i32 0)
  br label %6

; <label>:6:                                      ; preds = %1, %5
  %7 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 1, i32 1), align 8
  %8 = load i32, i32* %7, align 4
  %9 = icmp eq i32 %8, 0
  br i1 %9, label %12, label %10

; <label>:10:                                     ; preds = %6
  %11 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %0, i32 0, i32 -1, i32 0)
  br label %12

; <label>:12:                                     ; preds = %6, %10
  %13 = phi i32 [ %11, %10 ], [ %0, %6 ]
  %14 = lshr i32 %13, 12
  %15 = tail call i32 @tcg_llvm_fork_and_concretize(i32 %14, i32 0, i32 1048575, i32 0)
  %16 = and i32 %15, 1023
  %17 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %18 = getelementptr %struct.CPUX86State, %struct.CPUX86State* %17, i64 0, i32 8
  %19 = load i32, i32* %18, align 4
  %20 = and i32 %19, 3
  %21 = icmp eq i32 %20, 3
  %22 = zext i32 %16 to i64
  %23 = zext i1 %21 to i64
  %24 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %17, i64 0, i32 77, i64 %23, i64 %22, i32 0
  %25 = load i32, i32* %24, align 8
  %26 = and i32 %13, -4095
  %27 = icmp eq i32 %25, %26
  br i1 %27, label %32, label %28, !prof !2

; <label>:28:                                     ; preds = %12
  %29 = zext i1 %21 to i32
  %30 = tail call zeroext i16 @__ldw_mmu(i32 %13, i32 %29)
  %31 = sext i16 %30 to i32
  br label %45

; <label>:32:                                     ; preds = %12
  %33 = zext i32 %13 to i64
  %34 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %17, i64 0, i32 77, i64 %23, i64 %22, i32 4
  %35 = load i64, i64* %34, align 8
  %36 = add i64 %35, %33
  %37 = inttoptr i64 %36 to i16*
  %38 = load i16, i16* %37, align 2
  %39 = sext i16 %38 to i32
  %40 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 1), align 8
  %41 = load i32, i32* %40, align 4
  %42 = icmp eq i32 %41, 0
  br i1 %42, label %45, label %43

; <label>:43:                                     ; preds = %32
  %44 = sext i16 %38 to i64
  tail call void @tcg_llvm_after_memory_access(i32 %13, i64 %44, i32 4, i32 0, i64 0)
  br label %45

; <label>:45:                                     ; preds = %32, %43, %28
  %46 = phi i32 [ %31, %28 ], [ %39, %43 ], [ %39, %32 ]
  ret i32 %46
}

; Function Attrs: uwtable
define void @helper_boundl(i32, i32) local_unnamed_addr #2 {
  %3 = tail call fastcc i32 @ldl_data(i32 %0)
  %4 = add i32 %0, 4
  %5 = tail call fastcc i32 @ldl_data(i32 %4)
  %6 = icmp sgt i32 %3, %1
  %7 = icmp slt i32 %5, %1
  %8 = or i1 %6, %7
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  tail call fastcc void @raise_exception(i32 5) #12
  unreachable

; <label>:10:                                     ; preds = %2
  ret void
}

; Function Attrs: alwaysinline inlinehint uwtable
define zeroext i8 @io_read_chkb_mmu(i64, i32, i8*) local_unnamed_addr #9 {
  %4 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %0)
  %5 = and i64 %0, 4294963200
  %6 = zext i32 %1 to i64
  %7 = add nuw nsw i64 %6, %5
  %8 = ptrtoint i8* %2 to i64
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 72
  store i64 %8, i64* %10, align 16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %6, i32 0)
  %11 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %4, i32 0)
  br i1 %11, label %12, label %19

; <label>:12:                                     ; preds = %3
  %13 = and i64 %7, 4294963200
  %14 = tail call i64 @se_notdirty_mem_read(i64 %13)
  %15 = and i64 %7, 4095
  %16 = or i64 %14, %15
  %17 = inttoptr i64 %16 to i8*
  %18 = load i8, i8* %17, align 1
  br label %21

; <label>:19:                                     ; preds = %3
  %20 = tail call zeroext i8 @io_readb_mmu(i64 %0, i32 %1, i8* %2)
  br label %21

; <label>:21:                                     ; preds = %12, %19
  %22 = phi i8 [ %18, %12 ], [ %20, %19 ]
  %23 = zext i8 %22 to i64
  %24 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %6, i64 %23, i32 1, i32 0)
  %25 = trunc i64 %24 to i8
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  ret i8 %25
}

; Function Attrs: alwaysinline inlinehint uwtable
define void @io_write_chkb_mmu(i64, i8 zeroext, i32, i8*) local_unnamed_addr #9 {
  %5 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %0)
  %6 = and i64 %0, -4096
  %7 = zext i32 %2 to i64
  %8 = add i64 %7, %6
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %7, i32 0)
  %9 = ptrtoint i8* %3 to i64
  %10 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 72
  store i64 %9, i64* %11, align 16
  %12 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %5, i32 1)
  br i1 %12, label %13, label %19

; <label>:13:                                     ; preds = %4
  %14 = and i64 %8, -4096
  %15 = tail call i64 @se_notdirty_mem_write(i64 %14)
  %16 = and i64 %8, 4095
  %17 = or i64 %15, %16
  %18 = inttoptr i64 %17 to i8*
  store i8 %1, i8* %18, align 1
  br label %20

; <label>:19:                                     ; preds = %4
  tail call void @io_writeb_mmu(i64 %0, i8 zeroext %1, i32 %2, i8* %3)
  br label %20

; <label>:20:                                     ; preds = %13, %19
  %21 = zext i8 %1 to i64
  %22 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %7, i64 %21, i32 1, i32 1)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  ret void
}

; Function Attrs: alwaysinline inlinehint uwtable
define zeroext i16 @io_read_chkw_mmu(i64, i32, i8*) local_unnamed_addr #9 {
  %4 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %0)
  %5 = and i64 %0, 4294963200
  %6 = zext i32 %1 to i64
  %7 = add nuw nsw i64 %6, %5
  %8 = ptrtoint i8* %2 to i64
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 72
  store i64 %8, i64* %10, align 16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %6, i32 0)
  %11 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %4, i32 0)
  br i1 %11, label %12, label %19

; <label>:12:                                     ; preds = %3
  %13 = and i64 %7, 4294963200
  %14 = tail call i64 @se_notdirty_mem_read(i64 %13)
  %15 = and i64 %7, 4095
  %16 = or i64 %14, %15
  %17 = inttoptr i64 %16 to i16*
  %18 = load i16, i16* %17, align 2
  br label %21

; <label>:19:                                     ; preds = %3
  %20 = tail call zeroext i16 @io_readw_mmu(i64 %0, i32 %1, i8* %2)
  br label %21

; <label>:21:                                     ; preds = %12, %19
  %22 = phi i16 [ %18, %12 ], [ %20, %19 ]
  %23 = zext i16 %22 to i64
  %24 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %6, i64 %23, i32 2, i32 0)
  %25 = trunc i64 %24 to i16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  ret i16 %25
}

; Function Attrs: alwaysinline inlinehint uwtable
define void @io_write_chkw_mmu(i64, i16 zeroext, i32, i8*) local_unnamed_addr #9 {
  %5 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %0)
  %6 = and i64 %0, -4096
  %7 = zext i32 %2 to i64
  %8 = add i64 %7, %6
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %7, i32 0)
  %9 = ptrtoint i8* %3 to i64
  %10 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 72
  store i64 %9, i64* %11, align 16
  %12 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %5, i32 1)
  br i1 %12, label %13, label %19

; <label>:13:                                     ; preds = %4
  %14 = and i64 %8, -4096
  %15 = tail call i64 @se_notdirty_mem_write(i64 %14)
  %16 = and i64 %8, 4095
  %17 = or i64 %15, %16
  %18 = inttoptr i64 %17 to i16*
  store i16 %1, i16* %18, align 2
  br label %20

; <label>:19:                                     ; preds = %4
  tail call void @io_writew_mmu(i64 %0, i16 zeroext %1, i32 %2, i8* %3)
  br label %20

; <label>:20:                                     ; preds = %13, %19
  %21 = zext i16 %1 to i64
  %22 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %7, i64 %21, i32 2, i32 1)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  ret void
}

; Function Attrs: alwaysinline inlinehint uwtable
define i32 @io_read_chkl_mmu(i64, i32, i8*) local_unnamed_addr #9 {
  %4 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %0)
  %5 = and i64 %0, 4294963200
  %6 = zext i32 %1 to i64
  %7 = add nuw nsw i64 %6, %5
  %8 = ptrtoint i8* %2 to i64
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 72
  store i64 %8, i64* %10, align 16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %6, i32 0)
  %11 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %4, i32 0)
  br i1 %11, label %12, label %19

; <label>:12:                                     ; preds = %3
  %13 = and i64 %7, 4294963200
  %14 = tail call i64 @se_notdirty_mem_read(i64 %13)
  %15 = and i64 %7, 4095
  %16 = or i64 %14, %15
  %17 = inttoptr i64 %16 to i32*
  %18 = load i32, i32* %17, align 4
  br label %21

; <label>:19:                                     ; preds = %3
  %20 = tail call i32 @io_readl_mmu(i64 %0, i32 %1, i8* %2)
  br label %21

; <label>:21:                                     ; preds = %12, %19
  %22 = phi i32 [ %18, %12 ], [ %20, %19 ]
  %23 = zext i32 %22 to i64
  %24 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %6, i64 %23, i32 4, i32 0)
  %25 = trunc i64 %24 to i32
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  ret i32 %25
}

; Function Attrs: alwaysinline inlinehint uwtable
define void @io_write_chkl_mmu(i64, i32, i32, i8*) local_unnamed_addr #9 {
  %5 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %0)
  %6 = and i64 %0, -4096
  %7 = zext i32 %2 to i64
  %8 = add i64 %7, %6
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %7, i32 0)
  %9 = ptrtoint i8* %3 to i64
  %10 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 72
  store i64 %9, i64* %11, align 16
  %12 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %5, i32 1)
  br i1 %12, label %13, label %19

; <label>:13:                                     ; preds = %4
  %14 = and i64 %8, -4096
  %15 = tail call i64 @se_notdirty_mem_write(i64 %14)
  %16 = and i64 %8, 4095
  %17 = or i64 %15, %16
  %18 = inttoptr i64 %17 to i32*
  store i32 %1, i32* %18, align 4
  br label %20

; <label>:19:                                     ; preds = %4
  tail call void @io_writel_mmu(i64 %0, i32 %1, i32 %2, i8* %3)
  br label %20

; <label>:20:                                     ; preds = %13, %19
  %21 = zext i32 %1 to i64
  %22 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %7, i64 %21, i32 4, i32 1)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  ret void
}

; Function Attrs: alwaysinline inlinehint uwtable
define i64 @io_read_chkq_mmu(i64, i32, i8*) local_unnamed_addr #9 {
  %4 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %0)
  %5 = and i64 %0, 4294963200
  %6 = zext i32 %1 to i64
  %7 = add nuw nsw i64 %6, %5
  %8 = ptrtoint i8* %2 to i64
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 72
  store i64 %8, i64* %10, align 16
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %6, i32 0)
  %11 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %4, i32 0)
  br i1 %11, label %12, label %24

; <label>:12:                                     ; preds = %3
  %13 = and i64 %7, 4294963200
  %14 = tail call i64 @se_notdirty_mem_read(i64 %13)
  %15 = and i64 %7, 4095
  %16 = or i64 %14, %15
  %17 = inttoptr i64 %16 to i64*
  %18 = load i64, i64* %17, align 8
  %19 = add i64 %16, 4
  %20 = inttoptr i64 %19 to i64*
  %21 = load i64, i64* %20, align 8
  %22 = shl i64 %21, 32
  %23 = or i64 %22, %18
  br label %26

; <label>:24:                                     ; preds = %3
  %25 = tail call i64 @io_readq_mmu(i64 %0, i32 %1, i8* %2)
  br label %26

; <label>:26:                                     ; preds = %12, %24
  %27 = phi i64 [ %23, %12 ], [ %25, %24 ]
  %28 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %6, i64 %27, i32 8, i32 0)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  ret i64 %28
}

; Function Attrs: alwaysinline inlinehint uwtable
define void @io_write_chkq_mmu(i64, i64, i32, i8*) local_unnamed_addr #9 {
  %5 = tail call %struct.MemoryDescOps* @phys_get_ops(i64 %0)
  %6 = and i64 %0, -4096
  %7 = zext i32 %2 to i64
  %8 = add i64 %7, %6
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 %7, i32 0)
  %9 = ptrtoint i8* %3 to i64
  %10 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %10, i64 0, i32 72
  store i64 %9, i64* %11, align 16
  %12 = tail call zeroext i1 @se_ismemfunc(%struct.MemoryDescOps* %5, i32 1)
  br i1 %12, label %13, label %24

; <label>:13:                                     ; preds = %4
  %14 = and i64 %8, -4096
  %15 = tail call i64 @se_notdirty_mem_write(i64 %14)
  %16 = and i64 %8, 4095
  %17 = or i64 %15, %16
  %18 = trunc i64 %1 to i32
  %19 = inttoptr i64 %17 to i32*
  store i32 %18, i32* %19, align 4
  %20 = add i64 %17, 4
  %21 = lshr i64 %1, 32
  %22 = trunc i64 %21 to i32
  %23 = inttoptr i64 %20 to i32*
  store i32 %22, i32* %23, align 4
  br label %25

; <label>:24:                                     ; preds = %4
  tail call void @io_writeq_mmu(i64 %0, i64 %1, i32 %2, i8* %3)
  br label %25

; <label>:25:                                     ; preds = %13, %24
  %26 = tail call i64 @tcg_llvm_trace_mmio_access(i64 %7, i64 %1, i32 8, i32 1)
  tail call void @tcg_llvm_write_mem_io_vaddr(i64 0, i32 1)
  ret void
}

; Function Attrs: uwtable
define void @helper_vmrun(i32, i32) local_unnamed_addr #2 {
  %3 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %4 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 8
  %5 = load i32, i32* %4, align 4
  %6 = and i32 %5, 3
  %7 = and i32 %5, 2097152
  %8 = icmp eq i32 %7, 0
  br i1 %8, label %15, label %9, !prof !2

; <label>:9:                                      ; preds = %2
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 42
  %11 = load i64, i64* %10, align 8
  %12 = and i64 %11, 4294967296
  %13 = icmp eq i64 %12, 0
  br i1 %13, label %15, label %14

; <label>:14:                                     ; preds = %9
  tail call void @helper_vmexit(i32 128, i64 0)
  unreachable

; <label>:15:                                     ; preds = %2, %9
  %16 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 0, i64 0
  %17 = load i32, i32* %16, align 16
  %18 = load i32, i32* @loglevel, align 4
  %19 = and i32 %18, 2
  %20 = icmp eq i32 %19, 0
  br i1 %20, label %25, label %21

; <label>:21:                                     ; preds = %15
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %23 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %22, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.6, i64 0, i64 0), i32 %17)
  %24 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %25

; <label>:25:                                     ; preds = %15, %21
  %26 = phi %struct.CPUX86State* [ %3, %15 ], [ %24, %21 ]
  %27 = zext i32 %17 to i64
  %28 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 40
  store i64 %27, i64* %28, align 8
  %29 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 39
  %30 = load i64, i64* %29, align 16
  %31 = add i64 %30, 1128
  %32 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 14, i32 1
  %33 = load i32, i32* %32, align 4
  %34 = zext i32 %33 to i64
  tail call void @stq_phys(i64 %31, i64 %34)
  %35 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 39
  %37 = load i64, i64* %36, align 16
  %38 = add i64 %37, 1124
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 14, i32 2
  %40 = load i32, i32* %39, align 8
  tail call void @stl_phys(i64 %38, i32 %40)
  %41 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %42 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %41, i64 0, i32 39
  %43 = load i64, i64* %42, align 16
  %44 = add i64 %43, 1160
  %45 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %41, i64 0, i32 15, i32 1
  %46 = load i32, i32* %45, align 4
  %47 = zext i32 %46 to i64
  tail call void @stq_phys(i64 %44, i64 %47)
  %48 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %49 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %48, i64 0, i32 39
  %50 = load i64, i64* %49, align 16
  %51 = add i64 %50, 1156
  %52 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %48, i64 0, i32 15, i32 2
  %53 = load i32, i32* %52, align 8
  tail call void @stl_phys(i64 %51, i32 %53)
  %54 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %55 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %54, i64 0, i32 39
  %56 = load i64, i64* %55, align 16
  %57 = add i64 %56, 1368
  %58 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %54, i64 0, i32 16, i64 0
  %59 = load i32, i32* %58, align 8
  %60 = zext i32 %59 to i64
  tail call void @stq_phys(i64 %57, i64 %60)
  %61 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %62 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %61, i64 0, i32 39
  %63 = load i64, i64* %62, align 16
  %64 = add i64 %63, 1600
  %65 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %61, i64 0, i32 16, i64 2
  %66 = load i32, i32* %65, align 8
  %67 = zext i32 %66 to i64
  tail call void @stq_phys(i64 %64, i64 %67)
  %68 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %69 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %68, i64 0, i32 39
  %70 = load i64, i64* %69, align 16
  %71 = add i64 %70, 1360
  %72 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %68, i64 0, i32 16, i64 3
  %73 = load i32, i32* %72, align 4
  %74 = zext i32 %73 to i64
  tail call void @stq_phys(i64 %71, i64 %74)
  %75 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %76 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %75, i64 0, i32 39
  %77 = load i64, i64* %76, align 16
  %78 = add i64 %77, 1352
  %79 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %75, i64 0, i32 16, i64 4
  %80 = load i32, i32* %79, align 8
  %81 = zext i32 %80 to i64
  tail call void @stq_phys(i64 %78, i64 %81)
  %82 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %83 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %82, i64 0, i32 39
  %84 = load i64, i64* %83, align 16
  %85 = add i64 %84, 1384
  %86 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %82, i64 0, i32 61, i64 6
  %87 = load i32, i32* %86, align 4
  %88 = zext i32 %87 to i64
  tail call void @stq_phys(i64 %85, i64 %88)
  %89 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %90 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %89, i64 0, i32 39
  %91 = load i64, i64* %90, align 16
  %92 = add i64 %91, 1376
  %93 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %89, i64 0, i32 61, i64 7
  %94 = load i32, i32* %93, align 4
  %95 = zext i32 %94 to i64
  tail call void @stq_phys(i64 %92, i64 %95)
  %96 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %97 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %96, i64 0, i32 39
  %98 = load i64, i64* %97, align 16
  %99 = add i64 %98, 1232
  %100 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %96, i64 0, i32 37
  %101 = load i64, i64* %100, align 16
  tail call void @stq_phys(i64 %99, i64 %101)
  %102 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %103 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %102, i64 0, i32 39
  %104 = load i64, i64* %103, align 16
  %105 = add i64 %104, 1392
  %106 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %102, i64 0, i32 7
  %107 = load i32, i32* %106, align 8
  %108 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %102, i64 0, i32 1
  %109 = load i32, i32* %108, align 16
  %110 = tail call i32 @helper_cc_compute_all(i32 %109) #5
  %111 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %102, i64 0, i32 6
  %112 = load i32, i32* %111, align 4
  %113 = and i32 %112, 1024
  %114 = or i32 %107, %110
  %115 = or i32 %114, %113
  %116 = or i32 %115, 2
  %117 = zext i32 %116 to i64
  tail call void @stq_phys(i64 %105, i64 %117)
  %118 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %119 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %118, i64 0, i32 39
  %120 = load i64, i64* %119, align 16
  %121 = add i64 %120, 1024
  %122 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %118, i64 0, i32 11, i64 0, i32 0
  %123 = load i32, i32* %122, align 4
  tail call void @stw_phys(i64 %121, i32 %123)
  %124 = add i64 %120, 1032
  %125 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %118, i64 0, i32 11, i64 0, i32 1
  %126 = load i32, i32* %125, align 4
  %127 = zext i32 %126 to i64
  tail call void @stq_phys(i64 %124, i64 %127)
  %128 = add i64 %120, 1028
  %129 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %118, i64 0, i32 11, i64 0, i32 2
  %130 = load i32, i32* %129, align 4
  tail call void @stl_phys(i64 %128, i32 %130)
  %131 = add i64 %120, 1026
  %132 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %118, i64 0, i32 11, i64 0, i32 3
  %133 = load i32, i32* %132, align 4
  %134 = lshr i32 %133, 8
  %135 = and i32 %134, 255
  %136 = lshr i32 %133, 12
  %137 = and i32 %136, 3840
  %138 = or i32 %135, %137
  tail call void @stw_phys(i64 %131, i32 %138)
  %139 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %140 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %139, i64 0, i32 39
  %141 = load i64, i64* %140, align 16
  %142 = add i64 %141, 1040
  %143 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %139, i64 0, i32 11, i64 1, i32 0
  %144 = load i32, i32* %143, align 4
  tail call void @stw_phys(i64 %142, i32 %144)
  %145 = add i64 %141, 1048
  %146 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %139, i64 0, i32 11, i64 1, i32 1
  %147 = load i32, i32* %146, align 4
  %148 = zext i32 %147 to i64
  tail call void @stq_phys(i64 %145, i64 %148)
  %149 = add i64 %141, 1044
  %150 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %139, i64 0, i32 11, i64 1, i32 2
  %151 = load i32, i32* %150, align 4
  tail call void @stl_phys(i64 %149, i32 %151)
  %152 = add i64 %141, 1042
  %153 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %139, i64 0, i32 11, i64 1, i32 3
  %154 = load i32, i32* %153, align 4
  %155 = lshr i32 %154, 8
  %156 = and i32 %155, 255
  %157 = lshr i32 %154, 12
  %158 = and i32 %157, 3840
  %159 = or i32 %156, %158
  tail call void @stw_phys(i64 %152, i32 %159)
  %160 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %161 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %160, i64 0, i32 39
  %162 = load i64, i64* %161, align 16
  %163 = add i64 %162, 1056
  %164 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %160, i64 0, i32 11, i64 2, i32 0
  %165 = load i32, i32* %164, align 4
  tail call void @stw_phys(i64 %163, i32 %165)
  %166 = add i64 %162, 1064
  %167 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %160, i64 0, i32 11, i64 2, i32 1
  %168 = load i32, i32* %167, align 4
  %169 = zext i32 %168 to i64
  tail call void @stq_phys(i64 %166, i64 %169)
  %170 = add i64 %162, 1060
  %171 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %160, i64 0, i32 11, i64 2, i32 2
  %172 = load i32, i32* %171, align 4
  tail call void @stl_phys(i64 %170, i32 %172)
  %173 = add i64 %162, 1058
  %174 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %160, i64 0, i32 11, i64 2, i32 3
  %175 = load i32, i32* %174, align 4
  %176 = lshr i32 %175, 8
  %177 = and i32 %176, 255
  %178 = lshr i32 %175, 12
  %179 = and i32 %178, 3840
  %180 = or i32 %177, %179
  tail call void @stw_phys(i64 %173, i32 %180)
  %181 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %182 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %181, i64 0, i32 39
  %183 = load i64, i64* %182, align 16
  %184 = add i64 %183, 1072
  %185 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %181, i64 0, i32 11, i64 3, i32 0
  %186 = load i32, i32* %185, align 4
  tail call void @stw_phys(i64 %184, i32 %186)
  %187 = add i64 %183, 1080
  %188 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %181, i64 0, i32 11, i64 3, i32 1
  %189 = load i32, i32* %188, align 4
  %190 = zext i32 %189 to i64
  tail call void @stq_phys(i64 %187, i64 %190)
  %191 = add i64 %183, 1076
  %192 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %181, i64 0, i32 11, i64 3, i32 2
  %193 = load i32, i32* %192, align 4
  tail call void @stl_phys(i64 %191, i32 %193)
  %194 = add i64 %183, 1074
  %195 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %181, i64 0, i32 11, i64 3, i32 3
  %196 = load i32, i32* %195, align 4
  %197 = lshr i32 %196, 8
  %198 = and i32 %197, 255
  %199 = lshr i32 %196, 12
  %200 = and i32 %199, 3840
  %201 = or i32 %198, %200
  tail call void @stw_phys(i64 %194, i32 %201)
  %202 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %203 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %202, i64 0, i32 39
  %204 = load i64, i64* %203, align 16
  %205 = add i64 %204, 1400
  %206 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %202, i64 0, i32 5
  %207 = load i32, i32* %206, align 16
  %208 = add i32 %207, %1
  %209 = zext i32 %208 to i64
  tail call void @stq_phys(i64 %205, i64 %209)
  %210 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %211 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %210, i64 0, i32 39
  %212 = load i64, i64* %211, align 16
  %213 = add i64 %212, 1496
  %214 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %210, i64 0, i32 0, i64 4
  %215 = load i32, i32* %214, align 16
  %216 = zext i32 %215 to i64
  tail call void @stq_phys(i64 %213, i64 %216)
  %217 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %218 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %217, i64 0, i32 39
  %219 = load i64, i64* %218, align 16
  %220 = add i64 %219, 1528
  %221 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %217, i64 0, i32 0, i64 0
  %222 = load i32, i32* %221, align 16
  %223 = zext i32 %222 to i64
  tail call void @stq_phys(i64 %220, i64 %223)
  %224 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %225 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %224, i64 0, i32 40
  %226 = load i64, i64* %225, align 8
  %227 = add i64 %226, 12
  %228 = tail call i64 @ldq_phys(i64 %227)
  %229 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %230 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %229, i64 0, i32 42
  store i64 %228, i64* %230, align 8
  %231 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %229, i64 0, i32 40
  %232 = load i64, i64* %231, align 8
  %233 = tail call i32 @lduw_phys(i64 %232)
  %234 = trunc i32 %233 to i16
  %235 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %236 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %235, i64 0, i32 43
  store i16 %234, i16* %236, align 16
  %237 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %235, i64 0, i32 40
  %238 = load i64, i64* %237, align 8
  %239 = add i64 %238, 2
  %240 = tail call i32 @lduw_phys(i64 %239)
  %241 = trunc i32 %240 to i16
  %242 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %243 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %242, i64 0, i32 44
  store i16 %241, i16* %243, align 2
  %244 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %242, i64 0, i32 40
  %245 = load i64, i64* %244, align 8
  %246 = add i64 %245, 4
  %247 = tail call i32 @lduw_phys(i64 %246)
  %248 = trunc i32 %247 to i16
  %249 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %250 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %249, i64 0, i32 45
  store i16 %248, i16* %250, align 4
  %251 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %249, i64 0, i32 40
  %252 = load i64, i64* %251, align 8
  %253 = add i64 %252, 6
  %254 = tail call i32 @lduw_phys(i64 %253)
  %255 = trunc i32 %254 to i16
  %256 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %257 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %256, i64 0, i32 46
  store i16 %255, i16* %257, align 2
  %258 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %256, i64 0, i32 40
  %259 = load i64, i64* %258, align 8
  %260 = add i64 %259, 8
  %261 = tail call i32 @ldl_phys(i64 %260)
  %262 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %263 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %262, i64 0, i32 47
  store i32 %261, i32* %263, align 8
  %264 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %262, i64 0, i32 8
  %265 = load i32, i32* %264, align 4
  %266 = or i32 %265, 2097152
  store i32 %266, i32* %264, align 4
  %267 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %262, i64 0, i32 40
  %268 = load i64, i64* %267, align 8
  %269 = add i64 %268, 80
  %270 = tail call i64 @ldq_phys(i64 %269)
  %271 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %272 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %271, i64 0, i32 41
  store i64 %270, i64* %272, align 16
  %273 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %271, i64 0, i32 40
  %274 = load i64, i64* %273, align 8
  %275 = add i64 %274, 1128
  %276 = tail call i64 @ldq_phys(i64 %275)
  %277 = trunc i64 %276 to i32
  %278 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %279 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %278, i64 0, i32 14, i32 1
  store i32 %277, i32* %279, align 4
  %280 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %278, i64 0, i32 40
  %281 = load i64, i64* %280, align 8
  %282 = add i64 %281, 1124
  %283 = tail call i32 @ldl_phys(i64 %282)
  %284 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %285 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %284, i64 0, i32 14, i32 2
  store i32 %283, i32* %285, align 8
  %286 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %284, i64 0, i32 40
  %287 = load i64, i64* %286, align 8
  %288 = add i64 %287, 1160
  %289 = tail call i64 @ldq_phys(i64 %288)
  %290 = trunc i64 %289 to i32
  %291 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %292 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %291, i64 0, i32 15, i32 1
  store i32 %290, i32* %292, align 4
  %293 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %291, i64 0, i32 40
  %294 = load i64, i64* %293, align 8
  %295 = add i64 %294, 1156
  %296 = tail call i32 @ldl_phys(i64 %295)
  %297 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %298 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %297, i64 0, i32 15, i32 2
  store i32 %296, i32* %298, align 8
  %299 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %297, i64 0, i32 40
  %300 = load i64, i64* %299, align 8
  %301 = add i64 %300, 128
  tail call void @stq_phys(i64 %301, i64 0)
  %302 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %303 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %302, i64 0, i32 40
  %304 = load i64, i64* %303, align 8
  %305 = add i64 %304, 1368
  %306 = tail call i64 @ldq_phys(i64 %305)
  %307 = trunc i64 %306 to i32
  tail call void @cpu_x86_update_cr0(%struct.CPUX86State* %302, i32 %307)
  %308 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %309 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %308, i64 0, i32 40
  %310 = load i64, i64* %309, align 8
  %311 = add i64 %310, 1352
  %312 = tail call i64 @ldq_phys(i64 %311)
  %313 = trunc i64 %312 to i32
  tail call void @cpu_x86_update_cr4(%struct.CPUX86State* %308, i32 %313)
  %314 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %315 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %314, i64 0, i32 40
  %316 = load i64, i64* %315, align 8
  %317 = add i64 %316, 1360
  %318 = tail call i64 @ldq_phys(i64 %317)
  %319 = trunc i64 %318 to i32
  tail call void @cpu_x86_update_cr3(%struct.CPUX86State* %314, i32 %319)
  %320 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %321 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %320, i64 0, i32 40
  %322 = load i64, i64* %321, align 8
  %323 = add i64 %322, 1600
  %324 = tail call i64 @ldq_phys(i64 %323)
  %325 = trunc i64 %324 to i32
  %326 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %327 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %326, i64 0, i32 16, i64 2
  store i32 %325, i32* %327, align 8
  %328 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %326, i64 0, i32 40
  %329 = load i64, i64* %328, align 8
  %330 = add i64 %329, 96
  %331 = tail call i32 @ldl_phys(i64 %330)
  %332 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %333 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %332, i64 0, i32 9
  %334 = load i32, i32* %333, align 16
  %335 = and i32 %334, -11
  store i32 %335, i32* %333, align 16
  %336 = and i32 %331, 16777216
  %337 = icmp eq i32 %336, 0
  br i1 %337, label %349, label %338

; <label>:338:                                    ; preds = %25
  %339 = trunc i32 %331 to i8
  %340 = and i8 %339, 15
  %341 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %332, i64 0, i32 48
  store i8 %340, i8* %341, align 4
  %342 = or i32 %335, 8
  store i32 %342, i32* %333, align 16
  %343 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %332, i64 0, i32 7
  %344 = load i32, i32* %343, align 8
  %345 = and i32 %344, 512
  %346 = icmp eq i32 %345, 0
  br i1 %346, label %349, label %347

; <label>:347:                                    ; preds = %338
  %348 = or i32 %334, 10
  store i32 %348, i32* %333, align 16
  br label %349

; <label>:349:                                    ; preds = %338, %25, %347
  %350 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %332, i64 0, i32 40
  %351 = load i64, i64* %350, align 8
  %352 = add i64 %351, 1232
  %353 = tail call i64 @ldq_phys(i64 %352)
  %354 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %332, i64 0, i32 37
  store i64 %353, i64* %354, align 16
  %355 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %332, i64 0, i32 8
  %356 = load i32, i32* %355, align 4
  %357 = and i32 %356, -1064961
  %358 = trunc i64 %353 to i32
  %359 = shl i32 %358, 4
  %360 = and i32 %359, 16384
  %361 = or i32 %357, %360
  %362 = shl i32 %358, 8
  %363 = and i32 %362, 1048576
  %364 = or i32 %361, %363
  store i32 %364, i32* %355, align 4
  %365 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %366 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %365, i64 0, i32 7
  store i32 0, i32* %366, align 8
  %367 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %365, i64 0, i32 40
  %368 = load i64, i64* %367, align 8
  %369 = add i64 %368, 1392
  %370 = tail call i64 @ldq_phys(i64 %369)
  %371 = trunc i64 %370 to i32
  %372 = and i32 %371, 2261
  %373 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %374 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 2
  store i32 %372, i32* %374, align 4
  %375 = lshr i32 %371, 9
  %376 = and i32 %375, 2
  %377 = xor i32 %376, 2
  %378 = add nsw i32 %377, -1
  %379 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 6
  store i32 %378, i32* %379, align 4
  %380 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 7
  %381 = load i32, i32* %380, align 8
  %382 = and i32 %381, 3285
  %383 = and i32 %371, -3286
  %384 = or i32 %382, %383
  store i32 %384, i32* %380, align 8
  %385 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 1
  store i32 1, i32* %385, align 16
  %386 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 40
  %387 = load i64, i64* %386, align 8
  %388 = add i64 %387, 1024
  %389 = tail call i32 @lduw_phys(i64 %388)
  %390 = add i64 %387, 1032
  %391 = tail call i64 @ldq_phys(i64 %390)
  %392 = trunc i64 %391 to i32
  %393 = add i64 %387, 1028
  %394 = tail call i32 @ldl_phys(i64 %393)
  %395 = add i64 %387, 1026
  %396 = tail call i32 @lduw_phys(i64 %395)
  %397 = shl i32 %396, 8
  %398 = and i32 %397, 65280
  %399 = shl i32 %396, 12
  %400 = and i32 %399, 15728640
  %401 = or i32 %398, %400
  %402 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 11, i64 0, i32 0
  store i32 %389, i32* %402, align 4
  %403 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 11, i64 0, i32 1
  store i32 %392, i32* %403, align 4
  %404 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 11, i64 0, i32 2
  store i32 %394, i32* %404, align 4
  %405 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 11, i64 0, i32 3
  store i32 %401, i32* %405, align 4
  %406 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 8
  %407 = load i32, i32* %406, align 4
  %408 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 11, i64 2, i32 3
  %409 = load i32, i32* %408, align 4
  %410 = lshr i32 %409, 17
  %411 = and i32 %410, 32
  %412 = trunc i32 %407 to i16
  %413 = icmp slt i16 %412, 0
  br i1 %413, label %443, label %414

; <label>:414:                                    ; preds = %349
  %415 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 16, i64 0
  %416 = load i32, i32* %415, align 8
  %417 = and i32 %416, 1
  %418 = icmp eq i32 %417, 0
  br i1 %418, label %429, label %419

; <label>:419:                                    ; preds = %414
  %420 = bitcast i32* %380 to i64*
  %421 = load i64, i64* %420, align 8
  %422 = and i64 %421, 131072
  %423 = icmp ne i64 %422, 0
  %424 = and i32 %407, 16
  %425 = icmp eq i32 %424, 0
  %426 = or i1 %425, %423
  %427 = lshr i64 %421, 32
  %428 = trunc i64 %427 to i32
  br i1 %426, label %429, label %432

; <label>:429:                                    ; preds = %419, %414
  %430 = phi i32 [ %428, %419 ], [ %407, %414 ]
  %431 = or i32 %411, 64
  br label %443

; <label>:432:                                    ; preds = %419
  %433 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 11, i64 3, i32 1
  %434 = load i32, i32* %433, align 4
  %435 = or i32 %392, %434
  %436 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %373, i64 0, i32 11, i64 2, i32 1
  %437 = load i32, i32* %436, align 4
  %438 = or i32 %435, %437
  %439 = icmp ne i32 %438, 0
  %440 = zext i1 %439 to i32
  %441 = shl nuw nsw i32 %440, 6
  %442 = or i32 %441, %411
  br label %443

; <label>:443:                                    ; preds = %432, %429, %349
  %444 = phi i32 [ %407, %349 ], [ %430, %429 ], [ %428, %432 ]
  %445 = phi i32 [ %411, %349 ], [ %431, %429 ], [ %442, %432 ]
  %446 = and i32 %444, -97
  %447 = or i32 %446, %445
  store i32 %447, i32* %406, align 4
  %448 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %449 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %448, i64 0, i32 40
  %450 = load i64, i64* %449, align 8
  %451 = add i64 %450, 1040
  %452 = tail call i32 @lduw_phys(i64 %451)
  %453 = add i64 %450, 1048
  %454 = tail call i64 @ldq_phys(i64 %453)
  %455 = trunc i64 %454 to i32
  %456 = add i64 %450, 1044
  %457 = tail call i32 @ldl_phys(i64 %456)
  %458 = add i64 %450, 1042
  %459 = tail call i32 @lduw_phys(i64 %458)
  %460 = shl i32 %459, 8
  %461 = and i32 %460, 65280
  %462 = shl i32 %459, 12
  %463 = and i32 %462, 15728640
  %464 = or i32 %461, %463
  %465 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %448, i64 0, i32 11, i64 1, i32 0
  store i32 %452, i32* %465, align 4
  %466 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %448, i64 0, i32 11, i64 1, i32 1
  store i32 %455, i32* %466, align 4
  %467 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %448, i64 0, i32 11, i64 1, i32 2
  store i32 %457, i32* %467, align 4
  %468 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %448, i64 0, i32 11, i64 1, i32 3
  store i32 %464, i32* %468, align 4
  %469 = lshr exact i32 %463, 18
  %470 = and i32 %469, 16
  %471 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %448, i64 0, i32 8
  %472 = load i32, i32* %471, align 4
  %473 = and i32 %472, -32785
  %474 = or i32 %473, %470
  store i32 %474, i32* %471, align 4
  %475 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %448, i64 0, i32 11, i64 2, i32 3
  %476 = load i32, i32* %475, align 4
  %477 = lshr i32 %476, 17
  %478 = and i32 %477, 32
  %479 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %448, i64 0, i32 16, i64 0
  %480 = load i32, i32* %479, align 8
  %481 = and i32 %480, 1
  %482 = icmp eq i32 %481, 0
  br i1 %482, label %493, label %483

; <label>:483:                                    ; preds = %443
  %484 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %448, i64 0, i32 7
  %485 = bitcast i32* %484 to i64*
  %486 = load i64, i64* %485, align 8
  %487 = and i64 %486, 131072
  %488 = icmp ne i64 %487, 0
  %489 = icmp eq i32 %470, 0
  %490 = or i1 %489, %488
  %491 = lshr i64 %486, 32
  %492 = trunc i64 %491 to i32
  br i1 %490, label %493, label %496

; <label>:493:                                    ; preds = %483, %443
  %494 = phi i32 [ %492, %483 ], [ %474, %443 ]
  %495 = or i32 %478, 64
  br label %509

; <label>:496:                                    ; preds = %483
  %497 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %448, i64 0, i32 11, i64 3, i32 1
  %498 = load i32, i32* %497, align 4
  %499 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %448, i64 0, i32 11, i64 0, i32 1
  %500 = load i32, i32* %499, align 4
  %501 = or i32 %500, %498
  %502 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %448, i64 0, i32 11, i64 2, i32 1
  %503 = load i32, i32* %502, align 4
  %504 = or i32 %501, %503
  %505 = icmp ne i32 %504, 0
  %506 = zext i1 %505 to i32
  %507 = shl nuw nsw i32 %506, 6
  %508 = or i32 %507, %478
  br label %509

; <label>:509:                                    ; preds = %493, %496
  %510 = phi i32 [ %494, %493 ], [ %492, %496 ]
  %511 = phi i32 [ %495, %493 ], [ %508, %496 ]
  %512 = and i32 %510, -97
  %513 = or i32 %512, %511
  store i32 %513, i32* %471, align 4
  %514 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %515 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %514, i64 0, i32 40
  %516 = load i64, i64* %515, align 8
  %517 = add i64 %516, 1056
  %518 = tail call i32 @lduw_phys(i64 %517)
  %519 = add i64 %516, 1064
  %520 = tail call i64 @ldq_phys(i64 %519)
  %521 = trunc i64 %520 to i32
  %522 = add i64 %516, 1060
  %523 = tail call i32 @ldl_phys(i64 %522)
  %524 = add i64 %516, 1058
  %525 = tail call i32 @lduw_phys(i64 %524)
  %526 = shl i32 %525, 8
  %527 = and i32 %526, 65280
  %528 = shl i32 %525, 12
  %529 = and i32 %528, 15728640
  %530 = or i32 %527, %529
  %531 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %514, i64 0, i32 11, i64 2, i32 0
  store i32 %518, i32* %531, align 4
  %532 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %514, i64 0, i32 11, i64 2, i32 1
  store i32 %521, i32* %532, align 4
  %533 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %514, i64 0, i32 11, i64 2, i32 2
  store i32 %523, i32* %533, align 4
  %534 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %514, i64 0, i32 11, i64 2, i32 3
  store i32 %530, i32* %534, align 4
  %535 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %514, i64 0, i32 8
  %536 = load i32, i32* %535, align 4
  %537 = lshr exact i32 %529, 17
  %538 = and i32 %537, 32
  %539 = trunc i32 %536 to i16
  %540 = icmp slt i16 %539, 0
  br i1 %540, label %571, label %541

; <label>:541:                                    ; preds = %509
  %542 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %514, i64 0, i32 16, i64 0
  %543 = load i32, i32* %542, align 8
  %544 = and i32 %543, 1
  %545 = icmp eq i32 %544, 0
  br i1 %545, label %557, label %546

; <label>:546:                                    ; preds = %541
  %547 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %514, i64 0, i32 7
  %548 = bitcast i32* %547 to i64*
  %549 = load i64, i64* %548, align 8
  %550 = and i64 %549, 131072
  %551 = icmp ne i64 %550, 0
  %552 = and i32 %536, 16
  %553 = icmp eq i32 %552, 0
  %554 = or i1 %553, %551
  %555 = lshr i64 %549, 32
  %556 = trunc i64 %555 to i32
  br i1 %554, label %557, label %560

; <label>:557:                                    ; preds = %546, %541
  %558 = phi i32 [ %556, %546 ], [ %536, %541 ]
  %559 = or i32 %538, 64
  br label %571

; <label>:560:                                    ; preds = %546
  %561 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %514, i64 0, i32 11, i64 3, i32 1
  %562 = load i32, i32* %561, align 4
  %563 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %514, i64 0, i32 11, i64 0, i32 1
  %564 = load i32, i32* %563, align 4
  %565 = or i32 %564, %562
  %566 = or i32 %565, %521
  %567 = icmp ne i32 %566, 0
  %568 = zext i1 %567 to i32
  %569 = shl nuw nsw i32 %568, 6
  %570 = or i32 %569, %538
  br label %571

; <label>:571:                                    ; preds = %509, %557, %560
  %572 = phi i32 [ %536, %509 ], [ %558, %557 ], [ %556, %560 ]
  %573 = phi i32 [ %538, %509 ], [ %559, %557 ], [ %570, %560 ]
  %574 = and i32 %572, -97
  %575 = or i32 %574, %573
  store i32 %575, i32* %535, align 4
  %576 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %577 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 40
  %578 = load i64, i64* %577, align 8
  %579 = add i64 %578, 1072
  %580 = tail call i32 @lduw_phys(i64 %579)
  %581 = add i64 %578, 1080
  %582 = tail call i64 @ldq_phys(i64 %581)
  %583 = trunc i64 %582 to i32
  %584 = add i64 %578, 1076
  %585 = tail call i32 @ldl_phys(i64 %584)
  %586 = add i64 %578, 1074
  %587 = tail call i32 @lduw_phys(i64 %586)
  %588 = shl i32 %587, 8
  %589 = and i32 %588, 65280
  %590 = shl i32 %587, 12
  %591 = and i32 %590, 15728640
  %592 = or i32 %589, %591
  %593 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 11, i64 3, i32 0
  store i32 %580, i32* %593, align 4
  %594 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 11, i64 3, i32 1
  store i32 %583, i32* %594, align 4
  %595 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 11, i64 3, i32 2
  store i32 %585, i32* %595, align 4
  %596 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 11, i64 3, i32 3
  store i32 %592, i32* %596, align 4
  %597 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 8
  %598 = load i32, i32* %597, align 4
  %599 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 11, i64 2, i32 3
  %600 = load i32, i32* %599, align 4
  %601 = lshr i32 %600, 17
  %602 = and i32 %601, 32
  %603 = trunc i32 %598 to i16
  %604 = icmp slt i16 %603, 0
  br i1 %604, label %635, label %605

; <label>:605:                                    ; preds = %571
  %606 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 16, i64 0
  %607 = load i32, i32* %606, align 8
  %608 = and i32 %607, 1
  %609 = icmp eq i32 %608, 0
  br i1 %609, label %621, label %610

; <label>:610:                                    ; preds = %605
  %611 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 7
  %612 = bitcast i32* %611 to i64*
  %613 = load i64, i64* %612, align 8
  %614 = and i64 %613, 131072
  %615 = icmp ne i64 %614, 0
  %616 = and i32 %598, 16
  %617 = icmp eq i32 %616, 0
  %618 = or i1 %617, %615
  %619 = lshr i64 %613, 32
  %620 = trunc i64 %619 to i32
  br i1 %618, label %621, label %624

; <label>:621:                                    ; preds = %610, %605
  %622 = phi i32 [ %620, %610 ], [ %598, %605 ]
  %623 = or i32 %602, 64
  br label %635

; <label>:624:                                    ; preds = %610
  %625 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 11, i64 0, i32 1
  %626 = load i32, i32* %625, align 4
  %627 = or i32 %626, %583
  %628 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %576, i64 0, i32 11, i64 2, i32 1
  %629 = load i32, i32* %628, align 4
  %630 = or i32 %627, %629
  %631 = icmp ne i32 %630, 0
  %632 = zext i1 %631 to i32
  %633 = shl nuw nsw i32 %632, 6
  %634 = or i32 %633, %602
  br label %635

; <label>:635:                                    ; preds = %571, %621, %624
  %636 = phi i32 [ %598, %571 ], [ %622, %621 ], [ %620, %624 ]
  %637 = phi i32 [ %602, %571 ], [ %623, %621 ], [ %634, %624 ]
  %638 = and i32 %636, -97
  %639 = or i32 %638, %637
  store i32 %639, i32* %597, align 4
  %640 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %641 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %640, i64 0, i32 40
  %642 = load i64, i64* %641, align 8
  %643 = add i64 %642, 1400
  %644 = tail call i64 @ldq_phys(i64 %643)
  %645 = trunc i64 %644 to i32
  %646 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %647 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %646, i64 0, i32 5
  store i32 %645, i32* %647, align 16
  %648 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %646, i64 0, i32 40
  %649 = load i64, i64* %648, align 8
  %650 = add i64 %649, 1496
  %651 = tail call i64 @ldq_phys(i64 %650)
  %652 = trunc i64 %651 to i32
  %653 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %654 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %653, i64 0, i32 0, i64 4
  store i32 %652, i32* %654, align 16
  %655 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %653, i64 0, i32 40
  %656 = load i64, i64* %655, align 8
  %657 = add i64 %656, 1528
  %658 = tail call i64 @ldq_phys(i64 %657)
  %659 = trunc i64 %658 to i32
  %660 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %661 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %660, i64 0, i32 0, i64 0
  store i32 %659, i32* %661, align 16
  %662 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %663 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %662, i64 0, i32 40
  %664 = load i64, i64* %663, align 8
  %665 = add i64 %664, 1376
  %666 = tail call i64 @ldq_phys(i64 %665)
  %667 = trunc i64 %666 to i32
  %668 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %669 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %668, i64 0, i32 61, i64 7
  store i32 %667, i32* %669, align 4
  %670 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %668, i64 0, i32 40
  %671 = load i64, i64* %670, align 8
  %672 = add i64 %671, 1384
  %673 = tail call i64 @ldq_phys(i64 %672)
  %674 = trunc i64 %673 to i32
  %675 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %676 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %675, i64 0, i32 61, i64 6
  store i32 %674, i32* %676, align 4
  %677 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %675, i64 0, i32 40
  %678 = load i64, i64* %677, align 8
  %679 = add i64 %678, 1227
  %680 = tail call i32 @ldub_phys(i64 %679)
  %681 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %682 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %683 = load i32, i32* %682, align 4
  %684 = icmp eq i32 %683, 0
  br i1 %684, label %685, label %687, !prof !2

; <label>:685:                                    ; preds = %635
  %686 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %681, i64 0, i32 8
  br label %693

; <label>:687:                                    ; preds = %635
  %688 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  %689 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %681, i64 0, i32 8
  %690 = load i32, i32* %689, align 4
  %691 = and i32 %690, 3
  tail call void %688(i32 %691, i32 %680)
  %692 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %693

; <label>:693:                                    ; preds = %685, %687
  %694 = phi %struct.CPUX86State* [ %681, %685 ], [ %692, %687 ]
  %695 = phi i32* [ %686, %685 ], [ %689, %687 ]
  %696 = load i32, i32* %695, align 4
  %697 = and i32 %696, -4
  %698 = or i32 %697, %680
  store i32 %698, i32* %695, align 4
  %699 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %694, i64 0, i32 40
  %700 = load i64, i64* %699, align 8
  %701 = add i64 %700, 92
  %702 = tail call i32 @ldub_phys(i64 %701)
  %703 = icmp eq i32 %702, 1
  br i1 %703, label %704, label %706

; <label>:704:                                    ; preds = %693
  %705 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  tail call void @tlb_flush(%struct.CPUX86State* %705, i32 1)
  br label %706

; <label>:706:                                    ; preds = %693, %704
  %707 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %708 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %707, i64 0, i32 9
  %709 = load i32, i32* %708, align 16
  %710 = or i32 %709, 1
  store i32 %710, i32* %708, align 16
  %711 = and i32 %331, 256
  %712 = icmp eq i32 %711, 0
  br i1 %712, label %717, label %713

; <label>:713:                                    ; preds = %706
  %714 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %707, i64 0, i32 75
  %715 = load i32, i32* %714, align 16
  %716 = or i32 %715, 256
  store i32 %716, i32* %714, align 16
  br label %717

; <label>:717:                                    ; preds = %706, %713
  %718 = load i32*, i32** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 16), align 8
  %719 = load i32, i32* %718, align 4
  %720 = icmp eq i32 %719, 0
  br i1 %720, label %724, label %721, !prof !2

; <label>:721:                                    ; preds = %717
  %722 = load void (i32, i32)*, void (i32, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 9, i32 19), align 8
  tail call void %722(i32 %6, i32 %680)
  %723 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %724

; <label>:724:                                    ; preds = %717, %721
  %725 = phi %struct.CPUX86State* [ %707, %717 ], [ %723, %721 ]
  %726 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %725, i64 0, i32 40
  %727 = load i64, i64* %726, align 8
  %728 = add i64 %727, 168
  %729 = tail call i32 @ldl_phys(i64 %728)
  %730 = icmp slt i32 %729, 0
  br i1 %730, label %731, label %825

; <label>:731:                                    ; preds = %724
  %732 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %733 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %732, i64 0, i32 40
  %734 = load i64, i64* %733, align 8
  %735 = add i64 %734, 172
  %736 = tail call i32 @ldl_phys(i64 %735)
  %737 = load i32, i32* @loglevel, align 4
  %738 = and i32 %737, 2
  %739 = icmp eq i32 %738, 0
  br i1 %739, label %744, label %740

; <label>:740:                                    ; preds = %731
  %741 = and i32 %729, 2048
  %742 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %743 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %742, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.7, i64 0, i64 0), i32 %741)
  br label %744

; <label>:744:                                    ; preds = %731, %740
  %745 = and i32 %729, 1792
  %746 = trunc i32 %745 to i11
  switch i11 %746, label %813 [
    i11 0, label %747
    i11 512, label %762
    i11 768, label %779
    i11 -1024, label %795
  ]

; <label>:747:                                    ; preds = %744
  %748 = and i32 %729, 255
  %749 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %750 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %749, i64 0, i32 92
  store i32 %748, i32* %750, align 8
  %751 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %749, i64 0, i32 58
  store i32 %736, i32* %751, align 16
  %752 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %749, i64 0, i32 59
  store i32 0, i32* %752, align 4
  %753 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %749, i64 0, i32 60
  store i32 -1, i32* %753, align 8
  %754 = load i32, i32* @loglevel, align 4
  %755 = and i32 %754, 2
  %756 = icmp eq i32 %755, 0
  br i1 %756, label %760, label %757

; <label>:757:                                    ; preds = %747
  %758 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %759 = tail call i64 @fwrite(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.8, i64 0, i64 0), i64 4, i64 1, %struct._IO_FILE* %758)
  br label %760

; <label>:760:                                    ; preds = %747, %757
  %761 = load void (i32, i32, i32, i64, i32)*, void (i32, i32, i32, i64, i32)** getelementptr inbounds (%struct.se_libcpu_interface_t, %struct.se_libcpu_interface_t* @g_sqi, i64 0, i32 2, i32 9), align 8
  tail call void %761(i32 %748, i32 0, i32 0, i64 0, i32 1)
  br label %813

; <label>:762:                                    ; preds = %744
  %763 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %764 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %763, i64 0, i32 92
  store i32 2, i32* %764, align 8
  %765 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %763, i64 0, i32 58
  store i32 %736, i32* %765, align 16
  %766 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %763, i64 0, i32 59
  store i32 0, i32* %766, align 4
  %767 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %763, i64 0, i32 5
  %768 = load i32, i32* %767, align 16
  %769 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %763, i64 0, i32 60
  store i32 %768, i32* %769, align 8
  %770 = load i32, i32* @loglevel, align 4
  %771 = and i32 %770, 2
  %772 = icmp eq i32 %771, 0
  br i1 %772, label %777, label %773

; <label>:773:                                    ; preds = %762
  %774 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %775 = tail call i64 @fwrite(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.9, i64 0, i64 0), i64 3, i64 1, %struct._IO_FILE* %774)
  %776 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %777

; <label>:777:                                    ; preds = %762, %773
  %778 = phi %struct.CPUX86State* [ %763, %762 ], [ %776, %773 ]
  tail call void @cpu_loop_exit(%struct.CPUX86State* %778) #12
  unreachable

; <label>:779:                                    ; preds = %744
  %780 = and i32 %729, 255
  %781 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %782 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %781, i64 0, i32 92
  store i32 %780, i32* %782, align 8
  %783 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %781, i64 0, i32 58
  store i32 %736, i32* %783, align 16
  %784 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %781, i64 0, i32 59
  store i32 0, i32* %784, align 4
  %785 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %781, i64 0, i32 60
  store i32 -1, i32* %785, align 8
  %786 = load i32, i32* @loglevel, align 4
  %787 = and i32 %786, 2
  %788 = icmp eq i32 %787, 0
  br i1 %788, label %793, label %789

; <label>:789:                                    ; preds = %779
  %790 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %791 = tail call i64 @fwrite(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.10, i64 0, i64 0), i64 5, i64 1, %struct._IO_FILE* %790)
  %792 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %793

; <label>:793:                                    ; preds = %779, %789
  %794 = phi %struct.CPUX86State* [ %781, %779 ], [ %792, %789 ]
  tail call void @cpu_loop_exit(%struct.CPUX86State* %794) #12
  unreachable

; <label>:795:                                    ; preds = %744
  %796 = and i32 %729, 255
  %797 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %798 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %797, i64 0, i32 92
  store i32 %796, i32* %798, align 8
  %799 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %797, i64 0, i32 58
  store i32 %736, i32* %799, align 16
  %800 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %797, i64 0, i32 59
  store i32 1, i32* %800, align 4
  %801 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %797, i64 0, i32 5
  %802 = load i32, i32* %801, align 16
  %803 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %797, i64 0, i32 60
  store i32 %802, i32* %803, align 8
  %804 = load i32, i32* @loglevel, align 4
  %805 = and i32 %804, 2
  %806 = icmp eq i32 %805, 0
  br i1 %806, label %811, label %807

; <label>:807:                                    ; preds = %795
  %808 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %809 = tail call i64 @fwrite(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.11, i64 0, i64 0), i64 4, i64 1, %struct._IO_FILE* %808)
  %810 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %811

; <label>:811:                                    ; preds = %795, %807
  %812 = phi %struct.CPUX86State* [ %797, %795 ], [ %810, %807 ]
  tail call void @cpu_loop_exit(%struct.CPUX86State* %812) #12
  unreachable

; <label>:813:                                    ; preds = %760, %744
  %814 = load i32, i32* @loglevel, align 4
  %815 = and i32 %814, 2
  %816 = icmp eq i32 %815, 0
  br i1 %816, label %825, label %817

; <label>:817:                                    ; preds = %813
  %818 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %819 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %820 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %819, i64 0, i32 92
  %821 = load i32, i32* %820, align 8
  %822 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %819, i64 0, i32 58
  %823 = load i32, i32* %822, align 16
  %824 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %818, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.12, i64 0, i64 0), i32 %821, i32 %823)
  br label %825

; <label>:825:                                    ; preds = %817, %813, %724
  ret void
}

declare void @tlb_flush(%struct.CPUX86State*, i32) local_unnamed_addr #6

; Function Attrs: noreturn uwtable
define void @helper_vmmcall() local_unnamed_addr #3 {
  tail call void @helper_svm_check_intercept_param(i32 129, i64 0)
  tail call fastcc void @raise_exception(i32 6) #12
  unreachable
}

; Function Attrs: uwtable
define void @helper_vmload(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 8
  %4 = load i32, i32* %3, align 4
  %5 = and i32 %4, 2097152
  %6 = icmp eq i32 %5, 0
  br i1 %6, label %13, label %7, !prof !2

; <label>:7:                                      ; preds = %1
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 42
  %9 = load i64, i64* %8, align 8
  %10 = and i64 %9, 17179869184
  %11 = icmp eq i64 %10, 0
  br i1 %11, label %13, label %12

; <label>:12:                                     ; preds = %7
  tail call void @helper_vmexit(i32 130, i64 0)
  unreachable

; <label>:13:                                     ; preds = %1, %7
  %14 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 0
  %15 = load i32, i32* %14, align 16
  %16 = load i32, i32* @loglevel, align 4
  %17 = and i32 %16, 2
  %18 = icmp eq i32 %17, 0
  br i1 %18, label %19, label %22

; <label>:19:                                     ; preds = %13
  %20 = zext i32 %15 to i64
  %21 = add nuw nsw i64 %20, 1096
  br label %32

; <label>:22:                                     ; preds = %13
  %23 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %24 = zext i32 %15 to i64
  %25 = add nuw nsw i64 %24, 1096
  %26 = tail call i64 @ldq_phys(i64 %25)
  %27 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %28 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 11, i64 4, i32 1
  %29 = load i32, i32* %28, align 4
  %30 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %23, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.13, i64 0, i64 0), i32 %15, i64 %26, i32 %29)
  %31 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %32

; <label>:32:                                     ; preds = %19, %22
  %33 = phi i64 [ %21, %19 ], [ %25, %22 ]
  %34 = phi i64 [ %20, %19 ], [ %24, %22 ]
  %35 = phi %struct.CPUX86State* [ %2, %19 ], [ %31, %22 ]
  %36 = add nuw nsw i64 %34, 1088
  %37 = tail call i32 @lduw_phys(i64 %36)
  %38 = tail call i64 @ldq_phys(i64 %33)
  %39 = trunc i64 %38 to i32
  %40 = add nuw nsw i64 %34, 1092
  %41 = tail call i32 @ldl_phys(i64 %40)
  %42 = add nuw nsw i64 %34, 1090
  %43 = tail call i32 @lduw_phys(i64 %42)
  %44 = shl i32 %43, 8
  %45 = and i32 %44, 65280
  %46 = shl i32 %43, 12
  %47 = and i32 %46, 15728640
  %48 = or i32 %45, %47
  %49 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 4, i32 0
  store i32 %37, i32* %49, align 4
  %50 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 4, i32 1
  store i32 %39, i32* %50, align 4
  %51 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 4, i32 2
  store i32 %41, i32* %51, align 4
  %52 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 4, i32 3
  store i32 %48, i32* %52, align 4
  %53 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 8
  %54 = load i32, i32* %53, align 4
  %55 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 2, i32 3
  %56 = load i32, i32* %55, align 4
  %57 = lshr i32 %56, 17
  %58 = and i32 %57, 32
  %59 = trunc i32 %54 to i16
  %60 = icmp slt i16 %59, 0
  br i1 %60, label %93, label %61

; <label>:61:                                     ; preds = %32
  %62 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 16, i64 0
  %63 = load i32, i32* %62, align 8
  %64 = and i32 %63, 1
  %65 = icmp eq i32 %64, 0
  br i1 %65, label %77, label %66

; <label>:66:                                     ; preds = %61
  %67 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 7
  %68 = bitcast i32* %67 to i64*
  %69 = load i64, i64* %68, align 8
  %70 = and i64 %69, 131072
  %71 = icmp ne i64 %70, 0
  %72 = and i32 %54, 16
  %73 = icmp eq i32 %72, 0
  %74 = or i1 %73, %71
  %75 = lshr i64 %69, 32
  %76 = trunc i64 %75 to i32
  br i1 %74, label %77, label %80

; <label>:77:                                     ; preds = %66, %61
  %78 = phi i32 [ %76, %66 ], [ %54, %61 ]
  %79 = or i32 %58, 64
  br label %93

; <label>:80:                                     ; preds = %66
  %81 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 3, i32 1
  %82 = load i32, i32* %81, align 4
  %83 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 0, i32 1
  %84 = load i32, i32* %83, align 4
  %85 = or i32 %84, %82
  %86 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 2, i32 1
  %87 = load i32, i32* %86, align 4
  %88 = or i32 %85, %87
  %89 = icmp ne i32 %88, 0
  %90 = zext i1 %89 to i32
  %91 = shl nuw nsw i32 %90, 6
  %92 = or i32 %91, %58
  br label %93

; <label>:93:                                     ; preds = %32, %77, %80
  %94 = phi i32 [ %54, %32 ], [ %78, %77 ], [ %76, %80 ]
  %95 = phi i32 [ %58, %32 ], [ %79, %77 ], [ %92, %80 ]
  %96 = and i32 %94, -97
  %97 = or i32 %96, %95
  store i32 %97, i32* %53, align 4
  %98 = add nuw nsw i64 %34, 1104
  %99 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %100 = tail call i32 @lduw_phys(i64 %98)
  %101 = add nuw nsw i64 %34, 1112
  %102 = tail call i64 @ldq_phys(i64 %101)
  %103 = trunc i64 %102 to i32
  %104 = add nuw nsw i64 %34, 1108
  %105 = tail call i32 @ldl_phys(i64 %104)
  %106 = add nuw nsw i64 %34, 1106
  %107 = tail call i32 @lduw_phys(i64 %106)
  %108 = shl i32 %107, 8
  %109 = and i32 %108, 65280
  %110 = shl i32 %107, 12
  %111 = and i32 %110, 15728640
  %112 = or i32 %109, %111
  %113 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %99, i64 0, i32 11, i64 5, i32 0
  store i32 %100, i32* %113, align 4
  %114 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %99, i64 0, i32 11, i64 5, i32 1
  store i32 %103, i32* %114, align 4
  %115 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %99, i64 0, i32 11, i64 5, i32 2
  store i32 %105, i32* %115, align 4
  %116 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %99, i64 0, i32 11, i64 5, i32 3
  store i32 %112, i32* %116, align 4
  %117 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %99, i64 0, i32 8
  %118 = load i32, i32* %117, align 4
  %119 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %99, i64 0, i32 11, i64 2, i32 3
  %120 = load i32, i32* %119, align 4
  %121 = lshr i32 %120, 17
  %122 = and i32 %121, 32
  %123 = trunc i32 %118 to i16
  %124 = icmp slt i16 %123, 0
  br i1 %124, label %157, label %125

; <label>:125:                                    ; preds = %93
  %126 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %99, i64 0, i32 16, i64 0
  %127 = load i32, i32* %126, align 8
  %128 = and i32 %127, 1
  %129 = icmp eq i32 %128, 0
  br i1 %129, label %141, label %130

; <label>:130:                                    ; preds = %125
  %131 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %99, i64 0, i32 7
  %132 = bitcast i32* %131 to i64*
  %133 = load i64, i64* %132, align 8
  %134 = and i64 %133, 131072
  %135 = icmp ne i64 %134, 0
  %136 = and i32 %118, 16
  %137 = icmp eq i32 %136, 0
  %138 = or i1 %137, %135
  %139 = lshr i64 %133, 32
  %140 = trunc i64 %139 to i32
  br i1 %138, label %141, label %144

; <label>:141:                                    ; preds = %130, %125
  %142 = phi i32 [ %140, %130 ], [ %118, %125 ]
  %143 = or i32 %122, 64
  br label %157

; <label>:144:                                    ; preds = %130
  %145 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %99, i64 0, i32 11, i64 3, i32 1
  %146 = load i32, i32* %145, align 4
  %147 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %99, i64 0, i32 11, i64 0, i32 1
  %148 = load i32, i32* %147, align 4
  %149 = or i32 %148, %146
  %150 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %99, i64 0, i32 11, i64 2, i32 1
  %151 = load i32, i32* %150, align 4
  %152 = or i32 %149, %151
  %153 = icmp ne i32 %152, 0
  %154 = zext i1 %153 to i32
  %155 = shl nuw nsw i32 %154, 6
  %156 = or i32 %155, %122
  br label %157

; <label>:157:                                    ; preds = %93, %141, %144
  %158 = phi i32 [ %118, %93 ], [ %142, %141 ], [ %140, %144 ]
  %159 = phi i32 [ %122, %93 ], [ %143, %141 ], [ %156, %144 ]
  %160 = and i32 %158, -97
  %161 = or i32 %160, %159
  store i32 %161, i32* %117, align 4
  %162 = add nuw nsw i64 %34, 1168
  %163 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %164 = tail call i32 @lduw_phys(i64 %162)
  %165 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %163, i64 0, i32 13, i32 0
  store i32 %164, i32* %165, align 4
  %166 = add nuw nsw i64 %34, 1176
  %167 = tail call i64 @ldq_phys(i64 %166)
  %168 = trunc i64 %167 to i32
  %169 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %163, i64 0, i32 13, i32 1
  store i32 %168, i32* %169, align 4
  %170 = add nuw nsw i64 %34, 1172
  %171 = tail call i32 @ldl_phys(i64 %170)
  %172 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %163, i64 0, i32 13, i32 2
  store i32 %171, i32* %172, align 4
  %173 = add nuw nsw i64 %34, 1170
  %174 = tail call i32 @lduw_phys(i64 %173)
  %175 = shl i32 %174, 8
  %176 = and i32 %175, 65280
  %177 = shl i32 %174, 12
  %178 = and i32 %177, 15728640
  %179 = or i32 %176, %178
  %180 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %163, i64 0, i32 13, i32 3
  store i32 %179, i32* %180, align 4
  %181 = add nuw nsw i64 %34, 1136
  %182 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %183 = tail call i32 @lduw_phys(i64 %181)
  %184 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %182, i64 0, i32 12, i32 0
  store i32 %183, i32* %184, align 4
  %185 = add nuw nsw i64 %34, 1144
  %186 = tail call i64 @ldq_phys(i64 %185)
  %187 = trunc i64 %186 to i32
  %188 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %182, i64 0, i32 12, i32 1
  store i32 %187, i32* %188, align 4
  %189 = add nuw nsw i64 %34, 1140
  %190 = tail call i32 @ldl_phys(i64 %189)
  %191 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %182, i64 0, i32 12, i32 2
  store i32 %190, i32* %191, align 4
  %192 = add nuw nsw i64 %34, 1138
  %193 = tail call i32 @lduw_phys(i64 %192)
  %194 = shl i32 %193, 8
  %195 = and i32 %194, 65280
  %196 = shl i32 %193, 12
  %197 = and i32 %196, 15728640
  %198 = or i32 %195, %197
  %199 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %182, i64 0, i32 12, i32 3
  store i32 %198, i32* %199, align 4
  %200 = add nuw nsw i64 %34, 1536
  %201 = tail call i64 @ldq_phys(i64 %200)
  %202 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %203 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %202, i64 0, i32 38
  store i64 %201, i64* %203, align 8
  %204 = add nuw nsw i64 %34, 1576
  %205 = tail call i64 @ldq_phys(i64 %204)
  %206 = trunc i64 %205 to i32
  %207 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %208 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %207, i64 0, i32 34
  store i32 %206, i32* %208, align 16
  %209 = add nuw nsw i64 %34, 1584
  %210 = tail call i64 @ldq_phys(i64 %209)
  %211 = trunc i64 %210 to i32
  %212 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %213 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %212, i64 0, i32 35
  store i32 %211, i32* %213, align 4
  %214 = add nuw nsw i64 %34, 1592
  %215 = tail call i64 @ldq_phys(i64 %214)
  %216 = trunc i64 %215 to i32
  %217 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %218 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %217, i64 0, i32 36
  store i32 %216, i32* %218, align 8
  ret void
}

; Function Attrs: uwtable
define void @helper_vmsave(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 8
  %4 = load i32, i32* %3, align 4
  %5 = and i32 %4, 2097152
  %6 = icmp eq i32 %5, 0
  br i1 %6, label %13, label %7, !prof !2

; <label>:7:                                      ; preds = %1
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 42
  %9 = load i64, i64* %8, align 8
  %10 = and i64 %9, 34359738368
  %11 = icmp eq i64 %10, 0
  br i1 %11, label %13, label %12

; <label>:12:                                     ; preds = %7
  tail call void @helper_vmexit(i32 131, i64 0)
  unreachable

; <label>:13:                                     ; preds = %1, %7
  %14 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 0
  %15 = load i32, i32* %14, align 16
  %16 = load i32, i32* @loglevel, align 4
  %17 = and i32 %16, 2
  %18 = icmp eq i32 %17, 0
  br i1 %18, label %19, label %22

; <label>:19:                                     ; preds = %13
  %20 = zext i32 %15 to i64
  %21 = add nuw nsw i64 %20, 1096
  br label %32

; <label>:22:                                     ; preds = %13
  %23 = load %struct._IO_FILE*, %struct._IO_FILE** @logfile, align 8
  %24 = zext i32 %15 to i64
  %25 = add nuw nsw i64 %24, 1096
  %26 = tail call i64 @ldq_phys(i64 %25)
  %27 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %28 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %27, i64 0, i32 11, i64 4, i32 1
  %29 = load i32, i32* %28, align 4
  %30 = tail call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %23, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.14, i64 0, i64 0), i32 %15, i64 %26, i32 %29)
  %31 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br label %32

; <label>:32:                                     ; preds = %19, %22
  %33 = phi i64 [ %21, %19 ], [ %25, %22 ]
  %34 = phi i64 [ %20, %19 ], [ %24, %22 ]
  %35 = phi %struct.CPUX86State* [ %2, %19 ], [ %31, %22 ]
  %36 = add nuw nsw i64 %34, 1088
  %37 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 4, i32 0
  %38 = load i32, i32* %37, align 4
  tail call void @stw_phys(i64 %36, i32 %38)
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 4, i32 1
  %40 = load i32, i32* %39, align 4
  %41 = zext i32 %40 to i64
  tail call void @stq_phys(i64 %33, i64 %41)
  %42 = add nuw nsw i64 %34, 1092
  %43 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 4, i32 2
  %44 = load i32, i32* %43, align 4
  tail call void @stl_phys(i64 %42, i32 %44)
  %45 = add nuw nsw i64 %34, 1090
  %46 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %35, i64 0, i32 11, i64 4, i32 3
  %47 = load i32, i32* %46, align 4
  %48 = lshr i32 %47, 8
  %49 = and i32 %48, 255
  %50 = lshr i32 %47, 12
  %51 = and i32 %50, 3840
  %52 = or i32 %49, %51
  tail call void @stw_phys(i64 %45, i32 %52)
  %53 = add nuw nsw i64 %34, 1104
  %54 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %55 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %54, i64 0, i32 11, i64 5, i32 0
  %56 = load i32, i32* %55, align 4
  tail call void @stw_phys(i64 %53, i32 %56)
  %57 = add nuw nsw i64 %34, 1112
  %58 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %54, i64 0, i32 11, i64 5, i32 1
  %59 = load i32, i32* %58, align 4
  %60 = zext i32 %59 to i64
  tail call void @stq_phys(i64 %57, i64 %60)
  %61 = add nuw nsw i64 %34, 1108
  %62 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %54, i64 0, i32 11, i64 5, i32 2
  %63 = load i32, i32* %62, align 4
  tail call void @stl_phys(i64 %61, i32 %63)
  %64 = add nuw nsw i64 %34, 1106
  %65 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %54, i64 0, i32 11, i64 5, i32 3
  %66 = load i32, i32* %65, align 4
  %67 = lshr i32 %66, 8
  %68 = and i32 %67, 255
  %69 = lshr i32 %66, 12
  %70 = and i32 %69, 3840
  %71 = or i32 %68, %70
  tail call void @stw_phys(i64 %64, i32 %71)
  %72 = add nuw nsw i64 %34, 1168
  %73 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %74 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %73, i64 0, i32 13, i32 0
  %75 = load i32, i32* %74, align 4
  tail call void @stw_phys(i64 %72, i32 %75)
  %76 = add nuw nsw i64 %34, 1176
  %77 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %73, i64 0, i32 13, i32 1
  %78 = load i32, i32* %77, align 4
  %79 = zext i32 %78 to i64
  tail call void @stq_phys(i64 %76, i64 %79)
  %80 = add nuw nsw i64 %34, 1172
  %81 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %73, i64 0, i32 13, i32 2
  %82 = load i32, i32* %81, align 4
  tail call void @stl_phys(i64 %80, i32 %82)
  %83 = add nuw nsw i64 %34, 1170
  %84 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %73, i64 0, i32 13, i32 3
  %85 = load i32, i32* %84, align 4
  %86 = lshr i32 %85, 8
  %87 = and i32 %86, 255
  %88 = lshr i32 %85, 12
  %89 = and i32 %88, 3840
  %90 = or i32 %87, %89
  tail call void @stw_phys(i64 %83, i32 %90)
  %91 = add nuw nsw i64 %34, 1136
  %92 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %93 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %92, i64 0, i32 12, i32 0
  %94 = load i32, i32* %93, align 4
  tail call void @stw_phys(i64 %91, i32 %94)
  %95 = add nuw nsw i64 %34, 1144
  %96 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %92, i64 0, i32 12, i32 1
  %97 = load i32, i32* %96, align 4
  %98 = zext i32 %97 to i64
  tail call void @stq_phys(i64 %95, i64 %98)
  %99 = add nuw nsw i64 %34, 1140
  %100 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %92, i64 0, i32 12, i32 2
  %101 = load i32, i32* %100, align 4
  tail call void @stl_phys(i64 %99, i32 %101)
  %102 = add nuw nsw i64 %34, 1138
  %103 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %92, i64 0, i32 12, i32 3
  %104 = load i32, i32* %103, align 4
  %105 = lshr i32 %104, 8
  %106 = and i32 %105, 255
  %107 = lshr i32 %104, 12
  %108 = and i32 %107, 3840
  %109 = or i32 %106, %108
  tail call void @stw_phys(i64 %102, i32 %109)
  %110 = add nuw nsw i64 %34, 1536
  %111 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %112 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %111, i64 0, i32 38
  %113 = load i64, i64* %112, align 8
  tail call void @stq_phys(i64 %110, i64 %113)
  %114 = add nuw nsw i64 %34, 1576
  %115 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %116 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %115, i64 0, i32 34
  %117 = load i32, i32* %116, align 16
  %118 = zext i32 %117 to i64
  tail call void @stq_phys(i64 %114, i64 %118)
  %119 = add nuw nsw i64 %34, 1584
  %120 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %121 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %120, i64 0, i32 35
  %122 = load i32, i32* %121, align 4
  %123 = zext i32 %122 to i64
  tail call void @stq_phys(i64 %119, i64 %123)
  %124 = add nuw nsw i64 %34, 1592
  %125 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %126 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %125, i64 0, i32 36
  %127 = load i32, i32* %126, align 8
  %128 = zext i32 %127 to i64
  tail call void @stq_phys(i64 %124, i64 %128)
  ret void
}

; Function Attrs: uwtable
define void @helper_stgi() local_unnamed_addr #2 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 8
  %3 = load i32, i32* %2, align 4
  %4 = and i32 %3, 2097152
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %12, label %6, !prof !2

; <label>:6:                                      ; preds = %0
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 42
  %8 = load i64, i64* %7, align 8
  %9 = and i64 %8, 68719476736
  %10 = icmp eq i64 %9, 0
  br i1 %10, label %12, label %11

; <label>:11:                                     ; preds = %6
  tail call void @helper_vmexit(i32 132, i64 0)
  unreachable

; <label>:12:                                     ; preds = %0, %6
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 9
  %14 = load i32, i32* %13, align 16
  %15 = or i32 %14, 1
  store i32 %15, i32* %13, align 16
  ret void
}

; Function Attrs: uwtable
define void @helper_clgi() local_unnamed_addr #2 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 8
  %3 = load i32, i32* %2, align 4
  %4 = and i32 %3, 2097152
  %5 = icmp eq i32 %4, 0
  br i1 %5, label %12, label %6, !prof !2

; <label>:6:                                      ; preds = %0
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 42
  %8 = load i64, i64* %7, align 8
  %9 = and i64 %8, 137438953472
  %10 = icmp eq i64 %9, 0
  br i1 %10, label %12, label %11

; <label>:11:                                     ; preds = %6
  tail call void @helper_vmexit(i32 133, i64 0)
  unreachable

; <label>:12:                                     ; preds = %0, %6
  %13 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 9
  %14 = load i32, i32* %13, align 16
  %15 = and i32 %14, -2
  store i32 %15, i32* %13, align 16
  ret void
}

; Function Attrs: noreturn uwtable
define void @helper_skinit() local_unnamed_addr #3 {
  tail call void @helper_svm_check_intercept_param(i32 134, i64 0)
  tail call fastcc void @raise_exception(i32 6) #12
  unreachable
}

; Function Attrs: uwtable
define void @helper_invlpga(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 8
  %4 = load i32, i32* %3, align 4
  %5 = and i32 %4, 2097152
  %6 = icmp eq i32 %5, 0
  br i1 %6, label %13, label %7, !prof !2

; <label>:7:                                      ; preds = %1
  %8 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 42
  %9 = load i64, i64* %8, align 8
  %10 = and i64 %9, 67108864
  %11 = icmp eq i64 %10, 0
  br i1 %11, label %13, label %12

; <label>:12:                                     ; preds = %7
  tail call void @helper_vmexit(i32 122, i64 0)
  unreachable

; <label>:13:                                     ; preds = %1, %7
  %14 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 0, i64 0
  %15 = load i32, i32* %14, align 16
  tail call void @tlb_flush_page(%struct.CPUX86State* %2, i32 %15)
  ret void
}

; Function Attrs: uwtable
define void @svm_check_intercept(%struct.CPUX86State*, i32) local_unnamed_addr #2 {
  %3 = load i64, i64* bitcast (%struct.CPUX86State** @env to i64*), align 8
  store %struct.CPUX86State* %0, %struct.CPUX86State** @env, align 8
  tail call void @helper_svm_check_intercept_param(i32 %1, i64 0)
  store i64 %3, i64* bitcast (%struct.CPUX86State** @env to i64*), align 8
  ret void
}

; Function Attrs: uwtable
define void @helper_svm_check_io(i32, i32, i32) local_unnamed_addr #2 {
  %4 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 42
  %6 = load i64, i64* %5, align 8
  %7 = and i64 %6, 134217728
  %8 = icmp eq i64 %7, 0
  br i1 %8, label %39, label %9

; <label>:9:                                      ; preds = %3
  %10 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %4, i64 0, i32 40
  %11 = load i64, i64* %10, align 8
  %12 = add i64 %11, 64
  %13 = tail call i64 @ldq_phys(i64 %12)
  %14 = lshr i32 %1, 4
  %15 = and i32 %14, 7
  %16 = shl i32 1, %15
  %17 = add nuw nsw i32 %16, 65535
  %18 = lshr i32 %0, 3
  %19 = zext i32 %18 to i64
  %20 = add i64 %13, %19
  %21 = tail call i32 @lduw_phys(i64 %20)
  %22 = and i32 %17, 65535
  %23 = and i32 %0, 7
  %24 = shl i32 %22, %23
  %25 = and i32 %21, %24
  %26 = icmp eq i32 %25, 0
  br i1 %26, label %39, label %27

; <label>:27:                                     ; preds = %9
  %28 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %29 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %28, i64 0, i32 40
  %30 = load i64, i64* %29, align 8
  %31 = add i64 %30, 128
  %32 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %28, i64 0, i32 5
  %33 = load i32, i32* %32, align 16
  %34 = add i32 %33, %2
  %35 = zext i32 %34 to i64
  tail call void @stq_phys(i64 %31, i64 %35)
  %36 = shl i32 %0, 16
  %37 = or i32 %36, %1
  %38 = zext i32 %37 to i64
  tail call void @helper_vmexit(i32 123, i64 %38)
  unreachable

; <label>:39:                                     ; preds = %9, %3
  ret void
}

; Function Attrs: uwtable
define void @helper_ldmxcsr(i32) local_unnamed_addr #2 {
  %2 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 30
  store i32 %0, i32* %3, align 16
  %4 = and i32 %0, 24576
  %5 = trunc i32 %4 to i15
  switch i15 %5, label %6 [
    i15 0, label %10
    i15 8192, label %7
    i15 -16384, label %8
    i15 -8192, label %9
  ]

; <label>:6:                                      ; preds = %1
  unreachable

; <label>:7:                                      ; preds = %1
  br label %10

; <label>:8:                                      ; preds = %1
  br label %10

; <label>:9:                                      ; preds = %1
  br label %10

; <label>:10:                                     ; preds = %1, %7, %8, %9
  %11 = phi i32 [ 3, %9 ], [ 2, %8 ], [ 1, %7 ], [ 0, %1 ]
  %12 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %2, i64 0, i32 29
  tail call void @set_float_rounding_mode(i32 %11, %struct.float_status* %12)
  %13 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %14 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %13, i64 0, i32 30
  %15 = load i32, i32* %14, align 16
  %16 = lshr i32 %15, 6
  %17 = trunc i32 %16 to i8
  %18 = and i8 %17, 1
  %19 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %13, i64 0, i32 29, i32 5
  store i8 %18, i8* %19, align 1
  %20 = trunc i32 %15 to i16
  %21 = lshr i16 %20, 15
  %22 = trunc i16 %21 to i8
  %23 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %13, i64 0, i32 26, i32 4
  store i8 %22, i8* %23, align 1
  ret void
}

declare void @set_float_rounding_mode(i32, %struct.float_status*) local_unnamed_addr #6

; Function Attrs: norecurse nounwind uwtable
define void @helper_enter_mmx() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 18
  store i32 0, i32* %2, align 16
  %3 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 21, i64 0
  %4 = bitcast i8* %3 to i32*
  store i32 0, i32* %4, align 8
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 21, i64 4
  %6 = bitcast i8* %5 to i32*
  store i32 0, i32* %6, align 4
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_emms() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 21, i64 0
  %3 = bitcast i8* %2 to i32*
  store i32 16843009, i32* %3, align 8
  %4 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 21, i64 4
  %5 = bitcast i8* %4 to i32*
  store i32 16843009, i32* %5, align 4
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @helper_movq(i8* nocapture, i8* nocapture readonly) local_unnamed_addr #1 {
  %3 = bitcast i8* %1 to i64*
  %4 = load i64, i64* %3, align 8
  %5 = bitcast i8* %0 to i64*
  store i64 %4, i64* %5, align 8
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define i32 @helper_rclb(i32, i32) local_unnamed_addr #1 {
  %3 = and i32 %1, 31
  %4 = zext i32 %3 to i64
  %5 = getelementptr inbounds [32 x i8], [32 x i8]* @rclb_table, i64 0, i64 %4
  %6 = load i8, i8* %5, align 1
  %7 = zext i8 %6 to i32
  %8 = icmp eq i8 %6, 0
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br i1 %8, label %37, label %10

; <label>:10:                                     ; preds = %2
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 1
  %12 = load i32, i32* %11, align 16
  %13 = tail call i32 @helper_cc_compute_all(i32 %12)
  %14 = and i32 %0, 255
  %15 = shl i32 %14, %7
  %16 = and i32 %13, 1
  %17 = add nsw i32 %7, -1
  %18 = shl i32 %16, %17
  %19 = or i32 %18, %15
  %20 = icmp eq i8 %6, 1
  br i1 %20, label %25, label %21

; <label>:21:                                     ; preds = %10
  %22 = sub nsw i32 9, %7
  %23 = lshr i32 %14, %22
  %24 = or i32 %19, %23
  br label %25

; <label>:25:                                     ; preds = %10, %21
  %26 = phi i32 [ %24, %21 ], [ %19, %10 ]
  %27 = and i32 %13, -2050
  %28 = xor i32 %26, %0
  %29 = shl i32 %28, 4
  %30 = and i32 %29, 2048
  %31 = sub nsw i32 8, %7
  %32 = lshr i32 %14, %31
  %33 = and i32 %32, 1
  %34 = or i32 %27, %33
  %35 = or i32 %34, %30
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 4
  store i32 %35, i32* %36, align 4
  br label %39

; <label>:37:                                     ; preds = %2
  %38 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 4
  store i32 -1, i32* %38, align 4
  br label %39

; <label>:39:                                     ; preds = %37, %25
  %40 = phi i32 [ %26, %25 ], [ %0, %37 ]
  ret i32 %40
}

; Function Attrs: norecurse nounwind uwtable
define i32 @helper_rcrb(i32, i32) local_unnamed_addr #1 {
  %3 = and i32 %1, 31
  %4 = zext i32 %3 to i64
  %5 = getelementptr inbounds [32 x i8], [32 x i8]* @rclb_table, i64 0, i64 %4
  %6 = load i8, i8* %5, align 1
  %7 = zext i8 %6 to i32
  %8 = icmp eq i8 %6, 0
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br i1 %8, label %37, label %10

; <label>:10:                                     ; preds = %2
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 1
  %12 = load i32, i32* %11, align 16
  %13 = tail call i32 @helper_cc_compute_all(i32 %12)
  %14 = and i32 %0, 255
  %15 = lshr i32 %14, %7
  %16 = and i32 %13, 1
  %17 = sub nsw i32 8, %7
  %18 = shl i32 %16, %17
  %19 = or i32 %18, %15
  %20 = icmp eq i8 %6, 1
  br i1 %20, label %25, label %21

; <label>:21:                                     ; preds = %10
  %22 = sub nsw i32 9, %7
  %23 = shl i32 %14, %22
  %24 = or i32 %19, %23
  br label %25

; <label>:25:                                     ; preds = %10, %21
  %26 = phi i32 [ %24, %21 ], [ %19, %10 ]
  %27 = and i32 %13, -2050
  %28 = xor i32 %26, %0
  %29 = shl i32 %28, 4
  %30 = and i32 %29, 2048
  %31 = add nsw i32 %7, -1
  %32 = lshr i32 %14, %31
  %33 = and i32 %32, 1
  %34 = or i32 %27, %33
  %35 = or i32 %34, %30
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 4
  store i32 %35, i32* %36, align 4
  br label %39

; <label>:37:                                     ; preds = %2
  %38 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 4
  store i32 -1, i32* %38, align 4
  br label %39

; <label>:39:                                     ; preds = %37, %25
  %40 = phi i32 [ %26, %25 ], [ %0, %37 ]
  ret i32 %40
}

; Function Attrs: norecurse nounwind uwtable
define i32 @helper_rclw(i32, i32) local_unnamed_addr #1 {
  %3 = and i32 %1, 31
  %4 = zext i32 %3 to i64
  %5 = getelementptr inbounds [32 x i8], [32 x i8]* @rclw_table, i64 0, i64 %4
  %6 = load i8, i8* %5, align 1
  %7 = zext i8 %6 to i32
  %8 = icmp eq i8 %6, 0
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br i1 %8, label %37, label %10

; <label>:10:                                     ; preds = %2
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 1
  %12 = load i32, i32* %11, align 16
  %13 = tail call i32 @helper_cc_compute_all(i32 %12)
  %14 = and i32 %0, 65535
  %15 = shl i32 %14, %7
  %16 = and i32 %13, 1
  %17 = add nsw i32 %7, -1
  %18 = shl i32 %16, %17
  %19 = or i32 %18, %15
  %20 = icmp eq i8 %6, 1
  br i1 %20, label %25, label %21

; <label>:21:                                     ; preds = %10
  %22 = sub nsw i32 17, %7
  %23 = lshr i32 %14, %22
  %24 = or i32 %19, %23
  br label %25

; <label>:25:                                     ; preds = %10, %21
  %26 = phi i32 [ %24, %21 ], [ %19, %10 ]
  %27 = and i32 %13, -2050
  %28 = xor i32 %26, %0
  %29 = lshr i32 %28, 4
  %30 = and i32 %29, 2048
  %31 = sub nsw i32 16, %7
  %32 = lshr i32 %14, %31
  %33 = and i32 %32, 1
  %34 = or i32 %27, %33
  %35 = or i32 %34, %30
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 4
  store i32 %35, i32* %36, align 4
  br label %39

; <label>:37:                                     ; preds = %2
  %38 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 4
  store i32 -1, i32* %38, align 4
  br label %39

; <label>:39:                                     ; preds = %37, %25
  %40 = phi i32 [ %26, %25 ], [ %0, %37 ]
  ret i32 %40
}

; Function Attrs: norecurse nounwind uwtable
define i32 @helper_rcrw(i32, i32) local_unnamed_addr #1 {
  %3 = and i32 %1, 31
  %4 = zext i32 %3 to i64
  %5 = getelementptr inbounds [32 x i8], [32 x i8]* @rclw_table, i64 0, i64 %4
  %6 = load i8, i8* %5, align 1
  %7 = zext i8 %6 to i32
  %8 = icmp eq i8 %6, 0
  %9 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br i1 %8, label %37, label %10

; <label>:10:                                     ; preds = %2
  %11 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 1
  %12 = load i32, i32* %11, align 16
  %13 = tail call i32 @helper_cc_compute_all(i32 %12)
  %14 = and i32 %0, 65535
  %15 = lshr i32 %14, %7
  %16 = and i32 %13, 1
  %17 = sub nsw i32 16, %7
  %18 = shl i32 %16, %17
  %19 = or i32 %18, %15
  %20 = icmp eq i8 %6, 1
  br i1 %20, label %25, label %21

; <label>:21:                                     ; preds = %10
  %22 = sub nsw i32 17, %7
  %23 = shl i32 %14, %22
  %24 = or i32 %19, %23
  br label %25

; <label>:25:                                     ; preds = %10, %21
  %26 = phi i32 [ %24, %21 ], [ %19, %10 ]
  %27 = and i32 %13, -2050
  %28 = xor i32 %26, %0
  %29 = lshr i32 %28, 4
  %30 = and i32 %29, 2048
  %31 = add nsw i32 %7, -1
  %32 = lshr i32 %14, %31
  %33 = and i32 %32, 1
  %34 = or i32 %27, %33
  %35 = or i32 %34, %30
  %36 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 4
  store i32 %35, i32* %36, align 4
  br label %39

; <label>:37:                                     ; preds = %2
  %38 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %9, i64 0, i32 4
  store i32 -1, i32* %38, align 4
  br label %39

; <label>:39:                                     ; preds = %37, %25
  %40 = phi i32 [ %26, %25 ], [ %0, %37 ]
  ret i32 %40
}

; Function Attrs: norecurse nounwind uwtable
define i32 @helper_rcll(i32, i32) local_unnamed_addr #1 {
  %3 = and i32 %1, 31
  %4 = icmp eq i32 %3, 0
  %5 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br i1 %4, label %32, label %6

; <label>:6:                                      ; preds = %2
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 1
  %8 = load i32, i32* %7, align 16
  %9 = tail call i32 @helper_cc_compute_all(i32 %8)
  %10 = shl i32 %0, %3
  %11 = and i32 %9, 1
  %12 = add nsw i32 %3, -1
  %13 = shl i32 %11, %12
  %14 = or i32 %13, %10
  %15 = icmp eq i32 %3, 1
  br i1 %15, label %20, label %16

; <label>:16:                                     ; preds = %6
  %17 = sub nsw i32 33, %3
  %18 = lshr i32 %0, %17
  %19 = or i32 %14, %18
  br label %20

; <label>:20:                                     ; preds = %6, %16
  %21 = phi i32 [ %19, %16 ], [ %14, %6 ]
  %22 = and i32 %9, -2050
  %23 = xor i32 %21, %0
  %24 = lshr i32 %23, 20
  %25 = and i32 %24, 2048
  %26 = sub nsw i32 32, %3
  %27 = lshr i32 %0, %26
  %28 = and i32 %27, 1
  %29 = or i32 %22, %28
  %30 = or i32 %29, %25
  %31 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 4
  store i32 %30, i32* %31, align 4
  br label %34

; <label>:32:                                     ; preds = %2
  %33 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 4
  store i32 -1, i32* %33, align 4
  br label %34

; <label>:34:                                     ; preds = %32, %20
  %35 = phi i32 [ %21, %20 ], [ %0, %32 ]
  ret i32 %35
}

; Function Attrs: norecurse nounwind uwtable
define i32 @helper_rcrl(i32, i32) local_unnamed_addr #1 {
  %3 = and i32 %1, 31
  %4 = icmp eq i32 %3, 0
  %5 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  br i1 %4, label %32, label %6

; <label>:6:                                      ; preds = %2
  %7 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 1
  %8 = load i32, i32* %7, align 16
  %9 = tail call i32 @helper_cc_compute_all(i32 %8)
  %10 = lshr i32 %0, %3
  %11 = and i32 %9, 1
  %12 = sub nsw i32 32, %3
  %13 = shl i32 %11, %12
  %14 = or i32 %13, %10
  %15 = icmp eq i32 %3, 1
  br i1 %15, label %20, label %16

; <label>:16:                                     ; preds = %6
  %17 = sub nsw i32 33, %3
  %18 = shl i32 %0, %17
  %19 = or i32 %14, %18
  br label %20

; <label>:20:                                     ; preds = %6, %16
  %21 = phi i32 [ %19, %16 ], [ %14, %6 ]
  %22 = and i32 %9, -2050
  %23 = xor i32 %21, %0
  %24 = lshr i32 %23, 20
  %25 = and i32 %24, 2048
  %26 = add nsw i32 %3, -1
  %27 = lshr i32 %0, %26
  %28 = and i32 %27, 1
  %29 = or i32 %22, %28
  %30 = or i32 %29, %25
  %31 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 4
  store i32 %30, i32* %31, align 4
  br label %34

; <label>:32:                                     ; preds = %2
  %33 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %5, i64 0, i32 4
  store i32 -1, i32* %33, align 4
  br label %34

; <label>:34:                                     ; preds = %32, %20
  %35 = phi i32 [ %21, %20 ], [ %0, %32 ]
  ret i32 %35
}

; Function Attrs: norecurse nounwind readnone uwtable
define i32 @helper_bsf(i32) local_unnamed_addr #10 {
  %2 = and i32 %0, 1
  %3 = icmp eq i32 %2, 0
  br i1 %3, label %4, label %11

; <label>:4:                                      ; preds = %1, %4
  %5 = phi i32 [ %8, %4 ], [ %0, %1 ]
  %6 = phi i32 [ %7, %4 ], [ 0, %1 ]
  %7 = add nuw nsw i32 %6, 1
  %8 = lshr i32 %5, 1
  %9 = and i32 %8, 1
  %10 = icmp eq i32 %9, 0
  br i1 %10, label %4, label %11

; <label>:11:                                     ; preds = %4, %1
  %12 = phi i32 [ 0, %1 ], [ %7, %4 ]
  ret i32 %12
}

; Function Attrs: norecurse nounwind readnone uwtable
define i32 @helper_lzcnt(i32, i32) local_unnamed_addr #10 {
  %3 = icmp sgt i32 %1, 0
  %4 = icmp eq i32 %0, 0
  %5 = and i1 %4, %3
  br i1 %5, label %19, label %6

; <label>:6:                                      ; preds = %2
  %7 = icmp sgt i32 %0, -1
  br i1 %7, label %8, label %14

; <label>:8:                                      ; preds = %6, %8
  %9 = phi i32 [ %12, %8 ], [ %0, %6 ]
  %10 = phi i32 [ %11, %8 ], [ 31, %6 ]
  %11 = add nsw i32 %10, -1
  %12 = shl i32 %9, 1
  %13 = icmp sgt i32 %12, -1
  br i1 %13, label %8, label %14

; <label>:14:                                     ; preds = %8, %6
  %15 = phi i32 [ 31, %6 ], [ %11, %8 ]
  br i1 %3, label %16, label %19

; <label>:16:                                     ; preds = %14
  %17 = add nsw i32 %1, -1
  %18 = sub i32 %17, %15
  br label %19

; <label>:19:                                     ; preds = %14, %2, %16
  %20 = phi i32 [ %18, %16 ], [ %1, %2 ], [ %15, %14 ]
  ret i32 %20
}

; Function Attrs: norecurse nounwind readnone uwtable
define i32 @helper_bsr(i32) local_unnamed_addr #10 {
  %2 = icmp sgt i32 %0, -1
  br i1 %2, label %3, label %9

; <label>:3:                                      ; preds = %1, %3
  %4 = phi i32 [ %7, %3 ], [ %0, %1 ]
  %5 = phi i32 [ %6, %3 ], [ 31, %1 ]
  %6 = add nsw i32 %5, -1
  %7 = shl i32 %4, 1
  %8 = icmp sgt i32 %7, -1
  br i1 %8, label %3, label %9

; <label>:9:                                      ; preds = %3, %1
  %10 = phi i32 [ 31, %1 ], [ %6, %3 ]
  ret i32 %10
}

; Function Attrs: norecurse nounwind uwtable
define i32 @cpu_cc_compute_all(%struct.CPUX86State*, i32) local_unnamed_addr #1 {
  %3 = load i64, i64* bitcast (%struct.CPUX86State** @env to i64*), align 8
  store %struct.CPUX86State* %0, %struct.CPUX86State** @env, align 8
  %4 = tail call i32 @helper_cc_compute_all(i32 %1)
  store i64 %3, i64* bitcast (%struct.CPUX86State** @env to i64*), align 8
  ret i32 %4
}

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @helper_cc_compute_c(i32) local_unnamed_addr #0 {
  switch i32 %0, label %202 [
    i32 1, label %2
    i32 2, label %7
    i32 3, label %13
    i32 4, label %19
    i32 6, label %25
    i32 7, label %35
    i32 8, label %45
    i32 10, label %53
    i32 11, label %63
    i32 12, label %73
    i32 14, label %81
    i32 15, label %92
    i32 16, label %103
    i32 18, label %112
    i32 19, label %124
    i32 20, label %136
    i32 40, label %197
    i32 39, label %192
    i32 38, label %187
    i32 26, label %146
    i32 27, label %150
    i32 28, label %154
    i32 30, label %158
    i32 31, label %162
    i32 32, label %166
    i32 34, label %170
    i32 35, label %176
    i32 36, label %182
  ]

; <label>:2:                                      ; preds = %1
  %3 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %4 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %3, i64 0, i32 2
  %5 = load i32, i32* %4, align 4
  %6 = and i32 %5, 1
  br label %202

; <label>:7:                                      ; preds = %1
  %8 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %9 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %8, i64 0, i32 2
  %10 = load i32, i32* %9, align 4
  %11 = icmp ne i32 %10, 0
  %12 = zext i1 %11 to i32
  br label %202

; <label>:13:                                     ; preds = %1
  %14 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %15 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %14, i64 0, i32 2
  %16 = load i32, i32* %15, align 4
  %17 = icmp ne i32 %16, 0
  %18 = zext i1 %17 to i32
  br label %202

; <label>:19:                                     ; preds = %1
  %20 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %21 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %20, i64 0, i32 2
  %22 = load i32, i32* %21, align 4
  %23 = icmp ne i32 %22, 0
  %24 = zext i1 %23 to i32
  br label %202

; <label>:25:                                     ; preds = %1
  %26 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %27 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 2
  %28 = load i32, i32* %27, align 4
  %29 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %26, i64 0, i32 3
  %30 = load i32, i32* %29, align 8
  %31 = and i32 %30, 255
  %32 = and i32 %28, 255
  %33 = icmp ult i32 %31, %32
  %34 = zext i1 %33 to i32
  br label %202

; <label>:35:                                     ; preds = %1
  %36 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %37 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %36, i64 0, i32 2
  %38 = load i32, i32* %37, align 4
  %39 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %36, i64 0, i32 3
  %40 = load i32, i32* %39, align 8
  %41 = and i32 %40, 65535
  %42 = and i32 %38, 65535
  %43 = icmp ult i32 %41, %42
  %44 = zext i1 %43 to i32
  br label %202

; <label>:45:                                     ; preds = %1
  %46 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %47 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %46, i64 0, i32 2
  %48 = load i32, i32* %47, align 4
  %49 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %46, i64 0, i32 3
  %50 = load i32, i32* %49, align 8
  %51 = icmp ult i32 %50, %48
  %52 = zext i1 %51 to i32
  br label %202

; <label>:53:                                     ; preds = %1
  %54 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %55 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %54, i64 0, i32 2
  %56 = load i32, i32* %55, align 4
  %57 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %54, i64 0, i32 3
  %58 = load i32, i32* %57, align 8
  %59 = and i32 %58, 255
  %60 = and i32 %56, 255
  %61 = icmp ule i32 %59, %60
  %62 = zext i1 %61 to i32
  br label %202

; <label>:63:                                     ; preds = %1
  %64 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %65 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %64, i64 0, i32 2
  %66 = load i32, i32* %65, align 4
  %67 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %64, i64 0, i32 3
  %68 = load i32, i32* %67, align 8
  %69 = and i32 %68, 65535
  %70 = and i32 %66, 65535
  %71 = icmp ule i32 %69, %70
  %72 = zext i1 %71 to i32
  br label %202

; <label>:73:                                     ; preds = %1
  %74 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %75 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %74, i64 0, i32 2
  %76 = load i32, i32* %75, align 4
  %77 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %74, i64 0, i32 3
  %78 = load i32, i32* %77, align 8
  %79 = icmp ule i32 %78, %76
  %80 = zext i1 %79 to i32
  br label %202

; <label>:81:                                     ; preds = %1
  %82 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %83 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %82, i64 0, i32 3
  %84 = load i32, i32* %83, align 8
  %85 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %82, i64 0, i32 2
  %86 = load i32, i32* %85, align 4
  %87 = add i32 %86, %84
  %88 = and i32 %87, 255
  %89 = and i32 %86, 255
  %90 = icmp ult i32 %88, %89
  %91 = zext i1 %90 to i32
  br label %202

; <label>:92:                                     ; preds = %1
  %93 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %94 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %93, i64 0, i32 3
  %95 = load i32, i32* %94, align 8
  %96 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %93, i64 0, i32 2
  %97 = load i32, i32* %96, align 4
  %98 = add i32 %97, %95
  %99 = and i32 %98, 65535
  %100 = and i32 %97, 65535
  %101 = icmp ult i32 %99, %100
  %102 = zext i1 %101 to i32
  br label %202

; <label>:103:                                    ; preds = %1
  %104 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %105 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %104, i64 0, i32 3
  %106 = load i32, i32* %105, align 8
  %107 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %104, i64 0, i32 2
  %108 = load i32, i32* %107, align 4
  %109 = add i32 %108, %106
  %110 = icmp ult i32 %109, %108
  %111 = zext i1 %110 to i32
  br label %202

; <label>:112:                                    ; preds = %1
  %113 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %114 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %113, i64 0, i32 3
  %115 = load i32, i32* %114, align 8
  %116 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %113, i64 0, i32 2
  %117 = load i32, i32* %116, align 4
  %118 = add i32 %115, 1
  %119 = add i32 %118, %117
  %120 = and i32 %119, 255
  %121 = and i32 %117, 255
  %122 = icmp ule i32 %120, %121
  %123 = zext i1 %122 to i32
  br label %202

; <label>:124:                                    ; preds = %1
  %125 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %126 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %125, i64 0, i32 3
  %127 = load i32, i32* %126, align 8
  %128 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %125, i64 0, i32 2
  %129 = load i32, i32* %128, align 4
  %130 = add i32 %127, 1
  %131 = add i32 %130, %129
  %132 = and i32 %131, 65535
  %133 = and i32 %129, 65535
  %134 = icmp ule i32 %132, %133
  %135 = zext i1 %134 to i32
  br label %202

; <label>:136:                                    ; preds = %1
  %137 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %138 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %137, i64 0, i32 3
  %139 = load i32, i32* %138, align 8
  %140 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %137, i64 0, i32 2
  %141 = load i32, i32* %140, align 4
  %142 = add i32 %139, 1
  %143 = add i32 %142, %141
  %144 = icmp ule i32 %143, %141
  %145 = zext i1 %144 to i32
  br label %202

; <label>:146:                                    ; preds = %1
  %147 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %148 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %147, i64 0, i32 2
  %149 = load i32, i32* %148, align 4
  br label %202

; <label>:150:                                    ; preds = %1
  %151 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %152 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %151, i64 0, i32 2
  %153 = load i32, i32* %152, align 4
  br label %202

; <label>:154:                                    ; preds = %1
  %155 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %156 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %155, i64 0, i32 2
  %157 = load i32, i32* %156, align 4
  br label %202

; <label>:158:                                    ; preds = %1
  %159 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %160 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %159, i64 0, i32 2
  %161 = load i32, i32* %160, align 4
  br label %202

; <label>:162:                                    ; preds = %1
  %163 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %164 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %163, i64 0, i32 2
  %165 = load i32, i32* %164, align 4
  br label %202

; <label>:166:                                    ; preds = %1
  %167 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %168 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %167, i64 0, i32 2
  %169 = load i32, i32* %168, align 4
  br label %202

; <label>:170:                                    ; preds = %1
  %171 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %172 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %171, i64 0, i32 2
  %173 = load i32, i32* %172, align 4
  %174 = lshr i32 %173, 7
  %175 = and i32 %174, 1
  br label %202

; <label>:176:                                    ; preds = %1
  %177 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %178 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %177, i64 0, i32 2
  %179 = load i32, i32* %178, align 4
  %180 = lshr i32 %179, 15
  %181 = and i32 %180, 1
  br label %202

; <label>:182:                                    ; preds = %1
  %183 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %184 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %183, i64 0, i32 2
  %185 = load i32, i32* %184, align 4
  %186 = lshr i32 %185, 31
  br label %202

; <label>:187:                                    ; preds = %1
  %188 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %189 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %188, i64 0, i32 2
  %190 = load i32, i32* %189, align 4
  %191 = and i32 %190, 1
  br label %202

; <label>:192:                                    ; preds = %1
  %193 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %194 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %193, i64 0, i32 2
  %195 = load i32, i32* %194, align 4
  %196 = and i32 %195, 1
  br label %202

; <label>:197:                                    ; preds = %1
  %198 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %199 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %198, i64 0, i32 2
  %200 = load i32, i32* %199, align 4
  %201 = and i32 %200, 1
  br label %202

; <label>:202:                                    ; preds = %1, %197, %192, %187, %182, %176, %170, %166, %162, %158, %154, %150, %146, %136, %124, %112, %103, %92, %81, %73, %63, %53, %45, %35, %25, %19, %13, %7, %2
  %203 = phi i32 [ %201, %197 ], [ %196, %192 ], [ %191, %187 ], [ %186, %182 ], [ %181, %176 ], [ %175, %170 ], [ %169, %166 ], [ %165, %162 ], [ %161, %158 ], [ %157, %154 ], [ %153, %150 ], [ %149, %146 ], [ %145, %136 ], [ %135, %124 ], [ %123, %112 
  ret i32 %203
}

; Function Attrs: norecurse nounwind uwtable
define i64 @helper_set_cc_op_eflags() local_unnamed_addr #1 {
  %1 = load %struct.CPUX86State*, %struct.CPUX86State** @env, align 8
  %2 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 1
  %3 = load i32, i32* %2, align 16
  %4 = tail call i32 @helper_cc_compute_all(i32 %3)
  %5 = getelementptr inbounds %struct.CPUX86State, %struct.CPUX86State* %1, i64 0, i32 2
  store i32 %4, i32* %5, align 4
  store i32 1, i32* %2, align 16
  ret i64 0
}

; Function Attrs: norecurse nounwind readnone uwtable
define void @se_ensure_symbolic() local_unnamed_addr #10 {
  ret void
}

; Function Attrs: norecurse uwtable
define weak void @helper_se_call(i32) local_unnamed_addr #11 {
  ret void
}

; Function Attrs: norecurse uwtable
define weak void @helper_se_ret(i32) local_unnamed_addr #11 {
  ret void
}

define i1 @uadds(i16*, i16, i16) {
  %4 = add i16 %1, %2
  store i16 %4, i16* %0
  %5 = icmp ugt i16 %1, %2
  %6 = select i1 %5, i16 %1, i16 %2
  %7 = icmp ult i16 %4, %6
  ret i1 %7
}

define i1 @uadd(i32*, i32, i32) {
  %4 = add i32 %1, %2
  store i32 %4, i32* %0
  %5 = icmp ugt i32 %1, %2
  %6 = select i1 %5, i32 %1, i32 %2
  %7 = icmp ult i32 %4, %6
  ret i1 %7
}

define i1 @uaddl(i64*, i64, i64) {
  %4 = add i64 %1, %2
  store i64 %4, i64* %0
  %5 = icmp ugt i64 %1, %2
  %6 = select i1 %5, i64 %1, i64 %2
  %7 = icmp ult i64 %4, %6
  ret i1 %7
}

attributes #0 = { norecurse nounwind readonly uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" 
attributes #1 = { norecurse nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signe
attributes #2 = { uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="f
attributes #3 = { noreturn uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp
attributes #4 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-b
attributes #5 = { nounwind }
attributes #6 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-siz
attributes #7 = { noreturn "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-b
attributes #8 = { argmemonly nounwind }
attributes #9 = { alwaysinline inlinehint uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-
attributes #10 = { norecurse nounwind readnone uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false"
attributes #11 = { norecurse uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-
attributes #12 = { noreturn }

!llvm.ident = !{!0}
!llvm.module.flags = !{!1}

!0 = !{!"clang version 3.9.0 (tags/RELEASE_390/final)"}
!1 = !{i32 1, !"PIC Level", i32 2}
!2 = !{!"branch_weights", i32 2000, i32 1}
!3 = !{!"branch_weights", i32 1, i32 2000}
