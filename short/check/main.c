#include <linux/init.h>
#include <linux/module.h>


// Simple kernel module to test
// linking between the checker
// and a different module
// Also to test detection of memory
// free errors

struct rand_data {
	int random;
	int other;
};


// calls into the checkstate module
extern void kmalloc_check(void *p);
extern void kfree_check(void *p);

// kmalloc and kfree from linux/slab.h
extern void kfree(const void *);
extern void *__kmalloc(size_t size, gfp_t flags);

// REDEFINE kmalloc
// redirect kmalloc calls to the checker
__inline static void *(__attribute__((__always_inline__)) kmalloc(size_t size, gfp_t flags)) {
	void *p;

	p = __kmalloc(size, flags);
	kmalloc_check(p);

	return p;
}

// check if pointer has been allocated
void kfree_wrapper(void *p) {
	kfree_check(p);
	kfree(p);
}


static int __init start(void) {

	struct rand_data *c;

	// kmalloc
	c = kmalloc(sizeof(struct rand_data*), GFP_KERNEL);

	// initial free
	kfree_wrapper(c);

	// double free
	kfree_wrapper(c);

	return 0;
}

static void __exit quit(void) {
	printk("Exiting module\n");
}


module_init(start);
module_exit(quit);
