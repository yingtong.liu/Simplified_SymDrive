#ifndef STATE_H
#define STATE_H

// HASHTABLE implementation for tracking pointer and
// status of the corresponding allocated memory

#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>


struct entry {
        void *k;
        int v;
        struct entry *next;
};


struct hashtable {
        int tablelen;
        int load_lim;
        int size;
        struct entry **table;
};


struct hashtable * init_table(int minsize);

int hash(struct hashtable *h, void *k);

int getIndex(void *k, int size);

void table_insert(struct hashtable *h, void *k);

struct entry * table_search(struct hashtable *h, void *k);

struct entry * table_remove(struct hashtable *h, void *k);



#endif
