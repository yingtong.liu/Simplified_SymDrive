#ifndef CHECK_H
#define CHECK_H

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>

#include "state.h"
#include "s2e.h"



#define assert(x, msg)                                                                                          \
        if (!(x)) {                                                                                                             \
                char temp[256];                                                                                         \
                sprintf(temp, "Error: Function: %s; Line: %d, %s\n", __func__, __LINE__, msg);  \
                printk("%s", temp);                                                             \
                s2e_message(msg);						\
		s2e_kill_state(0, temp); \
        }                                                                                                                               


#endif
