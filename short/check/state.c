#include "state.h"



int hash(struct hashtable *h, void *k) {
        return ((unsigned long)k) % h->tablelen;
}


int getIndex(void *k, int size) {
        return ((unsigned long)k) % size;
}


struct hashtable * init_table(int minsize) {
        struct hashtable *h = kmalloc(sizeof(struct hashtable *), GFP_KERNEL);
        h->tablelen = minsize;
        h->load_lim = minsize * 3 / 4;
        h->size = 0;
        h->table = kmalloc(sizeof(struct entry) * minsize, GFP_KERNEL);
        memset(h->table, 0, h->size * sizeof(struct entry *));

        return h;
}


void table_insert(struct hashtable *h, void *k) {
        struct entry *e, *ent;
        int index;

        if (h->size + 1 == h->load_lim) {               // resizing, not that it actually needs this
                int i;
                int newsize = h->tablelen * 2;
                struct entry **newtable = kmalloc(sizeof(struct entry *) * newsize, GFP_KERNEL);
                memset(newtable, 0, newsize * sizeof(struct entry *));

                for (i = 0; i < h->tablelen; i++) {
                        while ((e = h->table[i]) != NULL) {
                                h->table[i] = e->next;
                                index = getIndex(e->k, newsize);
                                e->next = newtable[index];
                                newtable[index] = e;
                        }
                }
                kfree(h->table);
                h->table = newtable;
                h->tablelen = newsize;
                h->load_lim = newsize * 3 / 4;
        }

        ent = kmalloc(sizeof(struct entry *), GFP_KERNEL);
        index = hash(h, k);

        ent->k = k;
        ent->v = 1;
        ent->next = h->table[index];
        h->table[index] = ent;
        h->size++;
}


struct entry * table_search(struct hashtable *h, void *k) {
        struct entry *e;
        int index = hash(h, k);

        e = h->table[index];
        while (e != NULL) {
                if (k == e->k) return e;
                e = e->next;
        }

        return NULL;
}


struct entry * table_remove(struct hashtable *h, void *k) {
        struct entry *e;

        int index = hash(h, k);

        e = h->table[index];
        while (e != NULL) {
                if (e->k == k) {
                        e->v = 0;
                        return e;
                }

                e = e->next;
        }

        return NULL;
}

