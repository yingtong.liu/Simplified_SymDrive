#include "check.h"


// keep all pointers and their state
static struct hashtable *mem_state;


// record pointer allocated state in hashtable
void kmalloc_check(void *p) {
        table_insert(mem_state, p);
}


// search hashtable for pointer state (allocated or not)
// before allowing kfree
void kfree_check(void *p) {
        struct entry *e = table_search(mem_state, p);

        assert(e != NULL, "Attempting to free an object that was never allocated\n");
        assert(e->v != 0, "Double-free: Attempting to free an object that has already been freed\n");

        table_remove(mem_state, p);
}


// initialize state checking module
static int __init init_checker(void) {
        printk("Initializing checker and state\n");
        mem_state = init_table(20);

        if (mem_state == NULL) {
                return -1;
        }

        return 0;
}

// exit module
static void __exit exit_checker(void) {
        kfree(mem_state);
        printk("Module killed\n");
}


// export function for use in the driver
EXPORT_SYMBOL(kmalloc_check);
EXPORT_SYMBOL(kfree_check);

module_init(init_checker);
module_exit(exit_checker);
