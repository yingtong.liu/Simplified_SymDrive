//////test the short driver

#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>
#include "s2e.h" 
#define BUFFER_LENGTH 1              
static char receive[BUFFER_LENGTH];  
 
int main(){
   int ret, fd;
  char stringToSend[BUFFER_LENGTH];
   printf("Starting device test code example...\n");
   fd = open("/dev/short0", O_RDWR);             // Open the device with read/write access
   if (fd < 0){
      perror("Failed to open the device...");
      return errno;
   }
//    printf("Type in a short string to send to the kernel module:\n");
//    scanf("%[^\n]%*c", stringToSend);                // Read in a string (with spaces)
  
    *stringToSend = 'a';  
    printf("Writing message to the device [%s].\n", stringToSend);
    ret = write(fd, stringToSend, strlen(stringToSend)); // Send the string to the LKM
   if (ret < 0){
     perror("Failed to write the message to the device.");
     return errno;
   }
////comment these out if you wanna test the read entry function.
//---- test read start
//    printf("Press ENTER to read back from the device...\n");
//    getchar();
 
//   printf("Reading from the device...\n");
//   ret = read(fd, receive, BUFFER_LENGTH);        // Read the response from the LKM
//   if (ret < 0){
//      perror("Failed to read the message from the device.");
//      return errno;
//   }
///--- test read end 

   s2e_message("test 3\n");
	//both cases will not concretize the value.
   //char test = *receive;
  // printf("The received message is: [%s]\n", &test);
//   printf("The received message is: [%s]\n", receive);
   s2e_message("test 4\n");
   printf("End of the program\n");
   return 0;
}
