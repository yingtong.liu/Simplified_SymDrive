[Z3] Initializing
BEGIN searcher description
DFSSearcher
END searcher description
Creating plugin CorePlugin
Creating plugin BaseInstructions
Creating plugin HostFiles
Creating plugin Vmi
Creating plugin WebServiceInterface
Creating plugin ExecutionTracer
Creating plugin ModuleTracer
Creating plugin KeyValueStore
Creating plugin TranslationBlockCoverage
Creating plugin ModuleExecutionDetector
Creating plugin ForkLimiter
Creating plugin ProcessExecutionDetector
Creating plugin MultiSearcher
Creating plugin CUPASearcher
Creating plugin StaticFunctionModels
Creating plugin TestCaseGenerator
Creating plugin LinuxMonitor
Initializing TestCaseGenerator
Initializing MultiSearcher
Initializing ForkLimiter
Initializing KeyValueStore
Initializing ExecutionTracer
Initializing WebServiceInterface
Initializing Vmi
Initializing HostFiles
Initializing BaseInstructions
Initializing LinuxMonitor
Initializing ProcessExecutionDetector
Initializing ModuleExecutionDetector
Initializing StaticFunctionModels
StaticFunctionModels: Model count: 0
Initializing CUPASearcher
CUPASearcher: CUPASearcher is now active
Initializing TranslationBlockCoverage
Initializing ModuleTracer
Initializing CorePlugin
BEGIN searcher description
DFSSearcher
END searcher description
0 [State 0] Created initial state
14 [State 0] BaseInstructions: Message from guest (0x7fffffff43d0): Process map:
14 [State 0] BaseInstructions: Message from guest (0x7fffffff43d0): Base=0x400000 Limit=0x602000 Name=/home/s2e/test
14 [State 0] BaseInstructions: Message from guest (0x7fffffff43d0): Base=0x7ffa56cac000 Limit=0x7ffa56eb0000 Name=/lib/x86_64-linux-gnu/libdl-2.24.so
14 [State 0] BaseInstructions: Message from guest (0x7fffffff43d0): Base=0x7ffa56eb0000 Limit=0x7ffa5724b000 Name=/lib/x86_64-linux-gnu/libc-2.24.so
14 [State 0] BaseInstructions: Message from guest (0x7fffffff43d0): Base=0x7ffa5724f000 Limit=0x7ffa57455000 Name=/home/s2e/s2e.so
14 [State 0] BaseInstructions: Message from guest (0x7fffffff43d0): Base=0x7ffa57455000 Limit=0x7ffa5767a000 Name=/lib/x86_64-linux-gnu/ld-2.24.so
14 [State 0] BaseInstructions: Inserted symbolic data @0x7fffffff4505 of size 0x2: str pc=0x400629
14 [State 0] Forking state 0 at pc = 0x40066a at pagedir = 0xd436000
    state 1
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
14 [State 0] Forking state 0 at pc = 0x400672 at pagedir = 0xd436000
    state 0
    state 2
BEGIN searcher description
DFSSearcher
END searcher description
14 [State 0] Forking state 0 at pc = 0x400686 at pagedir = 0xd436000
    state 0
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
14 [State 0] Switching from state 0 to state 3
14 [State 3] Forking state 3 at pc = 0x40068e at pagedir = 0xd436000
    state 4
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
14 [State 3] Switching from state 3 to state 1
14 [State 1] Switching from state 1 to state 0
15 [State 0] Forking state 0 at pc = 0x4006ac at pagedir = 0xd436000
    state 0
    state 5
BEGIN searcher description
DFSSearcher
END searcher description
15 [State 0] Switching from state 0 to state 5
15 [State 5] Forking state 5 at pc = 0x4006b4 at pagedir = 0xd436000
    state 6
    state 5
BEGIN searcher description
DFSSearcher
END searcher description
15 [State 5] Switching from state 5 to state 0
15 [State 0] Forking state 0 at pc = 0x4006d6 at pagedir = 0xd436000
    state 7
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
15 [State 0] Switching from state 0 to state 6
15 [State 6] Switching from state 6 to state 0
15 [State 0] Switching from state 0 to state 1
15 [State 1] Switching from state 1 to state 0
16 [State 0] BaseInstructions: Killing state 0
16 [State 0] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
16 [State 0] TestCaseGenerator: generating test case at address 0x400eed
16 [State 0] Switching from state 0 to state 7
16 [State 7] Switching from state 7 to state 2
16 [State 2] Switching from state 2 to state 5
16 [State 5] Forking state 5 at pc = 0x4006d6 at pagedir = 0xd436000
    state 5
    state 8
16 [State 5] Switching from state 5 to state 3
16 [State 3] Switching from state 3 to state 1
16 [State 1] Switching from state 1 to state 4
16 [State 4] Switching from state 4 to state 2
17 [State 2] Switching from state 2 to state 4
17 [State 4] Switching from state 4 to state 7
17 [State 7] BaseInstructions: Killing state 7
17 [State 7] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
17 [State 7] TestCaseGenerator: generating test case at address 0x400eed
17 [State 7] Switching from state 7 to state 8
17 [State 8] Switching from state 8 to state 2
17 [State 2] Switching from state 2 to state 6
17 [State 6] Forking state 6 at pc = 0x4006d6 at pagedir = 0xd436000
    state 6
    state 9
17 [State 6] Switching from state 6 to state 8
18 [State 8] Switching from state 8 to state 1
18 [State 1] Switching from state 1 to state 4
18 [State 4] Switching from state 4 to state 6
18 [State 6] Switching from state 6 to state 3
18 [State 3] Forking state 3 at pc = 0x4006d6 at pagedir = 0xd436000
    state 3
    state 10
18 [State 3] Switching from state 3 to state 1
18 [State 1] Switching from state 1 to state 3
19 [State 3] Switching from state 3 to state 1
19 [State 1] Switching from state 1 to state 8
19 [State 8] Switching from state 8 to state 5
19 [State 5] Switching from state 5 to state 6
19 [State 6] Switching from state 6 to state 8
19 [State 8] BaseInstructions: Killing state 8
19 [State 8] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
19 [State 8] TestCaseGenerator: generating test case at address 0x400eed
19 [State 8] Switching from state 8 to state 10
19 [State 10] Switching from state 10 to state 4
19 [State 4] Forking state 4 at pc = 0x4006d6 at pagedir = 0xd436000
    state 4
    state 11
19 [State 4] Switching from state 4 to state 5
19 [State 5] Switching from state 5 to state 6
20 [State 6] Switching from state 6 to state 1
20 [State 1] Switching from state 1 to state 3
20 [State 3] Switching from state 3 to state 6
20 [State 6] Switching from state 6 to state 2
20 [State 2] Switching from state 2 to state 10
20 [State 10] Switching from state 10 to state 5
20 [State 5] Switching from state 5 to state 3
20 [State 3] Switching from state 3 to state 6
20 [State 6] BaseInstructions: Killing state 6
20 [State 6] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
20 [State 6] TestCaseGenerator: generating test case at address 0x400eed
21 [State 6] Switching from state 6 to state 5
21 [State 5] Switching from state 5 to state 3
21 [State 3] BaseInstructions: Killing state 3
21 [State 3] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
21 [State 3] TestCaseGenerator: generating test case at address 0x400eed
21 [State 3] Switching from state 3 to state 5
21 [State 5] Switching from state 5 to state 1
21 [State 1] BaseInstructions: Killing state 1
21 [State 1] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
21 [State 1] TestCaseGenerator: generating test case at address 0x400eed
21 [State 1] Switching from state 1 to state 2
21 [State 2] Switching from state 2 to state 4
21 [State 4] Switching from state 4 to state 5
21 [State 5] Switching from state 5 to state 10
22 [State 10] Switching from state 10 to state 2
22 [State 2] Switching from state 2 to state 4
22 [State 4] Switching from state 4 to state 10
22 [State 10] Switching from state 10 to state 2
22 [State 2] BaseInstructions: Killing state 2
22 [State 2] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
22 [State 2] TestCaseGenerator: generating test case at address 0x400eed
22 [State 2] Switching from state 2 to state 4
22 [State 4] Switching from state 4 to state 10
22 [State 10] BaseInstructions: Killing state 10
22 [State 10] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
22 [State 10] TestCaseGenerator: generating test case at address 0x400eed
23 [State 10] Switching from state 10 to state 11
23 [State 11] Switching from state 11 to state 4
23 [State 4] BaseInstructions: Killing state 4
23 [State 4] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
23 [State 4] TestCaseGenerator: generating test case at address 0x400eed
23 [State 4] Switching from state 4 to state 11
23 [State 11] Switching from state 11 to state 5
23 [State 5] BaseInstructions: Killing state 5
23 [State 5] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
23 [State 5] TestCaseGenerator: generating test case at address 0x400eed
23 [State 5] Switching from state 5 to state 11
23 [State 11] BaseInstructions: Killing state 11
23 [State 11] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
23 [State 11] TestCaseGenerator: generating test case at address 0x400eed
23 [State 11] Switching from state 11 to state 9
24 [State 9] BaseInstructions: Killing state 9
24 [State 9] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
24 [State 9] TestCaseGenerator: generating test case at address 0x400eed
