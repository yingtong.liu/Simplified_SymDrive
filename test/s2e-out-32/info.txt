[Z3] Initializing
BEGIN searcher description
DFSSearcher
END searcher description
Creating plugin CorePlugin
Creating plugin BaseInstructions
Creating plugin HostFiles
Creating plugin Vmi
Creating plugin WebServiceInterface
Creating plugin ExecutionTracer
Creating plugin ModuleTracer
Creating plugin KeyValueStore
Creating plugin TranslationBlockCoverage
Creating plugin ModuleExecutionDetector
Creating plugin ForkLimiter
Creating plugin ProcessExecutionDetector
Creating plugin MultiSearcher
Creating plugin CUPASearcher
Creating plugin StaticFunctionModels
Creating plugin TestCaseGenerator
Creating plugin LinuxMonitor
Initializing TestCaseGenerator
Initializing MultiSearcher
Initializing ForkLimiter
Initializing KeyValueStore
Initializing ExecutionTracer
Initializing WebServiceInterface
Initializing Vmi
Initializing HostFiles
Initializing BaseInstructions
Initializing LinuxMonitor
Initializing ProcessExecutionDetector
Initializing ModuleExecutionDetector
Initializing StaticFunctionModels
StaticFunctionModels: Model count: 0
Initializing CUPASearcher
CUPASearcher: CUPASearcher is now active
Initializing TranslationBlockCoverage
Initializing ModuleTracer
Initializing CorePlugin
BEGIN searcher description
DFSSearcher
END searcher description
0 [State 0] Created initial state
65 [State 0] BaseInstructions: Message from guest (0x7fff96a95050): Process map:
65 [State 0] BaseInstructions: Message from guest (0x7fff96a95050): Base=0x400000 Limit=0x602000 Name=/home/s2e/test
65 [State 0] BaseInstructions: Message from guest (0x7fff96a95050): Base=0x7f504eade000 Limit=0x7f504ece2000 Name=/lib/x86_64-linux-gnu/libdl-2.24.so
65 [State 0] BaseInstructions: Message from guest (0x7fff96a95050): Base=0x7f504ece2000 Limit=0x7f504f07d000 Name=/lib/x86_64-linux-gnu/libc-2.24.so
65 [State 0] BaseInstructions: Message from guest (0x7fff96a95050): Base=0x7f504f081000 Limit=0x7f504f287000 Name=/home/s2e/s2e.so
65 [State 0] BaseInstructions: Message from guest (0x7fff96a95050): Base=0x7f504f287000 Limit=0x7f504f4ac000 Name=/lib/x86_64-linux-gnu/ld-2.24.so
65 [State 0] BaseInstructions: Inserted symbolic data @0x7fff96a95185 of size 0x2: str pc=0x400629
65 [State 0] Forking state 0 at pc = 0x40066a at pagedir = 0xf3e1000
    state 1
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
65 [State 0] Forking state 0 at pc = 0x400672 at pagedir = 0xf3e1000
    state 0
    state 2
BEGIN searcher description
DFSSearcher
END searcher description
65 [State 0] Forking state 0 at pc = 0x400686 at pagedir = 0xf3e1000
    state 0
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
65 [State 0] Forking state 0 at pc = 0x4006ac at pagedir = 0xf3e1000
    state 0
    state 4
BEGIN searcher description
DFSSearcher
END searcher description
65 [State 0] Forking state 0 at pc = 0x4006d6 at pagedir = 0xf3e1000
    state 5
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
65 [State 0] Switching from state 0 to state 5
65 [State 5] Switching from state 5 to state 1
66 [State 1] Switching from state 1 to state 0
66 [State 0] BaseInstructions: Killing state 0
66 [State 0] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
66 [State 0] TestCaseGenerator: generating test case at address 0x400eed
66 [State 0] Switching from state 0 to state 5
67 [State 5] Switching from state 5 to state 1
67 [State 1] Switching from state 1 to state 5
68 [State 5] Switching from state 5 to state 2
68 [State 2] Switching from state 2 to state 4
68 [State 4] Forking state 4 at pc = 0x4006b4 at pagedir = 0xf3e1000
    state 6
    state 4
BEGIN searcher description
DFSSearcher
END searcher description
68 [State 4] Switching from state 4 to state 2
69 [State 2] Switching from state 2 to state 1
69 [State 1] Switching from state 1 to state 4
69 [State 4] Switching from state 4 to state 2
70 [State 2] Switching from state 2 to state 4
70 [State 4] Switching from state 4 to state 5
71 [State 5] Switching from state 5 to state 1
71 [State 1] Switching from state 1 to state 6
72 [State 6] Switching from state 6 to state 5
72 [State 5] BaseInstructions: Killing state 5
72 [State 5] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
72 [State 5] TestCaseGenerator: generating test case at address 0x400eed
72 [State 5] Switching from state 5 to state 6
73 [State 6] Switching from state 6 to state 1
73 [State 1] Switching from state 1 to state 3
73 [State 3] Forking state 3 at pc = 0x40068e at pagedir = 0xf3e1000
    state 7
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
73 [State 3] Switching from state 3 to state 4
74 [State 4] Forking state 4 at pc = 0x4006d6 at pagedir = 0xf3e1000
    state 4
    state 8
BEGIN searcher description
DFSSearcher
END searcher description
74 [State 4] Switching from state 4 to state 3
74 [State 3] Switching from state 3 to state 1
75 [State 1] Switching from state 1 to state 3
75 [State 3] Switching from state 3 to state 1
75 [State 1] Switching from state 1 to state 8
76 [State 8] Switching from state 8 to state 4
76 [State 4] Switching from state 4 to state 6
77 [State 6] Switching from state 6 to state 8
77 [State 8] Switching from state 8 to state 7
77 [State 7] Switching from state 7 to state 4
78 [State 4] Switching from state 4 to state 6
78 [State 6] Forking state 6 at pc = 0x4006d6 at pagedir = 0xf3e1000
    state 6
    state 9
78 [State 6] Switching from state 6 to state 1
78 [State 1] Switching from state 1 to state 3
79 [State 3] Switching from state 3 to state 6
79 [State 6] Switching from state 6 to state 2
79 [State 2] Switching from state 2 to state 8
80 [State 8] Switching from state 8 to state 4
80 [State 4] Switching from state 4 to state 3
81 [State 3] Switching from state 3 to state 6
81 [State 6] Switching from state 6 to state 7
81 [State 7] Switching from state 7 to state 3
82 [State 3] Forking state 3 at pc = 0x4006d6 at pagedir = 0xf3e1000
    state 3
    state 10
82 [State 3] Switching from state 3 to state 4
82 [State 4] Switching from state 4 to state 2
83 [State 2] Switching from state 2 to state 3
83 [State 3] Switching from state 3 to state 4
84 [State 4] Switching from state 4 to state 6
84 [State 6] Switching from state 6 to state 1
84 [State 1] Switching from state 1 to state 3
85 [State 3] Switching from state 3 to state 2
85 [State 2] Switching from state 2 to state 8
85 [State 8] Switching from state 8 to state 1
86 [State 1] Switching from state 1 to state 2
86 [State 2] Switching from state 2 to state 1
87 [State 1] Switching from state 1 to state 8
87 [State 8] Switching from state 8 to state 6
88 [State 6] Switching from state 6 to state 4
88 [State 4] Switching from state 4 to state 1
88 [State 1] Switching from state 1 to state 3
89 [State 3] Switching from state 3 to state 6
89 [State 6] Switching from state 6 to state 8
89 [State 8] Switching from state 8 to state 7
90 [State 7] Switching from state 7 to state 1
90 [State 1] Switching from state 1 to state 4
90 [State 4] Switching from state 4 to state 7
91 [State 7] Switching from state 7 to state 1
91 [State 1] Switching from state 1 to state 3
91 [State 3] Switching from state 3 to state 2
92 [State 2] Switching from state 2 to state 6
92 [State 6] Switching from state 6 to state 1
92 [State 1] Switching from state 1 to state 6
93 [State 6] Switching from state 6 to state 1
93 [State 1] Switching from state 1 to state 2
94 [State 2] Switching from state 2 to state 3
94 [State 3] Switching from state 3 to state 7
94 [State 7] Forking state 7 at pc = 0x4006d6 at pagedir = 0xf3e1000
    state 7
    state 11
95 [State 7] Switching from state 7 to state 4
95 [State 4] Switching from state 4 to state 6
95 [State 6] Switching from state 6 to state 4
96 [State 4] Switching from state 4 to state 8
96 [State 8] Switching from state 8 to state 6
97 [State 6] BaseInstructions: Killing state 6
97 [State 6] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
97 [State 6] TestCaseGenerator: generating test case at address 0x400eed
97 [State 6] Switching from state 6 to state 2
97 [State 2] Switching from state 2 to state 4
98 [State 4] Switching from state 4 to state 1
98 [State 1] Switching from state 1 to state 7
98 [State 7] Switching from state 7 to state 4
99 [State 4] Switching from state 4 to state 1
99 [State 1] Switching from state 1 to state 7
99 [State 7] Switching from state 7 to state 1
100 [State 1] Switching from state 1 to state 3
100 [State 3] Switching from state 3 to state 4
101 [State 4] Switching from state 4 to state 8
101 [State 8] Switching from state 8 to state 7
102 [State 7] Switching from state 7 to state 3
102 [State 3] Switching from state 3 to state 8
102 [State 8] Switching from state 8 to state 7
103 [State 7] Switching from state 7 to state 4
103 [State 4] Switching from state 4 to state 2
103 [State 2] Switching from state 2 to state 1
104 [State 1] Switching from state 1 to state 4
104 [State 4] Switching from state 4 to state 2
104 [State 2] Switching from state 2 to state 4
105 [State 4] Switching from state 4 to state 7
105 [State 7] Switching from state 7 to state 8
105 [State 8] Switching from state 8 to state 4
106 [State 4] Switching from state 4 to state 3
106 [State 3] Switching from state 3 to state 8
106 [State 8] Switching from state 8 to state 3
107 [State 3] Switching from state 3 to state 8
107 [State 8] Switching from state 8 to state 7
108 [State 7] BaseInstructions: Killing state 7
108 [State 7] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
108 [State 7] TestCaseGenerator: generating test case at address 0x400eed
108 [State 7] Switching from state 7 to state 1
108 [State 1] Switching from state 1 to state 4
108 [State 4] Switching from state 4 to state 1
109 [State 1] Switching from state 1 to state 2
109 [State 2] Switching from state 2 to state 8
110 [State 8] Switching from state 8 to state 2
110 [State 2] Switching from state 2 to state 8
111 [State 8] Switching from state 8 to state 2
111 [State 2] BaseInstructions: Killing state 2
111 [State 2] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
111 [State 2] TestCaseGenerator: generating test case at address 0x400eed
111 [State 2] Switching from state 2 to state 1
112 [State 1] Switching from state 1 to state 8
112 [State 8] Switching from state 8 to state 1
112 [State 1] Switching from state 1 to state 3
113 [State 3] Switching from state 3 to state 8
113 [State 8] Switching from state 8 to state 1
113 [State 1] Switching from state 1 to state 8
114 [State 8] Switching from state 8 to state 3
114 [State 3] Switching from state 3 to state 8
114 [State 8] Switching from state 8 to state 4
115 [State 4] Switching from state 4 to state 8
115 [State 8] Switching from state 8 to state 3
115 [State 3] Switching from state 3 to state 8
116 [State 8] Switching from state 8 to state 3
116 [State 3] Switching from state 3 to state 1
116 [State 1] Switching from state 1 to state 8
117 [State 8] BaseInstructions: Killing state 8
117 [State 8] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
117 [State 8] TestCaseGenerator: generating test case at address 0x400eed
117 [State 8] Switching from state 8 to state 4
118 [State 4] Switching from state 4 to state 11
118 [State 11] Switching from state 11 to state 4
118 [State 4] Switching from state 4 to state 3
119 [State 3] Switching from state 3 to state 4
119 [State 4] Switching from state 4 to state 11
119 [State 11] Switching from state 11 to state 3
120 [State 3] Switching from state 3 to state 11
120 [State 11] Switching from state 11 to state 4
120 [State 4] Switching from state 4 to state 3
121 [State 3] Switching from state 3 to state 1
121 [State 1] Switching from state 1 to state 4
121 [State 4] BaseInstructions: Killing state 4
121 [State 4] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
121 [State 4] TestCaseGenerator: generating test case at address 0x400eed
122 [State 4] Switching from state 4 to state 1
122 [State 1] Switching from state 1 to state 3
122 [State 3] Switching from state 3 to state 1
123 [State 1] Switching from state 1 to state 3
123 [State 3] Switching from state 3 to state 11
123 [State 11] Switching from state 11 to state 3
124 [State 3] Switching from state 3 to state 11
124 [State 11] Switching from state 11 to state 1
125 [State 1] BaseInstructions: Killing state 1
125 [State 1] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
125 [State 1] TestCaseGenerator: generating test case at address 0x400eed
125 [State 1] Switching from state 1 to state 11
125 [State 11] Switching from state 11 to state 3
125 [State 3] BaseInstructions: Killing state 3
125 [State 3] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
125 [State 3] TestCaseGenerator: generating test case at address 0x400eed
125 [State 3] Switching from state 3 to state 11
127 [State 11] BaseInstructions: Killing state 11
127 [State 11] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
127 [State 11] TestCaseGenerator: generating test case at address 0x400eed
127 [State 11] Switching from state 11 to state 10
128 [State 10] BaseInstructions: Killing state 10
128 [State 10] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
128 [State 10] TestCaseGenerator: generating test case at address 0x400eed
128 [State 10] Switching from state 10 to state 9
128 [State 9] BaseInstructions: Killing state 9
128 [State 9] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
128 [State 9] TestCaseGenerator: generating test case at address 0x400eed
