[Z3] Initializing
BEGIN searcher description
DFSSearcher
END searcher description
Creating plugin CorePlugin
Creating plugin BaseInstructions
Creating plugin HostFiles
Creating plugin Vmi
Creating plugin WebServiceInterface
Creating plugin ExecutionTracer
Creating plugin ModuleTracer
Creating plugin KeyValueStore
Creating plugin TranslationBlockCoverage
Creating plugin ModuleExecutionDetector
Creating plugin ForkLimiter
Creating plugin ProcessExecutionDetector
Creating plugin MultiSearcher
Creating plugin CUPASearcher
Creating plugin StaticFunctionModels
Creating plugin TestCaseGenerator
Creating plugin LinuxMonitor
Initializing TestCaseGenerator
Initializing MultiSearcher
Initializing ForkLimiter
Initializing KeyValueStore
Initializing ExecutionTracer
Initializing WebServiceInterface
Initializing Vmi
Initializing HostFiles
Initializing BaseInstructions
Initializing LinuxMonitor
Initializing ProcessExecutionDetector
Initializing ModuleExecutionDetector
Initializing StaticFunctionModels
StaticFunctionModels: Model count: 0
Initializing CUPASearcher
CUPASearcher: CUPASearcher is now active
Initializing TranslationBlockCoverage
Initializing ModuleTracer
Initializing CorePlugin
BEGIN searcher description
DFSSearcher
END searcher description
0 [State 0] Created initial state
63 [State 0] BaseInstructions: Message from guest (0x7fffeb181990): Process map:
63 [State 0] BaseInstructions: Message from guest (0x7fffeb181990): Base=0x400000 Limit=0x602000 Name=/home/s2e/test
63 [State 0] BaseInstructions: Message from guest (0x7fffeb181990): Base=0x7ff4ac862000 Limit=0x7ff4aca66000 Name=/lib/x86_64-linux-gnu/libdl-2.24.so
63 [State 0] BaseInstructions: Message from guest (0x7fffeb181990): Base=0x7ff4aca66000 Limit=0x7ff4ace01000 Name=/lib/x86_64-linux-gnu/libc-2.24.so
63 [State 0] BaseInstructions: Message from guest (0x7fffeb181990): Base=0x7ff4ace05000 Limit=0x7ff4ad00b000 Name=/home/s2e/s2e.so
63 [State 0] BaseInstructions: Message from guest (0x7fffeb181990): Base=0x7ff4ad00b000 Limit=0x7ff4ad230000 Name=/lib/x86_64-linux-gnu/ld-2.24.so
63 [State 0] BaseInstructions: Inserted symbolic data @0x7fffeb181ac5 of size 0x2: str pc=0x400629
63 [State 0] Forking state 0 at pc = 0x40066a at pagedir = 0xf3f9000
    state 1
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
63 [State 0] Forking state 0 at pc = 0x400672 at pagedir = 0xf3f9000
    state 0
    state 2
BEGIN searcher description
DFSSearcher
END searcher description
63 [State 0] Forking state 0 at pc = 0x400686 at pagedir = 0xf3f9000
    state 0
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
63 [State 0] Forking state 0 at pc = 0x4006ac at pagedir = 0xf3f9000
    state 0
    state 4
BEGIN searcher description
DFSSearcher
END searcher description
63 [State 0] Forking state 0 at pc = 0x4006d6 at pagedir = 0xf3f9000
    state 5
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
64 [State 0] Switching from state 0 to state 5
64 [State 5] Switching from state 5 to state 0
65 [State 0] Switching from state 0 to state 1
65 [State 1] Switching from state 1 to state 0
65 [State 0] Switching from state 0 to state 5
66 [State 5] Switching from state 5 to state 0
66 [State 0] Switching from state 0 to state 5
66 [State 5] Switching from state 5 to state 0
67 [State 0] Switching from state 0 to state 1
67 [State 1] Switching from state 1 to state 5
68 [State 5] Switching from state 5 to state 0
68 [State 0] BaseInstructions: Killing state 0
68 [State 0] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
68 [State 0] TestCaseGenerator: generating test case at address 0x400eed
68 [State 0] Switching from state 0 to state 5
68 [State 5] Switching from state 5 to state 2
69 [State 2] Switching from state 2 to state 4
69 [State 4] Forking state 4 at pc = 0x4006b4 at pagedir = 0xf3f9000
    state 6
    state 4
BEGIN searcher description
DFSSearcher
END searcher description
69 [State 4] Switching from state 4 to state 2
69 [State 2] Switching from state 2 to state 1
70 [State 1] Switching from state 1 to state 4
70 [State 4] Switching from state 4 to state 2
71 [State 2] Switching from state 2 to state 4
71 [State 4] Switching from state 4 to state 5
72 [State 5] Switching from state 5 to state 1
72 [State 1] Switching from state 1 to state 6
73 [State 6] Switching from state 6 to state 5
73 [State 5] Switching from state 5 to state 1
74 [State 1] Switching from state 1 to state 3
74 [State 3] Forking state 3 at pc = 0x40068e at pagedir = 0xf3f9000
    state 7
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
74 [State 3] Switching from state 3 to state 6
74 [State 6] Forking state 6 at pc = 0x4006d6 at pagedir = 0xf3f9000
    state 6
    state 8
74 [State 6] Switching from state 6 to state 3
75 [State 3] Switching from state 3 to state 1
75 [State 1] Switching from state 1 to state 3
75 [State 3] Switching from state 3 to state 1
76 [State 1] Switching from state 1 to state 5
76 [State 5] Switching from state 5 to state 4
77 [State 4] Forking state 4 at pc = 0x4006d6 at pagedir = 0xf3f9000
    state 4
    state 9
77 [State 4] Switching from state 4 to state 6
77 [State 6] Switching from state 6 to state 5
77 [State 5] BaseInstructions: Killing state 5
77 [State 5] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
77 [State 5] TestCaseGenerator: generating test case at address 0x400eed
77 [State 5] Switching from state 5 to state 7
78 [State 7] Switching from state 7 to state 4
78 [State 4] Switching from state 4 to state 6
78 [State 6] Switching from state 6 to state 1
79 [State 1] Switching from state 1 to state 3
79 [State 3] Switching from state 3 to state 6
79 [State 6] Switching from state 6 to state 2
80 [State 2] Switching from state 2 to state 9
80 [State 9] Switching from state 9 to state 4
81 [State 4] Switching from state 4 to state 3
81 [State 3] Switching from state 3 to state 6
82 [State 6] BaseInstructions: Killing state 6
82 [State 6] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
82 [State 6] TestCaseGenerator: generating test case at address 0x400eed
82 [State 6] Switching from state 6 to state 3
82 [State 3] Forking state 3 at pc = 0x4006d6 at pagedir = 0xf3f9000
    state 3
    state 10
82 [State 3] Switching from state 3 to state 7
83 [State 7] Switching from state 7 to state 2
83 [State 2] Switching from state 2 to state 4
84 [State 4] Switching from state 4 to state 1
84 [State 1] Switching from state 1 to state 2
84 [State 2] Switching from state 2 to state 9
85 [State 9] Switching from state 9 to state 1
86 [State 1] BaseInstructions: Killing state 1
86 [State 1] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
86 [State 1] TestCaseGenerator: generating test case at address 0x400eed
86 [State 1] Switching from state 1 to state 9
86 [State 9] Switching from state 9 to state 4
87 [State 4] Switching from state 4 to state 2
87 [State 2] Switching from state 2 to state 3
87 [State 3] Switching from state 3 to state 4
88 [State 4] Switching from state 4 to state 9
88 [State 9] Switching from state 9 to state 7
89 [State 7] Switching from state 7 to state 2
89 [State 2] Switching from state 2 to state 4
89 [State 4] Switching from state 4 to state 7
90 [State 7] Switching from state 7 to state 2
90 [State 2] Switching from state 2 to state 3
91 [State 3] Switching from state 3 to state 4
91 [State 4] Switching from state 4 to state 2
91 [State 2] Switching from state 2 to state 4
92 [State 4] Switching from state 4 to state 2
92 [State 2] BaseInstructions: Killing state 2
92 [State 2] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
92 [State 2] TestCaseGenerator: generating test case at address 0x400eed
92 [State 2] Switching from state 2 to state 7
93 [State 7] Switching from state 7 to state 4
93 [State 4] Switching from state 4 to state 9
94 [State 9] Switching from state 9 to state 4
94 [State 4] Switching from state 4 to state 9
95 [State 9] BaseInstructions: Killing state 9
95 [State 9] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
95 [State 9] TestCaseGenerator: generating test case at address 0x400eed
95 [State 9] Switching from state 9 to state 7
95 [State 7] Switching from state 7 to state 4
96 [State 4] Switching from state 4 to state 3
96 [State 3] Switching from state 3 to state 4
96 [State 4] Switching from state 4 to state 3
97 [State 3] Switching from state 3 to state 4
97 [State 4] Switching from state 4 to state 3
97 [State 3] Switching from state 3 to state 7
98 [State 7] Switching from state 7 to state 10
98 [State 10] Switching from state 10 to state 4
99 [State 4] Switching from state 4 to state 7
99 [State 7] Switching from state 7 to state 10
99 [State 10] Switching from state 10 to state 4
100 [State 4] Switching from state 4 to state 10
100 [State 10] Switching from state 10 to state 3
101 [State 3] BaseInstructions: Killing state 3
101 [State 3] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
101 [State 3] TestCaseGenerator: generating test case at address 0x400eed
101 [State 3] Switching from state 3 to state 10
101 [State 10] Switching from state 10 to state 7
101 [State 7] Switching from state 7 to state 10
102 [State 10] Switching from state 10 to state 4
102 [State 4] Switching from state 4 to state 10
103 [State 10] Switching from state 10 to state 4
103 [State 4] Switching from state 4 to state 10
103 [State 10] Switching from state 10 to state 4
104 [State 4] Switching from state 4 to state 10
104 [State 10] Switching from state 10 to state 4
104 [State 4] Switching from state 4 to state 7
105 [State 7] Forking state 7 at pc = 0x4006d6 at pagedir = 0xf3f9000
    state 7
    state 11
105 [State 7] Switching from state 7 to state 10
105 [State 10] Switching from state 10 to state 7
106 [State 7] BaseInstructions: Killing state 7
106 [State 7] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
106 [State 7] TestCaseGenerator: generating test case at address 0x400eed
106 [State 7] Switching from state 7 to state 4
106 [State 4] Switching from state 4 to state 10
107 [State 10] BaseInstructions: Killing state 10
107 [State 10] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
107 [State 10] TestCaseGenerator: generating test case at address 0x400eed
107 [State 10] Switching from state 10 to state 4
107 [State 4] Switching from state 4 to state 11
108 [State 11] Switching from state 11 to state 4
108 [State 4] BaseInstructions: Killing state 4
108 [State 4] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
108 [State 4] TestCaseGenerator: generating test case at address 0x400eed
108 [State 4] Switching from state 4 to state 11
109 [State 11] BaseInstructions: Killing state 11
109 [State 11] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
109 [State 11] TestCaseGenerator: generating test case at address 0x400eed
109 [State 11] Switching from state 11 to state 8
111 [State 8] BaseInstructions: Killing state 8
111 [State 8] Terminating state early: State was terminated by opcode
            message: "bootstrap terminated"
            status: 0x0
111 [State 8] TestCaseGenerator: generating test case at address 0x400eed
