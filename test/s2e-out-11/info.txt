[Z3] Initializing
BEGIN searcher description
DFSSearcher
END searcher description
Creating plugin CorePlugin
Creating plugin BaseInstructions
Creating plugin HostFiles
Creating plugin Vmi
Creating plugin WebServiceInterface
Creating plugin ExecutionTracer
Creating plugin ModuleTracer
Creating plugin KeyValueStore
Creating plugin TranslationBlockCoverage
Creating plugin ModuleExecutionDetector
Creating plugin ForkLimiter
Creating plugin ProcessExecutionDetector
Creating plugin MultiSearcher
Creating plugin CUPASearcher
Creating plugin StaticFunctionModels
Creating plugin TestCaseGenerator
Creating plugin LinuxMonitor
Initializing TestCaseGenerator
Initializing MultiSearcher
Initializing ForkLimiter
Initializing KeyValueStore
Initializing ExecutionTracer
Initializing WebServiceInterface
Initializing Vmi
Initializing HostFiles
Initializing BaseInstructions
Initializing LinuxMonitor
Initializing ProcessExecutionDetector
Initializing ModuleExecutionDetector
Initializing StaticFunctionModels
StaticFunctionModels: Model count: 0
Initializing CUPASearcher
CUPASearcher: CUPASearcher is now active
Initializing TranslationBlockCoverage
Initializing ModuleTracer
Initializing CorePlugin
BEGIN searcher description
DFSSearcher
END searcher description
0 [State 0] Created initial state
9 [State 0] BaseInstructions: Message from guest (0x7ffd78dfd570): Process map:
9 [State 0] BaseInstructions: Message from guest (0x7ffd78dfd570): Base=0x400000 Limit=0x602000 Name=/home/s2e/test
9 [State 0] BaseInstructions: Message from guest (0x7ffd78dfd570): Base=0x7fda29bc2000 Limit=0x7fda29dc6000 Name=/lib/x86_64-linux-gnu/libdl-2.24.so
9 [State 0] BaseInstructions: Message from guest (0x7ffd78dfd570): Base=0x7fda29dc6000 Limit=0x7fda2a161000 Name=/lib/x86_64-linux-gnu/libc-2.24.so
9 [State 0] BaseInstructions: Message from guest (0x7ffd78dfd570): Base=0x7fda2a165000 Limit=0x7fda2a36b000 Name=/home/s2e/s2e.so
9 [State 0] BaseInstructions: Message from guest (0x7ffd78dfd570): Base=0x7fda2a36b000 Limit=0x7fda2a590000 Name=/lib/x86_64-linux-gnu/ld-2.24.so
9 [State 0] BaseInstructions: Inserted symbolic data @0x7ffd78dfd6a5 of size 0x2: str pc=0x400629
9 [State 0] Forking state 0 at pc = 0x40066a at pagedir = 0xda25000
    state 1
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
9 [State 0] Forking state 0 at pc = 0x400672 at pagedir = 0xda25000
    state 0
    state 2
BEGIN searcher description
DFSSearcher
END searcher description
9 [State 0] Forking state 0 at pc = 0x400686 at pagedir = 0xda25000
    state 0
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
9 [State 0] Forking state 0 at pc = 0x4006ac at pagedir = 0xda25000
    state 0
    state 4
BEGIN searcher description
DFSSearcher
END searcher description
9 [State 0] Forking state 0 at pc = 0x4006d6 at pagedir = 0xda25000
    state 5
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
9 [State 0] BaseInstructions: Killing state 0
9 [State 0] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
9 [State 0] TestCaseGenerator: generating test case at address 0x400eed
9 [State 0] Switching from state 0 to state 5
9 [State 5] Switching from state 5 to state 1
10 [State 1] Switching from state 1 to state 5
11 [State 5] Switching from state 5 to state 1
11 [State 1] Switching from state 1 to state 5
11 [State 5] BaseInstructions: Killing state 5
11 [State 5] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
11 [State 5] TestCaseGenerator: generating test case at address 0x400eed
11 [State 5] Switching from state 5 to state 1
12 [State 1] Switching from state 1 to state 3
12 [State 3] Forking state 3 at pc = 0x40068e at pagedir = 0xda25000
    state 6
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
12 [State 3] Switching from state 3 to state 2
13 [State 2] Switching from state 2 to state 1
13 [State 1] Switching from state 1 to state 3
13 [State 3] Switching from state 3 to state 2
14 [State 2] Switching from state 2 to state 1
14 [State 1] Switching from state 1 to state 3
14 [State 3] Forking state 3 at pc = 0x4006d6 at pagedir = 0xda25000
    state 3
    state 7
BEGIN searcher description
DFSSearcher
END searcher description
14 [State 3] Switching from state 3 to state 7
15 [State 7] BaseInstructions: Killing state 7
15 [State 7] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
15 [State 7] TestCaseGenerator: generating test case at address 0x400eed
15 [State 7] Switching from state 7 to state 4
15 [State 4] Forking state 4 at pc = 0x4006b4 at pagedir = 0xda25000
    state 8
    state 4
BEGIN searcher description
DFSSearcher
END searcher description
16 [State 4] Switching from state 4 to state 1
16 [State 1] Switching from state 1 to state 4
16 [State 4] Forking state 4 at pc = 0x4006d6 at pagedir = 0xda25000
    state 4
    state 9
BEGIN searcher description
DFSSearcher
END searcher description
16 [State 4] Switching from state 4 to state 9
17 [State 9] Switching from state 9 to state 1
18 [State 1] Switching from state 1 to state 6
18 [State 6] Switching from state 6 to state 8
18 [State 8] Forking state 8 at pc = 0x4006d6 at pagedir = 0xda25000
    state 8
    state 10
19 [State 8] Switching from state 8 to state 3
19 [State 3] Switching from state 3 to state 1
20 [State 1] Switching from state 1 to state 3
20 [State 3] Switching from state 3 to state 1
21 [State 1] Switching from state 1 to state 9
21 [State 9] Switching from state 9 to state 4
21 [State 4] Switching from state 4 to state 8
22 [State 8] Switching from state 8 to state 9
22 [State 9] Switching from state 9 to state 6
23 [State 6] Switching from state 6 to state 4
23 [State 4] Switching from state 4 to state 8
23 [State 8] Switching from state 8 to state 1
24 [State 1] Switching from state 1 to state 3
24 [State 3] Switching from state 3 to state 8
25 [State 8] Switching from state 8 to state 2
25 [State 2] Switching from state 2 to state 9
25 [State 9] Switching from state 9 to state 4
26 [State 4] Switching from state 4 to state 3
27 [State 3] Switching from state 3 to state 8
27 [State 8] BaseInstructions: Killing state 8
27 [State 8] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
27 [State 8] TestCaseGenerator: generating test case at address 0x400eed
27 [State 8] Switching from state 8 to state 3
28 [State 3] Switching from state 3 to state 6
28 [State 6] Switching from state 6 to state 2
29 [State 2] Switching from state 2 to state 4
30 [State 4] BaseInstructions: Killing state 4
30 [State 4] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
30 [State 4] TestCaseGenerator: generating test case at address 0x400eed
30 [State 4] Switching from state 4 to state 1
30 [State 1] Switching from state 1 to state 2
30 [State 2] BaseInstructions: Killing state 2
30 [State 2] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
30 [State 2] TestCaseGenerator: generating test case at address 0x400eed
30 [State 2] Switching from state 2 to state 9
31 [State 9] Switching from state 9 to state 1
31 [State 1] BaseInstructions: Killing state 1
31 [State 1] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
31 [State 1] TestCaseGenerator: generating test case at address 0x400eed
31 [State 1] Switching from state 1 to state 3
32 [State 3] Switching from state 3 to state 9
32 [State 9] BaseInstructions: Killing state 9
32 [State 9] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
32 [State 9] TestCaseGenerator: generating test case at address 0x400eed
32 [State 9] Switching from state 9 to state 10
33 [State 10] Switching from state 10 to state 3
33 [State 3] BaseInstructions: Killing state 3
33 [State 3] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
33 [State 3] TestCaseGenerator: generating test case at address 0x400eed
33 [State 3] Switching from state 3 to state 10
34 [State 10] Switching from state 10 to state 6
34 [State 6] Switching from state 6 to state 10
35 [State 10] Switching from state 10 to state 6
35 [State 6] Forking state 6 at pc = 0x4006d6 at pagedir = 0xda25000
    state 6
    state 11
36 [State 6] BaseInstructions: Killing state 6
36 [State 6] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
36 [State 6] TestCaseGenerator: generating test case at address 0x400eed
36 [State 6] Switching from state 6 to state 10
36 [State 10] BaseInstructions: Killing state 10
36 [State 10] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
36 [State 10] TestCaseGenerator: generating test case at address 0x400eed
36 [State 10] Switching from state 10 to state 11
37 [State 11] BaseInstructions: Killing state 11
37 [State 11] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
37 [State 11] TestCaseGenerator: generating test case at address 0x400eed
