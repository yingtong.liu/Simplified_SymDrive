[Z3] Initializing
BEGIN searcher description
DFSSearcher
END searcher description
Creating plugin CorePlugin
Creating plugin BaseInstructions
Creating plugin HostFiles
Creating plugin Vmi
Creating plugin WebServiceInterface
Creating plugin ExecutionTracer
Creating plugin ModuleTracer
Creating plugin KeyValueStore
Creating plugin TranslationBlockCoverage
Creating plugin ModuleExecutionDetector
Creating plugin ForkLimiter
Creating plugin ProcessExecutionDetector
Creating plugin MultiSearcher
Creating plugin CUPASearcher
Creating plugin StaticFunctionModels
Creating plugin TestCaseGenerator
Creating plugin LinuxMonitor
Initializing TestCaseGenerator
Initializing MultiSearcher
Initializing ForkLimiter
Initializing KeyValueStore
Initializing ExecutionTracer
Initializing WebServiceInterface
Initializing Vmi
Initializing HostFiles
Initializing BaseInstructions
Initializing LinuxMonitor
Initializing ProcessExecutionDetector
Initializing ModuleExecutionDetector
Initializing StaticFunctionModels
StaticFunctionModels: Model count: 0
Initializing CUPASearcher
CUPASearcher: CUPASearcher is now active
Initializing TranslationBlockCoverage
Initializing ModuleTracer
Initializing CorePlugin
BEGIN searcher description
DFSSearcher
END searcher description
0 [State 0] Created initial state
5 [State 0] BaseInstructions: Message from guest (0x7ffe174f3c20): Process map:
5 [State 0] BaseInstructions: Message from guest (0x7ffe174f3c20): Base=0x400000 Limit=0x602000 Name=/home/s2e/test
5 [State 0] BaseInstructions: Message from guest (0x7ffe174f3c20): Base=0x7f4bc0505000 Limit=0x7f4bc0709000 Name=/lib/x86_64-linux-gnu/libdl-2.24.so
5 [State 0] BaseInstructions: Message from guest (0x7ffe174f3c20): Base=0x7f4bc0709000 Limit=0x7f4bc0aa4000 Name=/lib/x86_64-linux-gnu/libc-2.24.so
5 [State 0] BaseInstructions: Message from guest (0x7ffe174f3c20): Base=0x7f4bc0aa8000 Limit=0x7f4bc0cae000 Name=/home/s2e/s2e.so
5 [State 0] BaseInstructions: Message from guest (0x7ffe174f3c20): Base=0x7f4bc0cae000 Limit=0x7f4bc0ed3000 Name=/lib/x86_64-linux-gnu/ld-2.24.so
5 [State 0] BaseInstructions: Inserted symbolic data @0x7ffe174f3d55 of size 0x2: str pc=0x400629
5 [State 0] Forking state 0 at pc = 0x40066a at pagedir = 0xd41d000
    state 1
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
5 [State 0] Forking state 0 at pc = 0x400672 at pagedir = 0xd41d000
    state 0
    state 2
BEGIN searcher description
DFSSearcher
END searcher description
5 [State 0] Forking state 0 at pc = 0x400686 at pagedir = 0xd41d000
    state 0
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
5 [State 0] Forking state 0 at pc = 0x4006ac at pagedir = 0xd41d000
    state 0
    state 4
BEGIN searcher description
DFSSearcher
END searcher description
5 [State 0] Forking state 0 at pc = 0x4006d6 at pagedir = 0xd41d000
    state 5
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
5 [State 0] BaseInstructions: Killing state 0
5 [State 0] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
5 [State 0] TestCaseGenerator: generating test case at address 0x400eed
5 [State 0] Switching from state 0 to state 5
5 [State 5] Switching from state 5 to state 1
5 [State 1] Switching from state 1 to state 5
6 [State 5] BaseInstructions: Killing state 5
6 [State 5] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
6 [State 5] TestCaseGenerator: generating test case at address 0x400eed
6 [State 5] Switching from state 5 to state 4
6 [State 4] Forking state 4 at pc = 0x4006b4 at pagedir = 0xd41d000
    state 6
    state 4
BEGIN searcher description
DFSSearcher
END searcher description
6 [State 4] Forking state 4 at pc = 0x4006d6 at pagedir = 0xd41d000
    state 4
    state 7
BEGIN searcher description
DFSSearcher
END searcher description
6 [State 4] Switching from state 4 to state 1
6 [State 1] BaseInstructions: Killing state 1
6 [State 1] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
6 [State 1] TestCaseGenerator: generating test case at address 0x400eed
6 [State 1] Switching from state 1 to state 7
6 [State 7] BaseInstructions: Killing state 7
6 [State 7] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
6 [State 7] TestCaseGenerator: generating test case at address 0x400eed
6 [State 7] Switching from state 7 to state 2
6 [State 2] Switching from state 2 to state 4
6 [State 4] Switching from state 4 to state 3
6 [State 3] Forking state 3 at pc = 0x40068e at pagedir = 0xd41d000
    state 8
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
6 [State 3] Forking state 3 at pc = 0x4006d6 at pagedir = 0xd41d000
    state 3
    state 9
BEGIN searcher description
DFSSearcher
END searcher description
6 [State 3] Switching from state 3 to state 2
6 [State 2] BaseInstructions: Killing state 2
6 [State 2] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
6 [State 2] TestCaseGenerator: generating test case at address 0x400eed
6 [State 2] Switching from state 2 to state 4
6 [State 4] BaseInstructions: Killing state 4
6 [State 4] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
6 [State 4] TestCaseGenerator: generating test case at address 0x400eed
6 [State 4] Switching from state 4 to state 8
6 [State 8] Forking state 8 at pc = 0x4006d6 at pagedir = 0xd41d000
    state 8
    state 10
7 [State 8] Switching from state 8 to state 3
7 [State 3] Switching from state 3 to state 6
