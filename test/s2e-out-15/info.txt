[Z3] Initializing
BEGIN searcher description
DFSSearcher
END searcher description
Creating plugin CorePlugin
Creating plugin BaseInstructions
Creating plugin HostFiles
Creating plugin Vmi
Creating plugin WebServiceInterface
Creating plugin ExecutionTracer
Creating plugin ModuleTracer
Creating plugin KeyValueStore
Creating plugin TranslationBlockCoverage
Creating plugin ModuleExecutionDetector
Creating plugin ForkLimiter
Creating plugin ProcessExecutionDetector
Creating plugin MultiSearcher
Creating plugin CUPASearcher
Creating plugin StaticFunctionModels
Creating plugin TestCaseGenerator
Creating plugin LinuxMonitor
Initializing TestCaseGenerator
Initializing MultiSearcher
Initializing ForkLimiter
Initializing KeyValueStore
Initializing ExecutionTracer
Initializing WebServiceInterface
Initializing Vmi
Initializing HostFiles
Initializing BaseInstructions
Initializing LinuxMonitor
Initializing ProcessExecutionDetector
Initializing ModuleExecutionDetector
Initializing StaticFunctionModels
StaticFunctionModels: Model count: 0
Initializing CUPASearcher
CUPASearcher: CUPASearcher is now active
Initializing TranslationBlockCoverage
Initializing ModuleTracer
Initializing CorePlugin
BEGIN searcher description
DFSSearcher
END searcher description
0 [State 0] Created initial state
7 [State 0] BaseInstructions: Message from guest (0x7ffe22e0a730): Process map:
7 [State 0] BaseInstructions: Message from guest (0x7ffe22e0a730): Base=0x400000 Limit=0x602000 Name=/home/s2e/test
7 [State 0] BaseInstructions: Message from guest (0x7ffe22e0a730): Base=0x7fb03fef7000 Limit=0x7fb0400fb000 Name=/lib/x86_64-linux-gnu/libdl-2.24.so
7 [State 0] BaseInstructions: Message from guest (0x7ffe22e0a730): Base=0x7fb0400fb000 Limit=0x7fb040496000 Name=/lib/x86_64-linux-gnu/libc-2.24.so
7 [State 0] BaseInstructions: Message from guest (0x7ffe22e0a730): Base=0x7fb04049a000 Limit=0x7fb0406a0000 Name=/home/s2e/s2e.so
7 [State 0] BaseInstructions: Message from guest (0x7ffe22e0a730): Base=0x7fb0406a0000 Limit=0x7fb0408c5000 Name=/lib/x86_64-linux-gnu/ld-2.24.so
7 [State 0] BaseInstructions: Inserted symbolic data @0x7ffe22e0a865 of size 0x2: str pc=0x400629
7 [State 0] Forking state 0 at pc = 0x40066a at pagedir = 0xf39a000
    state 1
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
7 [State 0] Forking state 0 at pc = 0x400672 at pagedir = 0xf39a000
    state 0
    state 2
BEGIN searcher description
DFSSearcher
END searcher description
7 [State 0] Forking state 0 at pc = 0x400686 at pagedir = 0xf39a000
    state 0
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
7 [State 0] Forking state 0 at pc = 0x4006ac at pagedir = 0xf39a000
    state 0
    state 4
BEGIN searcher description
DFSSearcher
END searcher description
7 [State 0] Forking state 0 at pc = 0x4006d6 at pagedir = 0xf39a000
    state 5
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
7 [State 0] Switching from state 0 to state 5
8 [State 5] Switching from state 5 to state 0
8 [State 0] BaseInstructions: Killing state 0
8 [State 0] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
8 [State 0] TestCaseGenerator: generating test case at address 0x400eed
8 [State 0] Switching from state 0 to state 1
9 [State 1] Switching from state 1 to state 5
9 [State 5] Switching from state 5 to state 1
10 [State 1] Switching from state 1 to state 5
10 [State 5] BaseInstructions: Killing state 5
10 [State 5] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
10 [State 5] TestCaseGenerator: generating test case at address 0x400eed
10 [State 5] Switching from state 5 to state 1
11 [State 1] Switching from state 1 to state 3
11 [State 3] Forking state 3 at pc = 0x40068e at pagedir = 0xf39a000
    state 6
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
11 [State 3] Switching from state 3 to state 2
11 [State 2] Switching from state 2 to state 1
12 [State 1] Switching from state 1 to state 3
12 [State 3] Forking state 3 at pc = 0x4006d6 at pagedir = 0xf39a000
    state 3
    state 7
BEGIN searcher description
DFSSearcher
END searcher description
12 [State 3] Switching from state 3 to state 2
13 [State 2] Switching from state 2 to state 6
13 [State 6] Switching from state 6 to state 7
14 [State 7] BaseInstructions: Killing state 7
14 [State 7] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
14 [State 7] TestCaseGenerator: generating test case at address 0x400eed
14 [State 7] Switching from state 7 to state 4
14 [State 4] Forking state 4 at pc = 0x4006b4 at pagedir = 0xf39a000
    state 8
    state 4
BEGIN searcher description
DFSSearcher
END searcher description
15 [State 4] Switching from state 4 to state 8
15 [State 8] Forking state 8 at pc = 0x4006d6 at pagedir = 0xf39a000
    state 8
    state 9
BEGIN searcher description
DFSSearcher
END searcher description
15 [State 8] Switching from state 8 to state 2
15 [State 2] Switching from state 2 to state 8
16 [State 8] Switching from state 8 to state 9
17 [State 9] BaseInstructions: Killing state 9
17 [State 9] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
17 [State 9] TestCaseGenerator: generating test case at address 0x400eed
17 [State 9] Switching from state 9 to state 1
17 [State 1] Switching from state 1 to state 3
18 [State 3] Switching from state 3 to state 4
18 [State 4] Forking state 4 at pc = 0x4006d6 at pagedir = 0xf39a000
    state 4
    state 10
BEGIN searcher description
DFSSearcher
END searcher description
18 [State 4] Switching from state 4 to state 8
18 [State 8] Switching from state 8 to state 3
19 [State 3] Switching from state 3 to state 1
20 [State 1] Switching from state 1 to state 3
