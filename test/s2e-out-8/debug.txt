Revision: d70e950472133b740d8f92872319f420c904701b
Config date: Wed May  2 10:26:57 PDT 2018

[Z3] Initializing
Current data layout: e-m:e-i64:64-f80:128-n8:16:32:64-S128
Current target triple: 
BEGIN searcher description
DFSSearcher
END searcher description
Creating plugin CorePlugin
Creating plugin BaseInstructions
Creating plugin HostFiles
Creating plugin Vmi
Creating plugin WebServiceInterface
Creating plugin ExecutionTracer
Creating plugin ModuleTracer
Creating plugin KeyValueStore
Creating plugin TranslationBlockCoverage
Creating plugin ModuleExecutionDetector
Creating plugin ForkLimiter
Creating plugin ProcessExecutionDetector
Creating plugin MultiSearcher
Creating plugin CUPASearcher
Creating plugin StaticFunctionModels
Creating plugin TestCaseGenerator
Creating plugin LinuxMonitor
Initializing TestCaseGenerator
WARNING: Cannot get configuration value 'pluginsConfig['TestCaseGenerator'].logLevel':
    value of type nil can not be converted to string

Initializing MultiSearcher
WARNING: Cannot get configuration value 'pluginsConfig['MultiSearcher'].logLevel':
    [string "return pluginsConfig['MultiSearcher'].logLeve..."]:1: attempt to index a nil value (field 'MultiSearcher')

Initializing ForkLimiter
WARNING: Cannot get configuration value 'pluginsConfig['ForkLimiter'].logLevel':
    value of type nil can not be converted to string

Initializing KeyValueStore
WARNING: Cannot get configuration value 'pluginsConfig['KeyValueStore'].logLevel':
    [string "return pluginsConfig['KeyValueStore'].logLeve..."]:1: attempt to index a nil value (field 'KeyValueStore')

WARNING: Cannot get configuration value 'pluginsConfig['KeyValueStore'].server':
    [string "return pluginsConfig['KeyValueStore'].server"]:1: attempt to index a nil value (field 'KeyValueStore')

WARNING: Cannot get configuration value 'pluginsConfig['KeyValueStore'].port':
    [string "return pluginsConfig['KeyValueStore'].port"]:1: attempt to index a nil value (field 'KeyValueStore')

WARNING: Cannot get configuration value 'pluginsConfig['KeyValueStore'].type':
    [string "return pluginsConfig['KeyValueStore'].type"]:1: attempt to index a nil value (field 'KeyValueStore')

Initializing ExecutionTracer
WARNING: Cannot get configuration value 'pluginsConfig['ExecutionTracer'].logLevel':
    [string "return pluginsConfig['ExecutionTracer'].logLe..."]:1: attempt to index a nil value (field 'ExecutionTracer')

WARNING: Cannot get configuration value 'pluginsConfig['ExecutionTracer'].useCircularBuffer':
    [string "return pluginsConfig['ExecutionTracer'].useCi..."]:1: attempt to index a nil value (field 'ExecutionTracer')

Initializing WebServiceInterface
WARNING: Cannot get configuration value 'pluginsConfig['WebServiceInterface'].logLevel':
    value of type nil can not be converted to string

WebServiceInterface: SeedSearcher not present, seed statistics will not be available
WebServiceInterface: Recipe plugin not present, recipe statistics will not be available
Initializing Vmi
WARNING: Cannot get configuration value 'pluginsConfig['Vmi'].logLevel':
    value of type nil can not be converted to string

Vmi: adding path /home/yingtong/s2e/projects/test
WARNING: Cannot get configuration value 'pluginsConfig['Vmi'].modules':
    value of type nil can not be converted to lua_table with only string keys

Initializing HostFiles
WARNING: Cannot get configuration value 'pluginsConfig['HostFiles'].logLevel':
    value of type nil can not be converted to string

Initializing BaseInstructions
WARNING: Cannot get configuration value 'pluginsConfig['BaseInstructions'].logLevel':
    [string "return pluginsConfig['BaseInstructions'].logL..."]:1: attempt to index a nil value (field 'BaseInstructions')

WARNING: Cannot get configuration value 'pluginsConfig['BaseInstructions'].restrict':
    [string "return pluginsConfig['BaseInstructions'].rest..."]:1: attempt to index a nil value (field 'BaseInstructions')

Initializing LinuxMonitor
WARNING: Cannot get configuration value 'pluginsConfig['LinuxMonitor'].logLevel':
    value of type nil can not be converted to string

Initializing ProcessExecutionDetector
WARNING: Cannot get configuration value 'pluginsConfig['ProcessExecutionDetector'].logLevel':
    value of type nil can not be converted to string

Initializing ModuleExecutionDetector
WARNING: Cannot get configuration value 'pluginsConfig['ModuleExecutionDetector'].logLevel':
    value of type nil can not be converted to string

WARNING: Cannot get configuration value 'pluginsConfig['ModuleExecutionDetector'].trackAllModules':
    value of type nil can not be converted to boolean

WARNING: Cannot get configuration value 'pluginsConfig['ModuleExecutionDetector'].configureAllModules':
    value of type nil can not be converted to boolean

WARNING: Cannot get configuration value 'pluginsConfig['ModuleExecutionDetector'].trackExecution':
    value of type nil can not be converted to boolean

ModuleExecutionDetector: ModuleExecutionDetector: id=mod_0 moduleName=test context=
Initializing StaticFunctionModels
WARNING: Cannot get configuration value 'pluginsConfig['StaticFunctionModels'].logLevel':
    value of type nil can not be converted to string

StaticFunctionModels: Model count: WARNING: Cannot get configuration value 'pluginsConfig['StaticFunctionModels'].count':
    value of type nil can not be converted to integer

0
Initializing CUPASearcher
WARNING: Cannot get configuration value 'pluginsConfig['CUPASearcher'].batchTime':
    value of type nil can not be converted to integer

MultiSearcher: Registering CUPASearcher
MultiSearcher: Switching to CUPASearcher
WARNING: Cannot get configuration value 'pluginsConfig['CUPASearcher'].enabled':
    value of type nil can not be converted to boolean

CUPASearcher: CUPASearcher is now active
Initializing TranslationBlockCoverage
WARNING: Cannot get configuration value 'pluginsConfig['TranslationBlockCoverage'].logLevel':
    value of type nil can not be converted to string

WARNING: Cannot get configuration value 'pluginsConfig['TranslationBlockCoverage'].writeCoveragePeriod':
    value of type nil can not be converted to integer

Initializing ModuleTracer
WARNING: Cannot get configuration value 'pluginsConfig['ModuleTracer'].logLevel':
    [string "return pluginsConfig['ModuleTracer'].logLevel..."]:1: attempt to index a nil value (field 'ModuleTracer')

Initializing CorePlugin
WARNING: Cannot get configuration value 'pluginsConfig['CorePlugin'].logLevel':
    [string "return pluginsConfig['CorePlugin'].logLevel"]:1: attempt to index a nil value (field 'CorePlugin')

BEGIN searcher description
DFSSearcher
END searcher description
0 [State 0] Created initial state
Initializing periodic timer
Adding memory block (startAddr = 0xffffffffffffffff, size = 0x10000000, hostAddr = 0x155503e00000, isSharedConcrete=0, name=pc.ram)
Adding memory block (startAddr = 0xffffffffffffffff, size = 0x20000, hostAddr = 0x555557eff000, isSharedConcrete=1, name=pc.bios)
Adding memory block (startAddr = 0xffffffffffffffff, size = 0x20000, hostAddr = 0x555557f50000, isSharedConcrete=1, name=pc.rom)
Adding memory block (startAddr = 0xffffffffffffffff, size = 0x800000, hostAddr = 0x1554f6c00000, isSharedConcrete=1, name=vga.vram)
Adding memory block (startAddr = 0xffffffffffffffff, size = 0x10000, hostAddr = 0x5555589ed000, isSharedConcrete=1, name=cirrus_vga.rom)
Adding memory block (startAddr = 0xffffffffffffffff, size = 0x20000, hostAddr = 0x555558a5b000, isSharedConcrete=1, name=e1000.rom)
Warning: vlan 0 is not connected to host network
s2e-block: dirty sectors on close:0
s2e-block: dirty after restore: 360 (ro=1)
s2e-block: wasted sectors: 0
ModuleExecutionDetector: Creating initial module transition state
12 [State 0] LinuxMonitor: Received kernel init page_offset=0xffff880000000000 &current_task=0xffffffff820e73e0 task_struct.pid offset=1120 task_struct.tgid offset=1124
12 [State 0] ModuleExecutionDetector: Process 0xf25c000 (pid=0x510) is unloaded
12 [State 0] ModuleExecutionDetector: Process 0x1e08000 (pid=0x511) is unloaded
12 [State 0] ModuleExecutionDetector: Process 0x1e08000 (pid=0x50f) is unloaded
12 [State 0] LinuxMonitor: Process ./s2eget loaded entry_point=0x7ff272befc20 pid=0x512 start_code=0x400000 end_code=0x4012f4 start_data=0x601e10 end_data=0x6020c0 start_stack=0x7ffcde259760
12 [State 0] LinuxMonitor: ModuleDescriptor Name=s2eget Path=./s2eget NativeBase=0x400000 LoadBase=0x400000 Size=0x12f4 AddressSpace=0xf197000 Pid=0x512 EntryPoint=0x7ff272befc20 Checksum=0x10
12 [State 0] ModuleExecutionDetector: Module s2eget loaded - Base=0x400000 NativeBase=0x400000 Size=0x12f4 AS=0xf197000
12 [State 0] ModuleExecutionDetector: 
13 [State 0] ModuleExecutionDetector: Process 0x1e08000 (pid=0x512) is unloaded
13 [State 0] LinuxMonitor: Removing task (pid=0x512, cr3=0xf197000, exitCode=0) record from collector.
13 [State 0] LinuxMonitor: Process /bin/chmod loaded entry_point=0x7f184ab11c20 pid=0x513 start_code=0x55722cf13000 end_code=0x55722cf2012c start_data=0x55722d120c08 end_data=0x55722d1212c0 start_stack=0x7fff529dc5c0
13 [State 0] LinuxMonitor: ModuleDescriptor Name=chmod Path=/bin/chmod NativeBase=0x55722cf13000 LoadBase=0x55722cf13000 Size=0xd12c AddressSpace=0xf198000 Pid=0x513 EntryPoint=0x7f184ab11c20 Checksum=0x10
13 [State 0] ModuleExecutionDetector: Module chmod loaded - Base=0x55722cf13000 NativeBase=0x55722cf13000 Size=0xd12c AS=0xf198000
13 [State 0] ModuleExecutionDetector: 
13 [State 0] ModuleExecutionDetector: Process 0x1e08000 (pid=0x513) is unloaded
13 [State 0] LinuxMonitor: Removing task (pid=0x513, cr3=0xf198000, exitCode=0) record from collector.
13 [State 0] LinuxMonitor: Process ././test loaded entry_point=0x7ff93309cc20 pid=0x514 start_code=0x400000 end_code=0x400a0c start_data=0x600e10 end_data=0x601040 start_stack=0x7fff708aaf30
13 [State 0] ProcessExecutionDetector: starting to track: test (pid: 0x514 as: 0xdaa4000)
13 [State 0] LinuxMonitor: ModuleDescriptor Name=test Path=././test NativeBase=0x400000 LoadBase=0x400000 Size=0xa0c AddressSpace=0xdaa4000 Pid=0x514 EntryPoint=0x7ff93309cc20 Checksum=0x10
13 [State 0] ModuleExecutionDetector: Module test loaded - Base=0x400000 NativeBase=0x400000 Size=0xa0c AS=0xdaa4000
13 [State 0] ModuleExecutionDetector:  [REGISTERING ID=mod_0]
13 [State 0] BaseInstructions: Message from guest (0x7fff708aaac0): Process map:
13 [State 0] BaseInstructions: Message from guest (0x7fff708aaac0): Base=0x400000 Limit=0x602000 Name=/home/s2e/test
13 [State 0] BaseInstructions: Message from guest (0x7fff708aaac0): Base=0x7ff9328f3000 Limit=0x7ff932af7000 Name=/lib/x86_64-linux-gnu/libdl-2.24.so
13 [State 0] BaseInstructions: Message from guest (0x7fff708aaac0): Base=0x7ff932af7000 Limit=0x7ff932e92000 Name=/lib/x86_64-linux-gnu/libc-2.24.so
13 [State 0] BaseInstructions: Message from guest (0x7fff708aaac0): Base=0x7ff932e96000 Limit=0x7ff93309c000 Name=/home/s2e/s2e.so
13 [State 0] BaseInstructions: Message from guest (0x7fff708aaac0): Base=0x7ff93309c000 Limit=0x7ff9332c1000 Name=/lib/x86_64-linux-gnu/ld-2.24.so
13 [State 0] LinuxMonitor: Module load not yet implemented
13 [State 0] LinuxMonitor: Module load not yet implemented
13 [State 0] LinuxMonitor: Module load not yet implemented
13 [State 0] LinuxMonitor: Module load not yet implemented
13 [State 0] LinuxMonitor: Module load not yet implemented
13 [State 0] BaseInstructions: Inserted symbolic data @0x7fff708aabf5 of size 0x2: str pc=0x400629
13 [State 0] Forking state 0 at pc = 0x40066a at pagedir = 0xdaa4000
    state 1
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
13 [State 0] Forking state 0 at pc = 0x400672 at pagedir = 0xdaa4000
    state 0
    state 2
BEGIN searcher description
DFSSearcher
END searcher description
13 [State 0] Forking state 0 at pc = 0x400686 at pagedir = 0xdaa4000
    state 0
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
13 [State 0] Switching from state 0 to state 3
13 [State 3] Forking state 3 at pc = 0x40068e at pagedir = 0xdaa4000
    state 4
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
13 [State 3] Switching from state 3 to state 1
14 [State 1] Switching from state 1 to state 4
14 [State 4] Forking state 4 at pc = 0x4006d6 at pagedir = 0xdaa4000
    state 4
    state 5
BEGIN searcher description
DFSSearcher
END searcher description
14 [State 4] Switching from state 4 to state 0
14 [State 0] Forking state 0 at pc = 0x4006ac at pagedir = 0xdaa4000
    state 0
    state 6
BEGIN searcher description
DFSSearcher
END searcher description
14 [State 0] Forking state 0 at pc = 0x4006d6 at pagedir = 0xdaa4000
    state 7
    state 0
14 [State 0] Switching from state 0 to state 1
14 [State 1] ProcessExecutionDetector: Unloading process 0x514
14 [State 1] ModuleExecutionDetector: Process 0x1e08000 (pid=0x514) is unloaded
14 [State 1] LinuxMonitor: Removing task (pid=0x514, cr3=0xdaa4000, exitCode=0) record from collector.
14 [State 1] Switching from state 1 to state 5
14 [State 5] Switching from state 5 to state 0
14 [State 0] ProcessExecutionDetector: Unloading process 0x514
14 [State 0] ModuleExecutionDetector: Process 0xf26e000 (pid=0x514) is unloaded
14 [State 0] LinuxMonitor: Removing task (pid=0x514, cr3=0xdaa4000, exitCode=0) record from collector.
14 [State 0] Switching from state 0 to state 5
14 [State 5] ProcessExecutionDetector: Unloading process 0x514
14 [State 5] ModuleExecutionDetector: Process 0x1e08000 (pid=0x514) is unloaded
14 [State 5] LinuxMonitor: Removing task (pid=0x514, cr3=0xdaa4000, exitCode=0) record from collector.
14 [State 5] Switching from state 5 to state 0
15 [State 0] LinuxMonitor: Process ./s2ecmd loaded entry_point=0x7f955abddc20 pid=0x515 start_code=0x400000 end_code=0x402c6c start_data=0x602e10 end_data=0x6030f0 start_stack=0x7ffc0a722e10
15 [State 0] LinuxMonitor: ModuleDescriptor Name=s2ecmd Path=./s2ecmd NativeBase=0x400000 LoadBase=0x400000 Size=0x2c6c AddressSpace=0xf1cb000 Pid=0x515 EntryPoint=0x7f955abddc20 Checksum=0x10
15 [State 0] ModuleExecutionDetector: Module s2ecmd loaded - Base=0x400000 NativeBase=0x400000 Size=0x2c6c AS=0xf1cb000
15 [State 0] ModuleExecutionDetector: 
15 [State 0] BaseInstructions: Killing state 0
15 [State 0] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
15 [State 0] TestCaseGenerator: generating test case at address 0x400eed
15 [State 0] TestCaseGenerator:             v0_str_0 = {0x0, 0x0}; (string) ".."
15 [State 0] Switching from state 0 to state 2
15 [State 2] Switching from state 2 to state 4
15 [State 4] Switching from state 4 to state 2
15 [State 2] ProcessExecutionDetector: Unloading process 0x514
15 [State 2] ModuleExecutionDetector: Process 0x1e08000 (pid=0x514) is unloaded
15 [State 2] LinuxMonitor: Removing task (pid=0x514, cr3=0xdaa4000, exitCode=0) record from collector.
15 [State 2] Switching from state 2 to state 1
15 [State 1] Switching from state 1 to state 4
15 [State 4] ProcessExecutionDetector: Unloading process 0x514
15 [State 4] ModuleExecutionDetector: Process 0xf26e000 (pid=0x514) is unloaded
15 [State 4] LinuxMonitor: Removing task (pid=0x514, cr3=0xdaa4000, exitCode=0) record from collector.
15 [State 4] Switching from state 4 to state 2
15 [State 2] Switching from state 2 to state 4
16 [State 4] Switching from state 4 to state 5
16 [State 5] LinuxMonitor: Process ./s2ecmd loaded entry_point=0x7fea0b54cc20 pid=0x515 start_code=0x400000 end_code=0x402c6c start_data=0x602e10 end_data=0x6030f0 start_stack=0x7fff39b4d0a0
16 [State 5] LinuxMonitor: ModuleDescriptor Name=s2ecmd Path=./s2ecmd NativeBase=0x400000 LoadBase=0x400000 Size=0x2c6c AddressSpace=0xf1cb000 Pid=0x515 EntryPoint=0x7fea0b54cc20 Checksum=0x10
16 [State 5] ModuleExecutionDetector: Module s2ecmd loaded - Base=0x400000 NativeBase=0x400000 Size=0x2c6c AS=0xf1cb000
16 [State 5] ModuleExecutionDetector: 
16 [State 5] BaseInstructions: Killing state 5
16 [State 5] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
16 [State 5] TestCaseGenerator: generating test case at address 0x400eed
16 [State 5] TestCaseGenerator:             v0_str_0 = {0x7b, 0x7b}; (string) "{{"
16 [State 5] Switching from state 5 to state 7
16 [State 7] Switching from state 7 to state 1
16 [State 1] Switching from state 1 to state 6
16 [State 6] Forking state 6 at pc = 0x4006b4 at pagedir = 0xdaa4000
    state 8
    state 6
BEGIN searcher description
DFSSearcher
END searcher description
16 [State 6] Switching from state 6 to state 7
16 [State 7] ProcessExecutionDetector: Unloading process 0x514
16 [State 7] ModuleExecutionDetector: Process 0x1e08000 (pid=0x514) is unloaded
16 [State 7] LinuxMonitor: Removing task (pid=0x514, cr3=0xdaa4000, exitCode=0) record from collector.
s2e-block: dirty sectors on close:360
Terminating node 0 (instance slot 0)
