[Z3] Initializing
BEGIN searcher description
DFSSearcher
END searcher description
Creating plugin CorePlugin
Creating plugin BaseInstructions
Creating plugin HostFiles
Creating plugin Vmi
Creating plugin WebServiceInterface
Creating plugin ExecutionTracer
Creating plugin ModuleTracer
Creating plugin KeyValueStore
Creating plugin TranslationBlockCoverage
Creating plugin ModuleExecutionDetector
Creating plugin ForkLimiter
Creating plugin ProcessExecutionDetector
Creating plugin MultiSearcher
Creating plugin CUPASearcher
Creating plugin StaticFunctionModels
Creating plugin TestCaseGenerator
Creating plugin LinuxMonitor
Initializing TestCaseGenerator
Initializing MultiSearcher
Initializing ForkLimiter
Initializing KeyValueStore
Initializing ExecutionTracer
Initializing WebServiceInterface
Initializing Vmi
Initializing HostFiles
Initializing BaseInstructions
Initializing LinuxMonitor
Initializing ProcessExecutionDetector
Initializing ModuleExecutionDetector
Initializing StaticFunctionModels
StaticFunctionModels: Model count: 0
Initializing CUPASearcher
CUPASearcher: CUPASearcher is now active
Initializing TranslationBlockCoverage
Initializing ModuleTracer
Initializing CorePlugin
BEGIN searcher description
DFSSearcher
END searcher description
0 [State 0] Created initial state
13 [State 0] BaseInstructions: Message from guest (0x7fff708aaac0): Process map:
13 [State 0] BaseInstructions: Message from guest (0x7fff708aaac0): Base=0x400000 Limit=0x602000 Name=/home/s2e/test
13 [State 0] BaseInstructions: Message from guest (0x7fff708aaac0): Base=0x7ff9328f3000 Limit=0x7ff932af7000 Name=/lib/x86_64-linux-gnu/libdl-2.24.so
13 [State 0] BaseInstructions: Message from guest (0x7fff708aaac0): Base=0x7ff932af7000 Limit=0x7ff932e92000 Name=/lib/x86_64-linux-gnu/libc-2.24.so
13 [State 0] BaseInstructions: Message from guest (0x7fff708aaac0): Base=0x7ff932e96000 Limit=0x7ff93309c000 Name=/home/s2e/s2e.so
13 [State 0] BaseInstructions: Message from guest (0x7fff708aaac0): Base=0x7ff93309c000 Limit=0x7ff9332c1000 Name=/lib/x86_64-linux-gnu/ld-2.24.so
13 [State 0] BaseInstructions: Inserted symbolic data @0x7fff708aabf5 of size 0x2: str pc=0x400629
13 [State 0] Forking state 0 at pc = 0x40066a at pagedir = 0xdaa4000
    state 1
    state 0
BEGIN searcher description
DFSSearcher
END searcher description
13 [State 0] Forking state 0 at pc = 0x400672 at pagedir = 0xdaa4000
    state 0
    state 2
BEGIN searcher description
DFSSearcher
END searcher description
13 [State 0] Forking state 0 at pc = 0x400686 at pagedir = 0xdaa4000
    state 0
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
13 [State 0] Switching from state 0 to state 3
13 [State 3] Forking state 3 at pc = 0x40068e at pagedir = 0xdaa4000
    state 4
    state 3
BEGIN searcher description
DFSSearcher
END searcher description
13 [State 3] Switching from state 3 to state 1
14 [State 1] Switching from state 1 to state 4
14 [State 4] Forking state 4 at pc = 0x4006d6 at pagedir = 0xdaa4000
    state 4
    state 5
BEGIN searcher description
DFSSearcher
END searcher description
14 [State 4] Switching from state 4 to state 0
14 [State 0] Forking state 0 at pc = 0x4006ac at pagedir = 0xdaa4000
    state 0
    state 6
BEGIN searcher description
DFSSearcher
END searcher description
14 [State 0] Forking state 0 at pc = 0x4006d6 at pagedir = 0xdaa4000
    state 7
    state 0
14 [State 0] Switching from state 0 to state 1
14 [State 1] Switching from state 1 to state 5
14 [State 5] Switching from state 5 to state 0
14 [State 0] Switching from state 0 to state 5
14 [State 5] Switching from state 5 to state 0
15 [State 0] BaseInstructions: Killing state 0
15 [State 0] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
15 [State 0] TestCaseGenerator: generating test case at address 0x400eed
15 [State 0] Switching from state 0 to state 2
15 [State 2] Switching from state 2 to state 4
15 [State 4] Switching from state 4 to state 2
15 [State 2] Switching from state 2 to state 1
15 [State 1] Switching from state 1 to state 4
15 [State 4] Switching from state 4 to state 2
15 [State 2] Switching from state 2 to state 4
16 [State 4] Switching from state 4 to state 5
16 [State 5] BaseInstructions: Killing state 5
16 [State 5] Terminating state early: State was terminated by opcode
            message: "Target execution terminated"
            status: 0x0
16 [State 5] TestCaseGenerator: generating test case at address 0x400eed
16 [State 5] Switching from state 5 to state 7
16 [State 7] Switching from state 7 to state 1
16 [State 1] Switching from state 1 to state 6
16 [State 6] Forking state 6 at pc = 0x4006b4 at pagedir = 0xdaa4000
    state 8
    state 6
BEGIN searcher description
DFSSearcher
END searcher description
16 [State 6] Switching from state 6 to state 7
